import 'package:billzi/app/add/addCategory/add_category_controller.dart';
import 'package:billzi/app/add/add_controller.dart';
import 'package:billzi/app/addCard/card_controller.dart';
import 'package:billzi/app/addCard/add_card_page.dart';
import 'package:billzi/app/category/custom_category_edit_page.dart';
import 'package:billzi/app/category/custom_category_controller.dart';
import 'package:get/get.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:billzi/app/add/addMoney/add_money_page.dart';
import 'package:billzi/app/add/addType/add_type_page.dart';
import 'package:billzi/app/add/addCategory/add_category_page.dart';
import 'package:billzi/app/add/add_contents_page.dart';
import 'package:billzi/app/add/add_income_type_page.dart';
import 'package:billzi/app/budget/budget_detail_page.dart';
import 'package:billzi/app/budgetAdd/budget_add_controller.dart';
import 'package:billzi/app/budgetAdd/budget_add_page.dart';
import 'package:billzi/app/category/custom_category_page.dart';
import 'package:billzi/app/categorySummary/category_summary_page.dart';
import 'package:billzi/app/currency/currency_select_page.dart';
import 'package:billzi/app/easyForm/eastFormAddResult/easy_form_add_result_controller.dart';
import 'package:billzi/app/easyForm/eastFormAddResult/easy_form_add_result_page.dart';
import 'package:billzi/app/easyForm/easyFormAdd/easy_form_add_controller.dart';
import 'package:billzi/app/easyForm/easyFormAdd/easy_form_add_page.dart';
import 'package:billzi/app/easyForm/easy_form_category_page.dart';
import 'package:billzi/app/editPost/edit_post_category.dart';
import 'package:billzi/app/editPost/edit_post_page.dart';
import 'package:billzi/app/home/home_controller.dart';
import 'package:billzi/app/home/home_page.dart';
import 'package:billzi/app/invite/inviteAccept/invite_accept_controller.dart';
import 'package:billzi/app/invite/inviteAccept/invite_accept_page.dart';
import 'package:billzi/app/locationSearch/location_map_search_controller.dart';
import 'package:billzi/app/locationSearch/location_map_search_page.dart';
import 'package:billzi/app/locationSearch/location_search_controller.dart';
import 'package:billzi/app/locationSearch/location_search_page.dart';
import 'package:billzi/app/login/login_controller.dart';
import 'package:billzi/app/login/login_page.dart';
import 'package:billzi/app/login/sign_up_page.dart';
import 'package:billzi/app/budget/budget_edit_page.dart';
import 'package:billzi/app/budget/budget_home_page.dart';
import 'package:billzi/app/budget/controller/budget_home_page_controller.dart';
import 'package:billzi/app/budget/monthly_budget_page.dart';
import 'package:billzi/app/multiBook/book_add_page.dart';
import 'package:billzi/app/multiBook/book_reorder_page.dart';
import 'package:billzi/app/multiBook/book_select_page.dart';
import 'package:billzi/app/multiBook/book_setting_page.dart';
import 'package:billzi/app/multiBook/controller/book_add_page_controller.dart';
import 'package:billzi/app/multiBook/controller/book_page_controller.dart';
import 'package:billzi/app/multiBook/controller/book_setting_page_controller.dart';
import 'package:billzi/app/selectTransactionType/select_transaction_type_controller.dart';
import 'package:billzi/app/selectTransactionType/select_transaction_type_page.dart';
import 'package:billzi/app/splash/splash_controller.dart';
import 'package:billzi/app/splash/splash_page.dart';
import 'package:billzi/app/tagSelector/tag_select_page.dart';
import 'package:billzi/app/timeline/timeline_controller.dart';
import 'package:billzi/app/timelineCalendar/timeline_calendar_page.dart';
import 'package:billzi/route/route_name.dart';

class AppRouter {
  // static const initial = Routes.SplashPage;
  static const initial = Routes.SplashPage; //for debug and test

  static final routes = [
    GetPage(
      name: Routes.EasyFormInputPage,
      page: () => const EasyFormAddPage(),
      binding: BindingsBuilder(() =>
          Get.lazyPut<EasyFormAddController>(() => EasyFormAddController())),
    ),
    GetPage(
      name: Routes.EasyFormCategoryPage,
      page: () => const EasyFormCategoryPage(),
    ),
    GetPage(
      name: Routes.EasyFormResultPage,
      page: () => const EasyFormResultPage(),
      binding: BindingsBuilder(() => Get.lazyPut<EasyFormAddResultController>(
          () => EasyFormAddResultController())),
    ),
    GetPage(
      name: Routes.HomePage,
      page: () => const HomePage(),
      binding: BindingsBuilder(() {
        Get.lazyPut<HomeController>(() => HomeController());
        Get.lazyPut<BudgetHomeController>(() => BudgetHomeController());
        Get.lazyPut<TimelineController>(() => TimelineController());
      }),
    ),
    GetPage(
      name: Routes.SplashPage,
      page: () => const SplashPage(),
      binding: BindingsBuilder(
          () => Get.lazyPut<SplashController>(() => SplashController())),
    ),
    GetPage(
      name: Routes.AddTypePage,
      page: () => const AddTypePage(),
      binding: BindingsBuilder(
          () => Get.lazyPut<AddController>(() => AddController())),
    ),
    GetPage(
      name: Routes.AddMoneyPage,
      page: () => const AddMoneyPage(),
      binding: BindingsBuilder(
          () => Get.lazyPut<AddController>(() => AddController())),
    ),
    GetPage(
      name: Routes.AddCategoryPage,
      page: () => const AddCategoryPage(),
      binding: BindingsBuilder(() {
        Get.lazyPut<AddCategoryController>(() => AddCategoryController());
        Get.lazyPut<AddController>(() => AddController());
      }),
    ),
    GetPage(
      name: Routes.AddContentsPage,
      page: () => const AddContentsPage(),
      binding: BindingsBuilder(
          () => Get.lazyPut<AddController>(() => AddController())),
    ),
    GetPage(
      name: Routes.AddIncomePage,
      page: () => AddIncomePage(),
      binding: BindingsBuilder(() {
        Get.lazyPut<CardController>(() => CardController());
        Get.lazyPut<AddController>(() => AddController());
      }),
    ),
    GetPage(
      name: Routes.EditPostPage,
      page: () => const EditPostPage(),
    ),
    GetPage(
      name: Routes.EditPostCategoryPage,
      page: () => const EditPostCategoryPage(),
    ),
    GetPage(
      name: Routes.BudgetPage,
      page: () => const MonthlyBudgetPage(),
    ),
    GetPage(
      name: Routes.BudgetEditPage,
      page: () => const BudgetEditPage(),
    ),
    GetPage(
      name: Routes.CategorySummaryPage,
      page: () => const CategorySummaryPage(),
    ),
    GetPage(
      name: Routes.TimelineCalendar,
      page: () => const TimelineCalendar(),
    ),
    GetPage(
      name: Routes.CustomCategoryPage,
      page: () => const CustomCategoryPage(),
      binding: BindingsBuilder(() {
        Get.lazyPut<AddCategoryController>(() => AddCategoryController());
        Get.lazyPut<CustomCategoryController>(() => CustomCategoryController());
      }),
    ),
    GetPage(
      name: Routes.CustomCategoryEditPage,
      page: () => CustomCategoryEditPage(),
      binding: BindingsBuilder(() {
        Get.lazyPut<AddCategoryController>(() => AddCategoryController());
        Get.lazyPut<CustomCategoryController>(() => CustomCategoryController());
      }),
    ),
    GetPage(
      name: Routes.LoginPage,
      page: () => LoginPage(),
      binding: BindingsBuilder(
          () => Get.lazyPut<LoginController>(() => LoginController())),
    ),
    GetPage(
      name: Routes.SignUpPage,
      page: () => const SignUpPage(),
    ),
    GetPage(
      name: Routes.LocationSearchPage,
      page: () => const LocationSearchPage(),
      binding: BindingsBuilder(() => Get.lazyPut<LocationSearchController>(
          () => LocationSearchController())),
    ),
    GetPage(
      name: Routes.BudgetHomePage,
      page: () => BudgetHomePage(),
      binding: BindingsBuilder(
        () => Get.lazyPut<BudgetHomeController>(
          () => BudgetHomeController(),
        ),
      ),
    ),
    GetPage(
      name: Routes.BookSelectPage,
      page: () => const BookSelectPage(),
      binding: BindingsBuilder(
        () => Get.lazyPut<BookPageController>(
          () => BookPageController(),
        ),
      ),
    ),
    GetPage(
      name: Routes.BookAddPage,
      page: () => const BookAddPage(),
      binding: BindingsBuilder(
        () => Get.lazyPut<BookAddPageController>(
          () => BookAddPageController(),
        ),
      ),
    ),
    GetPage(
      name: Routes.BookSettingPage,
      page: () => BookSettingPage(),
      binding: BindingsBuilder(
        () => Get.put<BookSettingPageController>(
          BookSettingPageController(),
        ),
      ),
    ),
    GetPage(
      name: Routes.BookReorderPage,
      page: () => const BookReorderPage(),
      binding: BindingsBuilder(
        () => Get.lazyPut<BookPageController>(
          () => BookPageController(),
        ),
      ),
    ),
    GetPage(
      name: Routes.InviteAcceptPage,
      page: () => const InviteAcceptPage(),
      binding: BindingsBuilder(() => Get.lazyPut<InviteAcceptAddController>(
          () => InviteAcceptAddController())),
    ),
    GetPage(
      name: Routes.CurrencySelectPage,
      page: () => const CurrencySelectPage(),
    ),
    GetPage(
      name: Routes.SelectTransactionPage,
      page: () => const SelectTransactionPage(),
      binding: BindingsBuilder(() =>
          Get.lazyPut<SelectTransactionTypeController>(
              () => SelectTransactionTypeController())),
    ),
    GetPage(
      name: Routes.TagSelectPage,
      page: () => const TagSelectPage(),
    ),
    GetPage(
      name: Routes.LocationMapSearchPage,
      page: () => const LocationMapSearchPage(),
      binding: BindingsBuilder(() => Get.lazyPut<LocationMapSearchController>(
          () => LocationMapSearchController())),
    ),
    GetPage(
      name: Routes.BudgetAddPage,
      page: () => const BudgetAddPage(),
      binding: BindingsBuilder(
          () => Get.lazyPut<BudgetAddController>(() => BudgetAddController())),
    ),
    GetPage(
      name: Routes.BudgetDetailPage,
      page: () => const BudgetDetailPage(),
    ),
    GetPage(
      name: Routes.AddCardPage,
      page: () => const AddCardPage(),
      binding: BindingsBuilder(
          () => Get.lazyPut<CardController>(() => CardController())),
    ),
    // GetPage(
    //   name: '/InputPage',
    //   page: () => InputScreen(),
    // ),
  ];
}
