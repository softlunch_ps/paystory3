abstract class Routes {
  static const DEBUG = '/debug';

  static const HomePage = '/home';

  static const LoginPage = '/login';
  static const SignUpPage = '/signUp';
  static const EasyFormInputPage = '/easyFormAdd';
  static const EasyFormCategoryPage = '/easyFormCategory';
  static const EasyFormResultPage = '/eastFormAddResult';

  static const SplashPage = '/splash';

  static const AddTypePage = '/addType';
  static const AddMoneyPage = '/addMoney';
  static const AddCategoryPage = '/addCategory';
  static const AddContentsPage = '/addContents';
  static const AddIncomePage = '/addIncome';
  static const AddCardPage = '/addCard';

  static const EditPostPage = '/editPost';
  static const EditPostCategoryPage = '/editPostCategory';

  static const BudgetPage = '/budget';
  static const BudgetAddPage = '/budgetAddPage';
  static const BudgetEditPage = '/budgetEditPage';
  static const BudgetDetailPage = '/budgetDetailPage';
  static const CategorySummaryPage = '/categorySummary';

  static const TimelineCalendar = '/timelineCalendar';

  static const CustomCategoryPage = '/customCategory';
  static const CustomCategoryEditPage = '/customCategoryEdit';

  static const LocationSearchPage = '/locationSearchPage';
  static const LocationMapSearchPage = '/locationMapSearchPage';

  static const BookSelectPage = '/bookSelectPage';
  static const BookAddPage = '/bookAddPage';
  static const BookSettingPage = '/bookSettingPage';
  static const BookReorderPage = '/bookReorderPage';
  static const InviteAcceptPage = '/inviteAcceptPage';

  static const BudgetHomePage = '/budgetHomePage';

  static const CurrencySelectPage = '/currencySelectPage';
  static const SelectTransactionPage = '/selectTransactionPage';
  static const TagSelectPage = '/tagSelectPage';
}