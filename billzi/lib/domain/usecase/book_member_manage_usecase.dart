import 'package:billzi/data/dto/testDto/test_book_member_info.dart';
import 'package:billzi/domain/repositories/book_member_repo.dart';

class BookMemberManageUseCase {
  final BookMemberRepository repository;

  BookMemberManageUseCase(this.repository);

  List<BookMemberInfo> getUserList({String? bookId}) {
    return repository.getMemberListByBookId(bookId: bookId);
  }

  bool updateUser(BookMemberInfo member) {
    return repository.updateMemberInfo(member);
  }

  addUser(BookMemberInfo member) {
    return repository.addMember(member);
  }

  Future<bool> removeUser(num memberSeq, num bookSeq) {
    return repository.removeMember(memberSeq, bookSeq);
  }
}
