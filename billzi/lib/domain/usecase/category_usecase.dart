import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/response/category/category_res_dto.dart';
import 'package:billzi/domain/repositories/category_repo.dart';
import 'package:billzi/network/apis/common/api_result.dart';

enum CategoryType {
  expense, income
}

class CategoryUseCase {

  final CategoryRepository repository;

  CategoryUseCase(this.repository);

  Future<ApiResult<CategoryResDto>> getCategoryList({required CategoryType categoryType}) {
    return repository.getCategoryList(categoryType: categoryType);
  }

  Future<ApiResult<CategoryItem>> postMyCategory({required CategoryItem categoryItem}) {
    return repository.postMyCategory(categoryItem: categoryItem);
  }

  Future<ApiResult<CategoryItem>> putMyCategory({required CategoryItem categoryItem, required num accountCategorySeq}) {
    return repository.putMyCategory(categoryItem: categoryItem, accountCategorySeq: accountCategorySeq);
  }
}