import 'package:billzi/data/dto/response/auth/sign_out_res_dto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:billzi/domain/repositories/auth_repo.dart';
import 'package:billzi/network/apis/common/api_result.dart';

class AuthUseCase {
  final AuthRepository repository;

  AuthUseCase(this.repository);

  bool isLoggedIn() {
    return repository.isLoggedIn();
  }

  Future<User?> googleLogin() {
    return repository.googleLogin();
  }

  Future<User?> facebookLogin() {
    return repository.facebookLogin();
  }

  Future<User?> appleLogin() {
    return repository.appleLogin();
  }

  Future<ApiResult<SignOutResDto>> signOut() {
    return repository.signOut();
  }

  Future<bool> loginToPayStoryServer({bool shouldCheckSnsLogin = false}) {
    return repository.loginToPayStoryServer(shouldCheckSnsLogin: shouldCheckSnsLogin);
  }

  // Future<bool> updateFcmToken(String fcmToken) {
  //   return repository.updateFcmToken(fcmToken);
  // }
}
