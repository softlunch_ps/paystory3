import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';
import 'package:billzi/data/dto/request/book/create_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/edit_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/reorder_book_list_req_dto.dart';
import 'package:billzi/data/dto/response/book/book_detail_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_list_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_subscription_res_dto.dart';
import 'package:billzi/data/dto/response/book/create_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/detach_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/edit_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/reorder_book_res_dto.dart';
import 'package:billzi/domain/repositories/book_repo.dart';
import 'package:billzi/network/apis/common/api_result.dart';

class BookUseCase {
  final BookRepository repository;

  BookUseCase(this.repository);

  Future<ApiResult<CreateBookResDto>> createBook(CreateBookReqDto book) {
    return repository.createBook(book);
  }

  Future<ApiResult<EditBookResDto>> editBook(EditBookReqDto book) {
    return repository.editBook(book);
  }

  Future<ApiResult<BookListResDto>> getBookList() {
    return repository.getBookList();
  }

  Future<ApiResult<BookDetailResDto>> getBookDetail(num bookSeq) {
    return repository.getBookDetail(bookSeq);
  }

  Future<ApiResult<ReorderBookResDto>> updateReorderedBooksList(
      List<ReorderBooksReqDto> list) {
    return repository.updateReorderedBookList(list);
  }

  Future<ApiResult<BookSubscriptionResDto>> joinBook(
      num accountBookSeq, CreateBookMemberDto memberToJoin) {
    return repository.joinBook(accountBookSeq, memberToJoin);
  }

  Future<ApiResult<DetachBookResDto>> detachBook(
      num userSeq, num accountBookSeq) {
    return repository.detachBook(userSeq, accountBookSeq);
  }

  Future<BookResDto?> getSelectedBook() {
    return repository.getSelectedBook();
  }
}
