import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:billzi/data/dto/response/transaction/card_list_res_dto.dart';
import 'package:billzi/data/dto/response/transaction/payment_history_res_dto.dart';
import 'package:billzi/data/dto/response/transaction/transaction_list_res_dto.dart';
import 'package:billzi/domain/repositories/transaction_repo.dart';
import 'package:billzi/network/apis/common/api_result.dart';

class TransactionUseCase {

  final TransactionRepository repository;

  TransactionUseCase(this.repository);

  Future<ApiResult<TransactionListResDto>> getTransactionList({required num accountBookSeq, required int offset}) {
    return repository.getTransactionList(accountBookSeq: accountBookSeq, offset: offset);
  }

  Future<ApiResult<PaymentHistoryResDto>> getNewTransactionList({required int offset}) {
    return repository.getNewTransactionList(offset: offset);
  }

  Future<ApiResult<PaymentHistoryDto>> postNewTransaction({required PaymentHistoryDto paymentHistoryDto}) {
    return repository.postNewTransaction(paymentHistoryDto: paymentHistoryDto);
  }

  Future<ApiResult<CardListResDto>> getCardList() {
    return repository.getCardList();
  }

  Future<ApiResult<CardAccountDto>> postAddCard(
      {required CardAccountDto cardAccountDto}) {
    return repository.postAddCard(cardAccountDto: cardAccountDto);
  }
}