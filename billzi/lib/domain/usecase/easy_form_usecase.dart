import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/testDto/test_item.dart';
import 'package:billzi/data/dto/testDto/test_payment_type.dart';
import 'package:billzi/data/dto/testDto/test_transaction_type.dart';
import 'package:billzi/domain/repositories/easy_form_repo.dart';
import 'package:billzi/network/apis/common/api_result.dart';

class EasyFormUseCase {

  final EasyFormRepository repository;

  EasyFormUseCase(this.repository);

  Future<ApiResult<TransactionDto>> saveEasyForm(TransactionDto transactionDto) {
    return repository.saveEasyForm(transactionDto);
  }

  List<TestItem> getTestItemList() {
    return repository.getTestItemList();
  }

  saveCurrency(Currency currency) {
    return repository.saveCurrency(currency);
  }

  Currency getCurrency() {
    return repository.getCurrency();
  }

  TestTransactionType getLastTestTransactionType() {
    return repository.getLastTestTransactionType();
  }
  setLastTestTransactionType(TestTransactionType testTransactionType) {
    return repository.setLastTestTransactionType(testTransactionType);
  }
  TestPaymentType getLastTestPaymentType() {
    return repository.getLastTestPaymentType();
  }
  setLastTestPaymentType(TestPaymentType testPaymentType) {
    return repository.setLastTestPaymentType(testPaymentType);
  }

}
