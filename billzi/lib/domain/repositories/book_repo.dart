import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';
import 'package:billzi/data/dto/request/book/create_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/edit_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/reorder_book_list_req_dto.dart';
import 'package:billzi/data/dto/response/book/book_detail_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_list_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_subscription_res_dto.dart';
import 'package:billzi/data/dto/response/book/create_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/detach_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/edit_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/reorder_book_res_dto.dart';
import 'package:billzi/network/apis/common/api_result.dart';

abstract class BookRepository {
  Future<ApiResult<CreateBookResDto>> createBook(CreateBookReqDto book);
  Future<ApiResult<EditBookResDto>> editBook(EditBookReqDto testBook);
  Future<ApiResult<DetachBookResDto>> detachBook(
      num targetUserSeq, num bookSeq);
  Future<ApiResult<BookListResDto>> getBookList();
  Future<ApiResult<BookDetailResDto>> getBookDetail(num bookSeq);
  Future<ApiResult<ReorderBookResDto>> updateReorderedBookList(
      List<ReorderBooksReqDto> list);
  Future<ApiResult<BookSubscriptionResDto>> joinBook(
    num accountBook,
    CreateBookMemberDto memberToJoin,
  );
  Future<BookResDto?> getSelectedBook();
}
