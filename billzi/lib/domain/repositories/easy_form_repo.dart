import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/testDto/test_item.dart';
import 'package:billzi/data/dto/testDto/test_payment_type.dart';
import 'package:billzi/data/dto/testDto/test_transaction_type.dart';
import 'package:billzi/network/apis/common/api_result.dart';

abstract class EasyFormRepository {

  Future<ApiResult<TransactionDto>> saveEasyForm(TransactionDto transactionDto);

  //todo: test
  List<TestItem> getTestItemList();

  //todo: test
  TestTransactionType getLastTestTransactionType();
  setLastTestTransactionType(TestTransactionType testTransactionType);
  TestPaymentType getLastTestPaymentType();
  setLastTestPaymentType(TestPaymentType testPaymentType);
  saveCurrency(Currency currency);
  Currency getCurrency();
}