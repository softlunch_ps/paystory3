import 'package:billzi/data/dto/testDto/test_book_member_info.dart';

abstract class BookMemberRepository {
  bool updateMemberInfo(BookMemberInfo user);
  List<BookMemberInfo> getMemberListByBookId({String? bookId});
  addMember(BookMemberInfo user);
  Future<bool> removeMember(num memberSeq, num bookSeq);
}
