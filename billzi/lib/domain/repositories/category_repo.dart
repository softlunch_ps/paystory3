import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/response/category/category_res_dto.dart';
import 'package:billzi/domain/usecase/category_usecase.dart';
import 'package:billzi/network/apis/common/api_result.dart';

abstract class CategoryRepository {

  Future<ApiResult<CategoryResDto>> getCategoryList({required CategoryType categoryType});
  Future<ApiResult<CategoryItem>> postMyCategory({required CategoryItem categoryItem});
  Future<ApiResult<CategoryItem>> putMyCategory({required CategoryItem categoryItem, required num accountCategorySeq});
}