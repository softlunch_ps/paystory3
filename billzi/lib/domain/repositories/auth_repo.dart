import 'package:billzi/data/dto/response/auth/sign_out_res_dto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:billzi/network/apis/common/api_result.dart';

abstract class AuthRepository {

  bool isLoggedIn();
  Future<bool> loginToPayStoryServer({bool shouldCheckSnsLogin = false});
  Future<User?> googleLogin();
  Future<User?> facebookLogin();
  Future<User?> appleLogin();
  Future<ApiResult<SignOutResDto>> signOut();
  // Future<bool> updateFcmToken(String fcmToken);
}
