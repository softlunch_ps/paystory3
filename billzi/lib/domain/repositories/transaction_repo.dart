import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:billzi/data/dto/response/transaction/card_list_res_dto.dart';
import 'package:billzi/data/dto/response/transaction/payment_history_res_dto.dart';
import 'package:billzi/data/dto/response/transaction/transaction_list_res_dto.dart';
import 'package:billzi/network/apis/common/api_result.dart';

abstract class TransactionRepository {

  Future<ApiResult<TransactionListResDto>> getTransactionList({required num accountBookSeq, required int offset});

  Future<ApiResult<PaymentHistoryResDto>> getNewTransactionList({required int offset});

  Future<ApiResult<CardListResDto>> getCardList();
  // Future<ApiResult<CardAccountDto>> putCardList();
  Future<ApiResult<CardAccountDto>> postAddCard({required CardAccountDto cardAccountDto});
  Future<ApiResult<PaymentHistoryDto>> postNewTransaction({required PaymentHistoryDto paymentHistoryDto});
}