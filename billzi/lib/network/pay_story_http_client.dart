import 'package:billzi/network/apis/account_api.dart';
import 'package:billzi/network/apis/member_api.dart';
import 'package:billzi/network/apis/new_auth_api.dart';
import 'package:billzi/network/apis/new_transaction_api.dart';
import 'package:dio/dio.dart';
import 'package:billzi/network/apis/auth_api.dart';
import 'package:billzi/network/apis/book_api.dart';
import 'package:billzi/network/apis/transaction_api.dart';
import 'package:billzi/network/error_transform_interceptor.dart';
import 'package:billzi/network/jwt_token_intercepter.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

const String payStoryBaseUrl = "http://169.56.90.46/paystory2";
const String payStory3BaseUrl = "http://169.56.90.46/billzi";

final prettyDioLogger = PrettyDioLogger(
  requestHeader: true,
  requestBody: true,
  responseBody: true,
  responseHeader: false,
  error: true,
  compact: true,
  maxWidth: 90,
);

class PayStoryHttpClient {
  static BaseOptions options = BaseOptions(
    contentType: Headers.jsonContentType,
  );

  static Dio? _dio;
  static Dio get dio {
    if (_dio == null) {
      _dio = Dio(options)
        ..interceptors.addAll([
          // TODO : don't add it for prod && release mode in order to prevent the log from being printed
          prettyDioLogger,
          JwtTokenInterceptors(),
          ErrorTransformInterceptors(),
        ]);
    }
    return _dio!;
  }

  static AuthApi get authApi => AuthApi(dio, baseUrl: '$payStoryBaseUrl/auth');
  static BookApi get bookApi =>
      BookApi(dio, baseUrl: '$payStoryBaseUrl/accountBook');
  static TransactionApi get transactionApi =>
      TransactionApi(dio, baseUrl: '$payStoryBaseUrl/paymentHistory');

  static NewAuthApi get newAuthApi => NewAuthApi(dio, baseUrl: '$payStory3BaseUrl/auth');
  static AccountApi get accountApi => AccountApi(dio, baseUrl: '$payStory3BaseUrl/accountBook');
  static NewTransactionApi get newTransactionApi => NewTransactionApi(dio, baseUrl: payStory3BaseUrl);
  static MemberApi get memberApi => MemberApi(dio, baseUrl: '$payStory3BaseUrl/member/');
}
