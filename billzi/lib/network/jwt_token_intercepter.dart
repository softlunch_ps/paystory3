import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart' hide Response;
import 'package:billzi/data/repositories/auth_repo_impl.dart';
import 'package:billzi/domain/usecase/auth_usecase.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/network/pay_story_http_client.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/logger.dart';

class JwtTokenInterceptors extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (UserManager().getServerToken().length > 0) {
      options.headers
          .addAll({"X-Auth-Token": "${UserManager().getServerToken()}"});
    }
    return super.onRequest(options, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async {
    // refresh token first and then send the original request with the refreshed one again

    // if (err.response?.statusCode == 401) {
    //   try {
    //     AuthUseCase authUseCase = AuthUseCase(AuthRepositoryImpl());
    //     bool loginSuccess =
    //         await authUseCase.loginToPayStoryServer(shouldCheckSnsLogin: true);
    //     if (!loginSuccess) {
    //       throw Exception('server login fail');
    //     }
    //
    //     //다시 api호출
    //     final RequestOptions currentRequest = err.response!.requestOptions;
    //     Response response = await PayStoryHttpClient.dio.fetch(currentRequest);
    //     return handler.resolve(response);
    //   } catch (e) {
    //     logger.d('refresh logic fails : $e');
    //     // transition to login page if refresh token fails
    //     // firebase logout
    //     await FirebaseAuth.instance.signOut();
    //     Get.offAndToNamed(Routes.SplashPage);
    //   }
    // }
    return super.onError(err, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    return super.onResponse(response, handler);
  }
}
