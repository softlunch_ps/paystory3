import 'package:dio/dio.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

class ErrorTransformInterceptors extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    return super.onRequest(options, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    return super.onError(err, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    try {
      var body = (response.data as Map<String, dynamic>)['resultVo'];
      if (body == null) return super.onResponse(response, handler);

      var data = CommonResultResDto.fromJson(body);
      if (!data.isSuccess) {
        return handler.reject(
          DioError(
              type: DioErrorType.response,
              requestOptions: response.requestOptions,
              response: response,
              error: data),
        );
      }
    } catch (e) {
      return handler.reject(
        DioError(
            requestOptions: response.requestOptions,
            response: response,
            error: e),
      );
    }
    return super.onResponse(response, handler);
  }
}
