import 'package:dio/dio.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/response/transaction/transaction_list_res_dto.dart';
import 'package:retrofit/retrofit.dart';

part 'transaction_api.g.dart';

@RestApi()
abstract class TransactionApi {
  factory TransactionApi(Dio dio, {String baseUrl}) = _TransactionApi;

  @POST("/{userSeq}")
  Future<TransactionDto> postTransaction(
    @Path() num userSeq,
    @Body() TransactionDto transactionDto,
  );

  @GET("/{userSeq}/{accountBookSeq}")
  Future<TransactionListResDto> getTransactionList(
      @Path() num userSeq,
      @Path() num accountBookSeq,
      @Query("limit") int limit,
      @Query("offset") int offset,
      @Query("startDate") String startDate,
      @Query("endDate") String endDate);
}
