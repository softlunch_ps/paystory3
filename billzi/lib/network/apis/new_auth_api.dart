import 'package:billzi/data/dto/model/auth_dto.dart';
import 'package:dio/dio.dart';
import 'package:billzi/data/dto/request/auth/auth_req_dto.dart';
import 'package:billzi/data/dto/response/auth/auth_res_dto.dart';
import 'package:retrofit/retrofit.dart';

part 'new_auth_api.g.dart';

@RestApi()
abstract class NewAuthApi {
  factory NewAuthApi(Dio dio, {String baseUrl}) = _NewAuthApi;

  @POST("/auth")
  Future<AuthDto> postAuth(@Body() AuthDto requestAuth);

  // @PUT("/logout")
  // Future<SignOutResDto> postLogout(@Body() LogoutReqDto requestLogout);

}
