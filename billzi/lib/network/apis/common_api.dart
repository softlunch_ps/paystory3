import 'package:billzi/data/dto/model/auth_dto.dart';
import 'package:billzi/data/dto/response/common/config_res_dto.dart';
import 'package:billzi/data/dto/response/common/currency_rate_res_dto.dart';
import 'package:dio/dio.dart';
import 'package:billzi/data/dto/request/auth/auth_req_dto.dart';
import 'package:billzi/data/dto/response/auth/auth_res_dto.dart';
import 'package:retrofit/retrofit.dart';

part 'common_api.g.dart';

@RestApi()
abstract class CommonApi {
  factory CommonApi(Dio dio, {String baseUrl}) = _CommonApi;

  @GET("/config/app/{userSeq}")
  Future<ConfigResDto> getConfig(
    @Path("userSeq") String userSeq,
    @Query("configGroup") String configGroup,
  );

  @POST("/config/app/{userSeq}")
  Future<ConfigResDto> insertAccountStartDate(
    @Path("userSeq") String userSeq,
    @Body() ConfigResDto config,
  );

  @GET("/exchangeRates")
  Future<CurrencyRateResDto> getExchangeRate();
}
