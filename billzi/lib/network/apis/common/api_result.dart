import 'dart:io';

import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'api_result.freezed.dart';

@freezed
abstract class ApiResult<T> with _$ApiResult<T> {
  const factory ApiResult.success({required T data}) = Success<T>;

  const factory ApiResult.failure({required CommonResultResDto error}) =
      Failure;
}

// TODO: make sure to sync up with server error codes
enum PayStoryErrorCode { UNKNOWN, DB0100, DB0200 }

extension PayStoryErrorCodeEx on String {
  PayStoryErrorCode toPayStoryErrorCode() {
    switch (this) {
      case "DB0100":
        return PayStoryErrorCode.DB0100;
      default:
        return PayStoryErrorCode.UNKNOWN;
    }
  }
}

CommonResultResDto transformToResponseDto(error) {
  CommonResultResDto result = CommonResultResDto(
      resultMessage: 'unknown error', httpStatusCode: '', resultCode: '');
  if (error is Exception) {
    String resultMessage = error.toString();
    try {
      if (error is DioError) {
        result = result.copyWith(
          httpStatusCode: error.response?.statusCode.toString() ?? '',
          resultCode: error.type.toString(),
          resultMessage: error.message,
        );
        resultMessage = error.message;
        switch (error.type) {
          case DioErrorType.cancel:
            resultMessage = 'Request Cancelled';
            break;
          case DioErrorType.connectTimeout:
            resultMessage = 'Connection request timeout';
            break;
          case DioErrorType.sendTimeout:
          case DioErrorType.receiveTimeout:
            resultMessage = 'timeout in connection with API server';
            break;
          case DioErrorType.response:
            switch (error.response?.statusCode) {
              // if it is an pay story specific error with 200 http status code
              case 200:
                result = error.error as CommonResultResDto;
                break;
              case 400:
              case 401:
              case 403:
                resultMessage = 'Unauthorised request';
                break;
              case 404:
                resultMessage = 'Not found';
                break;
              case 409:
                resultMessage = 'Error due to a conflict';
                break;
              case 408:
                resultMessage = 'Connection request timeout';
                break;
              case 500:
                resultMessage = "Internal Server Error";
                break;
              case 503:
                resultMessage = "Service unavailable";
                break;
              default:
                var responseCode = error.response?.statusCode;
                resultMessage = "Received invalid status code: $responseCode";
            }
            break;
        }
      } else if (error is SocketException) {
        resultMessage = 'No internet connection';
      } else {
        resultMessage = 'Unexpected error occurred';
      }
      return result.copyWith(resultMessage: resultMessage);
    } on FormatException catch (_) {
      return result.copyWith(resultMessage: 'Unexpected format error occurred');
    } catch (_) {
      return result.copyWith(resultMessage: 'Unexpected error occurred');
    }
  } else {
    if (error.toString().contains("is not a subtype of")) {
      return result.copyWith(resultMessage: 'Unable to process the data');
    } else {
      return result.copyWith(resultMessage: 'Unexpected error occurred');
    }
  }
}
