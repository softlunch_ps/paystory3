import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:billzi/data/dto/response/transaction/payment_history_res_dto.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'new_transaction_api.g.dart';

@RestApi()
abstract class NewTransactionApi {
  factory NewTransactionApi(Dio dio, {String baseUrl}) = _NewTransactionApi;

  @POST("/paymentHistory/{userSeq}")
  Future<PaymentHistoryDto> postPaymentHistory(
      @Path() num userSeq,
      @Body() PaymentHistoryDto item,
      );

  @PUT("/paymentHistory/{userSeq}")
  Future<PaymentHistoryDto> updatePaymentHistory(
      @Path() num userSeq,
      @Body() PaymentHistoryDto item,
      );

  @POST("/paymentHistories/{userSeq}")
  Future<PaymentHistoryResDto> insertPaymentHistories(
      @Path() num userSeq,
      @Body() PaymentHistoryResDto items,
      );

  @DELETE("/paymentHistory/{userSeq}")
  Future deletePaymentHistories(
      @Path() num userSeq,
      @Body() PaymentHistoryDto item,
      );

  @PUT("/paymentHistory/score/{userSeq}")
  Future<PaymentHistoryDto> putScore(
      @Path() num userSeq,
      @Body() PaymentHistoryDto item,
      );

  @PUT("/paymentHistory/link2Store/{userSeq}")
  Future<PaymentHistoryDto> matchStore(
      @Path() num userSeq,
      @Body() PaymentHistoryDto item,
      );

  @PUT("/paymentHistory/link2Store/{userSeq}")
  Future changeCategory(
      @Path() num userSeq,
      @Body() PaymentHistoryDto item,
      );
}
