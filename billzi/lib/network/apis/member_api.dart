import 'package:billzi/data/dto/model/user_dto.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'member_api.g.dart';

@RestApi()
abstract class MemberApi {
  factory MemberApi(Dio dio, {String baseUrl}) = _MemberApi;

  @POST("/signup")
  Future<UserDto> signUp(@Body() UserDto signupUser);
}