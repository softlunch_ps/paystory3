// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'common_api.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _CommonApi implements CommonApi {
  _CommonApi(this._dio, {this.baseUrl});

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ConfigResDto> getConfig(userSeq, configGroup) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'configGroup': configGroup};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ConfigResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/config/app/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ConfigResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ConfigResDto> insertAccountStartDate(userSeq, config) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(config.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ConfigResDto>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, '/config/app/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ConfigResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CurrencyRateResDto> getExchangeRate() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CurrencyRateResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/exchangeRates',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CurrencyRateResDto.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
