// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_api.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _AccountApi implements AccountApi {
  _AccountApi(this._dio, {this.baseUrl});

  final Dio _dio;

  String? baseUrl;

  @override
  Future<AuthDto> postAuth(requestAuth) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(requestAuth.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<AuthDto>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, '/auth',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = AuthDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<AccountCategoryResDto> getAccountCategoryRatio(
      userSeq, startDate, endDate, groupCd) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'startDate': startDate,
      r'endDate': endDate,
      r'trstnTypeGroupCd': groupCd
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<AccountCategoryResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/categoryRatio/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = AccountCategoryResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<PaymentHistoryResDto> getPaymentTotal(
      userSeq, queryPeriodic, startDate, endDate, groupCd) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'queryPeriodic': queryPeriodic,
      r'startDate': startDate,
      r'endDate': endDate,
      r'trstnTypeGroupCd': groupCd
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PaymentHistoryResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/paymentTotalTrend/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PaymentHistoryResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<PaymentHistoryResDto> getPaymentTotalByCategory(
      userSeq, queryPeriodic, startDate, endDate, categorySeq, groupCd) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'queryPeriodic': queryPeriodic,
      r'startDate': startDate,
      r'endDate': endDate,
      r'accountCategorySeq': categorySeq,
      r'trstnTypeGroupCd': groupCd
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PaymentHistoryResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/paymentTotalTrend/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PaymentHistoryResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<PaymentHistoryResDto> getPaymentHistories(
      userSeq, startDate, endDate, offset, limit, gpsMode, groupCd) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'startDate': startDate,
      r'endDate': endDate,
      r'offset': offset,
      r'limit': limit,
      r'gpsMode': gpsMode,
      r'trstnTypeGroupCd': groupCd
    };
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PaymentHistoryResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/paymentHistories/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PaymentHistoryResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<PaymentHistoryResDto> getPaymentHistoriesByCategory(
      userSeq, startDate, endDate, offset, limit, gpsMode, categorySeq) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'startDate': startDate,
      r'endDate': endDate,
      r'offset': offset,
      r'limit': limit,
      r'gpsMode': gpsMode,
      r'accountCategorySeq': categorySeq
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PaymentHistoryResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/paymentHistories/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PaymentHistoryResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CategoryResDto> getMyCategoryList(
      userSeq, transactionType, categoryType) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'trstnTypeGroupCd': transactionType,
      r'typeCd': categoryType
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CategoryResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/categories/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CategoryResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CategoryItem> postMyCategory(userSeq, categoryItem) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(categoryItem.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CategoryItem>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, '/category/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CategoryItem.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CategoryItem> putMyCategory(
      userSeq, accountCategorySeq, categoryItem) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(categoryItem.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CategoryItem>(
            Options(method: 'PUT', headers: _headers, extra: _extra)
                .compose(
                    _dio.options, '/category/${userSeq}/${accountCategorySeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CategoryItem.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CategoryItem> deleteCategory(
      userSeq, accountCategorySeq, categoryItem) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(categoryItem.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CategoryItem>(
            Options(method: 'DELETE', headers: _headers, extra: _extra)
                .compose(
                    _dio.options, '/category/${userSeq}/${accountCategorySeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CategoryItem.fromJson(_result.data!);
    return value;
  }

  @override
  Future<PaymentHistoryResDto> getStoreVisitHistory(
      userSeq, startDate, endDate, offset, limit, gpsMode, storeSeq) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'startDate': startDate,
      r'endDate': endDate,
      r'offset': offset,
      r'limit': limit,
      r'gpsMode': gpsMode,
      r'storeSeq': storeSeq
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PaymentHistoryResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/paymentHistories/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PaymentHistoryResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BudgetCategoryItem> getMyBudgetCategory(
      userSeq, startDate, endDate) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'startDate': startDate,
      r'endDate': endDate
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BudgetCategoryItem>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/budgets/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BudgetCategoryItem.fromJson(_result.data!);
    return value;
  }

  @override
  Future<BudgetCategoryItem> setMyBudgetCategory(userSeq, object) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(object.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<BudgetCategoryItem>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, '/budgets/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = BudgetCategoryItem.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CardListResDto> getCardAccountList(userSeq) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CardListResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/cardAccount/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CardListResDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CardAccountDto> postCardAccount(userSeq, object) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(object.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CardAccountDto>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, '/cardAccount/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CardAccountDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CardAccountDto> setCardAccountEdit(userSeq, object) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(object.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CardAccountDto>(
            Options(method: 'PUT', headers: _headers, extra: _extra)
                .compose(_dio.options, '/cardAccount/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CardAccountDto.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
