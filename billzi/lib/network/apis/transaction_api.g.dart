// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_api.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _TransactionApi implements TransactionApi {
  _TransactionApi(this._dio, {this.baseUrl});

  final Dio _dio;

  String? baseUrl;

  @override
  Future<TransactionDto> postTransaction(userSeq, transactionDto) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(transactionDto.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<TransactionDto>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, '/${userSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = TransactionDto.fromJson(_result.data!);
    return value;
  }

  @override
  Future<TransactionListResDto> getTransactionList(
      userSeq, accountBookSeq, limit, offset, startDate, endDate) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'limit': limit,
      r'offset': offset,
      r'startDate': startDate,
      r'endDate': endDate
    };
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<TransactionListResDto>(
            Options(method: 'GET', headers: _headers, extra: _extra)
                .compose(_dio.options, '/${userSeq}/${accountBookSeq}',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = TransactionListResDto.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
