import 'package:billzi/data/dto/response/auth/sign_out_res_dto.dart';
import 'package:dio/dio.dart';
import 'package:billzi/data/dto/request/auth/auth_req_dto.dart';
import 'package:billzi/data/dto/request/auth/logout_req_dto.dart';
import 'package:billzi/data/dto/response/auth/auth_res_dto.dart';
import 'package:retrofit/retrofit.dart';

part 'auth_api.g.dart';

@RestApi()
abstract class AuthApi {
  factory AuthApi(Dio dio, {String baseUrl}) = _AuthApi;

  @POST("/auth")
  Future<AuthResDto> postAuth(@Body() AuthReqDto requestAuth);

  @PUT("/logout")
  Future<SignOutResDto> postLogout(@Body() LogoutReqDto requestLogout);
}
