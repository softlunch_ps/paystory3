import 'package:dio/dio.dart';
import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';
import 'package:billzi/data/dto/request/book/create_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/detach_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/edit_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/reorder_book_list_req_dto.dart';
import 'package:billzi/data/dto/response/book/book_detail_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_list_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_subscription_res_dto.dart';
import 'package:billzi/data/dto/response/book/create_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/detach_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/edit_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/reorder_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/set_primary_book_res_dto.dart';
import 'package:retrofit/retrofit.dart';

part 'book_api.g.dart';

@RestApi()
abstract class BookApi {
  factory BookApi(Dio dio, {String baseUrl}) = _BookApi;

  @POST("/{userSeq}")
  Future<CreateBookResDto> createBook(
    @Path('userSeq') num userSeq,
    @Body() CreateBookReqDto request,
  );

  @GET("/{userSeq}")
  Future<BookListResDto> getBooks(
    @Path('userSeq') num userSeq,
  );

  @PUT("/{userSeq}")
  Future<EditBookResDto> modifyBook(
    @Path('userSeq') num userSeq,
    @Body() EditBookReqDto request,
  );

  @Deprecated(
      "This api is deprecated as the first book with open status always becomes primary book")
  @PUT("/base/{userSeq}/{accountBookSeq}")
  Future<SetPrimaryBookResDto> setPrimaryBook(
    @Path('userSeq') num userSeq,
    @Path('accountBookSeq') num accountBookSeq,
  );

  @PUT("/order/{userSeq}")
  Future<ReorderBookResDto> reorderBookList(
    @Path('userSeq') num userSeq,
    @Body() List<ReorderBooksReqDto> accountBookList,
  );

  @GET("/{userSeq}/{accountBookSeq}")
  Future<BookDetailResDto> getBookDetail(
    @Path('userSeq') num userSeq,
    @Path('accountBookSeq') num accountBookSeq,
  );

  @PUT("/{userSeq}/{accountBookSeq}")
  Future<BookSubscriptionResDto> joinBook(
    @Path('userSeq') num userSeq,
    @Path('accountBookSeq') num accountBookSeq,
    @Body() CreateBookMemberDto memberInfo,
  );

  @DELETE("/{userSeq}")
  Future<DetachBookResDto> detachBook(
    @Path('userSeq') num userSeq,
    @Body() DetachBookReqDto request,
  );
}
