import 'package:billzi/data/dto/model/auth_dto.dart';
import 'package:billzi/data/dto/model/budget_category_dto.dart';
import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/response/account/account_category_res_dto.dart';
import 'package:billzi/data/dto/response/category/category_res_dto.dart';
import 'package:billzi/data/dto/response/transaction/card_list_res_dto.dart';
import 'package:billzi/data/dto/response/transaction/payment_history_res_dto.dart';
import 'package:dio/dio.dart';
import 'package:billzi/data/dto/request/auth/auth_req_dto.dart';
import 'package:billzi/data/dto/response/auth/auth_res_dto.dart';
import 'package:retrofit/retrofit.dart';

part 'account_api.g.dart';

@RestApi()
abstract class AccountApi {
  factory AccountApi(Dio dio, {String baseUrl}) = _AccountApi;

  @POST("/auth")
  Future<AuthDto> postAuth(@Body() AuthDto requestAuth);

  @GET("/categoryRatio/{userSeq}")
  Future<AccountCategoryResDto> getAccountCategoryRatio(
      @Path('userSeq') num userSeq,
      @Query("startDate") String startDate,
      @Query("endDate") String endDate,
      @Query("trstnTypeGroupCd") String groupCd
      );

  @GET("/paymentTotalTrend/{userSeq}")
  Future<PaymentHistoryResDto> getPaymentTotal(
      @Path('userSeq') num userSeq,
      @Query("queryPeriodic") String queryPeriodic,
      @Query("startDate") String startDate,
      @Query("endDate") String endDate,
      @Query("trstnTypeGroupCd") String groupCd
      );

  @GET("/paymentTotalTrend/{userSeq}")
  Future<PaymentHistoryResDto> getPaymentTotalByCategory(
      @Path('userSeq') num userSeq,
      @Query("queryPeriodic") String queryPeriodic,
      @Query("startDate") String startDate,
      @Query("endDate") String endDate,
      @Query("accountCategorySeq") String categorySeq,
      @Query("trstnTypeGroupCd") String groupCd
      );

  @GET("/paymentHistories/{userSeq}")
  Future<PaymentHistoryResDto> getPaymentHistories(
      @Path('userSeq') num userSeq,
      @Query("startDate") String startDate,
      @Query("endDate") String endDate,
      @Query("offset") int offset,
      @Query("limit") int limit,
      @Query("gpsMode") int? gpsMode,
      @Query("trstnTypeGroupCd") String? groupCd
      );

  @GET("/paymentHistories/{userSeq}")
  Future<PaymentHistoryResDto> getPaymentHistoriesByCategory(
      @Path('userSeq') num userSeq,
      @Query("startDate") String startDate,
      @Query("endDate") String endDate,
      @Query("offset") int offset,
      @Query("limit") int limit,
      @Query("gpsMode") int gpsMode,
      @Query("accountCategorySeq") String categorySeq
      );

  @GET("/categories/{userSeq}")
  Future<CategoryResDto> getMyCategoryList(
      @Path('userSeq') num userSeq,
      // @Query("startDate") String startDate,
      @Query("trstnTypeGroupCd") String transactionType,
      @Query("typeCd") String categoryType,
      );

  @POST("/category/{userSeq}")
  Future<CategoryItem> postMyCategory(
      @Path('userSeq') num userSeq,
      @Body() CategoryItem categoryItem,
      );

  @PUT("/category/{userSeq}/{accountCategorySeq}")
  Future<CategoryItem> putMyCategory(
      @Path('userSeq') num userSeq,
      @Path("accountCategorySeq") num accountCategorySeq,
      @Body() CategoryItem categoryItem,
      );

  @DELETE("/category/{userSeq}/{accountCategorySeq}")
  Future<CategoryItem> deleteCategory(
      @Path('userSeq') num userSeq,
      @Path("accountCategorySeq") String accountCategorySeq,
      @Body() CategoryItem categoryItem,
      );

  @GET("/paymentHistories/{userSeq}")
  Future<PaymentHistoryResDto> getStoreVisitHistory(
      @Path('userSeq') num userSeq,
      @Query("startDate") String startDate,
      @Query("endDate") String endDate,
      @Query("offset") int offset,
      @Query("limit") int limit,
      @Query("gpsMode") int gpsMode,
      @Query("storeSeq") String storeSeq,
      );

  @GET("/budgets/{userSeq}")
  Future<BudgetCategoryItem> getMyBudgetCategory(
      @Path('userSeq') num userSeq,
      @Query("startDate") String startDate,
      @Query("endDate") String endDate
      );

  @POST("/budgets/{userSeq}")
  Future<BudgetCategoryItem> setMyBudgetCategory(
      @Path('userSeq') num userSeq,
      @Body() BudgetCategoryItem object
      );

  @GET("/cardAccount/{userSeq}")
  Future<CardListResDto> getCardAccountList(
      @Path('userSeq') num userSeq,
      //@Query("accountTypeCd") String accountTypeCd,
      //@Query("useYN") String useYN
      );

  @POST("/cardAccount/{userSeq}")
  Future<CardAccountDto> postCardAccount(
      @Path('userSeq') num userSeq,
      @Body() CardAccountDto object
      );


  @PUT("/cardAccount/{userSeq}")
  Future<CardAccountDto> setCardAccountEdit(
      @Path('userSeq') num userSeq,
      @Body() CardAccountDto object
      );
}