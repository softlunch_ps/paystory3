import 'package:logger/logger.dart';

var logger = Logger(
  filter: null,
  printer: PrettyPrinter(lineLength: 200),
);

// class ProductionReleaseFilter extends LogFilter {
//   @override
//   Level level = Logger.level;
//
//   @override
//   bool shouldLog(LogEvent event) {
//     if (Environment.flavor == Flavor.PRD && kReleaseMode) return false;
//
//     var shouldLog = false;
//     if (event.level.index >= level.index) {
//       shouldLog = true;
//     }
//     return shouldLog;
//   }
// }
