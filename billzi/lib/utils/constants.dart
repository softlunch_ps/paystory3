/// Local Storage Keys
const String StorageKeySimpleInput = 'storage_key_simple_input';
const String StorageKeySimpleInputLocation =
    'storage_key_simple_input_location';
const String StorageKeyHistoryList = 'storage_key_history_list';
const String StorageKeyBookList = 'storage_key_book_list';
const String StorageKeyCurrencyString = 'storage_key_currency_string';
const String StorageKeyUserRightInfoList = 'storage_key_user_right_info_list';
const String StorageKeyLastSelectedTransactionType =
    'storage_key_last_selected_transaction_type';
const String StorageKeyLastSelectedPaymentType =
    'storage_key_last_selected_payment_type';

/// Local Storage Keys

/// get x key
const String GetXKeyUserSeq = 'get_x_key_user_seq';

/// get x key


/// Dynamic links const
// TODO: make sure to use a flavor based url ( dev, prod url should be different )
const String FirebaseDynamicLinkDomain = 'https://newpaystory.page.link';
const String DynamicLinkInviteBookPath = '/invite-join-book';

/// Dynamic links const

/// Package name
const String AndAppId = 'com.softlunch.newpaystory';
const String IOSAppId = 'com.softlunch.newpaystory';

/// Package name

/// Paging
const int timelinePageItemCount = 10;
/// Paging

const String authTypeCodeKakao = '1';
const String authTypeCodeFacebook = '2';
const String authTypeCodeGoogle = '3';
const String authTypeCodeApple = '4';

const String alreadySignedUpErrorCode = 'MEM0000';



const String inputTypeCodeIncome = '12';
const String inputTypeCodeExpense = '11';

const String expensePaymentTypeCodeCard = '00';
const String expensePaymentTypeCodeCash = '19';

const String categoryTypeCodeExpense = 'expense';
const String categoryTypeCodeIncome = 'income';

