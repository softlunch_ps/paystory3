import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:billzi/app/common/currency/currencies.dart';
import 'package:billzi/app/common/currency/currency.dart';

getKRWCurrencyText(double money) {
  return '${NumberFormat.currency(
    locale: "ko_KR",
    symbol: '',
  ).format(money)}원';
}

String getMoneyStringWithCurrency({required double money, Currency? currency}) {

  if (currency != null) {
    if (currency.code == 'KRW') {
      return getKRWCurrencyText(money);
    } else {
      return NumberFormat.currency(
        name: currency.code,
        symbol: currency.symbol,
      ).format(money);
    }

  } else {
    //no currency data
    return '$money';
  }

}

Currency? findCurrencyByCode(String currencyCode) {
  Currency? result;
  currencies.forEach((element) {
    if (element['code'] == currencyCode) {
      result = Currency.fromJson(element);
    }
  });
  return result;
}