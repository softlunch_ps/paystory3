import 'dart:convert';
import 'dart:math';

import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:crypto/crypto.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:billzi/data/dto/model/selectable_model.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/model/transaction_group.dart';
import 'package:billzi/utils/extensions.dart';
import 'package:intl/intl.dart';

DateTime oldPaystoryDateParser(String datetime) {
  return DateTime.parse(datetime);
}

Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}

String generatePassword({
  bool letter = true,
  bool isNumber = true,
  bool isSpecial = true,
}) {
  final length = 20;
  final letterLowerCase = "abcdefghijklmnopqrstuvwxyz";
  final letterUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  final number = '0123456789';
  final special = '@#%^*>\$@?/[]=+';

  String chars = "";
  if (letter) chars += '$letterLowerCase$letterUpperCase';
  if (isNumber) chars += '$number';
  if (isSpecial) chars += '$special';

  return List.generate(length, (index) {
    final indexRandom = Random.secure().nextInt(chars.length);
    return chars[indexRandom];
  }).join('');
}

/// Generates a cryptographically secure random nonce, to be included in a
/// credential request.
// String generateNonce([int length = 32]) {
//   final charset =
//       '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
//   final random = Random.secure();
//   return List.generate(length, (_) => charset[random.nextInt(charset.length)])
//       .join();
// }

/// Returns the sha256 hash of [input] in hex notation.
String sha256ofString(String input) {
  final bytes = utf8.encode(input);
  final digest = sha256.convert(bytes);
  return digest.toString();
}

List<TransactionItemGroup> groupingTransactionListByDate(
    List<PaymentHistoryDto> itemList) {
  List<TransactionItemGroup> list = [];

  //sorting
  itemList.sort((a, b) {
    return b.createDateTime!.compareTo(a.createDateTime!);
  });

  DateTime? dateTime;
  itemList.forEach((element) {
    if (dateTime == null) {
      dateTime = element.createDateTime;
      list.add(TransactionItemGroup(element.createDateTime!, [element]));
    } else {
      if (dateTime!.isSameDate(element.createDateTime!)) {
        list.last.groupList.add(element);
      } else {
        dateTime = element.createDateTime;
        list.add(TransactionItemGroup(element.createDateTime!, [element]));
      }
    }
  });
  return list;
}

//todo: need to localization
///지출방식 01:현금, 02:신용카드, 03:체크카드, 04:수표
String getPaymentTypeNameById(String? id) {
  switch (id) {
    case '01':
      return 'Cash';
    case '02':
      return 'Credit card';
    case '03':
      return 'Check card';
    case '04':
      return 'Check';
    default:
      return '';
  }
}

//todo: need to localization
String getTransactionTypeNameById(String? id) {
  switch (id) {
    case 'I':
      return 'Income';
    case 'E':
      return 'Expanse';
    default:
      return 'None';
  }
}

bool isAllSelected(List<SelectableModel> list) {
  return list.every((element) => element.isSelected);
}

String getDateTimeStringByDateTime(DateTime dateTime) {
  return formatDate(dateTime, [yyyy, '년 ', mm, '월 ', dd, '일']).toString();
}

String getTimeStringByDateTime(DateTime dateTime) {
  return formatDate(
      DateTime(dateTime.year, dateTime.month, dateTime.day, dateTime.hour,
          dateTime.minute),
      [am, ' ', hh, '시', ' ', nn, '분']).toString();
}

String getOldTimeStringByDateTime(DateTime dateTime) {
  return formatDate(
      DateTime(dateTime.year, dateTime.month, dateTime.day, dateTime.hour,
          dateTime.minute),
      [
        yyyy,
        mm,
        dd,
        HH,
        nn,
        ss,
      ]).toString();
}

IconData getCategoryIcon(String imageName) {
  switch (imageName) {
    case 'ic_r_category_eat':
      return Icons.restaurant;
    case 'ic_r_category_snack':
      return Icons.local_cafe;
    case 'ic_r_category_mart':
      return Icons.shopping_cart;
    case 'ic_r_category_shopping':
      return Icons.shopping_bag;
    case 'ic_r_category_house':
      return Icons.house;
    case 'ic_r_category_beauty':
      return Icons.watch_sharp;
    case 'ic_r_category_insurance':
      return Icons.comment_bank;
    case 'ic_r_category_internet':
      return Icons.wifi;
    case 'ic_r_category_traffic':
      return Icons.directions_car_sharp;
    case 'ic_r_category_healthcare':
      return Icons.local_hospital_sharp;
    case 'ic_r_category_play':
      return Icons.wine_bar_sharp;
    case 'ic_r_category_travel':
      return Icons.airplanemode_active_sharp;
    case 'ic_r_category_pet':
      return Icons.pets_sharp;
    case 'ic_r_category_event':
      return Icons.account_balance_sharp;
    case 'ic_r_category_study':
      return Icons.school_sharp;
    case 'ic_r_category_tax':
      return Icons.account_balance_wallet_sharp;
    case 'ic_r_category_etc':
      return Icons.widgets_sharp;

    case 'ic_r_category_income_pay':
      return Icons.work;
    case 'ic_r_category_income_work':
      return Icons.apartment;
    case 'ic_r_category_income_pocket':
      return Icons.money;
    case 'ic_r_category_income_bank':
      return Icons.stacked_line_chart;
    case 'ic_r_category_income_etc':
      return Icons.workspaces_filled;
    case 'add':
      return Icons.add;
    case 'call':
      return Icons.call;
    case 'info_outline':
      return Icons.info_outline;
    case 'padding':
      return Icons.padding;
    case 'shop':
      return Icons.shop;
    case 'widgets_sharp':
      return Icons.widgets_sharp;
    case 'pets_sharp':
      return Icons.pets_sharp;
    case 'airplanemode_active_sharp':
      return Icons.airplanemode_active_sharp;
    case 'android':
      return Icons.android;
    case 'ac_unit':
      return Icons.ac_unit;
    case 'local_hospital_sharp':
      return Icons.local_hospital_sharp;
    case 'school_sharp':
      return Icons.school_sharp;
    case 'directions_car_sharp':
      return Icons.directions_car_sharp;
    case 'wifi':
      return Icons.wifi;
    case 'restaurant':
      return Icons.restaurant;
    case 'local_cafe':
      return Icons.local_cafe;
    case 'account_balance_wallet_sharp':
      return Icons.account_balance_wallet_sharp;
    case 'account_box_outlined':
      return Icons.account_box_outlined;
    case 'volunteer_activism':
      return Icons.volunteer_activism;
    case 'whatshot':
      return Icons.whatshot;
    case 'alternate_email':
      return Icons.alternate_email;
    case 'vpn_key':
      return Icons.vpn_key;
    case 'watch_later_sharp':
      return Icons.watch_later_sharp;
    case 'wine_bar_rounded':
      return Icons.wine_bar_rounded;

    default:
      return Icons.workspaces_filled;
  }
}
