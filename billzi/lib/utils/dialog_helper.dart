import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoadingModalView {
  LoadingModalView();

  Future<T?> show<T>({
    Color backgroundColor = Colors.transparent,
  }) {
    return showDialog<T>(
      context: Get.context!,
      barrierDismissible: false,
      builder: (ctx) {
        return AlertDialog(
          elevation: 0,
          backgroundColor: backgroundColor,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 36,
                height: 36,
                child: CircularProgressIndicator(),
              ),
            ],
          ),
        );
      },
    );
  }

  void pop() {
    Get.back();
  }
}

showPopup(Widget children) {
  showDialog(
    context: Get.context!,
    builder: (context) => Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Material(
        color: Colors.transparent,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.8,
          decoration: BoxDecoration(
            color: Get.isDarkMode ? Colors.black : Colors.white,
            borderRadius: BorderRadius.circular(24),
          ),
          child: children.paddingAll(20),
        ),
      ),
    ),
  );
}
