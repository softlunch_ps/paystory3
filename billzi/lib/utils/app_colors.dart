import 'package:flutter/material.dart';

class AppColors {
  static Color primary = Colors.blue;

  static Color textBlack1 = Colors.black87;
  static Color textWhite1 = Colors.white;
  static Color textRed1 = Colors.redAccent;
  static Color textBlue1 = Colors.blueAccent;
  static Color widgetBlack1 = Colors.black87;
  static Color widgetWhite1 = Colors.white;
  static Color widgetDisableGray1 = Colors.grey;
  static Color backgroundWhite1 = Colors.white;
  static Color backgroundWhite2 = Color.fromRGBO(252, 252, 252, 1);

  static Color dividerGray1 = Colors.grey.withOpacity(0.1);
  static Color dividerGray2 = Colors.grey;
}

const List<Color> defaultBookColors = [
  Colors.red,
  Colors.pink,
  Colors.purple,
  Colors.deepPurple,
  Colors.indigo,
  Colors.blue,
  Colors.lightBlue,
  Colors.cyan,
  Colors.teal,
  Colors.green,
  Colors.lightGreen,
  Colors.lime,
  Colors.yellow,
  Colors.amber,
  Colors.orange,
  Colors.deepOrange,
  Colors.brown,
  Colors.grey,
  Colors.blueGrey,
  Colors.black,
];
