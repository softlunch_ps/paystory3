import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:billzi/utils/logger.dart';

class CrashlyticsReportKeySet {
  String key;
  String value;

  CrashlyticsReportKeySet(this.key, this.value);
}

Future reportErrorByCrashlytics(
    {dynamic exception,
    StackTrace? stackTrace,
    dynamic reason,
    bool isFatal = false,
    bool withDebugLog = false,
    CrashlyticsReportKeySet? crashlyticsReportKeySet}) async {
  if (crashlyticsReportKeySet != null) {
    FirebaseCrashlytics.instance.setCustomKey(
        crashlyticsReportKeySet.key, crashlyticsReportKeySet.value);
  }
  await FirebaseCrashlytics.instance.recordError(
    exception,
    stackTrace,
    reason: reason,
    fatal: isFatal,
  );
  //set value empty
  if (crashlyticsReportKeySet != null) {
    FirebaseCrashlytics.instance.setCustomKey(
        crashlyticsReportKeySet.key, 'no data');
  }
  if (withDebugLog) {
    logger.d('$exception\n\nreason : $reason');
  }
}
