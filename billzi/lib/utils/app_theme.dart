import 'package:flutter/material.dart';

class AppTheme {
  static final light = ThemeData.light().copyWith(
    backgroundColor: Colors.white,
    buttonColor: Colors.blue,
    // appBarTheme: AppBarTheme(
    //     centerTitle: true,
    //     backgroundColor: Colors.white,
    //     iconTheme: IconThemeData(
    //       color: AppColors.textBlack1,
    //     ),
    //     textTheme: TextTheme(
    //       bodyText2: TextStyle(color: AppColors.textBlack1, fontSize: 20),
    //     )),
  );
  static final dark = ThemeData.dark().copyWith(
    backgroundColor: Colors.black,
    buttonColor: Colors.red,
  );
}