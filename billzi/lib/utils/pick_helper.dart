import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:get/get.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/test_data.dart';

typedef OnSelectCustomIcon = void Function(String iconName);
typedef OnSelectCustomColor = void Function(Color iconColor);
typedef OnSelectBaseCategory = void Function(CategoryItem baseCategory);

onClickIcon(
    {required BuildContext context,
    required List<String> iconNameList,
    required Color selectedIconColor,
    required OnSelectCustomIcon onSelectCustomIcon}) {
  Get.bottomSheet(
    Container(
        height: MediaQuery.of(context).size.height * 0.5,
        padding: const EdgeInsets.only(right: 10, left: 10),
        color: Colors.white,
        child: Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                child: const Text('아이콘을 골라주세요', textScaleFactor: 1)),
            Expanded(
              child: GridView.builder(
                shrinkWrap: true,
                itemCount: iconNameList.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 1),
                itemBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () {
                        Get.back();
                        onSelectCustomIcon.call(iconNameList[index]);
                      },
                      child: SizedBox(
                        width: 40,
                        height: 40,
                        child: Icon(
                          getCategoryIcon(iconNameList[index]),
                          color: Colors.white,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                          shape: const CircleBorder(),
                          primary: selectedIconColor),
                    ),
                  );
                },
              ),
            ),
          ],
        )),
    enableDrag: false,
  );
}

onClickColor(
    {required BuildContext context,
    required String selectedIconName,
    required List<Color> iconColorList,
    required OnSelectCustomColor onSelectCustomColor}) {
  Get.bottomSheet(
    Container(
        height: Get.size.height * 0.5,
        padding: const EdgeInsets.only(right: 10, left: 10),
        color: Colors.white,
        child: Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                child: const Text('아이콘의 색상을 골라주세요', textScaleFactor: 1)),
            Expanded(
              child: GridView.builder(
                shrinkWrap: true,
                itemCount: TestData.getTestCategoryColors().length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 1),
                itemBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () {
                        Get.back();
                        onSelectCustomColor.call(iconColorList[index]);
                      },
                      child: SizedBox(
                        width: 40,
                        height: 40,
                        child: Icon(
                          getCategoryIcon(selectedIconName),
                          color: Colors.white,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                          shape: const CircleBorder(),
                          primary: iconColorList[index]),
                    ),
                  );
                },
              ),
            ),
          ],
        )),
    enableDrag: false,
  );
}

showColorPicker(
  ValueChanged<Color> onColorSelected, {
  Color? currentColor,
  String? title,
}) {
  // TODO : customize layout using below builder function
  PickerLayoutBuilder layoutBuilder =
      (BuildContext context, List<Color> colors, PickerItem child) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return SizedBox(
      width: 300,
      height: orientation == Orientation.portrait ? 360 : 240,
      child: GridView.count(
        crossAxisCount: orientation == Orientation.portrait ? 4 : 5,
        crossAxisSpacing: 5,
        mainAxisSpacing: 5,
        children: [for (Color color in colors) child(color)],
      ),
    );
  };

  Get.bottomSheet(
    Container(
        height: Get.size.height * 0.5,
        padding: const EdgeInsets.only(right: 10, left: 10),
        color: Colors.white,
        child: Column(
          children: [
            if (title != null)
              Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Text(title, textScaleFactor: 1)),
            Expanded(
              child: SingleChildScrollView(
                child: BlockPicker(
                  availableColors: defaultBookColors,
                  pickerColor: currentColor ?? defaultBookColors[0],
                  onColorChanged: onColorSelected,
                  layoutBuilder: layoutBuilder,
                ),
              ),
            ),
          ],
        )),
    enableDrag: false,
  );
}

onClickCategory(
    {required BuildContext context,
    required List<CategoryItem> baseCategoryList,
    required OnSelectBaseCategory onSelectBaseCategory}) {
  Get.bottomSheet(
    Container(
        height: MediaQuery.of(context).size.height * 0.5,
        padding: const EdgeInsets.only(right: 10, left: 10),
        color: Colors.white,
        child: Material(
          child: Column(
            children: [
              Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 20),
                  child: const Text('연관된 카테고리를 선택해주세요', textScaleFactor: 1)),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: baseCategoryList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      child: SizedBox(
                          height: 50,
                          child: Center(
                              child: Text(
                                  baseCategoryList[index].accountCategoryName!,
                                  textScaleFactor: 2))),
                      onTap: () {
                        onSelectBaseCategory.call(baseCategoryList[index]);
                        Get.back();
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        )),
    enableDrag: false,
  );
}

Future<DateTime?> selectDate(
    BuildContext context, DateTime selectedDateTime) async {
  final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDateTime,
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime(2015),
      lastDate: DateTime(2101));

  return picked;
}

Future<TimeOfDay?> selectTime(
    BuildContext context, TimeOfDay selectedTime) async {
  final TimeOfDay? picked = await showTimePicker(
    context: context,
    initialTime: selectedTime,
  );
  return picked;
}
