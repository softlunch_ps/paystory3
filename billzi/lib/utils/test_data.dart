import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/common/currency/currencies.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/data/dto/model/recurrence_model.dart';
import 'package:billzi/data/dto/testDto/test_category.dart';
import 'package:billzi/data/dto/testDto/test_item.dart';
import 'package:billzi/data/dto/testDto/test_payment_type.dart';
import 'package:billzi/data/dto/testDto/test_transaction_type.dart';
import 'package:billzi/utils/extensions.dart';
import 'package:table_calendar/table_calendar.dart';

enum TestItemType { income, expenditure, notInput }

class TestData {

  static List<RecurrenceModel> getTestRecurrenceList() {
    return [
      RecurrenceModel(name: 'Daily'),
      RecurrenceModel(name: 'Weekly'),
      RecurrenceModel(name: 'Biweekly'),
      RecurrenceModel(name: 'Monthly'),
      RecurrenceModel(name: 'Yearly'),
    ];
  }

  static List<TestTag> getTestTagList() {
    return[
      TestTag('tag 1'),
      TestTag('tag 2'),
      TestTag('tag 3'),
      TestTag('tag 4'),
      TestTag('tag 5'),
      TestTag('tag 6'),
      TestTag('tag 7'),
      TestTag('tag 8'),
      TestTag('tag 9'),
      TestTag('tag 10'),
      TestTag('tag 11'),
      TestTag('tag 12'),
      TestTag('tag 13'),
      TestTag('tag 14'),
      TestTag('tag 15'),
      TestTag('tag 16'),
      TestTag('tag 17'),
      TestTag('tag 18'),
      TestTag('tag 19'),
      TestTag('tag 20'),
    ];
  }

  static List<TestCategory> getTestCategories({bool hasAddButton = false}) {
    List<TestCategory> list = [
      TestCategory(name: '식비', iconName: 'restaurant'),
      TestCategory(name: '카페/간식', iconName: 'local_cafe'),
      TestCategory(name: '마트/편의점', iconName: 'shopping_cart'),
      TestCategory(name: '백화점/쇼핑', iconName: 'shopping_bag'),
      TestCategory(name: '주거/생활', iconName: 'house'),
      TestCategory(name: '패션/미용', iconName: 'watch_sharp'),
      TestCategory(name: '금융/보험', iconName: 'comment_bank'),
      TestCategory(name: '통신/인터넷', iconName: 'wifi'),
      TestCategory(name: '교통/차량', iconName: 'directions_car_sharp'),
      TestCategory(name: '건강/의료', iconName: 'local_hospital_sharp'),
      TestCategory(name: '문화/놀이', iconName: 'wine_bar_sharp'),
      TestCategory(name: '여행/항공/숙박', iconName: 'airplanemode_active_sharp'),
      TestCategory(name: '반려동물', iconName: 'pets_sharp'),
      TestCategory(name: '경조사/회비', iconName: 'account_balance_sharp'),
      TestCategory(name: '교육', iconName: 'school_sharp'),
      TestCategory(name: '공과금', iconName: 'account_balance_wallet_sharp'),
      TestCategory(name: '기타', iconName: 'widgets_sharp'),
      TestCategory(name: '미분류', iconName: 'workspaces_filled'),
    ];

    if (hasAddButton) {
      list.add(TestCategory(name: '카테고리 추가', iconName: 'add'));
    }
    return list;
  }

  static List<TestBudget> getTestBudgetList() {
    return [
      TestBudget(name: '한달 예산', moneyGoal: 1000, moneyUsed: 600, startDate: DateTime.parse('2022-01-01T06:35:05.805Z'), endDate: DateTime.parse('2022-01-31T06:35:05.805Z')),
      TestBudget(name: '교통비', moneyGoal: 1000, moneyUsed: 400, startDate: DateTime.parse('2022-01-01T06:35:05.805Z'), endDate: DateTime.parse('2022-01-31T06:35:05.805Z')),
      TestBudget(name: '점심값', moneyGoal: 1000, moneyUsed: 300, startDate: DateTime.parse('2022-01-01T06:35:05.805Z'), endDate: DateTime.parse('2022-01-31T06:35:05.805Z')),
    ];
  }

  static List<TestCategory> getTestAddCustomCategories(
      {bool hasAddButton = false}) {
    List<TestCategory> list = [
      TestCategory(name: '커스텀 식비', iconName: 'restaurant'),
      TestCategory(name: '커스텀 카페/간식', iconName: 'local_cafe'),
    ];

    if (hasAddButton) {
      list.add(TestCategory(name: '카테고리 추가', iconName: 'add'));
    }
    return list;
  }

  static TestItemType getTestItemTypeNyName(String typeName) {
    switch (typeName) {
      case 'income':
        return TestItemType.income;
      case 'expenditure':
        return TestItemType.expenditure;
      default:
        return TestItemType.notInput;
    }
  }

  static IconData getIconByName(String iconName) {
    switch (iconName) {
      case 'restaurant':
        return Icons.restaurant;
      case 'local_cafe':
        return Icons.local_cafe;
      case 'shopping_cart':
        return Icons.shopping_cart;
      case 'shopping_bag':
        return Icons.shopping_bag;
      case 'house':
        return Icons.house;
      case 'watch_sharp':
        return Icons.watch_sharp;
      case 'comment_bank':
        return Icons.comment_bank;
      case 'wifi':
        return Icons.wifi;
      case 'directions_car_sharp':
        return Icons.directions_car_sharp;
      case 'local_hospital_sharp':
        return Icons.local_hospital_sharp;
      case 'wine_bar_sharp':
        return Icons.wine_bar_sharp;
      case 'airplanemode_active_sharp':
        return Icons.airplanemode_active_sharp;
      case 'pets_sharp':
        return Icons.pets_sharp;
      case 'account_balance_sharp':
        return Icons.account_balance_sharp;
      case 'school_sharp':
        return Icons.school_sharp;
      case 'account_balance_wallet_sharp':
        return Icons.account_balance_wallet_sharp;
      case 'widgets_sharp':
        return Icons.widgets_sharp;
      case 'workspaces_filled':
        return Icons.workspaces_filled;
      case 'assignment_turned_in':
        return Icons.assignment_turned_in;
      default:
        return Icons.house;
    }
  }

  static List<TestCategory> getTestAddCategories() {
    return [
      TestCategory(name: '급여수입', iconName: 'restaurant'),
      TestCategory(name: '사업수익', iconName: 'local_cafe'),
      TestCategory(name: '용돈수입', iconName: 'shopping_cart'),
      TestCategory(name: '금융수입', iconName: 'shopping_bag'),
      TestCategory(name: '기타수입', iconName: 'house'),
    ];
  }

  static List<TestItem> getTestItems() {
    return [
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestAddCategories()[2],
          money: 100000,
          date: DateTime.parse('2021-09-13T06:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[2],
          money: 10000,
          date: DateTime.parse('2021-09-13T05:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[4],
          money: 20000,
          date: DateTime.parse('2021-09-13T04:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[3],
          money: 1000,
          date: DateTime.parse('2021-09-12T05:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[5],
          money: 2000,
          date: DateTime.parse('2021-09-12T04:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[6],
          money: 3000,
          date: DateTime.parse('2021-09-11T05:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[7],
          money: 4000,
          date: DateTime.parse('2021-09-11T04:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[8],
          money: 5000,
          date: DateTime.parse('2021-09-10T05:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[9],
          money: 6000,
          date: DateTime.parse('2021-09-10T04:35:05.805Z')),
    ];
  }

  static List<TestSummaryItem> getTestSummaryItems() {
    return [
      TestSummaryItem(Icons.restaurant, '식비', 100000, 3),
      TestSummaryItem(Icons.local_cafe, '카페/간식', 50000, 5),
      TestSummaryItem(Icons.house, '주거/생활', 400000, 1),
      TestSummaryItem(Icons.wifi, '통신/인터넷', 150000, 2),
      TestSummaryItem(Icons.local_hospital_sharp, '건강/의료', 200000, 4),
    ];
  }

  static List<TestItem> getTestCategoryItems() {
    return [
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestAddCategories()[2],
          money: 100000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-13T06:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[2],
          money: 10000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-13T05:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[4],
          money: 20000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-13T04:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[3],
          money: 1000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-12T05:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[5],
          money: 2000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-12T04:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[6],
          money: 3000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-11T05:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[7],
          money: 4000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-11T04:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[8],
          money: 5000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-10T05:35:05.805Z')),
      TestItem(
          transactionType: TestData.getTestTransactionTypeList()[0],
          paymentType: TestData.getTestPaymentTypeList()[0],
          category: getTestCategories()[9],
          money: 6000,
          currency: Currency.fromJson(currencies[0]),
          date: DateTime.parse('2021-09-10T04:35:05.805Z')),
    ];
  }

  static String getTestItemTypeName(String typeName) {
    switch (typeName) {
      case 'income':
        return '수입';
      case 'expenditure':
        return '지출';
      default:
        return '미입력';
    }
  }

  static List<TestItemGroup> warpListByDate(List<TestItem> itemList) {
    List<TestItemGroup> list = [];
    DateTime? dateTime;
    itemList.forEach((element) {
      if (dateTime == null) {
        dateTime = element.date;
        list.add(TestItemGroup(element.date, [element]));
      } else {
        if (dateTime!.isSameDate(element.date)) {
          list.last.groupList.add(element);
        } else {
          dateTime = element.date;
          list.add(TestItemGroup(element.date, [element]));
        }
      }
    });
    return list;
  }

  static List<TestShop> getTestCategoryRanking() {
    return [
      TestShop('버거킹', 10000),
      TestShop('스타벅스', 8000),
      TestShop('베스킨라빈스', 5000),
    ];
  }

  static List<TestPayment> getTestPayments() {
    return [
      TestPayment('현금', 250000, 14),
      TestPayment('우리체크', 150000, 5),
      TestPayment('롯데신용', 350000, 9),
      TestPayment('신한체크', 100000, 10),
    ];
  }

  static double getPaymentsTotalMoney() {
    double sum = 0;
    getTestPayments().forEach((element) {
      sum += element.totalMoney;
    });
    return sum;
  }

  static getTestCategoryIcons() {
    return [
      TestCategoryIcon(Icons.call, Colors.blue),
      TestCategoryIcon(Icons.info_outline, Colors.blue),
      TestCategoryIcon(Icons.padding, Colors.blue),
      TestCategoryIcon(Icons.shop, Colors.blue),
      TestCategoryIcon(Icons.widgets_sharp, Colors.blue),
      TestCategoryIcon(Icons.pets_sharp, Colors.blue),
      TestCategoryIcon(Icons.airplanemode_active_sharp, Colors.blue),
      TestCategoryIcon(Icons.android, Colors.blue),
      TestCategoryIcon(Icons.ac_unit, Colors.blue),
      TestCategoryIcon(Icons.local_hospital_sharp, Colors.blue),
      TestCategoryIcon(Icons.android_sharp, Colors.blue),
      TestCategoryIcon(Icons.school_sharp, Colors.blue),
      TestCategoryIcon(Icons.directions_car_sharp, Colors.blue),
      TestCategoryIcon(Icons.wifi, Colors.blue),
      TestCategoryIcon(Icons.restaurant, Colors.blue),
      TestCategoryIcon(Icons.local_cafe, Colors.blue),
      TestCategoryIcon(Icons.account_balance_wallet_sharp, Colors.blue),
      TestCategoryIcon(Icons.account_box_outlined, Colors.blue),
      TestCategoryIcon(Icons.volunteer_activism, Colors.blue),
      TestCategoryIcon(Icons.whatshot, Colors.blue),
      TestCategoryIcon(Icons.alternate_email, Colors.blue),
      TestCategoryIcon(Icons.vpn_key, Colors.blue),
      TestCategoryIcon(Icons.watch_later_sharp, Colors.blue),
      TestCategoryIcon(Icons.wine_bar_rounded, Colors.blue),
    ];
  }

  static getTestCategoryColors() {
    return [
      TestCategoryIcon(Icons.call, Colors.red),
      TestCategoryIcon(Icons.info_outline, Colors.orange),
      TestCategoryIcon(Icons.padding, Colors.deepOrange),
      TestCategoryIcon(Icons.shop, Colors.yellow),
      TestCategoryIcon(Icons.widgets_sharp, Colors.amber),
      TestCategoryIcon(Icons.pets_sharp, Colors.lightGreen),
      TestCategoryIcon(Icons.airplanemode_active_sharp, Colors.lime),
      TestCategoryIcon(Icons.android, Colors.green),
      TestCategoryIcon(Icons.ac_unit, Colors.cyan),
      TestCategoryIcon(Icons.local_hospital_sharp, Colors.blue),
      TestCategoryIcon(Icons.android_sharp, Colors.blueAccent),
      TestCategoryIcon(Icons.school_sharp, Colors.teal),
      TestCategoryIcon(Icons.directions_car_sharp, Colors.cyanAccent),
      TestCategoryIcon(Icons.wifi, Colors.indigo),
      TestCategoryIcon(Icons.restaurant, Colors.indigoAccent),
      TestCategoryIcon(Icons.local_cafe, Colors.lightBlue),
      TestCategoryIcon(Icons.account_balance_wallet_sharp, Colors.blueGrey),
      TestCategoryIcon(Icons.account_box_outlined, Colors.grey),
      TestCategoryIcon(Icons.volunteer_activism, Colors.purple),
      TestCategoryIcon(Icons.whatshot, Colors.pink),
      TestCategoryIcon(Icons.alternate_email, Colors.brown),
      TestCategoryIcon(Icons.vpn_key, Colors.deepPurple),
      TestCategoryIcon(Icons.watch_later_sharp, Colors.deepPurpleAccent),
      TestCategoryIcon(Icons.wine_bar_rounded, Colors.yellowAccent),
    ];
  }

  static List<TestItem> getTestItemListFromMap(
      List<Map<String, dynamic>> list) {
    List<TestItem> result = [];
    list.forEach((element) {
      result.add(TestItem.fromJson(element));
    });
    return result;
  }

  //지출방식 01:현금, 02:신용카드, 03:체크카드, 04:수표
  static List<TestPaymentType> getTestPaymentTypeList() {
    return [
      TestPaymentType(name: 'Cash', id: '01'),
      TestPaymentType(name: 'Credit Card', id: '02'),
      TestPaymentType(name: 'Check Card', id: '03'),
      TestPaymentType(name: 'Check', id: '04'),
    ];
  }

  static List<TestTransactionType> getTestTransactionTypeList() {
    return [
      TestTransactionType(name: 'Income', id: 'I'),
      TestTransactionType(name: 'Expense', id: 'E'),
    ];
  }
}

class TestTag {
  final String name;
  TestTag(this.name);
}

class TestCategoryIcon {
  IconData icon;
  Color color;

  TestCategoryIcon(this.icon, this.color);
}

class TestBudget {
  String name;
  double moneyGoal;
  double moneyUsed;
  DateTime startDate;
  DateTime endDate;
  TestBudget(
      {required this.name,
      required this.moneyGoal,
      required this.moneyUsed,
      required this.startDate,
      required this.endDate});
}

class TestItemGroup {
  List<TestItem> groupList;
  DateTime dateTime;

  TestItemGroup(this.dateTime, this.groupList);

  double getMoneySum(TestTransactionType transactionType) {
    double sum = 0;
    groupList.forEach((element) {
      if (TestData.getTestItemTypeNyName(element.transactionType.id) == transactionType.id) {
        sum = sum + element.money;
      }
    });
    return sum;
  }
}

class TestSummaryItem {
  IconData icon;
  String name;
  double money;
  int count;

  TestSummaryItem(this.icon, this.name, this.money, this.count);

  String getMoneyPercentWithTotal(double total) {
    return '${(money / total * 100).toStringAsFixed(1)}%';
  }
}

class TestShop {
  String name;
  double money;

  TestShop(this.name, this.money);
}

class TestPayment {
  String name;
  double totalMoney;
  int count;

  TestPayment(this.name, this.totalMoney, this.count);

  String totalMoneyPercent(double money) {
    return '${(totalMoney / money * 100).toStringAsFixed(1)}%';
  }
}

class Event {
  final String title;
  final DateTime? dateTime;

  const Event(this.title, {this.dateTime});

  @override
  String toString() => title;
}

/// Example events.
///
/// Using a [LinkedHashMap] is highly recommended if you decide to use a map.
final kEvents = LinkedHashMap<DateTime, List<Event>>(
  equals: isSameDay,
  hashCode: getHashCode,
)..addAll(_kEventSource);

final _kEventSource = Map.fromIterable(List.generate(50, (index) => index),
    key: (item) => DateTime.utc(kFirstDay.year, kFirstDay.month, item * 5),
    value: (item) => List.generate(
        item % 4 + 1,
        (index) => Event('Event $item | ${index + 1}',
            dateTime: DateTime.utc(kFirstDay.year, kFirstDay.month, item * 5))))
  ..addAll({
    kToday: [
      Event('Today\'s Event 1', dateTime: DateTime.now()),
      Event('Today\'s Event 2', dateTime: DateTime.now()),
    ],
  });

int getHashCode(DateTime key) {
  return key.day * 1000000 + key.month * 10000 + key.year;
}

/// Returns a list of [DateTime] objects from [first] to [last], inclusive.
List<DateTime> daysInRange(DateTime first, DateTime last) {
  final dayCount = last.difference(first).inDays + 1;
  return List.generate(
    dayCount,
    (index) => DateTime.utc(first.year, first.month, first.day + index),
  );
}

final kToday = DateTime.now();
final kFirstDay = DateTime(kToday.year, kToday.month - 3, kToday.day);
final kLastDay = DateTime(kToday.year, kToday.month + 3, kToday.day);

// /// Checks if two DateTime objects are the same day.
// /// Returns `false` if either of them is null.
// bool isSameDay(DateTime? a, DateTime? b) {
//   if (a == null || b == null) {
//     return false;
//   }
//   return a.year == b.year && a.month == b.month && a.day == b.day;
// }
