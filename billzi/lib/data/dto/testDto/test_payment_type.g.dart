// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_payment_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TestPaymentType _$$_TestPaymentTypeFromJson(Map<String, dynamic> json) =>
    _$_TestPaymentType(
      name: json['name'] as String,
      id: json['id'] as String,
    );

Map<String, dynamic> _$$_TestPaymentTypeToJson(_$_TestPaymentType instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
    };
