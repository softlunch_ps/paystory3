// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_book_storage.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TestBookStorage _$TestBookStorageFromJson(Map<String, dynamic> json) {
  return _TestBookStorage.fromJson(json);
}

/// @nodoc
class _$TestBookStorageTearOff {
  const _$TestBookStorageTearOff();

  _TestBookStorage call({required List<TestBook> testBookList}) {
    return _TestBookStorage(
      testBookList: testBookList,
    );
  }

  TestBookStorage fromJson(Map<String, Object?> json) {
    return TestBookStorage.fromJson(json);
  }
}

/// @nodoc
const $TestBookStorage = _$TestBookStorageTearOff();

/// @nodoc
mixin _$TestBookStorage {
  List<TestBook> get testBookList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TestBookStorageCopyWith<TestBookStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestBookStorageCopyWith<$Res> {
  factory $TestBookStorageCopyWith(
          TestBookStorage value, $Res Function(TestBookStorage) then) =
      _$TestBookStorageCopyWithImpl<$Res>;
  $Res call({List<TestBook> testBookList});
}

/// @nodoc
class _$TestBookStorageCopyWithImpl<$Res>
    implements $TestBookStorageCopyWith<$Res> {
  _$TestBookStorageCopyWithImpl(this._value, this._then);

  final TestBookStorage _value;
  // ignore: unused_field
  final $Res Function(TestBookStorage) _then;

  @override
  $Res call({
    Object? testBookList = freezed,
  }) {
    return _then(_value.copyWith(
      testBookList: testBookList == freezed
          ? _value.testBookList
          : testBookList // ignore: cast_nullable_to_non_nullable
              as List<TestBook>,
    ));
  }
}

/// @nodoc
abstract class _$TestBookStorageCopyWith<$Res>
    implements $TestBookStorageCopyWith<$Res> {
  factory _$TestBookStorageCopyWith(
          _TestBookStorage value, $Res Function(_TestBookStorage) then) =
      __$TestBookStorageCopyWithImpl<$Res>;
  @override
  $Res call({List<TestBook> testBookList});
}

/// @nodoc
class __$TestBookStorageCopyWithImpl<$Res>
    extends _$TestBookStorageCopyWithImpl<$Res>
    implements _$TestBookStorageCopyWith<$Res> {
  __$TestBookStorageCopyWithImpl(
      _TestBookStorage _value, $Res Function(_TestBookStorage) _then)
      : super(_value, (v) => _then(v as _TestBookStorage));

  @override
  _TestBookStorage get _value => super._value as _TestBookStorage;

  @override
  $Res call({
    Object? testBookList = freezed,
  }) {
    return _then(_TestBookStorage(
      testBookList: testBookList == freezed
          ? _value.testBookList
          : testBookList // ignore: cast_nullable_to_non_nullable
              as List<TestBook>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TestBookStorage implements _TestBookStorage {
  _$_TestBookStorage({required this.testBookList});

  factory _$_TestBookStorage.fromJson(Map<String, dynamic> json) =>
      _$$_TestBookStorageFromJson(json);

  @override
  final List<TestBook> testBookList;

  @override
  String toString() {
    return 'TestBookStorage(testBookList: $testBookList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TestBookStorage &&
            const DeepCollectionEquality()
                .equals(other.testBookList, testBookList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(testBookList));

  @JsonKey(ignore: true)
  @override
  _$TestBookStorageCopyWith<_TestBookStorage> get copyWith =>
      __$TestBookStorageCopyWithImpl<_TestBookStorage>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TestBookStorageToJson(this);
  }
}

abstract class _TestBookStorage implements TestBookStorage {
  factory _TestBookStorage({required List<TestBook> testBookList}) =
      _$_TestBookStorage;

  factory _TestBookStorage.fromJson(Map<String, dynamic> json) =
      _$_TestBookStorage.fromJson;

  @override
  List<TestBook> get testBookList;
  @override
  @JsonKey(ignore: true)
  _$TestBookStorageCopyWith<_TestBookStorage> get copyWith =>
      throw _privateConstructorUsedError;
}
