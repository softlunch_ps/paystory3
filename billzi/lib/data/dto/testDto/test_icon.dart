import 'package:flutter/material.dart';

class TestIcon {
  String name;
  IconData icon;

  TestIcon(this.name, this.icon);
}