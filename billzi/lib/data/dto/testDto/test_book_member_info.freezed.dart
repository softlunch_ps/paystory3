// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_book_member_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookMemberInfo _$BookMemberInfoFromJson(Map<String, dynamic> json) {
  return _BookMemberInfo.fromJson(json);
}

/// @nodoc
class _$BookMemberInfoTearOff {
  const _$BookMemberInfoTearOff();

  _BookMemberInfo call(
      {required String id,
      required String iconUnicode,
      required String nickName,
      required MemberGrade grade,
      List<String> bookIds = const <String>[]}) {
    return _BookMemberInfo(
      id: id,
      iconUnicode: iconUnicode,
      nickName: nickName,
      grade: grade,
      bookIds: bookIds,
    );
  }

  BookMemberInfo fromJson(Map<String, Object?> json) {
    return BookMemberInfo.fromJson(json);
  }
}

/// @nodoc
const $BookMemberInfo = _$BookMemberInfoTearOff();

/// @nodoc
mixin _$BookMemberInfo {
  String get id => throw _privateConstructorUsedError;
  String get iconUnicode => throw _privateConstructorUsedError;
  String get nickName => throw _privateConstructorUsedError;
  MemberGrade get grade => throw _privateConstructorUsedError;
  List<String> get bookIds => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookMemberInfoCopyWith<BookMemberInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookMemberInfoCopyWith<$Res> {
  factory $BookMemberInfoCopyWith(
          BookMemberInfo value, $Res Function(BookMemberInfo) then) =
      _$BookMemberInfoCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String iconUnicode,
      String nickName,
      MemberGrade grade,
      List<String> bookIds});
}

/// @nodoc
class _$BookMemberInfoCopyWithImpl<$Res>
    implements $BookMemberInfoCopyWith<$Res> {
  _$BookMemberInfoCopyWithImpl(this._value, this._then);

  final BookMemberInfo _value;
  // ignore: unused_field
  final $Res Function(BookMemberInfo) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? iconUnicode = freezed,
    Object? nickName = freezed,
    Object? grade = freezed,
    Object? bookIds = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      iconUnicode: iconUnicode == freezed
          ? _value.iconUnicode
          : iconUnicode // ignore: cast_nullable_to_non_nullable
              as String,
      nickName: nickName == freezed
          ? _value.nickName
          : nickName // ignore: cast_nullable_to_non_nullable
              as String,
      grade: grade == freezed
          ? _value.grade
          : grade // ignore: cast_nullable_to_non_nullable
              as MemberGrade,
      bookIds: bookIds == freezed
          ? _value.bookIds
          : bookIds // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
abstract class _$BookMemberInfoCopyWith<$Res>
    implements $BookMemberInfoCopyWith<$Res> {
  factory _$BookMemberInfoCopyWith(
          _BookMemberInfo value, $Res Function(_BookMemberInfo) then) =
      __$BookMemberInfoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String iconUnicode,
      String nickName,
      MemberGrade grade,
      List<String> bookIds});
}

/// @nodoc
class __$BookMemberInfoCopyWithImpl<$Res>
    extends _$BookMemberInfoCopyWithImpl<$Res>
    implements _$BookMemberInfoCopyWith<$Res> {
  __$BookMemberInfoCopyWithImpl(
      _BookMemberInfo _value, $Res Function(_BookMemberInfo) _then)
      : super(_value, (v) => _then(v as _BookMemberInfo));

  @override
  _BookMemberInfo get _value => super._value as _BookMemberInfo;

  @override
  $Res call({
    Object? id = freezed,
    Object? iconUnicode = freezed,
    Object? nickName = freezed,
    Object? grade = freezed,
    Object? bookIds = freezed,
  }) {
    return _then(_BookMemberInfo(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      iconUnicode: iconUnicode == freezed
          ? _value.iconUnicode
          : iconUnicode // ignore: cast_nullable_to_non_nullable
              as String,
      nickName: nickName == freezed
          ? _value.nickName
          : nickName // ignore: cast_nullable_to_non_nullable
              as String,
      grade: grade == freezed
          ? _value.grade
          : grade // ignore: cast_nullable_to_non_nullable
              as MemberGrade,
      bookIds: bookIds == freezed
          ? _value.bookIds
          : bookIds // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookMemberInfo extends _BookMemberInfo {
  const _$_BookMemberInfo(
      {required this.id,
      required this.iconUnicode,
      required this.nickName,
      required this.grade,
      this.bookIds = const <String>[]})
      : super._();

  factory _$_BookMemberInfo.fromJson(Map<String, dynamic> json) =>
      _$$_BookMemberInfoFromJson(json);

  @override
  final String id;
  @override
  final String iconUnicode;
  @override
  final String nickName;
  @override
  final MemberGrade grade;
  @JsonKey(defaultValue: const <String>[])
  @override
  final List<String> bookIds;

  @override
  String toString() {
    return 'BookMemberInfo(id: $id, iconUnicode: $iconUnicode, nickName: $nickName, grade: $grade, bookIds: $bookIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BookMemberInfo &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.iconUnicode, iconUnicode) ||
                other.iconUnicode == iconUnicode) &&
            (identical(other.nickName, nickName) ||
                other.nickName == nickName) &&
            (identical(other.grade, grade) || other.grade == grade) &&
            const DeepCollectionEquality().equals(other.bookIds, bookIds));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, iconUnicode, nickName, grade,
      const DeepCollectionEquality().hash(bookIds));

  @JsonKey(ignore: true)
  @override
  _$BookMemberInfoCopyWith<_BookMemberInfo> get copyWith =>
      __$BookMemberInfoCopyWithImpl<_BookMemberInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookMemberInfoToJson(this);
  }
}

abstract class _BookMemberInfo extends BookMemberInfo {
  const factory _BookMemberInfo(
      {required String id,
      required String iconUnicode,
      required String nickName,
      required MemberGrade grade,
      List<String> bookIds}) = _$_BookMemberInfo;
  const _BookMemberInfo._() : super._();

  factory _BookMemberInfo.fromJson(Map<String, dynamic> json) =
      _$_BookMemberInfo.fromJson;

  @override
  String get id;
  @override
  String get iconUnicode;
  @override
  String get nickName;
  @override
  MemberGrade get grade;
  @override
  List<String> get bookIds;
  @override
  @JsonKey(ignore: true)
  _$BookMemberInfoCopyWith<_BookMemberInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
