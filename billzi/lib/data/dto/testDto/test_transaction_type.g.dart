// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_transaction_type.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TestTransactionType _$$_TestTransactionTypeFromJson(
        Map<String, dynamic> json) =>
    _$_TestTransactionType(
      name: json['name'] as String,
      id: json['id'] as String,
    );

Map<String, dynamic> _$$_TestTransactionTypeToJson(
        _$_TestTransactionType instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
    };
