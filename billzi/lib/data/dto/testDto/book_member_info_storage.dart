import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/testDto/test_book_member_info.dart';

part 'book_member_info_storage.freezed.dart';
part 'book_member_info_storage.g.dart';

@freezed
class BookMemberStorage with _$BookMemberStorage {
  factory BookMemberStorage({
    required List<BookMemberInfo>
        userInfoList, // book ids that this user belongs to
  }) = _BookMemberStorage;

  factory BookMemberStorage.fromJson(Map<String, dynamic> json) =>
      _$BookMemberStorageFromJson(json);
}
