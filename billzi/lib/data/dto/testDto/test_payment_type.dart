import 'package:freezed_annotation/freezed_annotation.dart';

part 'test_payment_type.freezed.dart';
part 'test_payment_type.g.dart';

@freezed
class TestPaymentType with _$TestPaymentType {

  factory TestPaymentType({
    required String name,
    required String id,
  }) = _TestPaymentType;

  factory TestPaymentType.fromJson(Map<String, dynamic> json) =>
      _$TestPaymentTypeFromJson(json);
}