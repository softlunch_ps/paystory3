// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_storage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TestStorage _$$_TestStorageFromJson(Map<String, dynamic> json) =>
    _$_TestStorage(
      testItemList: (json['testItemList'] as List<dynamic>)
          .map((e) => TestItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_TestStorageToJson(_$_TestStorage instance) =>
    <String, dynamic>{
      'testItemList': instance.testItemList,
    };
