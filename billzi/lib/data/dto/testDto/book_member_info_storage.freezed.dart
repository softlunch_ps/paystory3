// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'book_member_info_storage.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookMemberStorage _$BookMemberStorageFromJson(Map<String, dynamic> json) {
  return _BookMemberStorage.fromJson(json);
}

/// @nodoc
class _$BookMemberStorageTearOff {
  const _$BookMemberStorageTearOff();

  _BookMemberStorage call({required List<BookMemberInfo> userInfoList}) {
    return _BookMemberStorage(
      userInfoList: userInfoList,
    );
  }

  BookMemberStorage fromJson(Map<String, Object?> json) {
    return BookMemberStorage.fromJson(json);
  }
}

/// @nodoc
const $BookMemberStorage = _$BookMemberStorageTearOff();

/// @nodoc
mixin _$BookMemberStorage {
  List<BookMemberInfo> get userInfoList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookMemberStorageCopyWith<BookMemberStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookMemberStorageCopyWith<$Res> {
  factory $BookMemberStorageCopyWith(
          BookMemberStorage value, $Res Function(BookMemberStorage) then) =
      _$BookMemberStorageCopyWithImpl<$Res>;
  $Res call({List<BookMemberInfo> userInfoList});
}

/// @nodoc
class _$BookMemberStorageCopyWithImpl<$Res>
    implements $BookMemberStorageCopyWith<$Res> {
  _$BookMemberStorageCopyWithImpl(this._value, this._then);

  final BookMemberStorage _value;
  // ignore: unused_field
  final $Res Function(BookMemberStorage) _then;

  @override
  $Res call({
    Object? userInfoList = freezed,
  }) {
    return _then(_value.copyWith(
      userInfoList: userInfoList == freezed
          ? _value.userInfoList
          : userInfoList // ignore: cast_nullable_to_non_nullable
              as List<BookMemberInfo>,
    ));
  }
}

/// @nodoc
abstract class _$BookMemberStorageCopyWith<$Res>
    implements $BookMemberStorageCopyWith<$Res> {
  factory _$BookMemberStorageCopyWith(
          _BookMemberStorage value, $Res Function(_BookMemberStorage) then) =
      __$BookMemberStorageCopyWithImpl<$Res>;
  @override
  $Res call({List<BookMemberInfo> userInfoList});
}

/// @nodoc
class __$BookMemberStorageCopyWithImpl<$Res>
    extends _$BookMemberStorageCopyWithImpl<$Res>
    implements _$BookMemberStorageCopyWith<$Res> {
  __$BookMemberStorageCopyWithImpl(
      _BookMemberStorage _value, $Res Function(_BookMemberStorage) _then)
      : super(_value, (v) => _then(v as _BookMemberStorage));

  @override
  _BookMemberStorage get _value => super._value as _BookMemberStorage;

  @override
  $Res call({
    Object? userInfoList = freezed,
  }) {
    return _then(_BookMemberStorage(
      userInfoList: userInfoList == freezed
          ? _value.userInfoList
          : userInfoList // ignore: cast_nullable_to_non_nullable
              as List<BookMemberInfo>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookMemberStorage implements _BookMemberStorage {
  _$_BookMemberStorage({required this.userInfoList});

  factory _$_BookMemberStorage.fromJson(Map<String, dynamic> json) =>
      _$$_BookMemberStorageFromJson(json);

  @override
  final List<BookMemberInfo> userInfoList;

  @override
  String toString() {
    return 'BookMemberStorage(userInfoList: $userInfoList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BookMemberStorage &&
            const DeepCollectionEquality()
                .equals(other.userInfoList, userInfoList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(userInfoList));

  @JsonKey(ignore: true)
  @override
  _$BookMemberStorageCopyWith<_BookMemberStorage> get copyWith =>
      __$BookMemberStorageCopyWithImpl<_BookMemberStorage>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookMemberStorageToJson(this);
  }
}

abstract class _BookMemberStorage implements BookMemberStorage {
  factory _BookMemberStorage({required List<BookMemberInfo> userInfoList}) =
      _$_BookMemberStorage;

  factory _BookMemberStorage.fromJson(Map<String, dynamic> json) =
      _$_BookMemberStorage.fromJson;

  @override
  List<BookMemberInfo> get userInfoList;
  @override
  @JsonKey(ignore: true)
  _$BookMemberStorageCopyWith<_BookMemberStorage> get copyWith =>
      throw _privateConstructorUsedError;
}
