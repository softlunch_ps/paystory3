// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_book.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TestBook _$TestBookFromJson(Map<String, dynamic> json) {
  return _TestBook.fromJson(json);
}

/// @nodoc
class _$TestBookTearOff {
  const _$TestBookTearOff();

  _TestBook call(
      {required String id,
      required String name,
      required String color,
      String description = '',
      bool isClosed = false}) {
    return _TestBook(
      id: id,
      name: name,
      color: color,
      description: description,
      isClosed: isClosed,
    );
  }

  TestBook fromJson(Map<String, Object?> json) {
    return TestBook.fromJson(json);
  }
}

/// @nodoc
const $TestBook = _$TestBookTearOff();

/// @nodoc
mixin _$TestBook {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get color => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  bool get isClosed => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TestBookCopyWith<TestBook> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestBookCopyWith<$Res> {
  factory $TestBookCopyWith(TestBook value, $Res Function(TestBook) then) =
      _$TestBookCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String name,
      String color,
      String description,
      bool isClosed});
}

/// @nodoc
class _$TestBookCopyWithImpl<$Res> implements $TestBookCopyWith<$Res> {
  _$TestBookCopyWithImpl(this._value, this._then);

  final TestBook _value;
  // ignore: unused_field
  final $Res Function(TestBook) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? color = freezed,
    Object? description = freezed,
    Object? isClosed = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      isClosed: isClosed == freezed
          ? _value.isClosed
          : isClosed // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$TestBookCopyWith<$Res> implements $TestBookCopyWith<$Res> {
  factory _$TestBookCopyWith(_TestBook value, $Res Function(_TestBook) then) =
      __$TestBookCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String name,
      String color,
      String description,
      bool isClosed});
}

/// @nodoc
class __$TestBookCopyWithImpl<$Res> extends _$TestBookCopyWithImpl<$Res>
    implements _$TestBookCopyWith<$Res> {
  __$TestBookCopyWithImpl(_TestBook _value, $Res Function(_TestBook) _then)
      : super(_value, (v) => _then(v as _TestBook));

  @override
  _TestBook get _value => super._value as _TestBook;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? color = freezed,
    Object? description = freezed,
    Object? isClosed = freezed,
  }) {
    return _then(_TestBook(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      isClosed: isClosed == freezed
          ? _value.isClosed
          : isClosed // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TestBook implements _TestBook {
  _$_TestBook(
      {required this.id,
      required this.name,
      required this.color,
      this.description = '',
      this.isClosed = false});

  factory _$_TestBook.fromJson(Map<String, dynamic> json) =>
      _$$_TestBookFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final String color;
  @JsonKey(defaultValue: '')
  @override
  final String description;
  @JsonKey(defaultValue: false)
  @override
  final bool isClosed;

  @override
  String toString() {
    return 'TestBook(id: $id, name: $name, color: $color, description: $description, isClosed: $isClosed)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TestBook &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.isClosed, isClosed) ||
                other.isClosed == isClosed));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, id, name, color, description, isClosed);

  @JsonKey(ignore: true)
  @override
  _$TestBookCopyWith<_TestBook> get copyWith =>
      __$TestBookCopyWithImpl<_TestBook>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TestBookToJson(this);
  }
}

abstract class _TestBook implements TestBook {
  factory _TestBook(
      {required String id,
      required String name,
      required String color,
      String description,
      bool isClosed}) = _$_TestBook;

  factory _TestBook.fromJson(Map<String, dynamic> json) = _$_TestBook.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  String get color;
  @override
  String get description;
  @override
  bool get isClosed;
  @override
  @JsonKey(ignore: true)
  _$TestBookCopyWith<_TestBook> get copyWith =>
      throw _privateConstructorUsedError;
}
