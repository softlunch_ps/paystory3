// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_payment_type.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TestPaymentType _$TestPaymentTypeFromJson(Map<String, dynamic> json) {
  return _TestPaymentType.fromJson(json);
}

/// @nodoc
class _$TestPaymentTypeTearOff {
  const _$TestPaymentTypeTearOff();

  _TestPaymentType call({required String name, required String id}) {
    return _TestPaymentType(
      name: name,
      id: id,
    );
  }

  TestPaymentType fromJson(Map<String, Object?> json) {
    return TestPaymentType.fromJson(json);
  }
}

/// @nodoc
const $TestPaymentType = _$TestPaymentTypeTearOff();

/// @nodoc
mixin _$TestPaymentType {
  String get name => throw _privateConstructorUsedError;
  String get id => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TestPaymentTypeCopyWith<TestPaymentType> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestPaymentTypeCopyWith<$Res> {
  factory $TestPaymentTypeCopyWith(
          TestPaymentType value, $Res Function(TestPaymentType) then) =
      _$TestPaymentTypeCopyWithImpl<$Res>;
  $Res call({String name, String id});
}

/// @nodoc
class _$TestPaymentTypeCopyWithImpl<$Res>
    implements $TestPaymentTypeCopyWith<$Res> {
  _$TestPaymentTypeCopyWithImpl(this._value, this._then);

  final TestPaymentType _value;
  // ignore: unused_field
  final $Res Function(TestPaymentType) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? id = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$TestPaymentTypeCopyWith<$Res>
    implements $TestPaymentTypeCopyWith<$Res> {
  factory _$TestPaymentTypeCopyWith(
          _TestPaymentType value, $Res Function(_TestPaymentType) then) =
      __$TestPaymentTypeCopyWithImpl<$Res>;
  @override
  $Res call({String name, String id});
}

/// @nodoc
class __$TestPaymentTypeCopyWithImpl<$Res>
    extends _$TestPaymentTypeCopyWithImpl<$Res>
    implements _$TestPaymentTypeCopyWith<$Res> {
  __$TestPaymentTypeCopyWithImpl(
      _TestPaymentType _value, $Res Function(_TestPaymentType) _then)
      : super(_value, (v) => _then(v as _TestPaymentType));

  @override
  _TestPaymentType get _value => super._value as _TestPaymentType;

  @override
  $Res call({
    Object? name = freezed,
    Object? id = freezed,
  }) {
    return _then(_TestPaymentType(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TestPaymentType implements _TestPaymentType {
  _$_TestPaymentType({required this.name, required this.id});

  factory _$_TestPaymentType.fromJson(Map<String, dynamic> json) =>
      _$$_TestPaymentTypeFromJson(json);

  @override
  final String name;
  @override
  final String id;

  @override
  String toString() {
    return 'TestPaymentType(name: $name, id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TestPaymentType &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name, id);

  @JsonKey(ignore: true)
  @override
  _$TestPaymentTypeCopyWith<_TestPaymentType> get copyWith =>
      __$TestPaymentTypeCopyWithImpl<_TestPaymentType>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TestPaymentTypeToJson(this);
  }
}

abstract class _TestPaymentType implements TestPaymentType {
  factory _TestPaymentType({required String name, required String id}) =
      _$_TestPaymentType;

  factory _TestPaymentType.fromJson(Map<String, dynamic> json) =
      _$_TestPaymentType.fromJson;

  @override
  String get name;
  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$TestPaymentTypeCopyWith<_TestPaymentType> get copyWith =>
      throw _privateConstructorUsedError;
}
