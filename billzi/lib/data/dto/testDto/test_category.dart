import 'package:freezed_annotation/freezed_annotation.dart';

part 'test_category.freezed.dart';
part 'test_category.g.dart';

@freezed
class TestCategory with _$TestCategory {

  factory TestCategory({
    required String name,
    required String iconName,
  }) = _TestCategory;

  factory TestCategory.fromJson(Map<String, dynamic> json) =>
      _$TestCategoryFromJson(json);
}