import 'package:freezed_annotation/freezed_annotation.dart';

part 'test_book.freezed.dart';
part 'test_book.g.dart';

@freezed
class TestBook with _$TestBook {
  factory TestBook({
    required String id,
    required String name,
    required String color,
    @Default('') String description,
    @Default(false) bool isClosed,
  }) = _TestBook;

  factory TestBook.fromJson(Map<String, dynamic> json) =>
      _$TestBookFromJson(json);
}
