// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TestItem _$$_TestItemFromJson(Map<String, dynamic> json) => _$_TestItem(
      transactionType: TestTransactionType.fromJson(
          json['transactionType'] as Map<String, dynamic>),
      paymentType:
          TestPaymentType.fromJson(json['paymentType'] as Map<String, dynamic>),
      category: TestCategory.fromJson(json['category'] as Map<String, dynamic>),
      money: (json['money'] as num).toDouble(),
      date: DateTime.parse(json['date'] as String),
      description: json['description'] as String?,
      currency: json['currency'] == null
          ? null
          : Currency.fromJson(json['currency'] as Map<String, dynamic>),
      gpsInfo: json['gpsInfo'] == null
          ? null
          : GpsInfo.fromJson(json['gpsInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_TestItemToJson(_$_TestItem instance) =>
    <String, dynamic>{
      'transactionType': instance.transactionType,
      'paymentType': instance.paymentType,
      'category': instance.category,
      'money': instance.money,
      'date': instance.date.toIso8601String(),
      'description': instance.description,
      'currency': instance.currency,
      'gpsInfo': instance.gpsInfo,
    };
