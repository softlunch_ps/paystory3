// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_category.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TestCategory _$TestCategoryFromJson(Map<String, dynamic> json) {
  return _TestCategory.fromJson(json);
}

/// @nodoc
class _$TestCategoryTearOff {
  const _$TestCategoryTearOff();

  _TestCategory call({required String name, required String iconName}) {
    return _TestCategory(
      name: name,
      iconName: iconName,
    );
  }

  TestCategory fromJson(Map<String, Object?> json) {
    return TestCategory.fromJson(json);
  }
}

/// @nodoc
const $TestCategory = _$TestCategoryTearOff();

/// @nodoc
mixin _$TestCategory {
  String get name => throw _privateConstructorUsedError;
  String get iconName => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TestCategoryCopyWith<TestCategory> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestCategoryCopyWith<$Res> {
  factory $TestCategoryCopyWith(
          TestCategory value, $Res Function(TestCategory) then) =
      _$TestCategoryCopyWithImpl<$Res>;
  $Res call({String name, String iconName});
}

/// @nodoc
class _$TestCategoryCopyWithImpl<$Res> implements $TestCategoryCopyWith<$Res> {
  _$TestCategoryCopyWithImpl(this._value, this._then);

  final TestCategory _value;
  // ignore: unused_field
  final $Res Function(TestCategory) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? iconName = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      iconName: iconName == freezed
          ? _value.iconName
          : iconName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$TestCategoryCopyWith<$Res>
    implements $TestCategoryCopyWith<$Res> {
  factory _$TestCategoryCopyWith(
          _TestCategory value, $Res Function(_TestCategory) then) =
      __$TestCategoryCopyWithImpl<$Res>;
  @override
  $Res call({String name, String iconName});
}

/// @nodoc
class __$TestCategoryCopyWithImpl<$Res> extends _$TestCategoryCopyWithImpl<$Res>
    implements _$TestCategoryCopyWith<$Res> {
  __$TestCategoryCopyWithImpl(
      _TestCategory _value, $Res Function(_TestCategory) _then)
      : super(_value, (v) => _then(v as _TestCategory));

  @override
  _TestCategory get _value => super._value as _TestCategory;

  @override
  $Res call({
    Object? name = freezed,
    Object? iconName = freezed,
  }) {
    return _then(_TestCategory(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      iconName: iconName == freezed
          ? _value.iconName
          : iconName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TestCategory implements _TestCategory {
  _$_TestCategory({required this.name, required this.iconName});

  factory _$_TestCategory.fromJson(Map<String, dynamic> json) =>
      _$$_TestCategoryFromJson(json);

  @override
  final String name;
  @override
  final String iconName;

  @override
  String toString() {
    return 'TestCategory(name: $name, iconName: $iconName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TestCategory &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.iconName, iconName) ||
                other.iconName == iconName));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name, iconName);

  @JsonKey(ignore: true)
  @override
  _$TestCategoryCopyWith<_TestCategory> get copyWith =>
      __$TestCategoryCopyWithImpl<_TestCategory>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TestCategoryToJson(this);
  }
}

abstract class _TestCategory implements TestCategory {
  factory _TestCategory({required String name, required String iconName}) =
      _$_TestCategory;

  factory _TestCategory.fromJson(Map<String, dynamic> json) =
      _$_TestCategory.fromJson;

  @override
  String get name;
  @override
  String get iconName;
  @override
  @JsonKey(ignore: true)
  _$TestCategoryCopyWith<_TestCategory> get copyWith =>
      throw _privateConstructorUsedError;
}
