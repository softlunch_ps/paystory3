// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_member_info_storage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookMemberStorage _$$_BookMemberStorageFromJson(Map<String, dynamic> json) =>
    _$_BookMemberStorage(
      userInfoList: (json['userInfoList'] as List<dynamic>)
          .map((e) => BookMemberInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_BookMemberStorageToJson(
        _$_BookMemberStorage instance) =>
    <String, dynamic>{
      'userInfoList': instance.userInfoList,
    };
