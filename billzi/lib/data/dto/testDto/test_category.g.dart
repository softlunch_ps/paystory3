// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TestCategory _$$_TestCategoryFromJson(Map<String, dynamic> json) =>
    _$_TestCategory(
      name: json['name'] as String,
      iconName: json['iconName'] as String,
    );

Map<String, dynamic> _$$_TestCategoryToJson(_$_TestCategory instance) =>
    <String, dynamic>{
      'name': instance.name,
      'iconName': instance.iconName,
    };
