import 'package:freezed_annotation/freezed_annotation.dart';

part 'test_transaction_type.freezed.dart';
part 'test_transaction_type.g.dart';

@freezed
class TestTransactionType with _$TestTransactionType {

  factory TestTransactionType({
    required String name,
    required String id,
  }) = _TestTransactionType;

  factory TestTransactionType.fromJson(Map<String, dynamic> json) =>
      _$TestTransactionTypeFromJson(json);
}