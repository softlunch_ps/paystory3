import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/testDto/test_book.dart';

part 'test_book_storage.freezed.dart';
part 'test_book_storage.g.dart';

@freezed
class TestBookStorage with _$TestBookStorage {

  factory TestBookStorage({
    required List<TestBook> testBookList,
  }) = _TestBookStorage;

  factory TestBookStorage.fromJson(Map<String, dynamic> json) =>
      _$TestBookStorageFromJson(json);
}