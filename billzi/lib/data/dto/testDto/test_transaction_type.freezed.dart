// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_transaction_type.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TestTransactionType _$TestTransactionTypeFromJson(Map<String, dynamic> json) {
  return _TestTransactionType.fromJson(json);
}

/// @nodoc
class _$TestTransactionTypeTearOff {
  const _$TestTransactionTypeTearOff();

  _TestTransactionType call({required String name, required String id}) {
    return _TestTransactionType(
      name: name,
      id: id,
    );
  }

  TestTransactionType fromJson(Map<String, Object?> json) {
    return TestTransactionType.fromJson(json);
  }
}

/// @nodoc
const $TestTransactionType = _$TestTransactionTypeTearOff();

/// @nodoc
mixin _$TestTransactionType {
  String get name => throw _privateConstructorUsedError;
  String get id => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TestTransactionTypeCopyWith<TestTransactionType> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestTransactionTypeCopyWith<$Res> {
  factory $TestTransactionTypeCopyWith(
          TestTransactionType value, $Res Function(TestTransactionType) then) =
      _$TestTransactionTypeCopyWithImpl<$Res>;
  $Res call({String name, String id});
}

/// @nodoc
class _$TestTransactionTypeCopyWithImpl<$Res>
    implements $TestTransactionTypeCopyWith<$Res> {
  _$TestTransactionTypeCopyWithImpl(this._value, this._then);

  final TestTransactionType _value;
  // ignore: unused_field
  final $Res Function(TestTransactionType) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? id = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$TestTransactionTypeCopyWith<$Res>
    implements $TestTransactionTypeCopyWith<$Res> {
  factory _$TestTransactionTypeCopyWith(_TestTransactionType value,
          $Res Function(_TestTransactionType) then) =
      __$TestTransactionTypeCopyWithImpl<$Res>;
  @override
  $Res call({String name, String id});
}

/// @nodoc
class __$TestTransactionTypeCopyWithImpl<$Res>
    extends _$TestTransactionTypeCopyWithImpl<$Res>
    implements _$TestTransactionTypeCopyWith<$Res> {
  __$TestTransactionTypeCopyWithImpl(
      _TestTransactionType _value, $Res Function(_TestTransactionType) _then)
      : super(_value, (v) => _then(v as _TestTransactionType));

  @override
  _TestTransactionType get _value => super._value as _TestTransactionType;

  @override
  $Res call({
    Object? name = freezed,
    Object? id = freezed,
  }) {
    return _then(_TestTransactionType(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TestTransactionType implements _TestTransactionType {
  _$_TestTransactionType({required this.name, required this.id});

  factory _$_TestTransactionType.fromJson(Map<String, dynamic> json) =>
      _$$_TestTransactionTypeFromJson(json);

  @override
  final String name;
  @override
  final String id;

  @override
  String toString() {
    return 'TestTransactionType(name: $name, id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TestTransactionType &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name, id);

  @JsonKey(ignore: true)
  @override
  _$TestTransactionTypeCopyWith<_TestTransactionType> get copyWith =>
      __$TestTransactionTypeCopyWithImpl<_TestTransactionType>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TestTransactionTypeToJson(this);
  }
}

abstract class _TestTransactionType implements TestTransactionType {
  factory _TestTransactionType({required String name, required String id}) =
      _$_TestTransactionType;

  factory _TestTransactionType.fromJson(Map<String, dynamic> json) =
      _$_TestTransactionType.fromJson;

  @override
  String get name;
  @override
  String get id;
  @override
  @JsonKey(ignore: true)
  _$TestTransactionTypeCopyWith<_TestTransactionType> get copyWith =>
      throw _privateConstructorUsedError;
}
