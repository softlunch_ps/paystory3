import 'package:freezed_annotation/freezed_annotation.dart';

part 'test_book_member_info.freezed.dart';
part 'test_book_member_info.g.dart';

@freezed
class BookMemberInfo with _$BookMemberInfo {
  const BookMemberInfo._();
  const factory BookMemberInfo({
    required String id,
    required String iconUnicode,
    required String nickName,
    required MemberGrade grade,
    @Default(<String>[])
        List<String> bookIds, // book ids that this user belongs to
  }) = _BookMemberInfo;

  factory BookMemberInfo.fromJson(Map<String, dynamic> json) =>
      _$BookMemberInfoFromJson(json);

  get memberStatus {
    switch (grade) {
      case MemberGrade.MEMBER:
        return 'member';
      case MemberGrade.MASTER:
      case MemberGrade.ADMIN:
        return 'admin';
      default:
        return '';
    }
  }

  get isGradeModifiable => grade.index >= MemberGrade.MEMBER.index;
}

enum MemberGrade { NONMEMBER, MEMBER, ADMIN, MASTER }
