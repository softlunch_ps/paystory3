import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/data/dto/model/gps_Info.dart';
import 'package:billzi/data/dto/testDto/test_category.dart';
import 'package:billzi/data/dto/testDto/test_payment_type.dart';
import 'package:billzi/data/dto/testDto/test_transaction_type.dart';

part 'test_item.freezed.dart';
part 'test_item.g.dart';

@freezed
class TestItem with _$TestItem {

  factory TestItem({
    required TestTransactionType transactionType,
    required TestPaymentType paymentType,
    required TestCategory category,
    required double money,
    required DateTime date,
    String? description,
    Currency? currency,
    GpsInfo? gpsInfo,
  }) = _TestItem;

  factory TestItem.fromJson(Map<String, dynamic> json) =>
      _$TestItemFromJson(json);
}