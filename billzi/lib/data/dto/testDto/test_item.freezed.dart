// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_item.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TestItem _$TestItemFromJson(Map<String, dynamic> json) {
  return _TestItem.fromJson(json);
}

/// @nodoc
class _$TestItemTearOff {
  const _$TestItemTearOff();

  _TestItem call(
      {required TestTransactionType transactionType,
      required TestPaymentType paymentType,
      required TestCategory category,
      required double money,
      required DateTime date,
      String? description,
      Currency? currency,
      GpsInfo? gpsInfo}) {
    return _TestItem(
      transactionType: transactionType,
      paymentType: paymentType,
      category: category,
      money: money,
      date: date,
      description: description,
      currency: currency,
      gpsInfo: gpsInfo,
    );
  }

  TestItem fromJson(Map<String, Object?> json) {
    return TestItem.fromJson(json);
  }
}

/// @nodoc
const $TestItem = _$TestItemTearOff();

/// @nodoc
mixin _$TestItem {
  TestTransactionType get transactionType => throw _privateConstructorUsedError;
  TestPaymentType get paymentType => throw _privateConstructorUsedError;
  TestCategory get category => throw _privateConstructorUsedError;
  double get money => throw _privateConstructorUsedError;
  DateTime get date => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  Currency? get currency => throw _privateConstructorUsedError;
  GpsInfo? get gpsInfo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TestItemCopyWith<TestItem> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestItemCopyWith<$Res> {
  factory $TestItemCopyWith(TestItem value, $Res Function(TestItem) then) =
      _$TestItemCopyWithImpl<$Res>;
  $Res call(
      {TestTransactionType transactionType,
      TestPaymentType paymentType,
      TestCategory category,
      double money,
      DateTime date,
      String? description,
      Currency? currency,
      GpsInfo? gpsInfo});

  $TestTransactionTypeCopyWith<$Res> get transactionType;
  $TestPaymentTypeCopyWith<$Res> get paymentType;
  $TestCategoryCopyWith<$Res> get category;
  $CurrencyCopyWith<$Res>? get currency;
  $GpsInfoCopyWith<$Res>? get gpsInfo;
}

/// @nodoc
class _$TestItemCopyWithImpl<$Res> implements $TestItemCopyWith<$Res> {
  _$TestItemCopyWithImpl(this._value, this._then);

  final TestItem _value;
  // ignore: unused_field
  final $Res Function(TestItem) _then;

  @override
  $Res call({
    Object? transactionType = freezed,
    Object? paymentType = freezed,
    Object? category = freezed,
    Object? money = freezed,
    Object? date = freezed,
    Object? description = freezed,
    Object? currency = freezed,
    Object? gpsInfo = freezed,
  }) {
    return _then(_value.copyWith(
      transactionType: transactionType == freezed
          ? _value.transactionType
          : transactionType // ignore: cast_nullable_to_non_nullable
              as TestTransactionType,
      paymentType: paymentType == freezed
          ? _value.paymentType
          : paymentType // ignore: cast_nullable_to_non_nullable
              as TestPaymentType,
      category: category == freezed
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as TestCategory,
      money: money == freezed
          ? _value.money
          : money // ignore: cast_nullable_to_non_nullable
              as double,
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as Currency?,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
    ));
  }

  @override
  $TestTransactionTypeCopyWith<$Res> get transactionType {
    return $TestTransactionTypeCopyWith<$Res>(_value.transactionType, (value) {
      return _then(_value.copyWith(transactionType: value));
    });
  }

  @override
  $TestPaymentTypeCopyWith<$Res> get paymentType {
    return $TestPaymentTypeCopyWith<$Res>(_value.paymentType, (value) {
      return _then(_value.copyWith(paymentType: value));
    });
  }

  @override
  $TestCategoryCopyWith<$Res> get category {
    return $TestCategoryCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }

  @override
  $CurrencyCopyWith<$Res>? get currency {
    if (_value.currency == null) {
      return null;
    }

    return $CurrencyCopyWith<$Res>(_value.currency!, (value) {
      return _then(_value.copyWith(currency: value));
    });
  }

  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo {
    if (_value.gpsInfo == null) {
      return null;
    }

    return $GpsInfoCopyWith<$Res>(_value.gpsInfo!, (value) {
      return _then(_value.copyWith(gpsInfo: value));
    });
  }
}

/// @nodoc
abstract class _$TestItemCopyWith<$Res> implements $TestItemCopyWith<$Res> {
  factory _$TestItemCopyWith(_TestItem value, $Res Function(_TestItem) then) =
      __$TestItemCopyWithImpl<$Res>;
  @override
  $Res call(
      {TestTransactionType transactionType,
      TestPaymentType paymentType,
      TestCategory category,
      double money,
      DateTime date,
      String? description,
      Currency? currency,
      GpsInfo? gpsInfo});

  @override
  $TestTransactionTypeCopyWith<$Res> get transactionType;
  @override
  $TestPaymentTypeCopyWith<$Res> get paymentType;
  @override
  $TestCategoryCopyWith<$Res> get category;
  @override
  $CurrencyCopyWith<$Res>? get currency;
  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo;
}

/// @nodoc
class __$TestItemCopyWithImpl<$Res> extends _$TestItemCopyWithImpl<$Res>
    implements _$TestItemCopyWith<$Res> {
  __$TestItemCopyWithImpl(_TestItem _value, $Res Function(_TestItem) _then)
      : super(_value, (v) => _then(v as _TestItem));

  @override
  _TestItem get _value => super._value as _TestItem;

  @override
  $Res call({
    Object? transactionType = freezed,
    Object? paymentType = freezed,
    Object? category = freezed,
    Object? money = freezed,
    Object? date = freezed,
    Object? description = freezed,
    Object? currency = freezed,
    Object? gpsInfo = freezed,
  }) {
    return _then(_TestItem(
      transactionType: transactionType == freezed
          ? _value.transactionType
          : transactionType // ignore: cast_nullable_to_non_nullable
              as TestTransactionType,
      paymentType: paymentType == freezed
          ? _value.paymentType
          : paymentType // ignore: cast_nullable_to_non_nullable
              as TestPaymentType,
      category: category == freezed
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as TestCategory,
      money: money == freezed
          ? _value.money
          : money // ignore: cast_nullable_to_non_nullable
              as double,
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      currency: currency == freezed
          ? _value.currency
          : currency // ignore: cast_nullable_to_non_nullable
              as Currency?,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TestItem implements _TestItem {
  _$_TestItem(
      {required this.transactionType,
      required this.paymentType,
      required this.category,
      required this.money,
      required this.date,
      this.description,
      this.currency,
      this.gpsInfo});

  factory _$_TestItem.fromJson(Map<String, dynamic> json) =>
      _$$_TestItemFromJson(json);

  @override
  final TestTransactionType transactionType;
  @override
  final TestPaymentType paymentType;
  @override
  final TestCategory category;
  @override
  final double money;
  @override
  final DateTime date;
  @override
  final String? description;
  @override
  final Currency? currency;
  @override
  final GpsInfo? gpsInfo;

  @override
  String toString() {
    return 'TestItem(transactionType: $transactionType, paymentType: $paymentType, category: $category, money: $money, date: $date, description: $description, currency: $currency, gpsInfo: $gpsInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TestItem &&
            (identical(other.transactionType, transactionType) ||
                other.transactionType == transactionType) &&
            (identical(other.paymentType, paymentType) ||
                other.paymentType == paymentType) &&
            (identical(other.category, category) ||
                other.category == category) &&
            (identical(other.money, money) || other.money == money) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.currency, currency) ||
                other.currency == currency) &&
            (identical(other.gpsInfo, gpsInfo) || other.gpsInfo == gpsInfo));
  }

  @override
  int get hashCode => Object.hash(runtimeType, transactionType, paymentType,
      category, money, date, description, currency, gpsInfo);

  @JsonKey(ignore: true)
  @override
  _$TestItemCopyWith<_TestItem> get copyWith =>
      __$TestItemCopyWithImpl<_TestItem>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TestItemToJson(this);
  }
}

abstract class _TestItem implements TestItem {
  factory _TestItem(
      {required TestTransactionType transactionType,
      required TestPaymentType paymentType,
      required TestCategory category,
      required double money,
      required DateTime date,
      String? description,
      Currency? currency,
      GpsInfo? gpsInfo}) = _$_TestItem;

  factory _TestItem.fromJson(Map<String, dynamic> json) = _$_TestItem.fromJson;

  @override
  TestTransactionType get transactionType;
  @override
  TestPaymentType get paymentType;
  @override
  TestCategory get category;
  @override
  double get money;
  @override
  DateTime get date;
  @override
  String? get description;
  @override
  Currency? get currency;
  @override
  GpsInfo? get gpsInfo;
  @override
  @JsonKey(ignore: true)
  _$TestItemCopyWith<_TestItem> get copyWith =>
      throw _privateConstructorUsedError;
}
