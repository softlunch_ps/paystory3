// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_book_storage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TestBookStorage _$$_TestBookStorageFromJson(Map<String, dynamic> json) =>
    _$_TestBookStorage(
      testBookList: (json['testBookList'] as List<dynamic>)
          .map((e) => TestBook.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_TestBookStorageToJson(_$_TestBookStorage instance) =>
    <String, dynamic>{
      'testBookList': instance.testBookList,
    };
