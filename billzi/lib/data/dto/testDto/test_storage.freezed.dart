// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_storage.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TestStorage _$TestStorageFromJson(Map<String, dynamic> json) {
  return _TestStorage.fromJson(json);
}

/// @nodoc
class _$TestStorageTearOff {
  const _$TestStorageTearOff();

  _TestStorage call({required List<TestItem> testItemList}) {
    return _TestStorage(
      testItemList: testItemList,
    );
  }

  TestStorage fromJson(Map<String, Object?> json) {
    return TestStorage.fromJson(json);
  }
}

/// @nodoc
const $TestStorage = _$TestStorageTearOff();

/// @nodoc
mixin _$TestStorage {
  List<TestItem> get testItemList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TestStorageCopyWith<TestStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestStorageCopyWith<$Res> {
  factory $TestStorageCopyWith(
          TestStorage value, $Res Function(TestStorage) then) =
      _$TestStorageCopyWithImpl<$Res>;
  $Res call({List<TestItem> testItemList});
}

/// @nodoc
class _$TestStorageCopyWithImpl<$Res> implements $TestStorageCopyWith<$Res> {
  _$TestStorageCopyWithImpl(this._value, this._then);

  final TestStorage _value;
  // ignore: unused_field
  final $Res Function(TestStorage) _then;

  @override
  $Res call({
    Object? testItemList = freezed,
  }) {
    return _then(_value.copyWith(
      testItemList: testItemList == freezed
          ? _value.testItemList
          : testItemList // ignore: cast_nullable_to_non_nullable
              as List<TestItem>,
    ));
  }
}

/// @nodoc
abstract class _$TestStorageCopyWith<$Res>
    implements $TestStorageCopyWith<$Res> {
  factory _$TestStorageCopyWith(
          _TestStorage value, $Res Function(_TestStorage) then) =
      __$TestStorageCopyWithImpl<$Res>;
  @override
  $Res call({List<TestItem> testItemList});
}

/// @nodoc
class __$TestStorageCopyWithImpl<$Res> extends _$TestStorageCopyWithImpl<$Res>
    implements _$TestStorageCopyWith<$Res> {
  __$TestStorageCopyWithImpl(
      _TestStorage _value, $Res Function(_TestStorage) _then)
      : super(_value, (v) => _then(v as _TestStorage));

  @override
  _TestStorage get _value => super._value as _TestStorage;

  @override
  $Res call({
    Object? testItemList = freezed,
  }) {
    return _then(_TestStorage(
      testItemList: testItemList == freezed
          ? _value.testItemList
          : testItemList // ignore: cast_nullable_to_non_nullable
              as List<TestItem>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TestStorage implements _TestStorage {
  _$_TestStorage({required this.testItemList});

  factory _$_TestStorage.fromJson(Map<String, dynamic> json) =>
      _$$_TestStorageFromJson(json);

  @override
  final List<TestItem> testItemList;

  @override
  String toString() {
    return 'TestStorage(testItemList: $testItemList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TestStorage &&
            const DeepCollectionEquality()
                .equals(other.testItemList, testItemList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(testItemList));

  @JsonKey(ignore: true)
  @override
  _$TestStorageCopyWith<_TestStorage> get copyWith =>
      __$TestStorageCopyWithImpl<_TestStorage>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TestStorageToJson(this);
  }
}

abstract class _TestStorage implements TestStorage {
  factory _TestStorage({required List<TestItem> testItemList}) = _$_TestStorage;

  factory _TestStorage.fromJson(Map<String, dynamic> json) =
      _$_TestStorage.fromJson;

  @override
  List<TestItem> get testItemList;
  @override
  @JsonKey(ignore: true)
  _$TestStorageCopyWith<_TestStorage> get copyWith =>
      throw _privateConstructorUsedError;
}
