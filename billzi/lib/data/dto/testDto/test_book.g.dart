// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TestBook _$$_TestBookFromJson(Map<String, dynamic> json) => _$_TestBook(
      id: json['id'] as String,
      name: json['name'] as String,
      color: json['color'] as String,
      description: json['description'] as String? ?? '',
      isClosed: json['isClosed'] as bool? ?? false,
    );

Map<String, dynamic> _$$_TestBookToJson(_$_TestBook instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'color': instance.color,
      'description': instance.description,
      'isClosed': instance.isClosed,
    };
