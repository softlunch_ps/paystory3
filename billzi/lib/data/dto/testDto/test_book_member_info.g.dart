// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_book_member_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookMemberInfo _$$_BookMemberInfoFromJson(Map<String, dynamic> json) =>
    _$_BookMemberInfo(
      id: json['id'] as String,
      iconUnicode: json['iconUnicode'] as String,
      nickName: json['nickName'] as String,
      grade: $enumDecode(_$MemberGradeEnumMap, json['grade']),
      bookIds: (json['bookIds'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          [],
    );

Map<String, dynamic> _$$_BookMemberInfoToJson(_$_BookMemberInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'iconUnicode': instance.iconUnicode,
      'nickName': instance.nickName,
      'grade': _$MemberGradeEnumMap[instance.grade],
      'bookIds': instance.bookIds,
    };

const _$MemberGradeEnumMap = {
  MemberGrade.NONMEMBER: 'NONMEMBER',
  MemberGrade.MEMBER: 'MEMBER',
  MemberGrade.ADMIN: 'ADMIN',
  MemberGrade.MASTER: 'MASTER',
};
