import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/testDto/test_item.dart';

part 'test_storage.freezed.dart';
part 'test_storage.g.dart';

@freezed
class TestStorage with _$TestStorage {

  factory TestStorage({
    required List<TestItem> testItemList,
  }) = _TestStorage;

  factory TestStorage.fromJson(Map<String, dynamic> json) =>
      _$TestStorageFromJson(json);
}