import 'package:billzi/data/dto/model/payment_history_dto.dart';

class TransactionItemGroup {
  List<PaymentHistoryDto> groupList;
  DateTime dateTime;

  TransactionItemGroup(this.dateTime, this.groupList);

  double getMoneySum(String transactionTypeId) {
    double sum = 0;
    groupList.forEach((element) {
      if (element.transactionTypeId == transactionTypeId) {
        sum = sum + element.totalMoney;
      }
    });
    return sum;
  }
}