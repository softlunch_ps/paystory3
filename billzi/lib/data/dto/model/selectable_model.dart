class SelectableModel<T> {
  bool isSelected;
  final T data;

  SelectableModel(this.data, {this.isSelected = true});
}