// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_push.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppPush _$$_AppPushFromJson(Map<String, dynamic> json) => _$_AppPush(
      CHANNEL_NOTICE: json['CHANNEL_NOTICE'] as String,
      CHANNEL_LIKES: json['CHANNEL_LIKES'] as String,
      CHANNEL_COMMENT: json['CHANNEL_COMMENT'] as String,
      CHANNEL_FOLLOW: json['CHANNEL_FOLLOW'] as String,
    );

Map<String, dynamic> _$$_AppPushToJson(_$_AppPush instance) =>
    <String, dynamic>{
      'CHANNEL_NOTICE': instance.CHANNEL_NOTICE,
      'CHANNEL_LIKES': instance.CHANNEL_LIKES,
      'CHANNEL_COMMENT': instance.CHANNEL_COMMENT,
      'CHANNEL_FOLLOW': instance.CHANNEL_FOLLOW,
    };
