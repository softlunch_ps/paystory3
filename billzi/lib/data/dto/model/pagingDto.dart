import 'package:freezed_annotation/freezed_annotation.dart';

part 'pagingDto.freezed.dart';
part 'pagingDto.g.dart';

@freezed
class PagingDto with _$PagingDto {
  factory PagingDto({
    @Default(0) int offset,
    @Default(0) int limit,
    @Default(0) int nextOffset,
    @Default(0) int listSize,
    @Default(0) int totalCount,
  }) = _PagingDto;

  factory PagingDto.fromJson(Map<String, dynamic> json) =>
      _$PagingDtoFromJson(json);
}