import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'custom_category_dto.freezed.dart';
part 'custom_category_dto.g.dart';

@freezed
class CustomCategoryDto with _$CustomCategoryDto {
  factory CustomCategoryDto({
    @Default(false) bool isEditMode,
    CategoryItem? selectedCategory,
    required List<CategoryItem> baseCategoryList,
  }) = _CustomCategoryDto;

  factory CustomCategoryDto.fromJson(Map<String, dynamic> json) =>
      _$CustomCategoryDtoFromJson(json);
}