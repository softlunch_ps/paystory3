// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AccountBook _$$_AccountBookFromJson(Map<String, dynamic> json) =>
    _$_AccountBook(
      startDate: json['startDate'] as String?,
    );

Map<String, dynamic> _$$_AccountBookToJson(_$_AccountBook instance) =>
    <String, dynamic>{
      'startDate': instance.startDate,
    };
