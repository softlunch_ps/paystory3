// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'auth_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AuthDto _$AuthDtoFromJson(Map<String, dynamic> json) {
  return _AuthDto.fromJson(json);
}

/// @nodoc
class _$AuthDtoTearOff {
  const _$AuthDtoTearOff();

  _AuthDto call(
      {num? userSeq,
      String? userId,
      String? prtyUserId,
      String? userNickname = 'NotUsed',
      String? password = 'NotUsed',
      String? fcmClientToken = 'NotUsed',
      String? email,
      String? lastAuthDt,
      String? userProfileImgUrl,
      String? birthDate,
      String? phoneNum1,
      String? phoneNum2,
      String? authTypeCd = '0',
      String? token,
      String? expireDate,
      String? authorities}) {
    return _AuthDto(
      userSeq: userSeq,
      userId: userId,
      prtyUserId: prtyUserId,
      userNickname: userNickname,
      password: password,
      fcmClientToken: fcmClientToken,
      email: email,
      lastAuthDt: lastAuthDt,
      userProfileImgUrl: userProfileImgUrl,
      birthDate: birthDate,
      phoneNum1: phoneNum1,
      phoneNum2: phoneNum2,
      authTypeCd: authTypeCd,
      token: token,
      expireDate: expireDate,
      authorities: authorities,
    );
  }

  AuthDto fromJson(Map<String, Object?> json) {
    return AuthDto.fromJson(json);
  }
}

/// @nodoc
const $AuthDto = _$AuthDtoTearOff();

/// @nodoc
mixin _$AuthDto {
  num? get userSeq => throw _privateConstructorUsedError;
  String? get userId => throw _privateConstructorUsedError;
  String? get prtyUserId => throw _privateConstructorUsedError;
  String? get userNickname => throw _privateConstructorUsedError;
  String? get password => throw _privateConstructorUsedError;
  String? get fcmClientToken => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get lastAuthDt => throw _privateConstructorUsedError;
  String? get userProfileImgUrl => throw _privateConstructorUsedError;
  String? get birthDate => throw _privateConstructorUsedError;
  String? get phoneNum1 => throw _privateConstructorUsedError;
  String? get phoneNum2 => throw _privateConstructorUsedError;
  String? get authTypeCd => throw _privateConstructorUsedError;
  String? get token => throw _privateConstructorUsedError;
  String? get expireDate => throw _privateConstructorUsedError;
  String? get authorities => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthDtoCopyWith<AuthDto> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthDtoCopyWith<$Res> {
  factory $AuthDtoCopyWith(AuthDto value, $Res Function(AuthDto) then) =
      _$AuthDtoCopyWithImpl<$Res>;
  $Res call(
      {num? userSeq,
      String? userId,
      String? prtyUserId,
      String? userNickname,
      String? password,
      String? fcmClientToken,
      String? email,
      String? lastAuthDt,
      String? userProfileImgUrl,
      String? birthDate,
      String? phoneNum1,
      String? phoneNum2,
      String? authTypeCd,
      String? token,
      String? expireDate,
      String? authorities});
}

/// @nodoc
class _$AuthDtoCopyWithImpl<$Res> implements $AuthDtoCopyWith<$Res> {
  _$AuthDtoCopyWithImpl(this._value, this._then);

  final AuthDto _value;
  // ignore: unused_field
  final $Res Function(AuthDto) _then;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? userId = freezed,
    Object? prtyUserId = freezed,
    Object? userNickname = freezed,
    Object? password = freezed,
    Object? fcmClientToken = freezed,
    Object? email = freezed,
    Object? lastAuthDt = freezed,
    Object? userProfileImgUrl = freezed,
    Object? birthDate = freezed,
    Object? phoneNum1 = freezed,
    Object? phoneNum2 = freezed,
    Object? authTypeCd = freezed,
    Object? token = freezed,
    Object? expireDate = freezed,
    Object? authorities = freezed,
  }) {
    return _then(_value.copyWith(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String?,
      prtyUserId: prtyUserId == freezed
          ? _value.prtyUserId
          : prtyUserId // ignore: cast_nullable_to_non_nullable
              as String?,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      fcmClientToken: fcmClientToken == freezed
          ? _value.fcmClientToken
          : fcmClientToken // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      lastAuthDt: lastAuthDt == freezed
          ? _value.lastAuthDt
          : lastAuthDt // ignore: cast_nullable_to_non_nullable
              as String?,
      userProfileImgUrl: userProfileImgUrl == freezed
          ? _value.userProfileImgUrl
          : userProfileImgUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNum1: phoneNum1 == freezed
          ? _value.phoneNum1
          : phoneNum1 // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNum2: phoneNum2 == freezed
          ? _value.phoneNum2
          : phoneNum2 // ignore: cast_nullable_to_non_nullable
              as String?,
      authTypeCd: authTypeCd == freezed
          ? _value.authTypeCd
          : authTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      token: token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      expireDate: expireDate == freezed
          ? _value.expireDate
          : expireDate // ignore: cast_nullable_to_non_nullable
              as String?,
      authorities: authorities == freezed
          ? _value.authorities
          : authorities // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$AuthDtoCopyWith<$Res> implements $AuthDtoCopyWith<$Res> {
  factory _$AuthDtoCopyWith(_AuthDto value, $Res Function(_AuthDto) then) =
      __$AuthDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? userSeq,
      String? userId,
      String? prtyUserId,
      String? userNickname,
      String? password,
      String? fcmClientToken,
      String? email,
      String? lastAuthDt,
      String? userProfileImgUrl,
      String? birthDate,
      String? phoneNum1,
      String? phoneNum2,
      String? authTypeCd,
      String? token,
      String? expireDate,
      String? authorities});
}

/// @nodoc
class __$AuthDtoCopyWithImpl<$Res> extends _$AuthDtoCopyWithImpl<$Res>
    implements _$AuthDtoCopyWith<$Res> {
  __$AuthDtoCopyWithImpl(_AuthDto _value, $Res Function(_AuthDto) _then)
      : super(_value, (v) => _then(v as _AuthDto));

  @override
  _AuthDto get _value => super._value as _AuthDto;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? userId = freezed,
    Object? prtyUserId = freezed,
    Object? userNickname = freezed,
    Object? password = freezed,
    Object? fcmClientToken = freezed,
    Object? email = freezed,
    Object? lastAuthDt = freezed,
    Object? userProfileImgUrl = freezed,
    Object? birthDate = freezed,
    Object? phoneNum1 = freezed,
    Object? phoneNum2 = freezed,
    Object? authTypeCd = freezed,
    Object? token = freezed,
    Object? expireDate = freezed,
    Object? authorities = freezed,
  }) {
    return _then(_AuthDto(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String?,
      prtyUserId: prtyUserId == freezed
          ? _value.prtyUserId
          : prtyUserId // ignore: cast_nullable_to_non_nullable
              as String?,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      fcmClientToken: fcmClientToken == freezed
          ? _value.fcmClientToken
          : fcmClientToken // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      lastAuthDt: lastAuthDt == freezed
          ? _value.lastAuthDt
          : lastAuthDt // ignore: cast_nullable_to_non_nullable
              as String?,
      userProfileImgUrl: userProfileImgUrl == freezed
          ? _value.userProfileImgUrl
          : userProfileImgUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNum1: phoneNum1 == freezed
          ? _value.phoneNum1
          : phoneNum1 // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNum2: phoneNum2 == freezed
          ? _value.phoneNum2
          : phoneNum2 // ignore: cast_nullable_to_non_nullable
              as String?,
      authTypeCd: authTypeCd == freezed
          ? _value.authTypeCd
          : authTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      token: token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      expireDate: expireDate == freezed
          ? _value.expireDate
          : expireDate // ignore: cast_nullable_to_non_nullable
              as String?,
      authorities: authorities == freezed
          ? _value.authorities
          : authorities // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AuthDto implements _AuthDto {
  _$_AuthDto(
      {this.userSeq,
      this.userId,
      this.prtyUserId,
      this.userNickname = 'NotUsed',
      this.password = 'NotUsed',
      this.fcmClientToken = 'NotUsed',
      this.email,
      this.lastAuthDt,
      this.userProfileImgUrl,
      this.birthDate,
      this.phoneNum1,
      this.phoneNum2,
      this.authTypeCd = '0',
      this.token,
      this.expireDate,
      this.authorities});

  factory _$_AuthDto.fromJson(Map<String, dynamic> json) =>
      _$$_AuthDtoFromJson(json);

  @override
  final num? userSeq;
  @override
  final String? userId;
  @override
  final String? prtyUserId;
  @JsonKey(defaultValue: 'NotUsed')
  @override
  final String? userNickname;
  @JsonKey(defaultValue: 'NotUsed')
  @override
  final String? password;
  @JsonKey(defaultValue: 'NotUsed')
  @override
  final String? fcmClientToken;
  @override
  final String? email;
  @override
  final String? lastAuthDt;
  @override
  final String? userProfileImgUrl;
  @override
  final String? birthDate;
  @override
  final String? phoneNum1;
  @override
  final String? phoneNum2;
  @JsonKey(defaultValue: '0')
  @override
  final String? authTypeCd;
  @override
  final String? token;
  @override
  final String? expireDate;
  @override
  final String? authorities;

  @override
  String toString() {
    return 'AuthDto(userSeq: $userSeq, userId: $userId, prtyUserId: $prtyUserId, userNickname: $userNickname, password: $password, fcmClientToken: $fcmClientToken, email: $email, lastAuthDt: $lastAuthDt, userProfileImgUrl: $userProfileImgUrl, birthDate: $birthDate, phoneNum1: $phoneNum1, phoneNum2: $phoneNum2, authTypeCd: $authTypeCd, token: $token, expireDate: $expireDate, authorities: $authorities)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthDto &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.prtyUserId, prtyUserId) ||
                other.prtyUserId == prtyUserId) &&
            (identical(other.userNickname, userNickname) ||
                other.userNickname == userNickname) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.fcmClientToken, fcmClientToken) ||
                other.fcmClientToken == fcmClientToken) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.lastAuthDt, lastAuthDt) ||
                other.lastAuthDt == lastAuthDt) &&
            (identical(other.userProfileImgUrl, userProfileImgUrl) ||
                other.userProfileImgUrl == userProfileImgUrl) &&
            (identical(other.birthDate, birthDate) ||
                other.birthDate == birthDate) &&
            (identical(other.phoneNum1, phoneNum1) ||
                other.phoneNum1 == phoneNum1) &&
            (identical(other.phoneNum2, phoneNum2) ||
                other.phoneNum2 == phoneNum2) &&
            (identical(other.authTypeCd, authTypeCd) ||
                other.authTypeCd == authTypeCd) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.expireDate, expireDate) ||
                other.expireDate == expireDate) &&
            (identical(other.authorities, authorities) ||
                other.authorities == authorities));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      userSeq,
      userId,
      prtyUserId,
      userNickname,
      password,
      fcmClientToken,
      email,
      lastAuthDt,
      userProfileImgUrl,
      birthDate,
      phoneNum1,
      phoneNum2,
      authTypeCd,
      token,
      expireDate,
      authorities);

  @JsonKey(ignore: true)
  @override
  _$AuthDtoCopyWith<_AuthDto> get copyWith =>
      __$AuthDtoCopyWithImpl<_AuthDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AuthDtoToJson(this);
  }
}

abstract class _AuthDto implements AuthDto {
  factory _AuthDto(
      {num? userSeq,
      String? userId,
      String? prtyUserId,
      String? userNickname,
      String? password,
      String? fcmClientToken,
      String? email,
      String? lastAuthDt,
      String? userProfileImgUrl,
      String? birthDate,
      String? phoneNum1,
      String? phoneNum2,
      String? authTypeCd,
      String? token,
      String? expireDate,
      String? authorities}) = _$_AuthDto;

  factory _AuthDto.fromJson(Map<String, dynamic> json) = _$_AuthDto.fromJson;

  @override
  num? get userSeq;
  @override
  String? get userId;
  @override
  String? get prtyUserId;
  @override
  String? get userNickname;
  @override
  String? get password;
  @override
  String? get fcmClientToken;
  @override
  String? get email;
  @override
  String? get lastAuthDt;
  @override
  String? get userProfileImgUrl;
  @override
  String? get birthDate;
  @override
  String? get phoneNum1;
  @override
  String? get phoneNum2;
  @override
  String? get authTypeCd;
  @override
  String? get token;
  @override
  String? get expireDate;
  @override
  String? get authorities;
  @override
  @JsonKey(ignore: true)
  _$AuthDtoCopyWith<_AuthDto> get copyWith =>
      throw _privateConstructorUsedError;
}
