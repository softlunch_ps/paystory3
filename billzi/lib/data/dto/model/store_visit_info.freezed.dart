// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'store_visit_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StoreVisitInfo _$StoreVisitInfoFromJson(Map<String, dynamic> json) {
  return _StoreVisitInfo.fromJson(json);
}

/// @nodoc
class _$StoreVisitInfoTearOff {
  const _$StoreVisitInfoTearOff();

  _StoreVisitInfo call(
      {num? visitCount, String? firstVisitDt, String? recentVisitDt}) {
    return _StoreVisitInfo(
      visitCount: visitCount,
      firstVisitDt: firstVisitDt,
      recentVisitDt: recentVisitDt,
    );
  }

  StoreVisitInfo fromJson(Map<String, Object?> json) {
    return StoreVisitInfo.fromJson(json);
  }
}

/// @nodoc
const $StoreVisitInfo = _$StoreVisitInfoTearOff();

/// @nodoc
mixin _$StoreVisitInfo {
  num? get visitCount => throw _privateConstructorUsedError;
  String? get firstVisitDt => throw _privateConstructorUsedError;
  String? get recentVisitDt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StoreVisitInfoCopyWith<StoreVisitInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StoreVisitInfoCopyWith<$Res> {
  factory $StoreVisitInfoCopyWith(
          StoreVisitInfo value, $Res Function(StoreVisitInfo) then) =
      _$StoreVisitInfoCopyWithImpl<$Res>;
  $Res call({num? visitCount, String? firstVisitDt, String? recentVisitDt});
}

/// @nodoc
class _$StoreVisitInfoCopyWithImpl<$Res>
    implements $StoreVisitInfoCopyWith<$Res> {
  _$StoreVisitInfoCopyWithImpl(this._value, this._then);

  final StoreVisitInfo _value;
  // ignore: unused_field
  final $Res Function(StoreVisitInfo) _then;

  @override
  $Res call({
    Object? visitCount = freezed,
    Object? firstVisitDt = freezed,
    Object? recentVisitDt = freezed,
  }) {
    return _then(_value.copyWith(
      visitCount: visitCount == freezed
          ? _value.visitCount
          : visitCount // ignore: cast_nullable_to_non_nullable
              as num?,
      firstVisitDt: firstVisitDt == freezed
          ? _value.firstVisitDt
          : firstVisitDt // ignore: cast_nullable_to_non_nullable
              as String?,
      recentVisitDt: recentVisitDt == freezed
          ? _value.recentVisitDt
          : recentVisitDt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$StoreVisitInfoCopyWith<$Res>
    implements $StoreVisitInfoCopyWith<$Res> {
  factory _$StoreVisitInfoCopyWith(
          _StoreVisitInfo value, $Res Function(_StoreVisitInfo) then) =
      __$StoreVisitInfoCopyWithImpl<$Res>;
  @override
  $Res call({num? visitCount, String? firstVisitDt, String? recentVisitDt});
}

/// @nodoc
class __$StoreVisitInfoCopyWithImpl<$Res>
    extends _$StoreVisitInfoCopyWithImpl<$Res>
    implements _$StoreVisitInfoCopyWith<$Res> {
  __$StoreVisitInfoCopyWithImpl(
      _StoreVisitInfo _value, $Res Function(_StoreVisitInfo) _then)
      : super(_value, (v) => _then(v as _StoreVisitInfo));

  @override
  _StoreVisitInfo get _value => super._value as _StoreVisitInfo;

  @override
  $Res call({
    Object? visitCount = freezed,
    Object? firstVisitDt = freezed,
    Object? recentVisitDt = freezed,
  }) {
    return _then(_StoreVisitInfo(
      visitCount: visitCount == freezed
          ? _value.visitCount
          : visitCount // ignore: cast_nullable_to_non_nullable
              as num?,
      firstVisitDt: firstVisitDt == freezed
          ? _value.firstVisitDt
          : firstVisitDt // ignore: cast_nullable_to_non_nullable
              as String?,
      recentVisitDt: recentVisitDt == freezed
          ? _value.recentVisitDt
          : recentVisitDt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_StoreVisitInfo implements _StoreVisitInfo {
  _$_StoreVisitInfo({this.visitCount, this.firstVisitDt, this.recentVisitDt});

  factory _$_StoreVisitInfo.fromJson(Map<String, dynamic> json) =>
      _$$_StoreVisitInfoFromJson(json);

  @override
  final num? visitCount;
  @override
  final String? firstVisitDt;
  @override
  final String? recentVisitDt;

  @override
  String toString() {
    return 'StoreVisitInfo(visitCount: $visitCount, firstVisitDt: $firstVisitDt, recentVisitDt: $recentVisitDt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _StoreVisitInfo &&
            (identical(other.visitCount, visitCount) ||
                other.visitCount == visitCount) &&
            (identical(other.firstVisitDt, firstVisitDt) ||
                other.firstVisitDt == firstVisitDt) &&
            (identical(other.recentVisitDt, recentVisitDt) ||
                other.recentVisitDt == recentVisitDt));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, visitCount, firstVisitDt, recentVisitDt);

  @JsonKey(ignore: true)
  @override
  _$StoreVisitInfoCopyWith<_StoreVisitInfo> get copyWith =>
      __$StoreVisitInfoCopyWithImpl<_StoreVisitInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StoreVisitInfoToJson(this);
  }
}

abstract class _StoreVisitInfo implements StoreVisitInfo {
  factory _StoreVisitInfo(
      {num? visitCount,
      String? firstVisitDt,
      String? recentVisitDt}) = _$_StoreVisitInfo;

  factory _StoreVisitInfo.fromJson(Map<String, dynamic> json) =
      _$_StoreVisitInfo.fromJson;

  @override
  num? get visitCount;
  @override
  String? get firstVisitDt;
  @override
  String? get recentVisitDt;
  @override
  @JsonKey(ignore: true)
  _$StoreVisitInfoCopyWith<_StoreVisitInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
