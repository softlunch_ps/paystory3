import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_push.freezed.dart';
part 'app_push.g.dart';

@freezed
class AppPush with _$AppPush {
  factory AppPush({
    required String CHANNEL_NOTICE,
    required String CHANNEL_LIKES,
    required String CHANNEL_COMMENT,
    required String CHANNEL_FOLLOW,
  }) = _AppPush;

  factory AppPush.fromJson(Map<String, dynamic> json) =>
      _$AppPushFromJson(json);
}