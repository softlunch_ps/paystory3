import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_dto.freezed.dart';
part 'user_dto.g.dart';

@freezed
class UserDto with _$UserDto {
  factory UserDto({
    num? userSeq,
    @Default('NotUsed') String? userName,
    String? userId,
    String? prtyUserId,
    @Default('NotUsed') String? userNickname,
    @Default('NotUsed') String? password,
    @Default('NotUsed') String? fcmClientToken,
    String? delYN,
    String? crtDt,
    String? updtDt,
    @Default('') String? email,
    String? lastAuthDt,
    @Default('') String? userProfileImgUrl,
    @Default('') String? birthDate,
    int? ageMin,
    int? ageMax,
    @Default('') String? gender,
    @Default('') String? phoneNum1,
    @Default('') String? phoneNum2,
    @Default('') String? address,
    @Default('1') String? userTypeCd,
    @Default('0') String? authTypeCd,
    String? followCd,
    String? locked,
    String? adId,
  }) = _UserDto;

  factory UserDto.fromJson(Map<String, dynamic> json) =>
      _$UserDtoFromJson(json);
}
//
// "userId":"abas767676",
// "prtyUserId":"123123123123",
// "userNickname": "수시니",
// "userName":"이순신",
// "password":"q1w2e3",
// "userProfileImgUrl":"https://s3.ap-northeast-2.amazonaws.com/paystory-profiles/pic/luka.png",
// "birthDate":"19880101",
// "phoneNum1":"010111122225",
// "email":"abas761@samsung.com",
// "adId" : "xxxxxxxxxxxxxxxxxxxxxxx",
// "authTypeCd":"0",
// "userTypeCd":"1",
// "fcmClientToken":"aaaa"