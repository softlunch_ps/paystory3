// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store_visit_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StoreVisitInfo _$$_StoreVisitInfoFromJson(Map<String, dynamic> json) =>
    _$_StoreVisitInfo(
      visitCount: json['visitCount'] as num?,
      firstVisitDt: json['firstVisitDt'] as String?,
      recentVisitDt: json['recentVisitDt'] as String?,
    );

Map<String, dynamic> _$$_StoreVisitInfoToJson(_$_StoreVisitInfo instance) =>
    <String, dynamic>{
      'visitCount': instance.visitCount,
      'firstVisitDt': instance.firstVisitDt,
      'recentVisitDt': instance.recentVisitDt,
    };
