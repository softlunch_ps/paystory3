// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'category_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CategoryItem _$CategoryItemFromJson(Map<String, dynamic> json) {
  return _CategoryItem.fromJson(json);
}

/// @nodoc
class _$CategoryItemTearOff {
  const _$CategoryItemTearOff();

  _CategoryItem call(
      {num? accountCategorySeq,
      String? accountCategoryName,
      num? userSeq,
      num? ratio,
      num? totPayAmt,
      int? order,
      num? supAccountCategorySeq,
      String? color,
      String? typeCd,
      String? imgURL}) {
    return _CategoryItem(
      accountCategorySeq: accountCategorySeq,
      accountCategoryName: accountCategoryName,
      userSeq: userSeq,
      ratio: ratio,
      totPayAmt: totPayAmt,
      order: order,
      supAccountCategorySeq: supAccountCategorySeq,
      color: color,
      typeCd: typeCd,
      imgURL: imgURL,
    );
  }

  CategoryItem fromJson(Map<String, Object?> json) {
    return CategoryItem.fromJson(json);
  }
}

/// @nodoc
const $CategoryItem = _$CategoryItemTearOff();

/// @nodoc
mixin _$CategoryItem {
  num? get accountCategorySeq => throw _privateConstructorUsedError;
  String? get accountCategoryName => throw _privateConstructorUsedError;
  num? get userSeq => throw _privateConstructorUsedError;
  num? get ratio => throw _privateConstructorUsedError;
  num? get totPayAmt => throw _privateConstructorUsedError;
  int? get order => throw _privateConstructorUsedError;
  num? get supAccountCategorySeq => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  String? get typeCd => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CategoryItemCopyWith<CategoryItem> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoryItemCopyWith<$Res> {
  factory $CategoryItemCopyWith(
          CategoryItem value, $Res Function(CategoryItem) then) =
      _$CategoryItemCopyWithImpl<$Res>;
  $Res call(
      {num? accountCategorySeq,
      String? accountCategoryName,
      num? userSeq,
      num? ratio,
      num? totPayAmt,
      int? order,
      num? supAccountCategorySeq,
      String? color,
      String? typeCd,
      String? imgURL});
}

/// @nodoc
class _$CategoryItemCopyWithImpl<$Res> implements $CategoryItemCopyWith<$Res> {
  _$CategoryItemCopyWithImpl(this._value, this._then);

  final CategoryItem _value;
  // ignore: unused_field
  final $Res Function(CategoryItem) _then;

  @override
  $Res call({
    Object? accountCategorySeq = freezed,
    Object? accountCategoryName = freezed,
    Object? userSeq = freezed,
    Object? ratio = freezed,
    Object? totPayAmt = freezed,
    Object? order = freezed,
    Object? supAccountCategorySeq = freezed,
    Object? color = freezed,
    Object? typeCd = freezed,
    Object? imgURL = freezed,
  }) {
    return _then(_value.copyWith(
      accountCategorySeq: accountCategorySeq == freezed
          ? _value.accountCategorySeq
          : accountCategorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountCategoryName: accountCategoryName == freezed
          ? _value.accountCategoryName
          : accountCategoryName // ignore: cast_nullable_to_non_nullable
              as String?,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      ratio: ratio == freezed
          ? _value.ratio
          : ratio // ignore: cast_nullable_to_non_nullable
              as num?,
      totPayAmt: totPayAmt == freezed
          ? _value.totPayAmt
          : totPayAmt // ignore: cast_nullable_to_non_nullable
              as num?,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int?,
      supAccountCategorySeq: supAccountCategorySeq == freezed
          ? _value.supAccountCategorySeq
          : supAccountCategorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      typeCd: typeCd == freezed
          ? _value.typeCd
          : typeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$CategoryItemCopyWith<$Res>
    implements $CategoryItemCopyWith<$Res> {
  factory _$CategoryItemCopyWith(
          _CategoryItem value, $Res Function(_CategoryItem) then) =
      __$CategoryItemCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? accountCategorySeq,
      String? accountCategoryName,
      num? userSeq,
      num? ratio,
      num? totPayAmt,
      int? order,
      num? supAccountCategorySeq,
      String? color,
      String? typeCd,
      String? imgURL});
}

/// @nodoc
class __$CategoryItemCopyWithImpl<$Res> extends _$CategoryItemCopyWithImpl<$Res>
    implements _$CategoryItemCopyWith<$Res> {
  __$CategoryItemCopyWithImpl(
      _CategoryItem _value, $Res Function(_CategoryItem) _then)
      : super(_value, (v) => _then(v as _CategoryItem));

  @override
  _CategoryItem get _value => super._value as _CategoryItem;

  @override
  $Res call({
    Object? accountCategorySeq = freezed,
    Object? accountCategoryName = freezed,
    Object? userSeq = freezed,
    Object? ratio = freezed,
    Object? totPayAmt = freezed,
    Object? order = freezed,
    Object? supAccountCategorySeq = freezed,
    Object? color = freezed,
    Object? typeCd = freezed,
    Object? imgURL = freezed,
  }) {
    return _then(_CategoryItem(
      accountCategorySeq: accountCategorySeq == freezed
          ? _value.accountCategorySeq
          : accountCategorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountCategoryName: accountCategoryName == freezed
          ? _value.accountCategoryName
          : accountCategoryName // ignore: cast_nullable_to_non_nullable
              as String?,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      ratio: ratio == freezed
          ? _value.ratio
          : ratio // ignore: cast_nullable_to_non_nullable
              as num?,
      totPayAmt: totPayAmt == freezed
          ? _value.totPayAmt
          : totPayAmt // ignore: cast_nullable_to_non_nullable
              as num?,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int?,
      supAccountCategorySeq: supAccountCategorySeq == freezed
          ? _value.supAccountCategorySeq
          : supAccountCategorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      typeCd: typeCd == freezed
          ? _value.typeCd
          : typeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CategoryItem implements _CategoryItem {
  _$_CategoryItem(
      {this.accountCategorySeq,
      this.accountCategoryName,
      this.userSeq,
      this.ratio,
      this.totPayAmt,
      this.order,
      this.supAccountCategorySeq,
      this.color,
      this.typeCd,
      this.imgURL});

  factory _$_CategoryItem.fromJson(Map<String, dynamic> json) =>
      _$$_CategoryItemFromJson(json);

  @override
  final num? accountCategorySeq;
  @override
  final String? accountCategoryName;
  @override
  final num? userSeq;
  @override
  final num? ratio;
  @override
  final num? totPayAmt;
  @override
  final int? order;
  @override
  final num? supAccountCategorySeq;
  @override
  final String? color;
  @override
  final String? typeCd;
  @override
  final String? imgURL;

  @override
  String toString() {
    return 'CategoryItem(accountCategorySeq: $accountCategorySeq, accountCategoryName: $accountCategoryName, userSeq: $userSeq, ratio: $ratio, totPayAmt: $totPayAmt, order: $order, supAccountCategorySeq: $supAccountCategorySeq, color: $color, typeCd: $typeCd, imgURL: $imgURL)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CategoryItem &&
            (identical(other.accountCategorySeq, accountCategorySeq) ||
                other.accountCategorySeq == accountCategorySeq) &&
            (identical(other.accountCategoryName, accountCategoryName) ||
                other.accountCategoryName == accountCategoryName) &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.ratio, ratio) || other.ratio == ratio) &&
            (identical(other.totPayAmt, totPayAmt) ||
                other.totPayAmt == totPayAmt) &&
            (identical(other.order, order) || other.order == order) &&
            (identical(other.supAccountCategorySeq, supAccountCategorySeq) ||
                other.supAccountCategorySeq == supAccountCategorySeq) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.typeCd, typeCd) || other.typeCd == typeCd) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      accountCategorySeq,
      accountCategoryName,
      userSeq,
      ratio,
      totPayAmt,
      order,
      supAccountCategorySeq,
      color,
      typeCd,
      imgURL);

  @JsonKey(ignore: true)
  @override
  _$CategoryItemCopyWith<_CategoryItem> get copyWith =>
      __$CategoryItemCopyWithImpl<_CategoryItem>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CategoryItemToJson(this);
  }
}

abstract class _CategoryItem implements CategoryItem {
  factory _CategoryItem(
      {num? accountCategorySeq,
      String? accountCategoryName,
      num? userSeq,
      num? ratio,
      num? totPayAmt,
      int? order,
      num? supAccountCategorySeq,
      String? color,
      String? typeCd,
      String? imgURL}) = _$_CategoryItem;

  factory _CategoryItem.fromJson(Map<String, dynamic> json) =
      _$_CategoryItem.fromJson;

  @override
  num? get accountCategorySeq;
  @override
  String? get accountCategoryName;
  @override
  num? get userSeq;
  @override
  num? get ratio;
  @override
  num? get totPayAmt;
  @override
  int? get order;
  @override
  num? get supAccountCategorySeq;
  @override
  String? get color;
  @override
  String? get typeCd;
  @override
  String? get imgURL;
  @override
  @JsonKey(ignore: true)
  _$CategoryItemCopyWith<_CategoryItem> get copyWith =>
      throw _privateConstructorUsedError;
}
