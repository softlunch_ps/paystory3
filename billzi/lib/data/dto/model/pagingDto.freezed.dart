// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'pagingDto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PagingDto _$PagingDtoFromJson(Map<String, dynamic> json) {
  return _PagingDto.fromJson(json);
}

/// @nodoc
class _$PagingDtoTearOff {
  const _$PagingDtoTearOff();

  _PagingDto call(
      {int offset = 0,
      int limit = 0,
      int nextOffset = 0,
      int listSize = 0,
      int totalCount = 0}) {
    return _PagingDto(
      offset: offset,
      limit: limit,
      nextOffset: nextOffset,
      listSize: listSize,
      totalCount: totalCount,
    );
  }

  PagingDto fromJson(Map<String, Object?> json) {
    return PagingDto.fromJson(json);
  }
}

/// @nodoc
const $PagingDto = _$PagingDtoTearOff();

/// @nodoc
mixin _$PagingDto {
  int get offset => throw _privateConstructorUsedError;
  int get limit => throw _privateConstructorUsedError;
  int get nextOffset => throw _privateConstructorUsedError;
  int get listSize => throw _privateConstructorUsedError;
  int get totalCount => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PagingDtoCopyWith<PagingDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PagingDtoCopyWith<$Res> {
  factory $PagingDtoCopyWith(PagingDto value, $Res Function(PagingDto) then) =
      _$PagingDtoCopyWithImpl<$Res>;
  $Res call(
      {int offset, int limit, int nextOffset, int listSize, int totalCount});
}

/// @nodoc
class _$PagingDtoCopyWithImpl<$Res> implements $PagingDtoCopyWith<$Res> {
  _$PagingDtoCopyWithImpl(this._value, this._then);

  final PagingDto _value;
  // ignore: unused_field
  final $Res Function(PagingDto) _then;

  @override
  $Res call({
    Object? offset = freezed,
    Object? limit = freezed,
    Object? nextOffset = freezed,
    Object? listSize = freezed,
    Object? totalCount = freezed,
  }) {
    return _then(_value.copyWith(
      offset: offset == freezed
          ? _value.offset
          : offset // ignore: cast_nullable_to_non_nullable
              as int,
      limit: limit == freezed
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int,
      nextOffset: nextOffset == freezed
          ? _value.nextOffset
          : nextOffset // ignore: cast_nullable_to_non_nullable
              as int,
      listSize: listSize == freezed
          ? _value.listSize
          : listSize // ignore: cast_nullable_to_non_nullable
              as int,
      totalCount: totalCount == freezed
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$PagingDtoCopyWith<$Res> implements $PagingDtoCopyWith<$Res> {
  factory _$PagingDtoCopyWith(
          _PagingDto value, $Res Function(_PagingDto) then) =
      __$PagingDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {int offset, int limit, int nextOffset, int listSize, int totalCount});
}

/// @nodoc
class __$PagingDtoCopyWithImpl<$Res> extends _$PagingDtoCopyWithImpl<$Res>
    implements _$PagingDtoCopyWith<$Res> {
  __$PagingDtoCopyWithImpl(_PagingDto _value, $Res Function(_PagingDto) _then)
      : super(_value, (v) => _then(v as _PagingDto));

  @override
  _PagingDto get _value => super._value as _PagingDto;

  @override
  $Res call({
    Object? offset = freezed,
    Object? limit = freezed,
    Object? nextOffset = freezed,
    Object? listSize = freezed,
    Object? totalCount = freezed,
  }) {
    return _then(_PagingDto(
      offset: offset == freezed
          ? _value.offset
          : offset // ignore: cast_nullable_to_non_nullable
              as int,
      limit: limit == freezed
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int,
      nextOffset: nextOffset == freezed
          ? _value.nextOffset
          : nextOffset // ignore: cast_nullable_to_non_nullable
              as int,
      listSize: listSize == freezed
          ? _value.listSize
          : listSize // ignore: cast_nullable_to_non_nullable
              as int,
      totalCount: totalCount == freezed
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PagingDto implements _PagingDto {
  _$_PagingDto(
      {this.offset = 0,
      this.limit = 0,
      this.nextOffset = 0,
      this.listSize = 0,
      this.totalCount = 0});

  factory _$_PagingDto.fromJson(Map<String, dynamic> json) =>
      _$$_PagingDtoFromJson(json);

  @JsonKey(defaultValue: 0)
  @override
  final int offset;
  @JsonKey(defaultValue: 0)
  @override
  final int limit;
  @JsonKey(defaultValue: 0)
  @override
  final int nextOffset;
  @JsonKey(defaultValue: 0)
  @override
  final int listSize;
  @JsonKey(defaultValue: 0)
  @override
  final int totalCount;

  @override
  String toString() {
    return 'PagingDto(offset: $offset, limit: $limit, nextOffset: $nextOffset, listSize: $listSize, totalCount: $totalCount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _PagingDto &&
            (identical(other.offset, offset) || other.offset == offset) &&
            (identical(other.limit, limit) || other.limit == limit) &&
            (identical(other.nextOffset, nextOffset) ||
                other.nextOffset == nextOffset) &&
            (identical(other.listSize, listSize) ||
                other.listSize == listSize) &&
            (identical(other.totalCount, totalCount) ||
                other.totalCount == totalCount));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, offset, limit, nextOffset, listSize, totalCount);

  @JsonKey(ignore: true)
  @override
  _$PagingDtoCopyWith<_PagingDto> get copyWith =>
      __$PagingDtoCopyWithImpl<_PagingDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PagingDtoToJson(this);
  }
}

abstract class _PagingDto implements PagingDto {
  factory _PagingDto(
      {int offset,
      int limit,
      int nextOffset,
      int listSize,
      int totalCount}) = _$_PagingDto;

  factory _PagingDto.fromJson(Map<String, dynamic> json) =
      _$_PagingDto.fromJson;

  @override
  int get offset;
  @override
  int get limit;
  @override
  int get nextOffset;
  @override
  int get listSize;
  @override
  int get totalCount;
  @override
  @JsonKey(ignore: true)
  _$PagingDtoCopyWith<_PagingDto> get copyWith =>
      throw _privateConstructorUsedError;
}
