// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'payment_history_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PaymentHistoryDto _$PaymentHistoryDtoFromJson(Map<String, dynamic> json) {
  return _PaymentDtoHistory.fromJson(json);
}

/// @nodoc
class _$PaymentHistoryDtoTearOff {
  const _$PaymentHistoryDtoTearOff();

  _PaymentDtoHistory call(
      {@JsonKey(name: 'trstnDt') String? trstnDt,
      @JsonKey(name: 'totPayAmtOrg') double totalMoney = 0.0,
      @JsonKey(name: 'totPayAmt') double totalConvertedMoney = 0.0,
      @JsonKey(name: 'curMoneyCd') String? currencyCode,
      @JsonKey(name: 'crdIssueCmpnyCd') String? paymentTypeId,
      @JsonKey(name: 'trstnTypeCd') String? transactionTypeId,
      @JsonKey(name: 'memo') String? description = '',
      num? userSeq,
      num? userNickname,
      String? updtDt,
      num? payRcptSeq,
      String? title,
      double? accumPayAmt,
      double? ratio,
      String? trstnCtgryInfo,
      String? trstnNum,
      String? orgMSG,
      String? orgTrstnDT,
      double? feedSeq,
      String? payRcptScore,
      String? text,
      int? payCount,
      String? inputCd,
      String? typeCd,
      String? vat,
      String? storeNameRcpt,
      List<String>? payRcptSeqList,
      String? storeLinkCd,
      String? payRcptCd,
      String? productName,
      String? totPrdctPrc,
      StoreInfo? storeInfo,
      GpsInfo? gpsInfo,
      CategoryItem? acntCateInfo,
      String? delYN,
      String? crdIssueCmpnyName,
      String? crdIssueCmpnyTel,
      String? appVersion,
      String? osVersion,
      String? deviceModel,
      int? crdSmsRawSeq,
      String? parserVersion,
      String? crdName,
      String? crdNum,
      String? trstnLocCd,
      String? trstnType,
      String? crdOwnerTypeCd,
      String? crdOwnerName,
      String? installment,
      String? accumType,
      String? accumTypeCd}) {
    return _PaymentDtoHistory(
      trstnDt: trstnDt,
      totalMoney: totalMoney,
      totalConvertedMoney: totalConvertedMoney,
      currencyCode: currencyCode,
      paymentTypeId: paymentTypeId,
      transactionTypeId: transactionTypeId,
      description: description,
      userSeq: userSeq,
      userNickname: userNickname,
      updtDt: updtDt,
      payRcptSeq: payRcptSeq,
      title: title,
      accumPayAmt: accumPayAmt,
      ratio: ratio,
      trstnCtgryInfo: trstnCtgryInfo,
      trstnNum: trstnNum,
      orgMSG: orgMSG,
      orgTrstnDT: orgTrstnDT,
      feedSeq: feedSeq,
      payRcptScore: payRcptScore,
      text: text,
      payCount: payCount,
      inputCd: inputCd,
      typeCd: typeCd,
      vat: vat,
      storeNameRcpt: storeNameRcpt,
      payRcptSeqList: payRcptSeqList,
      storeLinkCd: storeLinkCd,
      payRcptCd: payRcptCd,
      productName: productName,
      totPrdctPrc: totPrdctPrc,
      storeInfo: storeInfo,
      gpsInfo: gpsInfo,
      acntCateInfo: acntCateInfo,
      delYN: delYN,
      crdIssueCmpnyName: crdIssueCmpnyName,
      crdIssueCmpnyTel: crdIssueCmpnyTel,
      appVersion: appVersion,
      osVersion: osVersion,
      deviceModel: deviceModel,
      crdSmsRawSeq: crdSmsRawSeq,
      parserVersion: parserVersion,
      crdName: crdName,
      crdNum: crdNum,
      trstnLocCd: trstnLocCd,
      trstnType: trstnType,
      crdOwnerTypeCd: crdOwnerTypeCd,
      crdOwnerName: crdOwnerName,
      installment: installment,
      accumType: accumType,
      accumTypeCd: accumTypeCd,
    );
  }

  PaymentHistoryDto fromJson(Map<String, Object?> json) {
    return PaymentHistoryDto.fromJson(json);
  }
}

/// @nodoc
const $PaymentHistoryDto = _$PaymentHistoryDtoTearOff();

/// @nodoc
mixin _$PaymentHistoryDto {
  @JsonKey(name: 'trstnDt')
  String? get trstnDt => throw _privateConstructorUsedError;
  @JsonKey(name: 'totPayAmtOrg')
  double get totalMoney => throw _privateConstructorUsedError;
  @JsonKey(name: 'totPayAmt')
  double get totalConvertedMoney => throw _privateConstructorUsedError;
  @JsonKey(name: 'curMoneyCd')
  String? get currencyCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'crdIssueCmpnyCd')
  String? get paymentTypeId => throw _privateConstructorUsedError;
  @JsonKey(name: 'trstnTypeCd')
  String? get transactionTypeId => throw _privateConstructorUsedError;
  @JsonKey(name: 'memo')
  String? get description => throw _privateConstructorUsedError;
  num? get userSeq => throw _privateConstructorUsedError; //
  num? get userNickname => throw _privateConstructorUsedError; //
//String? crtDt, //
  String? get updtDt => throw _privateConstructorUsedError; //
  num? get payRcptSeq => throw _privateConstructorUsedError; //
  String? get title => throw _privateConstructorUsedError; //
//double? totPayAmt, //
  double? get accumPayAmt => throw _privateConstructorUsedError; //
//double? aprvlNum, //
  double? get ratio => throw _privateConstructorUsedError; //
//String? trstnDt, //
  String? get trstnCtgryInfo => throw _privateConstructorUsedError; //
  String? get trstnNum => throw _privateConstructorUsedError; //
  String? get orgMSG => throw _privateConstructorUsedError; //
  String? get orgTrstnDT => throw _privateConstructorUsedError; //
//String? curMoneyCd, //
  double? get feedSeq => throw _privateConstructorUsedError; //
  String? get payRcptScore => throw _privateConstructorUsedError; //
//String? memo, //
  String? get text => throw _privateConstructorUsedError; //
  int? get payCount => throw _privateConstructorUsedError; //
  String? get inputCd => throw _privateConstructorUsedError; //
  String? get typeCd => throw _privateConstructorUsedError; //
  String? get vat => throw _privateConstructorUsedError; //
  String? get storeNameRcpt => throw _privateConstructorUsedError; //
  List<String>? get payRcptSeqList => throw _privateConstructorUsedError; //
  String? get storeLinkCd => throw _privateConstructorUsedError;
  String? get payRcptCd => throw _privateConstructorUsedError; //
  String? get productName => throw _privateConstructorUsedError; //
  String? get totPrdctPrc => throw _privateConstructorUsedError; //
  StoreInfo? get storeInfo => throw _privateConstructorUsedError; //
  GpsInfo? get gpsInfo => throw _privateConstructorUsedError; //
  CategoryItem? get acntCateInfo => throw _privateConstructorUsedError; //
  String? get delYN => throw _privateConstructorUsedError; //
  String? get crdIssueCmpnyName => throw _privateConstructorUsedError; //
//String? crdIssueCmpnyCd, //
  String? get crdIssueCmpnyTel => throw _privateConstructorUsedError; //
  String? get appVersion => throw _privateConstructorUsedError; //
  String? get osVersion => throw _privateConstructorUsedError; //
  String? get deviceModel => throw _privateConstructorUsedError; //
  int? get crdSmsRawSeq => throw _privateConstructorUsedError; //
  String? get parserVersion => throw _privateConstructorUsedError; //
//String? totPayAmtOrg, //
  String? get crdName => throw _privateConstructorUsedError; //
  String? get crdNum => throw _privateConstructorUsedError; //
  String? get trstnLocCd => throw _privateConstructorUsedError; //
  String? get trstnType => throw _privateConstructorUsedError; //
//String? trstnTypeCd, //
  String? get crdOwnerTypeCd => throw _privateConstructorUsedError; //
  String? get crdOwnerName => throw _privateConstructorUsedError; //
  String? get installment => throw _privateConstructorUsedError; //
  String? get accumType => throw _privateConstructorUsedError; //
  String? get accumTypeCd => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaymentHistoryDtoCopyWith<PaymentHistoryDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaymentHistoryDtoCopyWith<$Res> {
  factory $PaymentHistoryDtoCopyWith(
          PaymentHistoryDto value, $Res Function(PaymentHistoryDto) then) =
      _$PaymentHistoryDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'trstnDt') String? trstnDt,
      @JsonKey(name: 'totPayAmtOrg') double totalMoney,
      @JsonKey(name: 'totPayAmt') double totalConvertedMoney,
      @JsonKey(name: 'curMoneyCd') String? currencyCode,
      @JsonKey(name: 'crdIssueCmpnyCd') String? paymentTypeId,
      @JsonKey(name: 'trstnTypeCd') String? transactionTypeId,
      @JsonKey(name: 'memo') String? description,
      num? userSeq,
      num? userNickname,
      String? updtDt,
      num? payRcptSeq,
      String? title,
      double? accumPayAmt,
      double? ratio,
      String? trstnCtgryInfo,
      String? trstnNum,
      String? orgMSG,
      String? orgTrstnDT,
      double? feedSeq,
      String? payRcptScore,
      String? text,
      int? payCount,
      String? inputCd,
      String? typeCd,
      String? vat,
      String? storeNameRcpt,
      List<String>? payRcptSeqList,
      String? storeLinkCd,
      String? payRcptCd,
      String? productName,
      String? totPrdctPrc,
      StoreInfo? storeInfo,
      GpsInfo? gpsInfo,
      CategoryItem? acntCateInfo,
      String? delYN,
      String? crdIssueCmpnyName,
      String? crdIssueCmpnyTel,
      String? appVersion,
      String? osVersion,
      String? deviceModel,
      int? crdSmsRawSeq,
      String? parserVersion,
      String? crdName,
      String? crdNum,
      String? trstnLocCd,
      String? trstnType,
      String? crdOwnerTypeCd,
      String? crdOwnerName,
      String? installment,
      String? accumType,
      String? accumTypeCd});

  $StoreInfoCopyWith<$Res>? get storeInfo;
  $GpsInfoCopyWith<$Res>? get gpsInfo;
  $CategoryItemCopyWith<$Res>? get acntCateInfo;
}

/// @nodoc
class _$PaymentHistoryDtoCopyWithImpl<$Res>
    implements $PaymentHistoryDtoCopyWith<$Res> {
  _$PaymentHistoryDtoCopyWithImpl(this._value, this._then);

  final PaymentHistoryDto _value;
  // ignore: unused_field
  final $Res Function(PaymentHistoryDto) _then;

  @override
  $Res call({
    Object? trstnDt = freezed,
    Object? totalMoney = freezed,
    Object? totalConvertedMoney = freezed,
    Object? currencyCode = freezed,
    Object? paymentTypeId = freezed,
    Object? transactionTypeId = freezed,
    Object? description = freezed,
    Object? userSeq = freezed,
    Object? userNickname = freezed,
    Object? updtDt = freezed,
    Object? payRcptSeq = freezed,
    Object? title = freezed,
    Object? accumPayAmt = freezed,
    Object? ratio = freezed,
    Object? trstnCtgryInfo = freezed,
    Object? trstnNum = freezed,
    Object? orgMSG = freezed,
    Object? orgTrstnDT = freezed,
    Object? feedSeq = freezed,
    Object? payRcptScore = freezed,
    Object? text = freezed,
    Object? payCount = freezed,
    Object? inputCd = freezed,
    Object? typeCd = freezed,
    Object? vat = freezed,
    Object? storeNameRcpt = freezed,
    Object? payRcptSeqList = freezed,
    Object? storeLinkCd = freezed,
    Object? payRcptCd = freezed,
    Object? productName = freezed,
    Object? totPrdctPrc = freezed,
    Object? storeInfo = freezed,
    Object? gpsInfo = freezed,
    Object? acntCateInfo = freezed,
    Object? delYN = freezed,
    Object? crdIssueCmpnyName = freezed,
    Object? crdIssueCmpnyTel = freezed,
    Object? appVersion = freezed,
    Object? osVersion = freezed,
    Object? deviceModel = freezed,
    Object? crdSmsRawSeq = freezed,
    Object? parserVersion = freezed,
    Object? crdName = freezed,
    Object? crdNum = freezed,
    Object? trstnLocCd = freezed,
    Object? trstnType = freezed,
    Object? crdOwnerTypeCd = freezed,
    Object? crdOwnerName = freezed,
    Object? installment = freezed,
    Object? accumType = freezed,
    Object? accumTypeCd = freezed,
  }) {
    return _then(_value.copyWith(
      trstnDt: trstnDt == freezed
          ? _value.trstnDt
          : trstnDt // ignore: cast_nullable_to_non_nullable
              as String?,
      totalMoney: totalMoney == freezed
          ? _value.totalMoney
          : totalMoney // ignore: cast_nullable_to_non_nullable
              as double,
      totalConvertedMoney: totalConvertedMoney == freezed
          ? _value.totalConvertedMoney
          : totalConvertedMoney // ignore: cast_nullable_to_non_nullable
              as double,
      currencyCode: currencyCode == freezed
          ? _value.currencyCode
          : currencyCode // ignore: cast_nullable_to_non_nullable
              as String?,
      paymentTypeId: paymentTypeId == freezed
          ? _value.paymentTypeId
          : paymentTypeId // ignore: cast_nullable_to_non_nullable
              as String?,
      transactionTypeId: transactionTypeId == freezed
          ? _value.transactionTypeId
          : transactionTypeId // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as num?,
      updtDt: updtDt == freezed
          ? _value.updtDt
          : updtDt // ignore: cast_nullable_to_non_nullable
              as String?,
      payRcptSeq: payRcptSeq == freezed
          ? _value.payRcptSeq
          : payRcptSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      accumPayAmt: accumPayAmt == freezed
          ? _value.accumPayAmt
          : accumPayAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      ratio: ratio == freezed
          ? _value.ratio
          : ratio // ignore: cast_nullable_to_non_nullable
              as double?,
      trstnCtgryInfo: trstnCtgryInfo == freezed
          ? _value.trstnCtgryInfo
          : trstnCtgryInfo // ignore: cast_nullable_to_non_nullable
              as String?,
      trstnNum: trstnNum == freezed
          ? _value.trstnNum
          : trstnNum // ignore: cast_nullable_to_non_nullable
              as String?,
      orgMSG: orgMSG == freezed
          ? _value.orgMSG
          : orgMSG // ignore: cast_nullable_to_non_nullable
              as String?,
      orgTrstnDT: orgTrstnDT == freezed
          ? _value.orgTrstnDT
          : orgTrstnDT // ignore: cast_nullable_to_non_nullable
              as String?,
      feedSeq: feedSeq == freezed
          ? _value.feedSeq
          : feedSeq // ignore: cast_nullable_to_non_nullable
              as double?,
      payRcptScore: payRcptScore == freezed
          ? _value.payRcptScore
          : payRcptScore // ignore: cast_nullable_to_non_nullable
              as String?,
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      payCount: payCount == freezed
          ? _value.payCount
          : payCount // ignore: cast_nullable_to_non_nullable
              as int?,
      inputCd: inputCd == freezed
          ? _value.inputCd
          : inputCd // ignore: cast_nullable_to_non_nullable
              as String?,
      typeCd: typeCd == freezed
          ? _value.typeCd
          : typeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      vat: vat == freezed
          ? _value.vat
          : vat // ignore: cast_nullable_to_non_nullable
              as String?,
      storeNameRcpt: storeNameRcpt == freezed
          ? _value.storeNameRcpt
          : storeNameRcpt // ignore: cast_nullable_to_non_nullable
              as String?,
      payRcptSeqList: payRcptSeqList == freezed
          ? _value.payRcptSeqList
          : payRcptSeqList // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      storeLinkCd: storeLinkCd == freezed
          ? _value.storeLinkCd
          : storeLinkCd // ignore: cast_nullable_to_non_nullable
              as String?,
      payRcptCd: payRcptCd == freezed
          ? _value.payRcptCd
          : payRcptCd // ignore: cast_nullable_to_non_nullable
              as String?,
      productName: productName == freezed
          ? _value.productName
          : productName // ignore: cast_nullable_to_non_nullable
              as String?,
      totPrdctPrc: totPrdctPrc == freezed
          ? _value.totPrdctPrc
          : totPrdctPrc // ignore: cast_nullable_to_non_nullable
              as String?,
      storeInfo: storeInfo == freezed
          ? _value.storeInfo
          : storeInfo // ignore: cast_nullable_to_non_nullable
              as StoreInfo?,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
      acntCateInfo: acntCateInfo == freezed
          ? _value.acntCateInfo
          : acntCateInfo // ignore: cast_nullable_to_non_nullable
              as CategoryItem?,
      delYN: delYN == freezed
          ? _value.delYN
          : delYN // ignore: cast_nullable_to_non_nullable
              as String?,
      crdIssueCmpnyName: crdIssueCmpnyName == freezed
          ? _value.crdIssueCmpnyName
          : crdIssueCmpnyName // ignore: cast_nullable_to_non_nullable
              as String?,
      crdIssueCmpnyTel: crdIssueCmpnyTel == freezed
          ? _value.crdIssueCmpnyTel
          : crdIssueCmpnyTel // ignore: cast_nullable_to_non_nullable
              as String?,
      appVersion: appVersion == freezed
          ? _value.appVersion
          : appVersion // ignore: cast_nullable_to_non_nullable
              as String?,
      osVersion: osVersion == freezed
          ? _value.osVersion
          : osVersion // ignore: cast_nullable_to_non_nullable
              as String?,
      deviceModel: deviceModel == freezed
          ? _value.deviceModel
          : deviceModel // ignore: cast_nullable_to_non_nullable
              as String?,
      crdSmsRawSeq: crdSmsRawSeq == freezed
          ? _value.crdSmsRawSeq
          : crdSmsRawSeq // ignore: cast_nullable_to_non_nullable
              as int?,
      parserVersion: parserVersion == freezed
          ? _value.parserVersion
          : parserVersion // ignore: cast_nullable_to_non_nullable
              as String?,
      crdName: crdName == freezed
          ? _value.crdName
          : crdName // ignore: cast_nullable_to_non_nullable
              as String?,
      crdNum: crdNum == freezed
          ? _value.crdNum
          : crdNum // ignore: cast_nullable_to_non_nullable
              as String?,
      trstnLocCd: trstnLocCd == freezed
          ? _value.trstnLocCd
          : trstnLocCd // ignore: cast_nullable_to_non_nullable
              as String?,
      trstnType: trstnType == freezed
          ? _value.trstnType
          : trstnType // ignore: cast_nullable_to_non_nullable
              as String?,
      crdOwnerTypeCd: crdOwnerTypeCd == freezed
          ? _value.crdOwnerTypeCd
          : crdOwnerTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      crdOwnerName: crdOwnerName == freezed
          ? _value.crdOwnerName
          : crdOwnerName // ignore: cast_nullable_to_non_nullable
              as String?,
      installment: installment == freezed
          ? _value.installment
          : installment // ignore: cast_nullable_to_non_nullable
              as String?,
      accumType: accumType == freezed
          ? _value.accumType
          : accumType // ignore: cast_nullable_to_non_nullable
              as String?,
      accumTypeCd: accumTypeCd == freezed
          ? _value.accumTypeCd
          : accumTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $StoreInfoCopyWith<$Res>? get storeInfo {
    if (_value.storeInfo == null) {
      return null;
    }

    return $StoreInfoCopyWith<$Res>(_value.storeInfo!, (value) {
      return _then(_value.copyWith(storeInfo: value));
    });
  }

  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo {
    if (_value.gpsInfo == null) {
      return null;
    }

    return $GpsInfoCopyWith<$Res>(_value.gpsInfo!, (value) {
      return _then(_value.copyWith(gpsInfo: value));
    });
  }

  @override
  $CategoryItemCopyWith<$Res>? get acntCateInfo {
    if (_value.acntCateInfo == null) {
      return null;
    }

    return $CategoryItemCopyWith<$Res>(_value.acntCateInfo!, (value) {
      return _then(_value.copyWith(acntCateInfo: value));
    });
  }
}

/// @nodoc
abstract class _$PaymentDtoHistoryCopyWith<$Res>
    implements $PaymentHistoryDtoCopyWith<$Res> {
  factory _$PaymentDtoHistoryCopyWith(
          _PaymentDtoHistory value, $Res Function(_PaymentDtoHistory) then) =
      __$PaymentDtoHistoryCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'trstnDt') String? trstnDt,
      @JsonKey(name: 'totPayAmtOrg') double totalMoney,
      @JsonKey(name: 'totPayAmt') double totalConvertedMoney,
      @JsonKey(name: 'curMoneyCd') String? currencyCode,
      @JsonKey(name: 'crdIssueCmpnyCd') String? paymentTypeId,
      @JsonKey(name: 'trstnTypeCd') String? transactionTypeId,
      @JsonKey(name: 'memo') String? description,
      num? userSeq,
      num? userNickname,
      String? updtDt,
      num? payRcptSeq,
      String? title,
      double? accumPayAmt,
      double? ratio,
      String? trstnCtgryInfo,
      String? trstnNum,
      String? orgMSG,
      String? orgTrstnDT,
      double? feedSeq,
      String? payRcptScore,
      String? text,
      int? payCount,
      String? inputCd,
      String? typeCd,
      String? vat,
      String? storeNameRcpt,
      List<String>? payRcptSeqList,
      String? storeLinkCd,
      String? payRcptCd,
      String? productName,
      String? totPrdctPrc,
      StoreInfo? storeInfo,
      GpsInfo? gpsInfo,
      CategoryItem? acntCateInfo,
      String? delYN,
      String? crdIssueCmpnyName,
      String? crdIssueCmpnyTel,
      String? appVersion,
      String? osVersion,
      String? deviceModel,
      int? crdSmsRawSeq,
      String? parserVersion,
      String? crdName,
      String? crdNum,
      String? trstnLocCd,
      String? trstnType,
      String? crdOwnerTypeCd,
      String? crdOwnerName,
      String? installment,
      String? accumType,
      String? accumTypeCd});

  @override
  $StoreInfoCopyWith<$Res>? get storeInfo;
  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo;
  @override
  $CategoryItemCopyWith<$Res>? get acntCateInfo;
}

/// @nodoc
class __$PaymentDtoHistoryCopyWithImpl<$Res>
    extends _$PaymentHistoryDtoCopyWithImpl<$Res>
    implements _$PaymentDtoHistoryCopyWith<$Res> {
  __$PaymentDtoHistoryCopyWithImpl(
      _PaymentDtoHistory _value, $Res Function(_PaymentDtoHistory) _then)
      : super(_value, (v) => _then(v as _PaymentDtoHistory));

  @override
  _PaymentDtoHistory get _value => super._value as _PaymentDtoHistory;

  @override
  $Res call({
    Object? trstnDt = freezed,
    Object? totalMoney = freezed,
    Object? totalConvertedMoney = freezed,
    Object? currencyCode = freezed,
    Object? paymentTypeId = freezed,
    Object? transactionTypeId = freezed,
    Object? description = freezed,
    Object? userSeq = freezed,
    Object? userNickname = freezed,
    Object? updtDt = freezed,
    Object? payRcptSeq = freezed,
    Object? title = freezed,
    Object? accumPayAmt = freezed,
    Object? ratio = freezed,
    Object? trstnCtgryInfo = freezed,
    Object? trstnNum = freezed,
    Object? orgMSG = freezed,
    Object? orgTrstnDT = freezed,
    Object? feedSeq = freezed,
    Object? payRcptScore = freezed,
    Object? text = freezed,
    Object? payCount = freezed,
    Object? inputCd = freezed,
    Object? typeCd = freezed,
    Object? vat = freezed,
    Object? storeNameRcpt = freezed,
    Object? payRcptSeqList = freezed,
    Object? storeLinkCd = freezed,
    Object? payRcptCd = freezed,
    Object? productName = freezed,
    Object? totPrdctPrc = freezed,
    Object? storeInfo = freezed,
    Object? gpsInfo = freezed,
    Object? acntCateInfo = freezed,
    Object? delYN = freezed,
    Object? crdIssueCmpnyName = freezed,
    Object? crdIssueCmpnyTel = freezed,
    Object? appVersion = freezed,
    Object? osVersion = freezed,
    Object? deviceModel = freezed,
    Object? crdSmsRawSeq = freezed,
    Object? parserVersion = freezed,
    Object? crdName = freezed,
    Object? crdNum = freezed,
    Object? trstnLocCd = freezed,
    Object? trstnType = freezed,
    Object? crdOwnerTypeCd = freezed,
    Object? crdOwnerName = freezed,
    Object? installment = freezed,
    Object? accumType = freezed,
    Object? accumTypeCd = freezed,
  }) {
    return _then(_PaymentDtoHistory(
      trstnDt: trstnDt == freezed
          ? _value.trstnDt
          : trstnDt // ignore: cast_nullable_to_non_nullable
              as String?,
      totalMoney: totalMoney == freezed
          ? _value.totalMoney
          : totalMoney // ignore: cast_nullable_to_non_nullable
              as double,
      totalConvertedMoney: totalConvertedMoney == freezed
          ? _value.totalConvertedMoney
          : totalConvertedMoney // ignore: cast_nullable_to_non_nullable
              as double,
      currencyCode: currencyCode == freezed
          ? _value.currencyCode
          : currencyCode // ignore: cast_nullable_to_non_nullable
              as String?,
      paymentTypeId: paymentTypeId == freezed
          ? _value.paymentTypeId
          : paymentTypeId // ignore: cast_nullable_to_non_nullable
              as String?,
      transactionTypeId: transactionTypeId == freezed
          ? _value.transactionTypeId
          : transactionTypeId // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as num?,
      updtDt: updtDt == freezed
          ? _value.updtDt
          : updtDt // ignore: cast_nullable_to_non_nullable
              as String?,
      payRcptSeq: payRcptSeq == freezed
          ? _value.payRcptSeq
          : payRcptSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      accumPayAmt: accumPayAmt == freezed
          ? _value.accumPayAmt
          : accumPayAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      ratio: ratio == freezed
          ? _value.ratio
          : ratio // ignore: cast_nullable_to_non_nullable
              as double?,
      trstnCtgryInfo: trstnCtgryInfo == freezed
          ? _value.trstnCtgryInfo
          : trstnCtgryInfo // ignore: cast_nullable_to_non_nullable
              as String?,
      trstnNum: trstnNum == freezed
          ? _value.trstnNum
          : trstnNum // ignore: cast_nullable_to_non_nullable
              as String?,
      orgMSG: orgMSG == freezed
          ? _value.orgMSG
          : orgMSG // ignore: cast_nullable_to_non_nullable
              as String?,
      orgTrstnDT: orgTrstnDT == freezed
          ? _value.orgTrstnDT
          : orgTrstnDT // ignore: cast_nullable_to_non_nullable
              as String?,
      feedSeq: feedSeq == freezed
          ? _value.feedSeq
          : feedSeq // ignore: cast_nullable_to_non_nullable
              as double?,
      payRcptScore: payRcptScore == freezed
          ? _value.payRcptScore
          : payRcptScore // ignore: cast_nullable_to_non_nullable
              as String?,
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      payCount: payCount == freezed
          ? _value.payCount
          : payCount // ignore: cast_nullable_to_non_nullable
              as int?,
      inputCd: inputCd == freezed
          ? _value.inputCd
          : inputCd // ignore: cast_nullable_to_non_nullable
              as String?,
      typeCd: typeCd == freezed
          ? _value.typeCd
          : typeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      vat: vat == freezed
          ? _value.vat
          : vat // ignore: cast_nullable_to_non_nullable
              as String?,
      storeNameRcpt: storeNameRcpt == freezed
          ? _value.storeNameRcpt
          : storeNameRcpt // ignore: cast_nullable_to_non_nullable
              as String?,
      payRcptSeqList: payRcptSeqList == freezed
          ? _value.payRcptSeqList
          : payRcptSeqList // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      storeLinkCd: storeLinkCd == freezed
          ? _value.storeLinkCd
          : storeLinkCd // ignore: cast_nullable_to_non_nullable
              as String?,
      payRcptCd: payRcptCd == freezed
          ? _value.payRcptCd
          : payRcptCd // ignore: cast_nullable_to_non_nullable
              as String?,
      productName: productName == freezed
          ? _value.productName
          : productName // ignore: cast_nullable_to_non_nullable
              as String?,
      totPrdctPrc: totPrdctPrc == freezed
          ? _value.totPrdctPrc
          : totPrdctPrc // ignore: cast_nullable_to_non_nullable
              as String?,
      storeInfo: storeInfo == freezed
          ? _value.storeInfo
          : storeInfo // ignore: cast_nullable_to_non_nullable
              as StoreInfo?,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
      acntCateInfo: acntCateInfo == freezed
          ? _value.acntCateInfo
          : acntCateInfo // ignore: cast_nullable_to_non_nullable
              as CategoryItem?,
      delYN: delYN == freezed
          ? _value.delYN
          : delYN // ignore: cast_nullable_to_non_nullable
              as String?,
      crdIssueCmpnyName: crdIssueCmpnyName == freezed
          ? _value.crdIssueCmpnyName
          : crdIssueCmpnyName // ignore: cast_nullable_to_non_nullable
              as String?,
      crdIssueCmpnyTel: crdIssueCmpnyTel == freezed
          ? _value.crdIssueCmpnyTel
          : crdIssueCmpnyTel // ignore: cast_nullable_to_non_nullable
              as String?,
      appVersion: appVersion == freezed
          ? _value.appVersion
          : appVersion // ignore: cast_nullable_to_non_nullable
              as String?,
      osVersion: osVersion == freezed
          ? _value.osVersion
          : osVersion // ignore: cast_nullable_to_non_nullable
              as String?,
      deviceModel: deviceModel == freezed
          ? _value.deviceModel
          : deviceModel // ignore: cast_nullable_to_non_nullable
              as String?,
      crdSmsRawSeq: crdSmsRawSeq == freezed
          ? _value.crdSmsRawSeq
          : crdSmsRawSeq // ignore: cast_nullable_to_non_nullable
              as int?,
      parserVersion: parserVersion == freezed
          ? _value.parserVersion
          : parserVersion // ignore: cast_nullable_to_non_nullable
              as String?,
      crdName: crdName == freezed
          ? _value.crdName
          : crdName // ignore: cast_nullable_to_non_nullable
              as String?,
      crdNum: crdNum == freezed
          ? _value.crdNum
          : crdNum // ignore: cast_nullable_to_non_nullable
              as String?,
      trstnLocCd: trstnLocCd == freezed
          ? _value.trstnLocCd
          : trstnLocCd // ignore: cast_nullable_to_non_nullable
              as String?,
      trstnType: trstnType == freezed
          ? _value.trstnType
          : trstnType // ignore: cast_nullable_to_non_nullable
              as String?,
      crdOwnerTypeCd: crdOwnerTypeCd == freezed
          ? _value.crdOwnerTypeCd
          : crdOwnerTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      crdOwnerName: crdOwnerName == freezed
          ? _value.crdOwnerName
          : crdOwnerName // ignore: cast_nullable_to_non_nullable
              as String?,
      installment: installment == freezed
          ? _value.installment
          : installment // ignore: cast_nullable_to_non_nullable
              as String?,
      accumType: accumType == freezed
          ? _value.accumType
          : accumType // ignore: cast_nullable_to_non_nullable
              as String?,
      accumTypeCd: accumTypeCd == freezed
          ? _value.accumTypeCd
          : accumTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PaymentDtoHistory extends _PaymentDtoHistory {
  _$_PaymentDtoHistory(
      {@JsonKey(name: 'trstnDt') this.trstnDt,
      @JsonKey(name: 'totPayAmtOrg') this.totalMoney = 0.0,
      @JsonKey(name: 'totPayAmt') this.totalConvertedMoney = 0.0,
      @JsonKey(name: 'curMoneyCd') this.currencyCode,
      @JsonKey(name: 'crdIssueCmpnyCd') this.paymentTypeId,
      @JsonKey(name: 'trstnTypeCd') this.transactionTypeId,
      @JsonKey(name: 'memo') this.description = '',
      this.userSeq,
      this.userNickname,
      this.updtDt,
      this.payRcptSeq,
      this.title,
      this.accumPayAmt,
      this.ratio,
      this.trstnCtgryInfo,
      this.trstnNum,
      this.orgMSG,
      this.orgTrstnDT,
      this.feedSeq,
      this.payRcptScore,
      this.text,
      this.payCount,
      this.inputCd,
      this.typeCd,
      this.vat,
      this.storeNameRcpt,
      this.payRcptSeqList,
      this.storeLinkCd,
      this.payRcptCd,
      this.productName,
      this.totPrdctPrc,
      this.storeInfo,
      this.gpsInfo,
      this.acntCateInfo,
      this.delYN,
      this.crdIssueCmpnyName,
      this.crdIssueCmpnyTel,
      this.appVersion,
      this.osVersion,
      this.deviceModel,
      this.crdSmsRawSeq,
      this.parserVersion,
      this.crdName,
      this.crdNum,
      this.trstnLocCd,
      this.trstnType,
      this.crdOwnerTypeCd,
      this.crdOwnerName,
      this.installment,
      this.accumType,
      this.accumTypeCd})
      : super._();

  factory _$_PaymentDtoHistory.fromJson(Map<String, dynamic> json) =>
      _$$_PaymentDtoHistoryFromJson(json);

  @override
  @JsonKey(name: 'trstnDt')
  final String? trstnDt;
  @override
  @JsonKey(name: 'totPayAmtOrg')
  final double totalMoney;
  @override
  @JsonKey(name: 'totPayAmt')
  final double totalConvertedMoney;
  @override
  @JsonKey(name: 'curMoneyCd')
  final String? currencyCode;
  @override
  @JsonKey(name: 'crdIssueCmpnyCd')
  final String? paymentTypeId;
  @override
  @JsonKey(name: 'trstnTypeCd')
  final String? transactionTypeId;
  @override
  @JsonKey(name: 'memo')
  final String? description;
  @override
  final num? userSeq;
  @override //
  final num? userNickname;
  @override //
//String? crtDt, //
  final String? updtDt;
  @override //
  final num? payRcptSeq;
  @override //
  final String? title;
  @override //
//double? totPayAmt, //
  final double? accumPayAmt;
  @override //
//double? aprvlNum, //
  final double? ratio;
  @override //
//String? trstnDt, //
  final String? trstnCtgryInfo;
  @override //
  final String? trstnNum;
  @override //
  final String? orgMSG;
  @override //
  final String? orgTrstnDT;
  @override //
//String? curMoneyCd, //
  final double? feedSeq;
  @override //
  final String? payRcptScore;
  @override //
//String? memo, //
  final String? text;
  @override //
  final int? payCount;
  @override //
  final String? inputCd;
  @override //
  final String? typeCd;
  @override //
  final String? vat;
  @override //
  final String? storeNameRcpt;
  @override //
  final List<String>? payRcptSeqList;
  @override //
  final String? storeLinkCd;
  @override
  final String? payRcptCd;
  @override //
  final String? productName;
  @override //
  final String? totPrdctPrc;
  @override //
  final StoreInfo? storeInfo;
  @override //
  final GpsInfo? gpsInfo;
  @override //
  final CategoryItem? acntCateInfo;
  @override //
  final String? delYN;
  @override //
  final String? crdIssueCmpnyName;
  @override //
//String? crdIssueCmpnyCd, //
  final String? crdIssueCmpnyTel;
  @override //
  final String? appVersion;
  @override //
  final String? osVersion;
  @override //
  final String? deviceModel;
  @override //
  final int? crdSmsRawSeq;
  @override //
  final String? parserVersion;
  @override //
//String? totPayAmtOrg, //
  final String? crdName;
  @override //
  final String? crdNum;
  @override //
  final String? trstnLocCd;
  @override //
  final String? trstnType;
  @override //
//String? trstnTypeCd, //
  final String? crdOwnerTypeCd;
  @override //
  final String? crdOwnerName;
  @override //
  final String? installment;
  @override //
  final String? accumType;
  @override //
  final String? accumTypeCd;

  @override
  String toString() {
    return 'PaymentHistoryDto(trstnDt: $trstnDt, totalMoney: $totalMoney, totalConvertedMoney: $totalConvertedMoney, currencyCode: $currencyCode, paymentTypeId: $paymentTypeId, transactionTypeId: $transactionTypeId, description: $description, userSeq: $userSeq, userNickname: $userNickname, updtDt: $updtDt, payRcptSeq: $payRcptSeq, title: $title, accumPayAmt: $accumPayAmt, ratio: $ratio, trstnCtgryInfo: $trstnCtgryInfo, trstnNum: $trstnNum, orgMSG: $orgMSG, orgTrstnDT: $orgTrstnDT, feedSeq: $feedSeq, payRcptScore: $payRcptScore, text: $text, payCount: $payCount, inputCd: $inputCd, typeCd: $typeCd, vat: $vat, storeNameRcpt: $storeNameRcpt, payRcptSeqList: $payRcptSeqList, storeLinkCd: $storeLinkCd, payRcptCd: $payRcptCd, productName: $productName, totPrdctPrc: $totPrdctPrc, storeInfo: $storeInfo, gpsInfo: $gpsInfo, acntCateInfo: $acntCateInfo, delYN: $delYN, crdIssueCmpnyName: $crdIssueCmpnyName, crdIssueCmpnyTel: $crdIssueCmpnyTel, appVersion: $appVersion, osVersion: $osVersion, deviceModel: $deviceModel, crdSmsRawSeq: $crdSmsRawSeq, parserVersion: $parserVersion, crdName: $crdName, crdNum: $crdNum, trstnLocCd: $trstnLocCd, trstnType: $trstnType, crdOwnerTypeCd: $crdOwnerTypeCd, crdOwnerName: $crdOwnerName, installment: $installment, accumType: $accumType, accumTypeCd: $accumTypeCd)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _PaymentDtoHistory &&
            (identical(other.trstnDt, trstnDt) || other.trstnDt == trstnDt) &&
            (identical(other.totalMoney, totalMoney) ||
                other.totalMoney == totalMoney) &&
            (identical(other.totalConvertedMoney, totalConvertedMoney) ||
                other.totalConvertedMoney == totalConvertedMoney) &&
            (identical(other.currencyCode, currencyCode) ||
                other.currencyCode == currencyCode) &&
            (identical(other.paymentTypeId, paymentTypeId) ||
                other.paymentTypeId == paymentTypeId) &&
            (identical(other.transactionTypeId, transactionTypeId) ||
                other.transactionTypeId == transactionTypeId) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.userNickname, userNickname) ||
                other.userNickname == userNickname) &&
            (identical(other.updtDt, updtDt) || other.updtDt == updtDt) &&
            (identical(other.payRcptSeq, payRcptSeq) ||
                other.payRcptSeq == payRcptSeq) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.accumPayAmt, accumPayAmt) ||
                other.accumPayAmt == accumPayAmt) &&
            (identical(other.ratio, ratio) || other.ratio == ratio) &&
            (identical(other.trstnCtgryInfo, trstnCtgryInfo) ||
                other.trstnCtgryInfo == trstnCtgryInfo) &&
            (identical(other.trstnNum, trstnNum) ||
                other.trstnNum == trstnNum) &&
            (identical(other.orgMSG, orgMSG) || other.orgMSG == orgMSG) &&
            (identical(other.orgTrstnDT, orgTrstnDT) ||
                other.orgTrstnDT == orgTrstnDT) &&
            (identical(other.feedSeq, feedSeq) || other.feedSeq == feedSeq) &&
            (identical(other.payRcptScore, payRcptScore) ||
                other.payRcptScore == payRcptScore) &&
            (identical(other.text, text) || other.text == text) &&
            (identical(other.payCount, payCount) ||
                other.payCount == payCount) &&
            (identical(other.inputCd, inputCd) || other.inputCd == inputCd) &&
            (identical(other.typeCd, typeCd) || other.typeCd == typeCd) &&
            (identical(other.vat, vat) || other.vat == vat) &&
            (identical(other.storeNameRcpt, storeNameRcpt) ||
                other.storeNameRcpt == storeNameRcpt) &&
            const DeepCollectionEquality()
                .equals(other.payRcptSeqList, payRcptSeqList) &&
            (identical(other.storeLinkCd, storeLinkCd) ||
                other.storeLinkCd == storeLinkCd) &&
            (identical(other.payRcptCd, payRcptCd) ||
                other.payRcptCd == payRcptCd) &&
            (identical(other.productName, productName) ||
                other.productName == productName) &&
            (identical(other.totPrdctPrc, totPrdctPrc) ||
                other.totPrdctPrc == totPrdctPrc) &&
            (identical(other.storeInfo, storeInfo) ||
                other.storeInfo == storeInfo) &&
            (identical(other.gpsInfo, gpsInfo) || other.gpsInfo == gpsInfo) &&
            (identical(other.acntCateInfo, acntCateInfo) ||
                other.acntCateInfo == acntCateInfo) &&
            (identical(other.delYN, delYN) || other.delYN == delYN) &&
            (identical(other.crdIssueCmpnyName, crdIssueCmpnyName) ||
                other.crdIssueCmpnyName == crdIssueCmpnyName) &&
            (identical(other.crdIssueCmpnyTel, crdIssueCmpnyTel) ||
                other.crdIssueCmpnyTel == crdIssueCmpnyTel) &&
            (identical(other.appVersion, appVersion) ||
                other.appVersion == appVersion) &&
            (identical(other.osVersion, osVersion) ||
                other.osVersion == osVersion) &&
            (identical(other.deviceModel, deviceModel) ||
                other.deviceModel == deviceModel) &&
            (identical(other.crdSmsRawSeq, crdSmsRawSeq) ||
                other.crdSmsRawSeq == crdSmsRawSeq) &&
            (identical(other.parserVersion, parserVersion) ||
                other.parserVersion == parserVersion) &&
            (identical(other.crdName, crdName) || other.crdName == crdName) &&
            (identical(other.crdNum, crdNum) || other.crdNum == crdNum) &&
            (identical(other.trstnLocCd, trstnLocCd) ||
                other.trstnLocCd == trstnLocCd) &&
            (identical(other.trstnType, trstnType) ||
                other.trstnType == trstnType) &&
            (identical(other.crdOwnerTypeCd, crdOwnerTypeCd) ||
                other.crdOwnerTypeCd == crdOwnerTypeCd) &&
            (identical(other.crdOwnerName, crdOwnerName) ||
                other.crdOwnerName == crdOwnerName) &&
            (identical(other.installment, installment) ||
                other.installment == installment) &&
            (identical(other.accumType, accumType) ||
                other.accumType == accumType) &&
            (identical(other.accumTypeCd, accumTypeCd) ||
                other.accumTypeCd == accumTypeCd));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        trstnDt,
        totalMoney,
        totalConvertedMoney,
        currencyCode,
        paymentTypeId,
        transactionTypeId,
        description,
        userSeq,
        userNickname,
        updtDt,
        payRcptSeq,
        title,
        accumPayAmt,
        ratio,
        trstnCtgryInfo,
        trstnNum,
        orgMSG,
        orgTrstnDT,
        feedSeq,
        payRcptScore,
        text,
        payCount,
        inputCd,
        typeCd,
        vat,
        storeNameRcpt,
        const DeepCollectionEquality().hash(payRcptSeqList),
        storeLinkCd,
        payRcptCd,
        productName,
        totPrdctPrc,
        storeInfo,
        gpsInfo,
        acntCateInfo,
        delYN,
        crdIssueCmpnyName,
        crdIssueCmpnyTel,
        appVersion,
        osVersion,
        deviceModel,
        crdSmsRawSeq,
        parserVersion,
        crdName,
        crdNum,
        trstnLocCd,
        trstnType,
        crdOwnerTypeCd,
        crdOwnerName,
        installment,
        accumType,
        accumTypeCd
      ]);

  @JsonKey(ignore: true)
  @override
  _$PaymentDtoHistoryCopyWith<_PaymentDtoHistory> get copyWith =>
      __$PaymentDtoHistoryCopyWithImpl<_PaymentDtoHistory>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PaymentDtoHistoryToJson(this);
  }
}

abstract class _PaymentDtoHistory extends PaymentHistoryDto {
  factory _PaymentDtoHistory(
      {@JsonKey(name: 'trstnDt') String? trstnDt,
      @JsonKey(name: 'totPayAmtOrg') double totalMoney,
      @JsonKey(name: 'totPayAmt') double totalConvertedMoney,
      @JsonKey(name: 'curMoneyCd') String? currencyCode,
      @JsonKey(name: 'crdIssueCmpnyCd') String? paymentTypeId,
      @JsonKey(name: 'trstnTypeCd') String? transactionTypeId,
      @JsonKey(name: 'memo') String? description,
      num? userSeq,
      num? userNickname,
      String? updtDt,
      num? payRcptSeq,
      String? title,
      double? accumPayAmt,
      double? ratio,
      String? trstnCtgryInfo,
      String? trstnNum,
      String? orgMSG,
      String? orgTrstnDT,
      double? feedSeq,
      String? payRcptScore,
      String? text,
      int? payCount,
      String? inputCd,
      String? typeCd,
      String? vat,
      String? storeNameRcpt,
      List<String>? payRcptSeqList,
      String? storeLinkCd,
      String? payRcptCd,
      String? productName,
      String? totPrdctPrc,
      StoreInfo? storeInfo,
      GpsInfo? gpsInfo,
      CategoryItem? acntCateInfo,
      String? delYN,
      String? crdIssueCmpnyName,
      String? crdIssueCmpnyTel,
      String? appVersion,
      String? osVersion,
      String? deviceModel,
      int? crdSmsRawSeq,
      String? parserVersion,
      String? crdName,
      String? crdNum,
      String? trstnLocCd,
      String? trstnType,
      String? crdOwnerTypeCd,
      String? crdOwnerName,
      String? installment,
      String? accumType,
      String? accumTypeCd}) = _$_PaymentDtoHistory;
  _PaymentDtoHistory._() : super._();

  factory _PaymentDtoHistory.fromJson(Map<String, dynamic> json) =
      _$_PaymentDtoHistory.fromJson;

  @override
  @JsonKey(name: 'trstnDt')
  String? get trstnDt;
  @override
  @JsonKey(name: 'totPayAmtOrg')
  double get totalMoney;
  @override
  @JsonKey(name: 'totPayAmt')
  double get totalConvertedMoney;
  @override
  @JsonKey(name: 'curMoneyCd')
  String? get currencyCode;
  @override
  @JsonKey(name: 'crdIssueCmpnyCd')
  String? get paymentTypeId;
  @override
  @JsonKey(name: 'trstnTypeCd')
  String? get transactionTypeId;
  @override
  @JsonKey(name: 'memo')
  String? get description;
  @override
  num? get userSeq;
  @override //
  num? get userNickname;
  @override //
//String? crtDt, //
  String? get updtDt;
  @override //
  num? get payRcptSeq;
  @override //
  String? get title;
  @override //
//double? totPayAmt, //
  double? get accumPayAmt;
  @override //
//double? aprvlNum, //
  double? get ratio;
  @override //
//String? trstnDt, //
  String? get trstnCtgryInfo;
  @override //
  String? get trstnNum;
  @override //
  String? get orgMSG;
  @override //
  String? get orgTrstnDT;
  @override //
//String? curMoneyCd, //
  double? get feedSeq;
  @override //
  String? get payRcptScore;
  @override //
//String? memo, //
  String? get text;
  @override //
  int? get payCount;
  @override //
  String? get inputCd;
  @override //
  String? get typeCd;
  @override //
  String? get vat;
  @override //
  String? get storeNameRcpt;
  @override //
  List<String>? get payRcptSeqList;
  @override //
  String? get storeLinkCd;
  @override
  String? get payRcptCd;
  @override //
  String? get productName;
  @override //
  String? get totPrdctPrc;
  @override //
  StoreInfo? get storeInfo;
  @override //
  GpsInfo? get gpsInfo;
  @override //
  CategoryItem? get acntCateInfo;
  @override //
  String? get delYN;
  @override //
  String? get crdIssueCmpnyName;
  @override //
//String? crdIssueCmpnyCd, //
  String? get crdIssueCmpnyTel;
  @override //
  String? get appVersion;
  @override //
  String? get osVersion;
  @override //
  String? get deviceModel;
  @override //
  int? get crdSmsRawSeq;
  @override //
  String? get parserVersion;
  @override //
//String? totPayAmtOrg, //
  String? get crdName;
  @override //
  String? get crdNum;
  @override //
  String? get trstnLocCd;
  @override //
  String? get trstnType;
  @override //
//String? trstnTypeCd, //
  String? get crdOwnerTypeCd;
  @override //
  String? get crdOwnerName;
  @override //
  String? get installment;
  @override //
  String? get accumType;
  @override //
  String? get accumTypeCd;
  @override
  @JsonKey(ignore: true)
  _$PaymentDtoHistoryCopyWith<_PaymentDtoHistory> get copyWith =>
      throw _privateConstructorUsedError;
}
