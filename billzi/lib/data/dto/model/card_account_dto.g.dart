// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_account_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CardAccountDto _$$_CardAccountDtoFromJson(Map<String, dynamic> json) =>
    _$_CardAccountDto(
      userSeq: json['userSeq'] as num?,
      title: json['title'] as String? ?? '',
      crdName: json['crdName'] as String? ?? '',
      crdNum: json['crdNum'] as String? ?? '0000',
      crdIssueCmpnyCd: json['crdIssueCmpnyCd'] as String? ?? '00',
      accountTypeCd: json['accountTypeCd'] as String? ?? '',
      memo: json['memo'] as String? ?? '',
      useYN: json['useYN'] as String? ?? '',
      inputCd: json['inputCd'] as String? ?? '',
      updtDt: json['updtDt'] as String?,
    );

Map<String, dynamic> _$$_CardAccountDtoToJson(_$_CardAccountDto instance) =>
    <String, dynamic>{
      'userSeq': instance.userSeq,
      'title': instance.title,
      'crdName': instance.crdName,
      'crdNum': instance.crdNum,
      'crdIssueCmpnyCd': instance.crdIssueCmpnyCd,
      'accountTypeCd': instance.accountTypeCd,
      'memo': instance.memo,
      'useYN': instance.useYN,
      'inputCd': instance.inputCd,
      'updtDt': instance.updtDt,
    };
