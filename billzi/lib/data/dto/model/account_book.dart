import 'package:freezed_annotation/freezed_annotation.dart';

part 'account_book.freezed.dart';
part 'account_book.g.dart';

@freezed
class AccountBook with _$AccountBook {
  factory AccountBook({
    String? startDate
  }) = _AccountBook;

  factory AccountBook.fromJson(Map<String, dynamic> json) =>
      _$AccountBookFromJson(json);
}