// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AuthDto _$$_AuthDtoFromJson(Map<String, dynamic> json) => _$_AuthDto(
      userSeq: json['userSeq'] as num?,
      userId: json['userId'] as String?,
      prtyUserId: json['prtyUserId'] as String?,
      userNickname: json['userNickname'] as String? ?? 'NotUsed',
      password: json['password'] as String? ?? 'NotUsed',
      fcmClientToken: json['fcmClientToken'] as String? ?? 'NotUsed',
      email: json['email'] as String?,
      lastAuthDt: json['lastAuthDt'] as String?,
      userProfileImgUrl: json['userProfileImgUrl'] as String?,
      birthDate: json['birthDate'] as String?,
      phoneNum1: json['phoneNum1'] as String?,
      phoneNum2: json['phoneNum2'] as String?,
      authTypeCd: json['authTypeCd'] as String? ?? '0',
      token: json['token'] as String?,
      expireDate: json['expireDate'] as String?,
      authorities: json['authorities'] as String?,
    );

Map<String, dynamic> _$$_AuthDtoToJson(_$_AuthDto instance) =>
    <String, dynamic>{
      'userSeq': instance.userSeq,
      'userId': instance.userId,
      'prtyUserId': instance.prtyUserId,
      'userNickname': instance.userNickname,
      'password': instance.password,
      'fcmClientToken': instance.fcmClientToken,
      'email': instance.email,
      'lastAuthDt': instance.lastAuthDt,
      'userProfileImgUrl': instance.userProfileImgUrl,
      'birthDate': instance.birthDate,
      'phoneNum1': instance.phoneNum1,
      'phoneNum2': instance.phoneNum2,
      'authTypeCd': instance.authTypeCd,
      'token': instance.token,
      'expireDate': instance.expireDate,
      'authorities': instance.authorities,
    };
