// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserDto _$UserDtoFromJson(Map<String, dynamic> json) {
  return _UserDto.fromJson(json);
}

/// @nodoc
class _$UserDtoTearOff {
  const _$UserDtoTearOff();

  _UserDto call(
      {num? userSeq,
      String? userName = 'NotUsed',
      String? userId,
      String? prtyUserId,
      String? userNickname = 'NotUsed',
      String? password = 'NotUsed',
      String? fcmClientToken = 'NotUsed',
      String? delYN,
      String? crtDt,
      String? updtDt,
      String? email = '',
      String? lastAuthDt,
      String? userProfileImgUrl = '',
      String? birthDate = '',
      int? ageMin,
      int? ageMax,
      String? gender = '',
      String? phoneNum1 = '',
      String? phoneNum2 = '',
      String? address = '',
      String? userTypeCd = '1',
      String? authTypeCd = '0',
      String? followCd,
      String? locked,
      String? adId}) {
    return _UserDto(
      userSeq: userSeq,
      userName: userName,
      userId: userId,
      prtyUserId: prtyUserId,
      userNickname: userNickname,
      password: password,
      fcmClientToken: fcmClientToken,
      delYN: delYN,
      crtDt: crtDt,
      updtDt: updtDt,
      email: email,
      lastAuthDt: lastAuthDt,
      userProfileImgUrl: userProfileImgUrl,
      birthDate: birthDate,
      ageMin: ageMin,
      ageMax: ageMax,
      gender: gender,
      phoneNum1: phoneNum1,
      phoneNum2: phoneNum2,
      address: address,
      userTypeCd: userTypeCd,
      authTypeCd: authTypeCd,
      followCd: followCd,
      locked: locked,
      adId: adId,
    );
  }

  UserDto fromJson(Map<String, Object?> json) {
    return UserDto.fromJson(json);
  }
}

/// @nodoc
const $UserDto = _$UserDtoTearOff();

/// @nodoc
mixin _$UserDto {
  num? get userSeq => throw _privateConstructorUsedError;
  String? get userName => throw _privateConstructorUsedError;
  String? get userId => throw _privateConstructorUsedError;
  String? get prtyUserId => throw _privateConstructorUsedError;
  String? get userNickname => throw _privateConstructorUsedError;
  String? get password => throw _privateConstructorUsedError;
  String? get fcmClientToken => throw _privateConstructorUsedError;
  String? get delYN => throw _privateConstructorUsedError;
  String? get crtDt => throw _privateConstructorUsedError;
  String? get updtDt => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get lastAuthDt => throw _privateConstructorUsedError;
  String? get userProfileImgUrl => throw _privateConstructorUsedError;
  String? get birthDate => throw _privateConstructorUsedError;
  int? get ageMin => throw _privateConstructorUsedError;
  int? get ageMax => throw _privateConstructorUsedError;
  String? get gender => throw _privateConstructorUsedError;
  String? get phoneNum1 => throw _privateConstructorUsedError;
  String? get phoneNum2 => throw _privateConstructorUsedError;
  String? get address => throw _privateConstructorUsedError;
  String? get userTypeCd => throw _privateConstructorUsedError;
  String? get authTypeCd => throw _privateConstructorUsedError;
  String? get followCd => throw _privateConstructorUsedError;
  String? get locked => throw _privateConstructorUsedError;
  String? get adId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserDtoCopyWith<UserDto> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserDtoCopyWith<$Res> {
  factory $UserDtoCopyWith(UserDto value, $Res Function(UserDto) then) =
      _$UserDtoCopyWithImpl<$Res>;
  $Res call(
      {num? userSeq,
      String? userName,
      String? userId,
      String? prtyUserId,
      String? userNickname,
      String? password,
      String? fcmClientToken,
      String? delYN,
      String? crtDt,
      String? updtDt,
      String? email,
      String? lastAuthDt,
      String? userProfileImgUrl,
      String? birthDate,
      int? ageMin,
      int? ageMax,
      String? gender,
      String? phoneNum1,
      String? phoneNum2,
      String? address,
      String? userTypeCd,
      String? authTypeCd,
      String? followCd,
      String? locked,
      String? adId});
}

/// @nodoc
class _$UserDtoCopyWithImpl<$Res> implements $UserDtoCopyWith<$Res> {
  _$UserDtoCopyWithImpl(this._value, this._then);

  final UserDto _value;
  // ignore: unused_field
  final $Res Function(UserDto) _then;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? userName = freezed,
    Object? userId = freezed,
    Object? prtyUserId = freezed,
    Object? userNickname = freezed,
    Object? password = freezed,
    Object? fcmClientToken = freezed,
    Object? delYN = freezed,
    Object? crtDt = freezed,
    Object? updtDt = freezed,
    Object? email = freezed,
    Object? lastAuthDt = freezed,
    Object? userProfileImgUrl = freezed,
    Object? birthDate = freezed,
    Object? ageMin = freezed,
    Object? ageMax = freezed,
    Object? gender = freezed,
    Object? phoneNum1 = freezed,
    Object? phoneNum2 = freezed,
    Object? address = freezed,
    Object? userTypeCd = freezed,
    Object? authTypeCd = freezed,
    Object? followCd = freezed,
    Object? locked = freezed,
    Object? adId = freezed,
  }) {
    return _then(_value.copyWith(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String?,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String?,
      prtyUserId: prtyUserId == freezed
          ? _value.prtyUserId
          : prtyUserId // ignore: cast_nullable_to_non_nullable
              as String?,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      fcmClientToken: fcmClientToken == freezed
          ? _value.fcmClientToken
          : fcmClientToken // ignore: cast_nullable_to_non_nullable
              as String?,
      delYN: delYN == freezed
          ? _value.delYN
          : delYN // ignore: cast_nullable_to_non_nullable
              as String?,
      crtDt: crtDt == freezed
          ? _value.crtDt
          : crtDt // ignore: cast_nullable_to_non_nullable
              as String?,
      updtDt: updtDt == freezed
          ? _value.updtDt
          : updtDt // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      lastAuthDt: lastAuthDt == freezed
          ? _value.lastAuthDt
          : lastAuthDt // ignore: cast_nullable_to_non_nullable
              as String?,
      userProfileImgUrl: userProfileImgUrl == freezed
          ? _value.userProfileImgUrl
          : userProfileImgUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as String?,
      ageMin: ageMin == freezed
          ? _value.ageMin
          : ageMin // ignore: cast_nullable_to_non_nullable
              as int?,
      ageMax: ageMax == freezed
          ? _value.ageMax
          : ageMax // ignore: cast_nullable_to_non_nullable
              as int?,
      gender: gender == freezed
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNum1: phoneNum1 == freezed
          ? _value.phoneNum1
          : phoneNum1 // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNum2: phoneNum2 == freezed
          ? _value.phoneNum2
          : phoneNum2 // ignore: cast_nullable_to_non_nullable
              as String?,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      userTypeCd: userTypeCd == freezed
          ? _value.userTypeCd
          : userTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      authTypeCd: authTypeCd == freezed
          ? _value.authTypeCd
          : authTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      followCd: followCd == freezed
          ? _value.followCd
          : followCd // ignore: cast_nullable_to_non_nullable
              as String?,
      locked: locked == freezed
          ? _value.locked
          : locked // ignore: cast_nullable_to_non_nullable
              as String?,
      adId: adId == freezed
          ? _value.adId
          : adId // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$UserDtoCopyWith<$Res> implements $UserDtoCopyWith<$Res> {
  factory _$UserDtoCopyWith(_UserDto value, $Res Function(_UserDto) then) =
      __$UserDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? userSeq,
      String? userName,
      String? userId,
      String? prtyUserId,
      String? userNickname,
      String? password,
      String? fcmClientToken,
      String? delYN,
      String? crtDt,
      String? updtDt,
      String? email,
      String? lastAuthDt,
      String? userProfileImgUrl,
      String? birthDate,
      int? ageMin,
      int? ageMax,
      String? gender,
      String? phoneNum1,
      String? phoneNum2,
      String? address,
      String? userTypeCd,
      String? authTypeCd,
      String? followCd,
      String? locked,
      String? adId});
}

/// @nodoc
class __$UserDtoCopyWithImpl<$Res> extends _$UserDtoCopyWithImpl<$Res>
    implements _$UserDtoCopyWith<$Res> {
  __$UserDtoCopyWithImpl(_UserDto _value, $Res Function(_UserDto) _then)
      : super(_value, (v) => _then(v as _UserDto));

  @override
  _UserDto get _value => super._value as _UserDto;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? userName = freezed,
    Object? userId = freezed,
    Object? prtyUserId = freezed,
    Object? userNickname = freezed,
    Object? password = freezed,
    Object? fcmClientToken = freezed,
    Object? delYN = freezed,
    Object? crtDt = freezed,
    Object? updtDt = freezed,
    Object? email = freezed,
    Object? lastAuthDt = freezed,
    Object? userProfileImgUrl = freezed,
    Object? birthDate = freezed,
    Object? ageMin = freezed,
    Object? ageMax = freezed,
    Object? gender = freezed,
    Object? phoneNum1 = freezed,
    Object? phoneNum2 = freezed,
    Object? address = freezed,
    Object? userTypeCd = freezed,
    Object? authTypeCd = freezed,
    Object? followCd = freezed,
    Object? locked = freezed,
    Object? adId = freezed,
  }) {
    return _then(_UserDto(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String?,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String?,
      prtyUserId: prtyUserId == freezed
          ? _value.prtyUserId
          : prtyUserId // ignore: cast_nullable_to_non_nullable
              as String?,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      fcmClientToken: fcmClientToken == freezed
          ? _value.fcmClientToken
          : fcmClientToken // ignore: cast_nullable_to_non_nullable
              as String?,
      delYN: delYN == freezed
          ? _value.delYN
          : delYN // ignore: cast_nullable_to_non_nullable
              as String?,
      crtDt: crtDt == freezed
          ? _value.crtDt
          : crtDt // ignore: cast_nullable_to_non_nullable
              as String?,
      updtDt: updtDt == freezed
          ? _value.updtDt
          : updtDt // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      lastAuthDt: lastAuthDt == freezed
          ? _value.lastAuthDt
          : lastAuthDt // ignore: cast_nullable_to_non_nullable
              as String?,
      userProfileImgUrl: userProfileImgUrl == freezed
          ? _value.userProfileImgUrl
          : userProfileImgUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as String?,
      ageMin: ageMin == freezed
          ? _value.ageMin
          : ageMin // ignore: cast_nullable_to_non_nullable
              as int?,
      ageMax: ageMax == freezed
          ? _value.ageMax
          : ageMax // ignore: cast_nullable_to_non_nullable
              as int?,
      gender: gender == freezed
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNum1: phoneNum1 == freezed
          ? _value.phoneNum1
          : phoneNum1 // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNum2: phoneNum2 == freezed
          ? _value.phoneNum2
          : phoneNum2 // ignore: cast_nullable_to_non_nullable
              as String?,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      userTypeCd: userTypeCd == freezed
          ? _value.userTypeCd
          : userTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      authTypeCd: authTypeCd == freezed
          ? _value.authTypeCd
          : authTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      followCd: followCd == freezed
          ? _value.followCd
          : followCd // ignore: cast_nullable_to_non_nullable
              as String?,
      locked: locked == freezed
          ? _value.locked
          : locked // ignore: cast_nullable_to_non_nullable
              as String?,
      adId: adId == freezed
          ? _value.adId
          : adId // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserDto implements _UserDto {
  _$_UserDto(
      {this.userSeq,
      this.userName = 'NotUsed',
      this.userId,
      this.prtyUserId,
      this.userNickname = 'NotUsed',
      this.password = 'NotUsed',
      this.fcmClientToken = 'NotUsed',
      this.delYN,
      this.crtDt,
      this.updtDt,
      this.email = '',
      this.lastAuthDt,
      this.userProfileImgUrl = '',
      this.birthDate = '',
      this.ageMin,
      this.ageMax,
      this.gender = '',
      this.phoneNum1 = '',
      this.phoneNum2 = '',
      this.address = '',
      this.userTypeCd = '1',
      this.authTypeCd = '0',
      this.followCd,
      this.locked,
      this.adId});

  factory _$_UserDto.fromJson(Map<String, dynamic> json) =>
      _$$_UserDtoFromJson(json);

  @override
  final num? userSeq;
  @JsonKey(defaultValue: 'NotUsed')
  @override
  final String? userName;
  @override
  final String? userId;
  @override
  final String? prtyUserId;
  @JsonKey(defaultValue: 'NotUsed')
  @override
  final String? userNickname;
  @JsonKey(defaultValue: 'NotUsed')
  @override
  final String? password;
  @JsonKey(defaultValue: 'NotUsed')
  @override
  final String? fcmClientToken;
  @override
  final String? delYN;
  @override
  final String? crtDt;
  @override
  final String? updtDt;
  @JsonKey(defaultValue: '')
  @override
  final String? email;
  @override
  final String? lastAuthDt;
  @JsonKey(defaultValue: '')
  @override
  final String? userProfileImgUrl;
  @JsonKey(defaultValue: '')
  @override
  final String? birthDate;
  @override
  final int? ageMin;
  @override
  final int? ageMax;
  @JsonKey(defaultValue: '')
  @override
  final String? gender;
  @JsonKey(defaultValue: '')
  @override
  final String? phoneNum1;
  @JsonKey(defaultValue: '')
  @override
  final String? phoneNum2;
  @JsonKey(defaultValue: '')
  @override
  final String? address;
  @JsonKey(defaultValue: '1')
  @override
  final String? userTypeCd;
  @JsonKey(defaultValue: '0')
  @override
  final String? authTypeCd;
  @override
  final String? followCd;
  @override
  final String? locked;
  @override
  final String? adId;

  @override
  String toString() {
    return 'UserDto(userSeq: $userSeq, userName: $userName, userId: $userId, prtyUserId: $prtyUserId, userNickname: $userNickname, password: $password, fcmClientToken: $fcmClientToken, delYN: $delYN, crtDt: $crtDt, updtDt: $updtDt, email: $email, lastAuthDt: $lastAuthDt, userProfileImgUrl: $userProfileImgUrl, birthDate: $birthDate, ageMin: $ageMin, ageMax: $ageMax, gender: $gender, phoneNum1: $phoneNum1, phoneNum2: $phoneNum2, address: $address, userTypeCd: $userTypeCd, authTypeCd: $authTypeCd, followCd: $followCd, locked: $locked, adId: $adId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _UserDto &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.userName, userName) ||
                other.userName == userName) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.prtyUserId, prtyUserId) ||
                other.prtyUserId == prtyUserId) &&
            (identical(other.userNickname, userNickname) ||
                other.userNickname == userNickname) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.fcmClientToken, fcmClientToken) ||
                other.fcmClientToken == fcmClientToken) &&
            (identical(other.delYN, delYN) || other.delYN == delYN) &&
            (identical(other.crtDt, crtDt) || other.crtDt == crtDt) &&
            (identical(other.updtDt, updtDt) || other.updtDt == updtDt) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.lastAuthDt, lastAuthDt) ||
                other.lastAuthDt == lastAuthDt) &&
            (identical(other.userProfileImgUrl, userProfileImgUrl) ||
                other.userProfileImgUrl == userProfileImgUrl) &&
            (identical(other.birthDate, birthDate) ||
                other.birthDate == birthDate) &&
            (identical(other.ageMin, ageMin) || other.ageMin == ageMin) &&
            (identical(other.ageMax, ageMax) || other.ageMax == ageMax) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.phoneNum1, phoneNum1) ||
                other.phoneNum1 == phoneNum1) &&
            (identical(other.phoneNum2, phoneNum2) ||
                other.phoneNum2 == phoneNum2) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.userTypeCd, userTypeCd) ||
                other.userTypeCd == userTypeCd) &&
            (identical(other.authTypeCd, authTypeCd) ||
                other.authTypeCd == authTypeCd) &&
            (identical(other.followCd, followCd) ||
                other.followCd == followCd) &&
            (identical(other.locked, locked) || other.locked == locked) &&
            (identical(other.adId, adId) || other.adId == adId));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        userSeq,
        userName,
        userId,
        prtyUserId,
        userNickname,
        password,
        fcmClientToken,
        delYN,
        crtDt,
        updtDt,
        email,
        lastAuthDt,
        userProfileImgUrl,
        birthDate,
        ageMin,
        ageMax,
        gender,
        phoneNum1,
        phoneNum2,
        address,
        userTypeCd,
        authTypeCd,
        followCd,
        locked,
        adId
      ]);

  @JsonKey(ignore: true)
  @override
  _$UserDtoCopyWith<_UserDto> get copyWith =>
      __$UserDtoCopyWithImpl<_UserDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserDtoToJson(this);
  }
}

abstract class _UserDto implements UserDto {
  factory _UserDto(
      {num? userSeq,
      String? userName,
      String? userId,
      String? prtyUserId,
      String? userNickname,
      String? password,
      String? fcmClientToken,
      String? delYN,
      String? crtDt,
      String? updtDt,
      String? email,
      String? lastAuthDt,
      String? userProfileImgUrl,
      String? birthDate,
      int? ageMin,
      int? ageMax,
      String? gender,
      String? phoneNum1,
      String? phoneNum2,
      String? address,
      String? userTypeCd,
      String? authTypeCd,
      String? followCd,
      String? locked,
      String? adId}) = _$_UserDto;

  factory _UserDto.fromJson(Map<String, dynamic> json) = _$_UserDto.fromJson;

  @override
  num? get userSeq;
  @override
  String? get userName;
  @override
  String? get userId;
  @override
  String? get prtyUserId;
  @override
  String? get userNickname;
  @override
  String? get password;
  @override
  String? get fcmClientToken;
  @override
  String? get delYN;
  @override
  String? get crtDt;
  @override
  String? get updtDt;
  @override
  String? get email;
  @override
  String? get lastAuthDt;
  @override
  String? get userProfileImgUrl;
  @override
  String? get birthDate;
  @override
  int? get ageMin;
  @override
  int? get ageMax;
  @override
  String? get gender;
  @override
  String? get phoneNum1;
  @override
  String? get phoneNum2;
  @override
  String? get address;
  @override
  String? get userTypeCd;
  @override
  String? get authTypeCd;
  @override
  String? get followCd;
  @override
  String? get locked;
  @override
  String? get adId;
  @override
  @JsonKey(ignore: true)
  _$UserDtoCopyWith<_UserDto> get copyWith =>
      throw _privateConstructorUsedError;
}
