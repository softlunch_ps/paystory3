// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CategoryItem _$$_CategoryItemFromJson(Map<String, dynamic> json) =>
    _$_CategoryItem(
      accountCategorySeq: json['accountCategorySeq'] as num?,
      accountCategoryName: json['accountCategoryName'] as String?,
      userSeq: json['userSeq'] as num?,
      ratio: json['ratio'] as num?,
      totPayAmt: json['totPayAmt'] as num?,
      order: json['order'] as int?,
      supAccountCategorySeq: json['supAccountCategorySeq'] as num?,
      color: json['color'] as String?,
      typeCd: json['typeCd'] as String?,
      imgURL: json['imgURL'] as String?,
    );

Map<String, dynamic> _$$_CategoryItemToJson(_$_CategoryItem instance) =>
    <String, dynamic>{
      'accountCategorySeq': instance.accountCategorySeq,
      'accountCategoryName': instance.accountCategoryName,
      'userSeq': instance.userSeq,
      'ratio': instance.ratio,
      'totPayAmt': instance.totPayAmt,
      'order': instance.order,
      'supAccountCategorySeq': instance.supAccountCategorySeq,
      'color': instance.color,
      'typeCd': instance.typeCd,
      'imgURL': instance.imgURL,
    };
