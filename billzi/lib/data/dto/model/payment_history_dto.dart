import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/model/gps_Info.dart';
import 'package:billzi/data/dto/model/store_info.dart';
import 'package:billzi/utils/utils.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'payment_history_dto.freezed.dart';
part 'payment_history_dto.g.dart';

@freezed
class PaymentHistoryDto with _$PaymentHistoryDto {
  factory PaymentHistoryDto({
    @JsonKey(name: 'trstnDt') String? trstnDt,
    @JsonKey(name: 'totPayAmtOrg') @Default(0.0) double totalMoney,
    @JsonKey(name: 'totPayAmt') @Default(0.0) double totalConvertedMoney,
    @JsonKey(name: 'curMoneyCd') String? currencyCode,
    @JsonKey(name: 'crdIssueCmpnyCd') String? paymentTypeId,
    @JsonKey(name: 'trstnTypeCd') String? transactionTypeId,
    @JsonKey(name: 'memo') @Default('') String? description,

    num? userSeq,//
    num? userNickname,//
    //String? crtDt, //
    String? updtDt, //
    num? payRcptSeq, //
    String? title, //
    //double? totPayAmt, //
    double? accumPayAmt, //
    //double? aprvlNum, //
    double? ratio, //
    //String? trstnDt, //
    String? trstnCtgryInfo, //
    String? trstnNum, //
    String? orgMSG, //
    String? orgTrstnDT, //
    //String? curMoneyCd, //
    double? feedSeq, //
    String? payRcptScore, //
    //String? memo, //
    String? text, //
    int? payCount, //
    String? inputCd, //
    String? typeCd, //
    String? vat, //
    String? storeNameRcpt, //
    List<String>? payRcptSeqList, //
    String? storeLinkCd,
    String? payRcptCd, //
    String? productName, //
    String? totPrdctPrc, //
    StoreInfo? storeInfo, //
    GpsInfo? gpsInfo, //
    CategoryItem? acntCateInfo, //
    String? delYN, //
    String? crdIssueCmpnyName, //
    //String? crdIssueCmpnyCd, //
    String? crdIssueCmpnyTel, //
    String? appVersion, //
    String? osVersion, //
    String? deviceModel, //
    int? crdSmsRawSeq, //
    String? parserVersion, //
    //String? totPayAmtOrg, //
    String? crdName, //
    String? crdNum, //
    String? trstnLocCd, //
    String? trstnType, //
    //String? trstnTypeCd, //
    String? crdOwnerTypeCd, //
    String? crdOwnerName, //
    String? installment, //
    String? accumType, //
    String? accumTypeCd, //
    //PaymentScheduleInfoObject? paymentScheduleInfo, //
  }) = _PaymentDtoHistory;

  PaymentHistoryDto._();

  get createDateTime => oldPaystoryDateParser(trstnDt!);
  // DateTime getCreateDateTime() {
  //   if (createDateTime == null) {
  //     return oldPaystoryDateParser(trstnDt!);
  //   } else {
  //     return createDateTime!;
  //   }
  // }

  factory PaymentHistoryDto.fromJson(Map<String, dynamic> json) =>
      _$PaymentHistoryDtoFromJson(json);
}