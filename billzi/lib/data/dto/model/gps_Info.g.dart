// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gps_Info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_GpsInfo _$$_GpsInfoFromJson(Map<String, dynamic> json) => _$_GpsInfo(
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
    );

Map<String, dynamic> _$$_GpsInfoToJson(_$_GpsInfo instance) =>
    <String, dynamic>{
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
