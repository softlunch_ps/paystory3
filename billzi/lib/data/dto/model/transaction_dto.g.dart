// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TransactionDto _$$_TransactionDtoFromJson(Map<String, dynamic> json) =>
    _$_TransactionDto(
      createDateTime: DateTime.parse(json['trstnDt'] as String),
      totalMoney: (json['totPayAmtOrg'] as num?)?.toDouble() ?? 0.0,
      totalConvertedMoney: (json['totPayAmt'] as num?)?.toDouble() ?? 0.0,
      currencyCode: json['curMoneyCd'] as String,
      paymentTypeId: json['payTypeCd'] as String?,
      transactionTypeId: json['trstnTypeGroupCd'] as String?,
      description: json['memo'] as String? ?? '',
      book: json['acntbookInfo'] == null
          ? null
          : BookResDto.fromJson(json['acntbookInfo'] as Map<String, dynamic>),
      gpsInfo: json['gpsInfo'] == null
          ? null
          : GpsInfo.fromJson(json['gpsInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_TransactionDtoToJson(_$_TransactionDto instance) =>
    <String, dynamic>{
      'trstnDt': instance.createDateTime.toIso8601String(),
      'totPayAmtOrg': instance.totalMoney,
      'totPayAmt': instance.totalConvertedMoney,
      'curMoneyCd': instance.currencyCode,
      'payTypeCd': instance.paymentTypeId,
      'trstnTypeGroupCd': instance.transactionTypeId,
      'memo': instance.description,
      'acntbookInfo': instance.book,
      'gpsInfo': instance.gpsInfo,
    };
