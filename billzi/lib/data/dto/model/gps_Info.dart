import 'package:freezed_annotation/freezed_annotation.dart';

part 'gps_Info.freezed.dart';
part 'gps_Info.g.dart';

@freezed
class GpsInfo with _$GpsInfo {
  factory GpsInfo({
    required double latitude,
    required double longitude,
  }) = _GpsInfo;

  factory GpsInfo.fromJson(Map<String, dynamic> json) =>
      _$GpsInfoFromJson(json);
}