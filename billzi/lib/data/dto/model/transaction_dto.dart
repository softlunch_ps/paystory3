import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/model/gps_Info.dart';
import 'package:billzi/data/dto/model/transaction_category_dto.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';

part 'transaction_dto.freezed.dart';
part 'transaction_dto.g.dart';

@freezed
class TransactionDto with _$TransactionDto {
  factory TransactionDto({

    @JsonKey(name: 'trstnDt') required DateTime createDateTime,
    @JsonKey(name: 'totPayAmtOrg') @Default(0.0) double totalMoney,
    @JsonKey(name: 'totPayAmt') @Default(0.0) double totalConvertedMoney,
    @JsonKey(name: 'curMoneyCd') required String currencyCode,
    @JsonKey(name: 'payTypeCd') String? paymentTypeId,
    @JsonKey(name: 'trstnTypeGroupCd') String? transactionTypeId,
    @JsonKey(name: 'memo') @Default('') String? description,
    @JsonKey(name: 'acntbookInfo') BookResDto? book, //todo change book object
    //@JsonKey(name: 'acntCateInfo') TransactionCategoryDto? category, //todo change catagory object
    GpsInfo? gpsInfo,
  }) = _TransactionDto;

  factory TransactionDto.fromJson(Map<String, dynamic> json) =>
      _$TransactionDtoFromJson(json);
}

// {
//   "userSeq": 900000000000000,
//   "payRcptSeq": 100000000000076,
//   "totPayAmt": 1500.00,
//   "trstnDt": "2021-12-06 16:05:45",
//   "trstnType": "",
//   "trstnTypeCd": "01",
//   "trstnTypeGroupCd": "O",
//   "totPayAmtOrg": 10.00,
//   "memo": "점심 #플렉스 #먹스타",
//   "curMoneyCd": "USD",
//   "gpsInfo": {
//   "latitude": 37.465567,
//   "longitude": 127.035661
//   },
//   "acntbookInfo": {
//     "accountBookSeq": 101000000000000,
//     "accountBookName": "Basic AccountBook",
//     "accountBookDesc": "A AccountBook Made by Paystory present",
//     "imgURL": "https://s3.ap-northeast-2.amazonaws.com/paystory-profiles/pic/luka.png",
//     "color": "red"
//   },
//   "acntCateInfo": {
//     "accountCategorySeq": 770000000010000,
//     "accountCategoryName": "미분류",
//     "typeCd": "0",
//     "imgURL": "ic_r_category_unclassified",
//     "color": "868686"
//     },
//   "crdSmsRawSeq": 
// },