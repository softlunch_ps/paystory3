import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/model/gps_Info.dart';

part 'transaction_category_dto.freezed.dart';
part 'transaction_category_dto.g.dart';

@freezed
class TransactionCategoryDto with _$TransactionCategoryDto {
  factory TransactionCategoryDto({

    @JsonKey(name: 'accountCategorySeq') required int categorySeq,
    @JsonKey(name: 'accountCategoryName') @Default('') String categoryName,
    @JsonKey(name: 'typeCd') required String categoryId,
    @JsonKey(name: 'imgURL') String? imgURL,
    @JsonKey(name: 'color') @Default('000000') String backgroundColor,
    GpsInfo? gpsInfo,
  }) = _TransactionCategoryDto;

  factory TransactionCategoryDto.fromJson(Map<String, dynamic> json) =>
      _$TransactionCategoryDtoFromJson(json);
}