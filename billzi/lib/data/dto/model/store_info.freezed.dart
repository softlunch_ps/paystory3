// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'store_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StoreInfo _$StoreInfoFromJson(Map<String, dynamic> json) {
  return _StoreInfo.fromJson(json);
}

/// @nodoc
class _$StoreInfoTearOff {
  const _$StoreInfoTearOff();

  _StoreInfo call(
      {num? storeSeq,
      String? storeName,
      String? storeNameRcpt,
      String? bizCategory,
      num? categorySeq,
      String? addr1,
      String? addr2,
      String? email,
      String? tel1,
      String? info,
      String? inputCd,
      String? storeNameInputCd,
      GpsInfo? gpsInfo,
      StoreVisitInfo? storeVisitInfo}) {
    return _StoreInfo(
      storeSeq: storeSeq,
      storeName: storeName,
      storeNameRcpt: storeNameRcpt,
      bizCategory: bizCategory,
      categorySeq: categorySeq,
      addr1: addr1,
      addr2: addr2,
      email: email,
      tel1: tel1,
      info: info,
      inputCd: inputCd,
      storeNameInputCd: storeNameInputCd,
      gpsInfo: gpsInfo,
      storeVisitInfo: storeVisitInfo,
    );
  }

  StoreInfo fromJson(Map<String, Object?> json) {
    return StoreInfo.fromJson(json);
  }
}

/// @nodoc
const $StoreInfo = _$StoreInfoTearOff();

/// @nodoc
mixin _$StoreInfo {
  num? get storeSeq => throw _privateConstructorUsedError;
  String? get storeName => throw _privateConstructorUsedError;
  String? get storeNameRcpt => throw _privateConstructorUsedError;
  String? get bizCategory => throw _privateConstructorUsedError;
  num? get categorySeq => throw _privateConstructorUsedError;
  String? get addr1 => throw _privateConstructorUsedError;
  String? get addr2 => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get tel1 => throw _privateConstructorUsedError;
  String? get info => throw _privateConstructorUsedError;
  String? get inputCd => throw _privateConstructorUsedError;
  String? get storeNameInputCd => throw _privateConstructorUsedError;
  GpsInfo? get gpsInfo => throw _privateConstructorUsedError;
  StoreVisitInfo? get storeVisitInfo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StoreInfoCopyWith<StoreInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StoreInfoCopyWith<$Res> {
  factory $StoreInfoCopyWith(StoreInfo value, $Res Function(StoreInfo) then) =
      _$StoreInfoCopyWithImpl<$Res>;
  $Res call(
      {num? storeSeq,
      String? storeName,
      String? storeNameRcpt,
      String? bizCategory,
      num? categorySeq,
      String? addr1,
      String? addr2,
      String? email,
      String? tel1,
      String? info,
      String? inputCd,
      String? storeNameInputCd,
      GpsInfo? gpsInfo,
      StoreVisitInfo? storeVisitInfo});

  $GpsInfoCopyWith<$Res>? get gpsInfo;
  $StoreVisitInfoCopyWith<$Res>? get storeVisitInfo;
}

/// @nodoc
class _$StoreInfoCopyWithImpl<$Res> implements $StoreInfoCopyWith<$Res> {
  _$StoreInfoCopyWithImpl(this._value, this._then);

  final StoreInfo _value;
  // ignore: unused_field
  final $Res Function(StoreInfo) _then;

  @override
  $Res call({
    Object? storeSeq = freezed,
    Object? storeName = freezed,
    Object? storeNameRcpt = freezed,
    Object? bizCategory = freezed,
    Object? categorySeq = freezed,
    Object? addr1 = freezed,
    Object? addr2 = freezed,
    Object? email = freezed,
    Object? tel1 = freezed,
    Object? info = freezed,
    Object? inputCd = freezed,
    Object? storeNameInputCd = freezed,
    Object? gpsInfo = freezed,
    Object? storeVisitInfo = freezed,
  }) {
    return _then(_value.copyWith(
      storeSeq: storeSeq == freezed
          ? _value.storeSeq
          : storeSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      storeName: storeName == freezed
          ? _value.storeName
          : storeName // ignore: cast_nullable_to_non_nullable
              as String?,
      storeNameRcpt: storeNameRcpt == freezed
          ? _value.storeNameRcpt
          : storeNameRcpt // ignore: cast_nullable_to_non_nullable
              as String?,
      bizCategory: bizCategory == freezed
          ? _value.bizCategory
          : bizCategory // ignore: cast_nullable_to_non_nullable
              as String?,
      categorySeq: categorySeq == freezed
          ? _value.categorySeq
          : categorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
      addr1: addr1 == freezed
          ? _value.addr1
          : addr1 // ignore: cast_nullable_to_non_nullable
              as String?,
      addr2: addr2 == freezed
          ? _value.addr2
          : addr2 // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      tel1: tel1 == freezed
          ? _value.tel1
          : tel1 // ignore: cast_nullable_to_non_nullable
              as String?,
      info: info == freezed
          ? _value.info
          : info // ignore: cast_nullable_to_non_nullable
              as String?,
      inputCd: inputCd == freezed
          ? _value.inputCd
          : inputCd // ignore: cast_nullable_to_non_nullable
              as String?,
      storeNameInputCd: storeNameInputCd == freezed
          ? _value.storeNameInputCd
          : storeNameInputCd // ignore: cast_nullable_to_non_nullable
              as String?,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
      storeVisitInfo: storeVisitInfo == freezed
          ? _value.storeVisitInfo
          : storeVisitInfo // ignore: cast_nullable_to_non_nullable
              as StoreVisitInfo?,
    ));
  }

  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo {
    if (_value.gpsInfo == null) {
      return null;
    }

    return $GpsInfoCopyWith<$Res>(_value.gpsInfo!, (value) {
      return _then(_value.copyWith(gpsInfo: value));
    });
  }

  @override
  $StoreVisitInfoCopyWith<$Res>? get storeVisitInfo {
    if (_value.storeVisitInfo == null) {
      return null;
    }

    return $StoreVisitInfoCopyWith<$Res>(_value.storeVisitInfo!, (value) {
      return _then(_value.copyWith(storeVisitInfo: value));
    });
  }
}

/// @nodoc
abstract class _$StoreInfoCopyWith<$Res> implements $StoreInfoCopyWith<$Res> {
  factory _$StoreInfoCopyWith(
          _StoreInfo value, $Res Function(_StoreInfo) then) =
      __$StoreInfoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? storeSeq,
      String? storeName,
      String? storeNameRcpt,
      String? bizCategory,
      num? categorySeq,
      String? addr1,
      String? addr2,
      String? email,
      String? tel1,
      String? info,
      String? inputCd,
      String? storeNameInputCd,
      GpsInfo? gpsInfo,
      StoreVisitInfo? storeVisitInfo});

  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo;
  @override
  $StoreVisitInfoCopyWith<$Res>? get storeVisitInfo;
}

/// @nodoc
class __$StoreInfoCopyWithImpl<$Res> extends _$StoreInfoCopyWithImpl<$Res>
    implements _$StoreInfoCopyWith<$Res> {
  __$StoreInfoCopyWithImpl(_StoreInfo _value, $Res Function(_StoreInfo) _then)
      : super(_value, (v) => _then(v as _StoreInfo));

  @override
  _StoreInfo get _value => super._value as _StoreInfo;

  @override
  $Res call({
    Object? storeSeq = freezed,
    Object? storeName = freezed,
    Object? storeNameRcpt = freezed,
    Object? bizCategory = freezed,
    Object? categorySeq = freezed,
    Object? addr1 = freezed,
    Object? addr2 = freezed,
    Object? email = freezed,
    Object? tel1 = freezed,
    Object? info = freezed,
    Object? inputCd = freezed,
    Object? storeNameInputCd = freezed,
    Object? gpsInfo = freezed,
    Object? storeVisitInfo = freezed,
  }) {
    return _then(_StoreInfo(
      storeSeq: storeSeq == freezed
          ? _value.storeSeq
          : storeSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      storeName: storeName == freezed
          ? _value.storeName
          : storeName // ignore: cast_nullable_to_non_nullable
              as String?,
      storeNameRcpt: storeNameRcpt == freezed
          ? _value.storeNameRcpt
          : storeNameRcpt // ignore: cast_nullable_to_non_nullable
              as String?,
      bizCategory: bizCategory == freezed
          ? _value.bizCategory
          : bizCategory // ignore: cast_nullable_to_non_nullable
              as String?,
      categorySeq: categorySeq == freezed
          ? _value.categorySeq
          : categorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
      addr1: addr1 == freezed
          ? _value.addr1
          : addr1 // ignore: cast_nullable_to_non_nullable
              as String?,
      addr2: addr2 == freezed
          ? _value.addr2
          : addr2 // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      tel1: tel1 == freezed
          ? _value.tel1
          : tel1 // ignore: cast_nullable_to_non_nullable
              as String?,
      info: info == freezed
          ? _value.info
          : info // ignore: cast_nullable_to_non_nullable
              as String?,
      inputCd: inputCd == freezed
          ? _value.inputCd
          : inputCd // ignore: cast_nullable_to_non_nullable
              as String?,
      storeNameInputCd: storeNameInputCd == freezed
          ? _value.storeNameInputCd
          : storeNameInputCd // ignore: cast_nullable_to_non_nullable
              as String?,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
      storeVisitInfo: storeVisitInfo == freezed
          ? _value.storeVisitInfo
          : storeVisitInfo // ignore: cast_nullable_to_non_nullable
              as StoreVisitInfo?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_StoreInfo implements _StoreInfo {
  _$_StoreInfo(
      {this.storeSeq,
      this.storeName,
      this.storeNameRcpt,
      this.bizCategory,
      this.categorySeq,
      this.addr1,
      this.addr2,
      this.email,
      this.tel1,
      this.info,
      this.inputCd,
      this.storeNameInputCd,
      this.gpsInfo,
      this.storeVisitInfo});

  factory _$_StoreInfo.fromJson(Map<String, dynamic> json) =>
      _$$_StoreInfoFromJson(json);

  @override
  final num? storeSeq;
  @override
  final String? storeName;
  @override
  final String? storeNameRcpt;
  @override
  final String? bizCategory;
  @override
  final num? categorySeq;
  @override
  final String? addr1;
  @override
  final String? addr2;
  @override
  final String? email;
  @override
  final String? tel1;
  @override
  final String? info;
  @override
  final String? inputCd;
  @override
  final String? storeNameInputCd;
  @override
  final GpsInfo? gpsInfo;
  @override
  final StoreVisitInfo? storeVisitInfo;

  @override
  String toString() {
    return 'StoreInfo(storeSeq: $storeSeq, storeName: $storeName, storeNameRcpt: $storeNameRcpt, bizCategory: $bizCategory, categorySeq: $categorySeq, addr1: $addr1, addr2: $addr2, email: $email, tel1: $tel1, info: $info, inputCd: $inputCd, storeNameInputCd: $storeNameInputCd, gpsInfo: $gpsInfo, storeVisitInfo: $storeVisitInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _StoreInfo &&
            (identical(other.storeSeq, storeSeq) ||
                other.storeSeq == storeSeq) &&
            (identical(other.storeName, storeName) ||
                other.storeName == storeName) &&
            (identical(other.storeNameRcpt, storeNameRcpt) ||
                other.storeNameRcpt == storeNameRcpt) &&
            (identical(other.bizCategory, bizCategory) ||
                other.bizCategory == bizCategory) &&
            (identical(other.categorySeq, categorySeq) ||
                other.categorySeq == categorySeq) &&
            (identical(other.addr1, addr1) || other.addr1 == addr1) &&
            (identical(other.addr2, addr2) || other.addr2 == addr2) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.tel1, tel1) || other.tel1 == tel1) &&
            (identical(other.info, info) || other.info == info) &&
            (identical(other.inputCd, inputCd) || other.inputCd == inputCd) &&
            (identical(other.storeNameInputCd, storeNameInputCd) ||
                other.storeNameInputCd == storeNameInputCd) &&
            (identical(other.gpsInfo, gpsInfo) || other.gpsInfo == gpsInfo) &&
            (identical(other.storeVisitInfo, storeVisitInfo) ||
                other.storeVisitInfo == storeVisitInfo));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      storeSeq,
      storeName,
      storeNameRcpt,
      bizCategory,
      categorySeq,
      addr1,
      addr2,
      email,
      tel1,
      info,
      inputCd,
      storeNameInputCd,
      gpsInfo,
      storeVisitInfo);

  @JsonKey(ignore: true)
  @override
  _$StoreInfoCopyWith<_StoreInfo> get copyWith =>
      __$StoreInfoCopyWithImpl<_StoreInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StoreInfoToJson(this);
  }
}

abstract class _StoreInfo implements StoreInfo {
  factory _StoreInfo(
      {num? storeSeq,
      String? storeName,
      String? storeNameRcpt,
      String? bizCategory,
      num? categorySeq,
      String? addr1,
      String? addr2,
      String? email,
      String? tel1,
      String? info,
      String? inputCd,
      String? storeNameInputCd,
      GpsInfo? gpsInfo,
      StoreVisitInfo? storeVisitInfo}) = _$_StoreInfo;

  factory _StoreInfo.fromJson(Map<String, dynamic> json) =
      _$_StoreInfo.fromJson;

  @override
  num? get storeSeq;
  @override
  String? get storeName;
  @override
  String? get storeNameRcpt;
  @override
  String? get bizCategory;
  @override
  num? get categorySeq;
  @override
  String? get addr1;
  @override
  String? get addr2;
  @override
  String? get email;
  @override
  String? get tel1;
  @override
  String? get info;
  @override
  String? get inputCd;
  @override
  String? get storeNameInputCd;
  @override
  GpsInfo? get gpsInfo;
  @override
  StoreVisitInfo? get storeVisitInfo;
  @override
  @JsonKey(ignore: true)
  _$StoreInfoCopyWith<_StoreInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
