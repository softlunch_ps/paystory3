// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'transaction_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TransactionDto _$TransactionDtoFromJson(Map<String, dynamic> json) {
  return _TransactionDto.fromJson(json);
}

/// @nodoc
class _$TransactionDtoTearOff {
  const _$TransactionDtoTearOff();

  _TransactionDto call(
      {@JsonKey(name: 'trstnDt') required DateTime createDateTime,
      @JsonKey(name: 'totPayAmtOrg') double totalMoney = 0.0,
      @JsonKey(name: 'totPayAmt') double totalConvertedMoney = 0.0,
      @JsonKey(name: 'curMoneyCd') required String currencyCode,
      @JsonKey(name: 'payTypeCd') String? paymentTypeId,
      @JsonKey(name: 'trstnTypeGroupCd') String? transactionTypeId,
      @JsonKey(name: 'memo') String? description = '',
      @JsonKey(name: 'acntbookInfo') BookResDto? book,
      GpsInfo? gpsInfo}) {
    return _TransactionDto(
      createDateTime: createDateTime,
      totalMoney: totalMoney,
      totalConvertedMoney: totalConvertedMoney,
      currencyCode: currencyCode,
      paymentTypeId: paymentTypeId,
      transactionTypeId: transactionTypeId,
      description: description,
      book: book,
      gpsInfo: gpsInfo,
    );
  }

  TransactionDto fromJson(Map<String, Object?> json) {
    return TransactionDto.fromJson(json);
  }
}

/// @nodoc
const $TransactionDto = _$TransactionDtoTearOff();

/// @nodoc
mixin _$TransactionDto {
  @JsonKey(name: 'trstnDt')
  DateTime get createDateTime => throw _privateConstructorUsedError;
  @JsonKey(name: 'totPayAmtOrg')
  double get totalMoney => throw _privateConstructorUsedError;
  @JsonKey(name: 'totPayAmt')
  double get totalConvertedMoney => throw _privateConstructorUsedError;
  @JsonKey(name: 'curMoneyCd')
  String get currencyCode => throw _privateConstructorUsedError;
  @JsonKey(name: 'payTypeCd')
  String? get paymentTypeId => throw _privateConstructorUsedError;
  @JsonKey(name: 'trstnTypeGroupCd')
  String? get transactionTypeId => throw _privateConstructorUsedError;
  @JsonKey(name: 'memo')
  String? get description => throw _privateConstructorUsedError;
  @JsonKey(name: 'acntbookInfo')
  BookResDto? get book =>
      throw _privateConstructorUsedError; //todo change book object
//@JsonKey(name: 'acntCateInfo') TransactionCategoryDto? category, //todo change catagory object
  GpsInfo? get gpsInfo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionDtoCopyWith<TransactionDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionDtoCopyWith<$Res> {
  factory $TransactionDtoCopyWith(
          TransactionDto value, $Res Function(TransactionDto) then) =
      _$TransactionDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'trstnDt') DateTime createDateTime,
      @JsonKey(name: 'totPayAmtOrg') double totalMoney,
      @JsonKey(name: 'totPayAmt') double totalConvertedMoney,
      @JsonKey(name: 'curMoneyCd') String currencyCode,
      @JsonKey(name: 'payTypeCd') String? paymentTypeId,
      @JsonKey(name: 'trstnTypeGroupCd') String? transactionTypeId,
      @JsonKey(name: 'memo') String? description,
      @JsonKey(name: 'acntbookInfo') BookResDto? book,
      GpsInfo? gpsInfo});

  $BookResDtoCopyWith<$Res>? get book;
  $GpsInfoCopyWith<$Res>? get gpsInfo;
}

/// @nodoc
class _$TransactionDtoCopyWithImpl<$Res>
    implements $TransactionDtoCopyWith<$Res> {
  _$TransactionDtoCopyWithImpl(this._value, this._then);

  final TransactionDto _value;
  // ignore: unused_field
  final $Res Function(TransactionDto) _then;

  @override
  $Res call({
    Object? createDateTime = freezed,
    Object? totalMoney = freezed,
    Object? totalConvertedMoney = freezed,
    Object? currencyCode = freezed,
    Object? paymentTypeId = freezed,
    Object? transactionTypeId = freezed,
    Object? description = freezed,
    Object? book = freezed,
    Object? gpsInfo = freezed,
  }) {
    return _then(_value.copyWith(
      createDateTime: createDateTime == freezed
          ? _value.createDateTime
          : createDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      totalMoney: totalMoney == freezed
          ? _value.totalMoney
          : totalMoney // ignore: cast_nullable_to_non_nullable
              as double,
      totalConvertedMoney: totalConvertedMoney == freezed
          ? _value.totalConvertedMoney
          : totalConvertedMoney // ignore: cast_nullable_to_non_nullable
              as double,
      currencyCode: currencyCode == freezed
          ? _value.currencyCode
          : currencyCode // ignore: cast_nullable_to_non_nullable
              as String,
      paymentTypeId: paymentTypeId == freezed
          ? _value.paymentTypeId
          : paymentTypeId // ignore: cast_nullable_to_non_nullable
              as String?,
      transactionTypeId: transactionTypeId == freezed
          ? _value.transactionTypeId
          : transactionTypeId // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      book: book == freezed
          ? _value.book
          : book // ignore: cast_nullable_to_non_nullable
              as BookResDto?,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
    ));
  }

  @override
  $BookResDtoCopyWith<$Res>? get book {
    if (_value.book == null) {
      return null;
    }

    return $BookResDtoCopyWith<$Res>(_value.book!, (value) {
      return _then(_value.copyWith(book: value));
    });
  }

  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo {
    if (_value.gpsInfo == null) {
      return null;
    }

    return $GpsInfoCopyWith<$Res>(_value.gpsInfo!, (value) {
      return _then(_value.copyWith(gpsInfo: value));
    });
  }
}

/// @nodoc
abstract class _$TransactionDtoCopyWith<$Res>
    implements $TransactionDtoCopyWith<$Res> {
  factory _$TransactionDtoCopyWith(
          _TransactionDto value, $Res Function(_TransactionDto) then) =
      __$TransactionDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'trstnDt') DateTime createDateTime,
      @JsonKey(name: 'totPayAmtOrg') double totalMoney,
      @JsonKey(name: 'totPayAmt') double totalConvertedMoney,
      @JsonKey(name: 'curMoneyCd') String currencyCode,
      @JsonKey(name: 'payTypeCd') String? paymentTypeId,
      @JsonKey(name: 'trstnTypeGroupCd') String? transactionTypeId,
      @JsonKey(name: 'memo') String? description,
      @JsonKey(name: 'acntbookInfo') BookResDto? book,
      GpsInfo? gpsInfo});

  @override
  $BookResDtoCopyWith<$Res>? get book;
  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo;
}

/// @nodoc
class __$TransactionDtoCopyWithImpl<$Res>
    extends _$TransactionDtoCopyWithImpl<$Res>
    implements _$TransactionDtoCopyWith<$Res> {
  __$TransactionDtoCopyWithImpl(
      _TransactionDto _value, $Res Function(_TransactionDto) _then)
      : super(_value, (v) => _then(v as _TransactionDto));

  @override
  _TransactionDto get _value => super._value as _TransactionDto;

  @override
  $Res call({
    Object? createDateTime = freezed,
    Object? totalMoney = freezed,
    Object? totalConvertedMoney = freezed,
    Object? currencyCode = freezed,
    Object? paymentTypeId = freezed,
    Object? transactionTypeId = freezed,
    Object? description = freezed,
    Object? book = freezed,
    Object? gpsInfo = freezed,
  }) {
    return _then(_TransactionDto(
      createDateTime: createDateTime == freezed
          ? _value.createDateTime
          : createDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      totalMoney: totalMoney == freezed
          ? _value.totalMoney
          : totalMoney // ignore: cast_nullable_to_non_nullable
              as double,
      totalConvertedMoney: totalConvertedMoney == freezed
          ? _value.totalConvertedMoney
          : totalConvertedMoney // ignore: cast_nullable_to_non_nullable
              as double,
      currencyCode: currencyCode == freezed
          ? _value.currencyCode
          : currencyCode // ignore: cast_nullable_to_non_nullable
              as String,
      paymentTypeId: paymentTypeId == freezed
          ? _value.paymentTypeId
          : paymentTypeId // ignore: cast_nullable_to_non_nullable
              as String?,
      transactionTypeId: transactionTypeId == freezed
          ? _value.transactionTypeId
          : transactionTypeId // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      book: book == freezed
          ? _value.book
          : book // ignore: cast_nullable_to_non_nullable
              as BookResDto?,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TransactionDto implements _TransactionDto {
  _$_TransactionDto(
      {@JsonKey(name: 'trstnDt') required this.createDateTime,
      @JsonKey(name: 'totPayAmtOrg') this.totalMoney = 0.0,
      @JsonKey(name: 'totPayAmt') this.totalConvertedMoney = 0.0,
      @JsonKey(name: 'curMoneyCd') required this.currencyCode,
      @JsonKey(name: 'payTypeCd') this.paymentTypeId,
      @JsonKey(name: 'trstnTypeGroupCd') this.transactionTypeId,
      @JsonKey(name: 'memo') this.description = '',
      @JsonKey(name: 'acntbookInfo') this.book,
      this.gpsInfo});

  factory _$_TransactionDto.fromJson(Map<String, dynamic> json) =>
      _$$_TransactionDtoFromJson(json);

  @override
  @JsonKey(name: 'trstnDt')
  final DateTime createDateTime;
  @override
  @JsonKey(name: 'totPayAmtOrg')
  final double totalMoney;
  @override
  @JsonKey(name: 'totPayAmt')
  final double totalConvertedMoney;
  @override
  @JsonKey(name: 'curMoneyCd')
  final String currencyCode;
  @override
  @JsonKey(name: 'payTypeCd')
  final String? paymentTypeId;
  @override
  @JsonKey(name: 'trstnTypeGroupCd')
  final String? transactionTypeId;
  @override
  @JsonKey(name: 'memo')
  final String? description;
  @override
  @JsonKey(name: 'acntbookInfo')
  final BookResDto? book;
  @override //todo change book object
//@JsonKey(name: 'acntCateInfo') TransactionCategoryDto? category, //todo change catagory object
  final GpsInfo? gpsInfo;

  @override
  String toString() {
    return 'TransactionDto(createDateTime: $createDateTime, totalMoney: $totalMoney, totalConvertedMoney: $totalConvertedMoney, currencyCode: $currencyCode, paymentTypeId: $paymentTypeId, transactionTypeId: $transactionTypeId, description: $description, book: $book, gpsInfo: $gpsInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TransactionDto &&
            (identical(other.createDateTime, createDateTime) ||
                other.createDateTime == createDateTime) &&
            (identical(other.totalMoney, totalMoney) ||
                other.totalMoney == totalMoney) &&
            (identical(other.totalConvertedMoney, totalConvertedMoney) ||
                other.totalConvertedMoney == totalConvertedMoney) &&
            (identical(other.currencyCode, currencyCode) ||
                other.currencyCode == currencyCode) &&
            (identical(other.paymentTypeId, paymentTypeId) ||
                other.paymentTypeId == paymentTypeId) &&
            (identical(other.transactionTypeId, transactionTypeId) ||
                other.transactionTypeId == transactionTypeId) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.book, book) || other.book == book) &&
            (identical(other.gpsInfo, gpsInfo) || other.gpsInfo == gpsInfo));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      createDateTime,
      totalMoney,
      totalConvertedMoney,
      currencyCode,
      paymentTypeId,
      transactionTypeId,
      description,
      book,
      gpsInfo);

  @JsonKey(ignore: true)
  @override
  _$TransactionDtoCopyWith<_TransactionDto> get copyWith =>
      __$TransactionDtoCopyWithImpl<_TransactionDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TransactionDtoToJson(this);
  }
}

abstract class _TransactionDto implements TransactionDto {
  factory _TransactionDto(
      {@JsonKey(name: 'trstnDt') required DateTime createDateTime,
      @JsonKey(name: 'totPayAmtOrg') double totalMoney,
      @JsonKey(name: 'totPayAmt') double totalConvertedMoney,
      @JsonKey(name: 'curMoneyCd') required String currencyCode,
      @JsonKey(name: 'payTypeCd') String? paymentTypeId,
      @JsonKey(name: 'trstnTypeGroupCd') String? transactionTypeId,
      @JsonKey(name: 'memo') String? description,
      @JsonKey(name: 'acntbookInfo') BookResDto? book,
      GpsInfo? gpsInfo}) = _$_TransactionDto;

  factory _TransactionDto.fromJson(Map<String, dynamic> json) =
      _$_TransactionDto.fromJson;

  @override
  @JsonKey(name: 'trstnDt')
  DateTime get createDateTime;
  @override
  @JsonKey(name: 'totPayAmtOrg')
  double get totalMoney;
  @override
  @JsonKey(name: 'totPayAmt')
  double get totalConvertedMoney;
  @override
  @JsonKey(name: 'curMoneyCd')
  String get currencyCode;
  @override
  @JsonKey(name: 'payTypeCd')
  String? get paymentTypeId;
  @override
  @JsonKey(name: 'trstnTypeGroupCd')
  String? get transactionTypeId;
  @override
  @JsonKey(name: 'memo')
  String? get description;
  @override
  @JsonKey(name: 'acntbookInfo')
  BookResDto? get book;
  @override //todo change book object
//@JsonKey(name: 'acntCateInfo') TransactionCategoryDto? category, //todo change catagory object
  GpsInfo? get gpsInfo;
  @override
  @JsonKey(ignore: true)
  _$TransactionDtoCopyWith<_TransactionDto> get copyWith =>
      throw _privateConstructorUsedError;
}
