// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'account_book.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AccountBook _$AccountBookFromJson(Map<String, dynamic> json) {
  return _AccountBook.fromJson(json);
}

/// @nodoc
class _$AccountBookTearOff {
  const _$AccountBookTearOff();

  _AccountBook call({String? startDate}) {
    return _AccountBook(
      startDate: startDate,
    );
  }

  AccountBook fromJson(Map<String, Object?> json) {
    return AccountBook.fromJson(json);
  }
}

/// @nodoc
const $AccountBook = _$AccountBookTearOff();

/// @nodoc
mixin _$AccountBook {
  String? get startDate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AccountBookCopyWith<AccountBook> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AccountBookCopyWith<$Res> {
  factory $AccountBookCopyWith(
          AccountBook value, $Res Function(AccountBook) then) =
      _$AccountBookCopyWithImpl<$Res>;
  $Res call({String? startDate});
}

/// @nodoc
class _$AccountBookCopyWithImpl<$Res> implements $AccountBookCopyWith<$Res> {
  _$AccountBookCopyWithImpl(this._value, this._then);

  final AccountBook _value;
  // ignore: unused_field
  final $Res Function(AccountBook) _then;

  @override
  $Res call({
    Object? startDate = freezed,
  }) {
    return _then(_value.copyWith(
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$AccountBookCopyWith<$Res>
    implements $AccountBookCopyWith<$Res> {
  factory _$AccountBookCopyWith(
          _AccountBook value, $Res Function(_AccountBook) then) =
      __$AccountBookCopyWithImpl<$Res>;
  @override
  $Res call({String? startDate});
}

/// @nodoc
class __$AccountBookCopyWithImpl<$Res> extends _$AccountBookCopyWithImpl<$Res>
    implements _$AccountBookCopyWith<$Res> {
  __$AccountBookCopyWithImpl(
      _AccountBook _value, $Res Function(_AccountBook) _then)
      : super(_value, (v) => _then(v as _AccountBook));

  @override
  _AccountBook get _value => super._value as _AccountBook;

  @override
  $Res call({
    Object? startDate = freezed,
  }) {
    return _then(_AccountBook(
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AccountBook implements _AccountBook {
  _$_AccountBook({this.startDate});

  factory _$_AccountBook.fromJson(Map<String, dynamic> json) =>
      _$$_AccountBookFromJson(json);

  @override
  final String? startDate;

  @override
  String toString() {
    return 'AccountBook(startDate: $startDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AccountBook &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate));
  }

  @override
  int get hashCode => Object.hash(runtimeType, startDate);

  @JsonKey(ignore: true)
  @override
  _$AccountBookCopyWith<_AccountBook> get copyWith =>
      __$AccountBookCopyWithImpl<_AccountBook>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AccountBookToJson(this);
  }
}

abstract class _AccountBook implements AccountBook {
  factory _AccountBook({String? startDate}) = _$_AccountBook;

  factory _AccountBook.fromJson(Map<String, dynamic> json) =
      _$_AccountBook.fromJson;

  @override
  String? get startDate;
  @override
  @JsonKey(ignore: true)
  _$AccountBookCopyWith<_AccountBook> get copyWith =>
      throw _privateConstructorUsedError;
}
