// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'budget_category_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BudgetCategoryItem _$$_BudgetCategoryItemFromJson(
        Map<String, dynamic> json) =>
    _$_BudgetCategoryItem(
      accountCategorySeq: json['accountCategorySeq'] as num?,
      accountCategoryName: json['accountCategoryName'] as String?,
      userSeq: json['userSeq'] as num?,
      budgetAmt: (json['budgetAmt'] as num?)?.toDouble(),
      totPayAmt: (json['totPayAmt'] as num?)?.toDouble(),
      budgetBalanceAmt: (json['budgetBalanceAmt'] as num?)?.toDouble(),
      supAccountCategorySeq: json['supAccountCategorySeq'] as String?,
      color: json['color'] as String?,
      typeCd: json['typeCd'] as String?,
      imgURL: json['imgURL'] as String?,
    );

Map<String, dynamic> _$$_BudgetCategoryItemToJson(
        _$_BudgetCategoryItem instance) =>
    <String, dynamic>{
      'accountCategorySeq': instance.accountCategorySeq,
      'accountCategoryName': instance.accountCategoryName,
      'userSeq': instance.userSeq,
      'budgetAmt': instance.budgetAmt,
      'totPayAmt': instance.totPayAmt,
      'budgetBalanceAmt': instance.budgetBalanceAmt,
      'supAccountCategorySeq': instance.supAccountCategorySeq,
      'color': instance.color,
      'typeCd': instance.typeCd,
      'imgURL': instance.imgURL,
    };
