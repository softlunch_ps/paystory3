import 'package:freezed_annotation/freezed_annotation.dart';

part 'store_visit_info.freezed.dart';
part 'store_visit_info.g.dart';

@freezed
class StoreVisitInfo with _$StoreVisitInfo {
  factory StoreVisitInfo({
    num? visitCount,
    String? firstVisitDt,
    String? recentVisitDt,
  }) = _StoreVisitInfo;

  factory StoreVisitInfo.fromJson(Map<String, dynamic> json) =>
      _$StoreVisitInfoFromJson(json);
}