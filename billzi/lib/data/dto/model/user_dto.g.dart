// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserDto _$$_UserDtoFromJson(Map<String, dynamic> json) => _$_UserDto(
      userSeq: json['userSeq'] as num?,
      userName: json['userName'] as String? ?? 'NotUsed',
      userId: json['userId'] as String?,
      prtyUserId: json['prtyUserId'] as String?,
      userNickname: json['userNickname'] as String? ?? 'NotUsed',
      password: json['password'] as String? ?? 'NotUsed',
      fcmClientToken: json['fcmClientToken'] as String? ?? 'NotUsed',
      delYN: json['delYN'] as String?,
      crtDt: json['crtDt'] as String?,
      updtDt: json['updtDt'] as String?,
      email: json['email'] as String? ?? '',
      lastAuthDt: json['lastAuthDt'] as String?,
      userProfileImgUrl: json['userProfileImgUrl'] as String? ?? '',
      birthDate: json['birthDate'] as String? ?? '',
      ageMin: json['ageMin'] as int?,
      ageMax: json['ageMax'] as int?,
      gender: json['gender'] as String? ?? '',
      phoneNum1: json['phoneNum1'] as String? ?? '',
      phoneNum2: json['phoneNum2'] as String? ?? '',
      address: json['address'] as String? ?? '',
      userTypeCd: json['userTypeCd'] as String? ?? '1',
      authTypeCd: json['authTypeCd'] as String? ?? '0',
      followCd: json['followCd'] as String?,
      locked: json['locked'] as String?,
      adId: json['adId'] as String?,
    );

Map<String, dynamic> _$$_UserDtoToJson(_$_UserDto instance) =>
    <String, dynamic>{
      'userSeq': instance.userSeq,
      'userName': instance.userName,
      'userId': instance.userId,
      'prtyUserId': instance.prtyUserId,
      'userNickname': instance.userNickname,
      'password': instance.password,
      'fcmClientToken': instance.fcmClientToken,
      'delYN': instance.delYN,
      'crtDt': instance.crtDt,
      'updtDt': instance.updtDt,
      'email': instance.email,
      'lastAuthDt': instance.lastAuthDt,
      'userProfileImgUrl': instance.userProfileImgUrl,
      'birthDate': instance.birthDate,
      'ageMin': instance.ageMin,
      'ageMax': instance.ageMax,
      'gender': instance.gender,
      'phoneNum1': instance.phoneNum1,
      'phoneNum2': instance.phoneNum2,
      'address': instance.address,
      'userTypeCd': instance.userTypeCd,
      'authTypeCd': instance.authTypeCd,
      'followCd': instance.followCd,
      'locked': instance.locked,
      'adId': instance.adId,
    };
