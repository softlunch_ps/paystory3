import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_dto.freezed.dart';
part 'auth_dto.g.dart';

@freezed
class AuthDto with _$AuthDto {
  factory AuthDto({
    num? userSeq,
    String? userId,
    String? prtyUserId,
    @Default('NotUsed') String? userNickname,
    @Default('NotUsed') String? password,
    @Default('NotUsed') String? fcmClientToken,
    String? email,
    String? lastAuthDt,
    String? userProfileImgUrl,
    String? birthDate,
    String? phoneNum1,
    String? phoneNum2,
    @Default('0') String? authTypeCd,
    String? token,
    String? expireDate,
    String? authorities,

  }) = _AuthDto;

  factory AuthDto.fromJson(Map<String, dynamic> json) =>
      _$AuthDtoFromJson(json);
}