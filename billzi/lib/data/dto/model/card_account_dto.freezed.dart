// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'card_account_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CardAccountDto _$CardAccountDtoFromJson(Map<String, dynamic> json) {
  return _CardAccountDto.fromJson(json);
}

/// @nodoc
class _$CardAccountDtoTearOff {
  const _$CardAccountDtoTearOff();

  _CardAccountDto call(
      {num? userSeq,
      String? title = '',
      String? crdName = '',
      String? crdNum = '0000',
      String? crdIssueCmpnyCd = '00',
      String? accountTypeCd = '',
      String? memo = '',
      String? useYN = '',
      String? inputCd = '',
      String? updtDt}) {
    return _CardAccountDto(
      userSeq: userSeq,
      title: title,
      crdName: crdName,
      crdNum: crdNum,
      crdIssueCmpnyCd: crdIssueCmpnyCd,
      accountTypeCd: accountTypeCd,
      memo: memo,
      useYN: useYN,
      inputCd: inputCd,
      updtDt: updtDt,
    );
  }

  CardAccountDto fromJson(Map<String, Object?> json) {
    return CardAccountDto.fromJson(json);
  }
}

/// @nodoc
const $CardAccountDto = _$CardAccountDtoTearOff();

/// @nodoc
mixin _$CardAccountDto {
  num? get userSeq => throw _privateConstructorUsedError;
  String? get title => throw _privateConstructorUsedError;
  String? get crdName => throw _privateConstructorUsedError;
  String? get crdNum => throw _privateConstructorUsedError;
  String? get crdIssueCmpnyCd => throw _privateConstructorUsedError;
  String? get accountTypeCd => throw _privateConstructorUsedError;
  String? get memo => throw _privateConstructorUsedError;
  String? get useYN => throw _privateConstructorUsedError;
  String? get inputCd => throw _privateConstructorUsedError;
  String? get updtDt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CardAccountDtoCopyWith<CardAccountDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CardAccountDtoCopyWith<$Res> {
  factory $CardAccountDtoCopyWith(
          CardAccountDto value, $Res Function(CardAccountDto) then) =
      _$CardAccountDtoCopyWithImpl<$Res>;
  $Res call(
      {num? userSeq,
      String? title,
      String? crdName,
      String? crdNum,
      String? crdIssueCmpnyCd,
      String? accountTypeCd,
      String? memo,
      String? useYN,
      String? inputCd,
      String? updtDt});
}

/// @nodoc
class _$CardAccountDtoCopyWithImpl<$Res>
    implements $CardAccountDtoCopyWith<$Res> {
  _$CardAccountDtoCopyWithImpl(this._value, this._then);

  final CardAccountDto _value;
  // ignore: unused_field
  final $Res Function(CardAccountDto) _then;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? title = freezed,
    Object? crdName = freezed,
    Object? crdNum = freezed,
    Object? crdIssueCmpnyCd = freezed,
    Object? accountTypeCd = freezed,
    Object? memo = freezed,
    Object? useYN = freezed,
    Object? inputCd = freezed,
    Object? updtDt = freezed,
  }) {
    return _then(_value.copyWith(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      crdName: crdName == freezed
          ? _value.crdName
          : crdName // ignore: cast_nullable_to_non_nullable
              as String?,
      crdNum: crdNum == freezed
          ? _value.crdNum
          : crdNum // ignore: cast_nullable_to_non_nullable
              as String?,
      crdIssueCmpnyCd: crdIssueCmpnyCd == freezed
          ? _value.crdIssueCmpnyCd
          : crdIssueCmpnyCd // ignore: cast_nullable_to_non_nullable
              as String?,
      accountTypeCd: accountTypeCd == freezed
          ? _value.accountTypeCd
          : accountTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      memo: memo == freezed
          ? _value.memo
          : memo // ignore: cast_nullable_to_non_nullable
              as String?,
      useYN: useYN == freezed
          ? _value.useYN
          : useYN // ignore: cast_nullable_to_non_nullable
              as String?,
      inputCd: inputCd == freezed
          ? _value.inputCd
          : inputCd // ignore: cast_nullable_to_non_nullable
              as String?,
      updtDt: updtDt == freezed
          ? _value.updtDt
          : updtDt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$CardAccountDtoCopyWith<$Res>
    implements $CardAccountDtoCopyWith<$Res> {
  factory _$CardAccountDtoCopyWith(
          _CardAccountDto value, $Res Function(_CardAccountDto) then) =
      __$CardAccountDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? userSeq,
      String? title,
      String? crdName,
      String? crdNum,
      String? crdIssueCmpnyCd,
      String? accountTypeCd,
      String? memo,
      String? useYN,
      String? inputCd,
      String? updtDt});
}

/// @nodoc
class __$CardAccountDtoCopyWithImpl<$Res>
    extends _$CardAccountDtoCopyWithImpl<$Res>
    implements _$CardAccountDtoCopyWith<$Res> {
  __$CardAccountDtoCopyWithImpl(
      _CardAccountDto _value, $Res Function(_CardAccountDto) _then)
      : super(_value, (v) => _then(v as _CardAccountDto));

  @override
  _CardAccountDto get _value => super._value as _CardAccountDto;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? title = freezed,
    Object? crdName = freezed,
    Object? crdNum = freezed,
    Object? crdIssueCmpnyCd = freezed,
    Object? accountTypeCd = freezed,
    Object? memo = freezed,
    Object? useYN = freezed,
    Object? inputCd = freezed,
    Object? updtDt = freezed,
  }) {
    return _then(_CardAccountDto(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      crdName: crdName == freezed
          ? _value.crdName
          : crdName // ignore: cast_nullable_to_non_nullable
              as String?,
      crdNum: crdNum == freezed
          ? _value.crdNum
          : crdNum // ignore: cast_nullable_to_non_nullable
              as String?,
      crdIssueCmpnyCd: crdIssueCmpnyCd == freezed
          ? _value.crdIssueCmpnyCd
          : crdIssueCmpnyCd // ignore: cast_nullable_to_non_nullable
              as String?,
      accountTypeCd: accountTypeCd == freezed
          ? _value.accountTypeCd
          : accountTypeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      memo: memo == freezed
          ? _value.memo
          : memo // ignore: cast_nullable_to_non_nullable
              as String?,
      useYN: useYN == freezed
          ? _value.useYN
          : useYN // ignore: cast_nullable_to_non_nullable
              as String?,
      inputCd: inputCd == freezed
          ? _value.inputCd
          : inputCd // ignore: cast_nullable_to_non_nullable
              as String?,
      updtDt: updtDt == freezed
          ? _value.updtDt
          : updtDt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CardAccountDto implements _CardAccountDto {
  _$_CardAccountDto(
      {this.userSeq,
      this.title = '',
      this.crdName = '',
      this.crdNum = '0000',
      this.crdIssueCmpnyCd = '00',
      this.accountTypeCd = '',
      this.memo = '',
      this.useYN = '',
      this.inputCd = '',
      this.updtDt});

  factory _$_CardAccountDto.fromJson(Map<String, dynamic> json) =>
      _$$_CardAccountDtoFromJson(json);

  @override
  final num? userSeq;
  @JsonKey(defaultValue: '')
  @override
  final String? title;
  @JsonKey(defaultValue: '')
  @override
  final String? crdName;
  @JsonKey(defaultValue: '0000')
  @override
  final String? crdNum;
  @JsonKey(defaultValue: '00')
  @override
  final String? crdIssueCmpnyCd;
  @JsonKey(defaultValue: '')
  @override
  final String? accountTypeCd;
  @JsonKey(defaultValue: '')
  @override
  final String? memo;
  @JsonKey(defaultValue: '')
  @override
  final String? useYN;
  @JsonKey(defaultValue: '')
  @override
  final String? inputCd;
  @override
  final String? updtDt;

  @override
  String toString() {
    return 'CardAccountDto(userSeq: $userSeq, title: $title, crdName: $crdName, crdNum: $crdNum, crdIssueCmpnyCd: $crdIssueCmpnyCd, accountTypeCd: $accountTypeCd, memo: $memo, useYN: $useYN, inputCd: $inputCd, updtDt: $updtDt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CardAccountDto &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.crdName, crdName) || other.crdName == crdName) &&
            (identical(other.crdNum, crdNum) || other.crdNum == crdNum) &&
            (identical(other.crdIssueCmpnyCd, crdIssueCmpnyCd) ||
                other.crdIssueCmpnyCd == crdIssueCmpnyCd) &&
            (identical(other.accountTypeCd, accountTypeCd) ||
                other.accountTypeCd == accountTypeCd) &&
            (identical(other.memo, memo) || other.memo == memo) &&
            (identical(other.useYN, useYN) || other.useYN == useYN) &&
            (identical(other.inputCd, inputCd) || other.inputCd == inputCd) &&
            (identical(other.updtDt, updtDt) || other.updtDt == updtDt));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userSeq, title, crdName, crdNum,
      crdIssueCmpnyCd, accountTypeCd, memo, useYN, inputCd, updtDt);

  @JsonKey(ignore: true)
  @override
  _$CardAccountDtoCopyWith<_CardAccountDto> get copyWith =>
      __$CardAccountDtoCopyWithImpl<_CardAccountDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CardAccountDtoToJson(this);
  }
}

abstract class _CardAccountDto implements CardAccountDto {
  factory _CardAccountDto(
      {num? userSeq,
      String? title,
      String? crdName,
      String? crdNum,
      String? crdIssueCmpnyCd,
      String? accountTypeCd,
      String? memo,
      String? useYN,
      String? inputCd,
      String? updtDt}) = _$_CardAccountDto;

  factory _CardAccountDto.fromJson(Map<String, dynamic> json) =
      _$_CardAccountDto.fromJson;

  @override
  num? get userSeq;
  @override
  String? get title;
  @override
  String? get crdName;
  @override
  String? get crdNum;
  @override
  String? get crdIssueCmpnyCd;
  @override
  String? get accountTypeCd;
  @override
  String? get memo;
  @override
  String? get useYN;
  @override
  String? get inputCd;
  @override
  String? get updtDt;
  @override
  @JsonKey(ignore: true)
  _$CardAccountDtoCopyWith<_CardAccountDto> get copyWith =>
      throw _privateConstructorUsedError;
}
