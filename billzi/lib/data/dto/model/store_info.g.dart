// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StoreInfo _$$_StoreInfoFromJson(Map<String, dynamic> json) => _$_StoreInfo(
      storeSeq: json['storeSeq'] as num?,
      storeName: json['storeName'] as String?,
      storeNameRcpt: json['storeNameRcpt'] as String?,
      bizCategory: json['bizCategory'] as String?,
      categorySeq: json['categorySeq'] as num?,
      addr1: json['addr1'] as String?,
      addr2: json['addr2'] as String?,
      email: json['email'] as String?,
      tel1: json['tel1'] as String?,
      info: json['info'] as String?,
      inputCd: json['inputCd'] as String?,
      storeNameInputCd: json['storeNameInputCd'] as String?,
      gpsInfo: json['gpsInfo'] == null
          ? null
          : GpsInfo.fromJson(json['gpsInfo'] as Map<String, dynamic>),
      storeVisitInfo: json['storeVisitInfo'] == null
          ? null
          : StoreVisitInfo.fromJson(
              json['storeVisitInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_StoreInfoToJson(_$_StoreInfo instance) =>
    <String, dynamic>{
      'storeSeq': instance.storeSeq,
      'storeName': instance.storeName,
      'storeNameRcpt': instance.storeNameRcpt,
      'bizCategory': instance.bizCategory,
      'categorySeq': instance.categorySeq,
      'addr1': instance.addr1,
      'addr2': instance.addr2,
      'email': instance.email,
      'tel1': instance.tel1,
      'info': instance.info,
      'inputCd': instance.inputCd,
      'storeNameInputCd': instance.storeNameInputCd,
      'gpsInfo': instance.gpsInfo,
      'storeVisitInfo': instance.storeVisitInfo,
    };
