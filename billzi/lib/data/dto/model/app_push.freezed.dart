// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'app_push.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppPush _$AppPushFromJson(Map<String, dynamic> json) {
  return _AppPush.fromJson(json);
}

/// @nodoc
class _$AppPushTearOff {
  const _$AppPushTearOff();

  _AppPush call(
      {required String CHANNEL_NOTICE,
      required String CHANNEL_LIKES,
      required String CHANNEL_COMMENT,
      required String CHANNEL_FOLLOW}) {
    return _AppPush(
      CHANNEL_NOTICE: CHANNEL_NOTICE,
      CHANNEL_LIKES: CHANNEL_LIKES,
      CHANNEL_COMMENT: CHANNEL_COMMENT,
      CHANNEL_FOLLOW: CHANNEL_FOLLOW,
    );
  }

  AppPush fromJson(Map<String, Object?> json) {
    return AppPush.fromJson(json);
  }
}

/// @nodoc
const $AppPush = _$AppPushTearOff();

/// @nodoc
mixin _$AppPush {
  String get CHANNEL_NOTICE => throw _privateConstructorUsedError;
  String get CHANNEL_LIKES => throw _privateConstructorUsedError;
  String get CHANNEL_COMMENT => throw _privateConstructorUsedError;
  String get CHANNEL_FOLLOW => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppPushCopyWith<AppPush> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppPushCopyWith<$Res> {
  factory $AppPushCopyWith(AppPush value, $Res Function(AppPush) then) =
      _$AppPushCopyWithImpl<$Res>;
  $Res call(
      {String CHANNEL_NOTICE,
      String CHANNEL_LIKES,
      String CHANNEL_COMMENT,
      String CHANNEL_FOLLOW});
}

/// @nodoc
class _$AppPushCopyWithImpl<$Res> implements $AppPushCopyWith<$Res> {
  _$AppPushCopyWithImpl(this._value, this._then);

  final AppPush _value;
  // ignore: unused_field
  final $Res Function(AppPush) _then;

  @override
  $Res call({
    Object? CHANNEL_NOTICE = freezed,
    Object? CHANNEL_LIKES = freezed,
    Object? CHANNEL_COMMENT = freezed,
    Object? CHANNEL_FOLLOW = freezed,
  }) {
    return _then(_value.copyWith(
      CHANNEL_NOTICE: CHANNEL_NOTICE == freezed
          ? _value.CHANNEL_NOTICE
          : CHANNEL_NOTICE // ignore: cast_nullable_to_non_nullable
              as String,
      CHANNEL_LIKES: CHANNEL_LIKES == freezed
          ? _value.CHANNEL_LIKES
          : CHANNEL_LIKES // ignore: cast_nullable_to_non_nullable
              as String,
      CHANNEL_COMMENT: CHANNEL_COMMENT == freezed
          ? _value.CHANNEL_COMMENT
          : CHANNEL_COMMENT // ignore: cast_nullable_to_non_nullable
              as String,
      CHANNEL_FOLLOW: CHANNEL_FOLLOW == freezed
          ? _value.CHANNEL_FOLLOW
          : CHANNEL_FOLLOW // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$AppPushCopyWith<$Res> implements $AppPushCopyWith<$Res> {
  factory _$AppPushCopyWith(_AppPush value, $Res Function(_AppPush) then) =
      __$AppPushCopyWithImpl<$Res>;
  @override
  $Res call(
      {String CHANNEL_NOTICE,
      String CHANNEL_LIKES,
      String CHANNEL_COMMENT,
      String CHANNEL_FOLLOW});
}

/// @nodoc
class __$AppPushCopyWithImpl<$Res> extends _$AppPushCopyWithImpl<$Res>
    implements _$AppPushCopyWith<$Res> {
  __$AppPushCopyWithImpl(_AppPush _value, $Res Function(_AppPush) _then)
      : super(_value, (v) => _then(v as _AppPush));

  @override
  _AppPush get _value => super._value as _AppPush;

  @override
  $Res call({
    Object? CHANNEL_NOTICE = freezed,
    Object? CHANNEL_LIKES = freezed,
    Object? CHANNEL_COMMENT = freezed,
    Object? CHANNEL_FOLLOW = freezed,
  }) {
    return _then(_AppPush(
      CHANNEL_NOTICE: CHANNEL_NOTICE == freezed
          ? _value.CHANNEL_NOTICE
          : CHANNEL_NOTICE // ignore: cast_nullable_to_non_nullable
              as String,
      CHANNEL_LIKES: CHANNEL_LIKES == freezed
          ? _value.CHANNEL_LIKES
          : CHANNEL_LIKES // ignore: cast_nullable_to_non_nullable
              as String,
      CHANNEL_COMMENT: CHANNEL_COMMENT == freezed
          ? _value.CHANNEL_COMMENT
          : CHANNEL_COMMENT // ignore: cast_nullable_to_non_nullable
              as String,
      CHANNEL_FOLLOW: CHANNEL_FOLLOW == freezed
          ? _value.CHANNEL_FOLLOW
          : CHANNEL_FOLLOW // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AppPush implements _AppPush {
  _$_AppPush(
      {required this.CHANNEL_NOTICE,
      required this.CHANNEL_LIKES,
      required this.CHANNEL_COMMENT,
      required this.CHANNEL_FOLLOW});

  factory _$_AppPush.fromJson(Map<String, dynamic> json) =>
      _$$_AppPushFromJson(json);

  @override
  final String CHANNEL_NOTICE;
  @override
  final String CHANNEL_LIKES;
  @override
  final String CHANNEL_COMMENT;
  @override
  final String CHANNEL_FOLLOW;

  @override
  String toString() {
    return 'AppPush(CHANNEL_NOTICE: $CHANNEL_NOTICE, CHANNEL_LIKES: $CHANNEL_LIKES, CHANNEL_COMMENT: $CHANNEL_COMMENT, CHANNEL_FOLLOW: $CHANNEL_FOLLOW)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AppPush &&
            (identical(other.CHANNEL_NOTICE, CHANNEL_NOTICE) ||
                other.CHANNEL_NOTICE == CHANNEL_NOTICE) &&
            (identical(other.CHANNEL_LIKES, CHANNEL_LIKES) ||
                other.CHANNEL_LIKES == CHANNEL_LIKES) &&
            (identical(other.CHANNEL_COMMENT, CHANNEL_COMMENT) ||
                other.CHANNEL_COMMENT == CHANNEL_COMMENT) &&
            (identical(other.CHANNEL_FOLLOW, CHANNEL_FOLLOW) ||
                other.CHANNEL_FOLLOW == CHANNEL_FOLLOW));
  }

  @override
  int get hashCode => Object.hash(runtimeType, CHANNEL_NOTICE, CHANNEL_LIKES,
      CHANNEL_COMMENT, CHANNEL_FOLLOW);

  @JsonKey(ignore: true)
  @override
  _$AppPushCopyWith<_AppPush> get copyWith =>
      __$AppPushCopyWithImpl<_AppPush>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AppPushToJson(this);
  }
}

abstract class _AppPush implements AppPush {
  factory _AppPush(
      {required String CHANNEL_NOTICE,
      required String CHANNEL_LIKES,
      required String CHANNEL_COMMENT,
      required String CHANNEL_FOLLOW}) = _$_AppPush;

  factory _AppPush.fromJson(Map<String, dynamic> json) = _$_AppPush.fromJson;

  @override
  String get CHANNEL_NOTICE;
  @override
  String get CHANNEL_LIKES;
  @override
  String get CHANNEL_COMMENT;
  @override
  String get CHANNEL_FOLLOW;
  @override
  @JsonKey(ignore: true)
  _$AppPushCopyWith<_AppPush> get copyWith =>
      throw _privateConstructorUsedError;
}
