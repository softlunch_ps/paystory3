import 'package:freezed_annotation/freezed_annotation.dart';

part 'budget_category_dto.freezed.dart';
part 'budget_category_dto.g.dart';

@freezed
class BudgetCategoryItem with _$BudgetCategoryItem {
  factory BudgetCategoryItem({
    num? accountCategorySeq,
    String? accountCategoryName,
    num? userSeq,
    double? budgetAmt,
    double? totPayAmt,
    double? budgetBalanceAmt,
    String? supAccountCategorySeq,
    String? color,
    String? typeCd,
    String? imgURL,
  }) = _BudgetCategoryItem;

  factory BudgetCategoryItem.fromJson(Map<String, dynamic> json) =>
      _$BudgetCategoryItemFromJson(json);
}
