import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_dto.freezed.dart';
part 'category_dto.g.dart';

@freezed
class CategoryItem with _$CategoryItem {
  factory CategoryItem({
    num? accountCategorySeq,
    String? accountCategoryName,
    num? userSeq,
    num? ratio,
    num? totPayAmt,
    int? order,
    num? supAccountCategorySeq,
    String? color,
    String? typeCd,
    String? imgURL,
  }) = _CategoryItem;

  factory CategoryItem.fromJson(Map<String, dynamic> json) =>
      _$CategoryItemFromJson(json);
}