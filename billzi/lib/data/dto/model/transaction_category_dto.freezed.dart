// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'transaction_category_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TransactionCategoryDto _$TransactionCategoryDtoFromJson(
    Map<String, dynamic> json) {
  return _TransactionCategoryDto.fromJson(json);
}

/// @nodoc
class _$TransactionCategoryDtoTearOff {
  const _$TransactionCategoryDtoTearOff();

  _TransactionCategoryDto call(
      {@JsonKey(name: 'accountCategorySeq') required int categorySeq,
      @JsonKey(name: 'accountCategoryName') String categoryName = '',
      @JsonKey(name: 'typeCd') required String categoryId,
      @JsonKey(name: 'imgURL') String? imgURL,
      @JsonKey(name: 'color') String backgroundColor = '000000',
      GpsInfo? gpsInfo}) {
    return _TransactionCategoryDto(
      categorySeq: categorySeq,
      categoryName: categoryName,
      categoryId: categoryId,
      imgURL: imgURL,
      backgroundColor: backgroundColor,
      gpsInfo: gpsInfo,
    );
  }

  TransactionCategoryDto fromJson(Map<String, Object?> json) {
    return TransactionCategoryDto.fromJson(json);
  }
}

/// @nodoc
const $TransactionCategoryDto = _$TransactionCategoryDtoTearOff();

/// @nodoc
mixin _$TransactionCategoryDto {
  @JsonKey(name: 'accountCategorySeq')
  int get categorySeq => throw _privateConstructorUsedError;
  @JsonKey(name: 'accountCategoryName')
  String get categoryName => throw _privateConstructorUsedError;
  @JsonKey(name: 'typeCd')
  String get categoryId => throw _privateConstructorUsedError;
  @JsonKey(name: 'imgURL')
  String? get imgURL => throw _privateConstructorUsedError;
  @JsonKey(name: 'color')
  String get backgroundColor => throw _privateConstructorUsedError;
  GpsInfo? get gpsInfo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionCategoryDtoCopyWith<TransactionCategoryDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionCategoryDtoCopyWith<$Res> {
  factory $TransactionCategoryDtoCopyWith(TransactionCategoryDto value,
          $Res Function(TransactionCategoryDto) then) =
      _$TransactionCategoryDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'accountCategorySeq') int categorySeq,
      @JsonKey(name: 'accountCategoryName') String categoryName,
      @JsonKey(name: 'typeCd') String categoryId,
      @JsonKey(name: 'imgURL') String? imgURL,
      @JsonKey(name: 'color') String backgroundColor,
      GpsInfo? gpsInfo});

  $GpsInfoCopyWith<$Res>? get gpsInfo;
}

/// @nodoc
class _$TransactionCategoryDtoCopyWithImpl<$Res>
    implements $TransactionCategoryDtoCopyWith<$Res> {
  _$TransactionCategoryDtoCopyWithImpl(this._value, this._then);

  final TransactionCategoryDto _value;
  // ignore: unused_field
  final $Res Function(TransactionCategoryDto) _then;

  @override
  $Res call({
    Object? categorySeq = freezed,
    Object? categoryName = freezed,
    Object? categoryId = freezed,
    Object? imgURL = freezed,
    Object? backgroundColor = freezed,
    Object? gpsInfo = freezed,
  }) {
    return _then(_value.copyWith(
      categorySeq: categorySeq == freezed
          ? _value.categorySeq
          : categorySeq // ignore: cast_nullable_to_non_nullable
              as int,
      categoryName: categoryName == freezed
          ? _value.categoryName
          : categoryName // ignore: cast_nullable_to_non_nullable
              as String,
      categoryId: categoryId == freezed
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as String,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      backgroundColor: backgroundColor == freezed
          ? _value.backgroundColor
          : backgroundColor // ignore: cast_nullable_to_non_nullable
              as String,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
    ));
  }

  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo {
    if (_value.gpsInfo == null) {
      return null;
    }

    return $GpsInfoCopyWith<$Res>(_value.gpsInfo!, (value) {
      return _then(_value.copyWith(gpsInfo: value));
    });
  }
}

/// @nodoc
abstract class _$TransactionCategoryDtoCopyWith<$Res>
    implements $TransactionCategoryDtoCopyWith<$Res> {
  factory _$TransactionCategoryDtoCopyWith(_TransactionCategoryDto value,
          $Res Function(_TransactionCategoryDto) then) =
      __$TransactionCategoryDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'accountCategorySeq') int categorySeq,
      @JsonKey(name: 'accountCategoryName') String categoryName,
      @JsonKey(name: 'typeCd') String categoryId,
      @JsonKey(name: 'imgURL') String? imgURL,
      @JsonKey(name: 'color') String backgroundColor,
      GpsInfo? gpsInfo});

  @override
  $GpsInfoCopyWith<$Res>? get gpsInfo;
}

/// @nodoc
class __$TransactionCategoryDtoCopyWithImpl<$Res>
    extends _$TransactionCategoryDtoCopyWithImpl<$Res>
    implements _$TransactionCategoryDtoCopyWith<$Res> {
  __$TransactionCategoryDtoCopyWithImpl(_TransactionCategoryDto _value,
      $Res Function(_TransactionCategoryDto) _then)
      : super(_value, (v) => _then(v as _TransactionCategoryDto));

  @override
  _TransactionCategoryDto get _value => super._value as _TransactionCategoryDto;

  @override
  $Res call({
    Object? categorySeq = freezed,
    Object? categoryName = freezed,
    Object? categoryId = freezed,
    Object? imgURL = freezed,
    Object? backgroundColor = freezed,
    Object? gpsInfo = freezed,
  }) {
    return _then(_TransactionCategoryDto(
      categorySeq: categorySeq == freezed
          ? _value.categorySeq
          : categorySeq // ignore: cast_nullable_to_non_nullable
              as int,
      categoryName: categoryName == freezed
          ? _value.categoryName
          : categoryName // ignore: cast_nullable_to_non_nullable
              as String,
      categoryId: categoryId == freezed
          ? _value.categoryId
          : categoryId // ignore: cast_nullable_to_non_nullable
              as String,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      backgroundColor: backgroundColor == freezed
          ? _value.backgroundColor
          : backgroundColor // ignore: cast_nullable_to_non_nullable
              as String,
      gpsInfo: gpsInfo == freezed
          ? _value.gpsInfo
          : gpsInfo // ignore: cast_nullable_to_non_nullable
              as GpsInfo?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TransactionCategoryDto implements _TransactionCategoryDto {
  _$_TransactionCategoryDto(
      {@JsonKey(name: 'accountCategorySeq') required this.categorySeq,
      @JsonKey(name: 'accountCategoryName') this.categoryName = '',
      @JsonKey(name: 'typeCd') required this.categoryId,
      @JsonKey(name: 'imgURL') this.imgURL,
      @JsonKey(name: 'color') this.backgroundColor = '000000',
      this.gpsInfo});

  factory _$_TransactionCategoryDto.fromJson(Map<String, dynamic> json) =>
      _$$_TransactionCategoryDtoFromJson(json);

  @override
  @JsonKey(name: 'accountCategorySeq')
  final int categorySeq;
  @override
  @JsonKey(name: 'accountCategoryName')
  final String categoryName;
  @override
  @JsonKey(name: 'typeCd')
  final String categoryId;
  @override
  @JsonKey(name: 'imgURL')
  final String? imgURL;
  @override
  @JsonKey(name: 'color')
  final String backgroundColor;
  @override
  final GpsInfo? gpsInfo;

  @override
  String toString() {
    return 'TransactionCategoryDto(categorySeq: $categorySeq, categoryName: $categoryName, categoryId: $categoryId, imgURL: $imgURL, backgroundColor: $backgroundColor, gpsInfo: $gpsInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TransactionCategoryDto &&
            (identical(other.categorySeq, categorySeq) ||
                other.categorySeq == categorySeq) &&
            (identical(other.categoryName, categoryName) ||
                other.categoryName == categoryName) &&
            (identical(other.categoryId, categoryId) ||
                other.categoryId == categoryId) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL) &&
            (identical(other.backgroundColor, backgroundColor) ||
                other.backgroundColor == backgroundColor) &&
            (identical(other.gpsInfo, gpsInfo) || other.gpsInfo == gpsInfo));
  }

  @override
  int get hashCode => Object.hash(runtimeType, categorySeq, categoryName,
      categoryId, imgURL, backgroundColor, gpsInfo);

  @JsonKey(ignore: true)
  @override
  _$TransactionCategoryDtoCopyWith<_TransactionCategoryDto> get copyWith =>
      __$TransactionCategoryDtoCopyWithImpl<_TransactionCategoryDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TransactionCategoryDtoToJson(this);
  }
}

abstract class _TransactionCategoryDto implements TransactionCategoryDto {
  factory _TransactionCategoryDto(
      {@JsonKey(name: 'accountCategorySeq') required int categorySeq,
      @JsonKey(name: 'accountCategoryName') String categoryName,
      @JsonKey(name: 'typeCd') required String categoryId,
      @JsonKey(name: 'imgURL') String? imgURL,
      @JsonKey(name: 'color') String backgroundColor,
      GpsInfo? gpsInfo}) = _$_TransactionCategoryDto;

  factory _TransactionCategoryDto.fromJson(Map<String, dynamic> json) =
      _$_TransactionCategoryDto.fromJson;

  @override
  @JsonKey(name: 'accountCategorySeq')
  int get categorySeq;
  @override
  @JsonKey(name: 'accountCategoryName')
  String get categoryName;
  @override
  @JsonKey(name: 'typeCd')
  String get categoryId;
  @override
  @JsonKey(name: 'imgURL')
  String? get imgURL;
  @override
  @JsonKey(name: 'color')
  String get backgroundColor;
  @override
  GpsInfo? get gpsInfo;
  @override
  @JsonKey(ignore: true)
  _$TransactionCategoryDtoCopyWith<_TransactionCategoryDto> get copyWith =>
      throw _privateConstructorUsedError;
}
