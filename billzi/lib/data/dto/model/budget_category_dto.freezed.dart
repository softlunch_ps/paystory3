// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'budget_category_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BudgetCategoryItem _$BudgetCategoryItemFromJson(Map<String, dynamic> json) {
  return _BudgetCategoryItem.fromJson(json);
}

/// @nodoc
class _$BudgetCategoryItemTearOff {
  const _$BudgetCategoryItemTearOff();

  _BudgetCategoryItem call(
      {num? accountCategorySeq,
      String? accountCategoryName,
      num? userSeq,
      double? budgetAmt,
      double? totPayAmt,
      double? budgetBalanceAmt,
      String? supAccountCategorySeq,
      String? color,
      String? typeCd,
      String? imgURL}) {
    return _BudgetCategoryItem(
      accountCategorySeq: accountCategorySeq,
      accountCategoryName: accountCategoryName,
      userSeq: userSeq,
      budgetAmt: budgetAmt,
      totPayAmt: totPayAmt,
      budgetBalanceAmt: budgetBalanceAmt,
      supAccountCategorySeq: supAccountCategorySeq,
      color: color,
      typeCd: typeCd,
      imgURL: imgURL,
    );
  }

  BudgetCategoryItem fromJson(Map<String, Object?> json) {
    return BudgetCategoryItem.fromJson(json);
  }
}

/// @nodoc
const $BudgetCategoryItem = _$BudgetCategoryItemTearOff();

/// @nodoc
mixin _$BudgetCategoryItem {
  num? get accountCategorySeq => throw _privateConstructorUsedError;
  String? get accountCategoryName => throw _privateConstructorUsedError;
  num? get userSeq => throw _privateConstructorUsedError;
  double? get budgetAmt => throw _privateConstructorUsedError;
  double? get totPayAmt => throw _privateConstructorUsedError;
  double? get budgetBalanceAmt => throw _privateConstructorUsedError;
  String? get supAccountCategorySeq => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  String? get typeCd => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BudgetCategoryItemCopyWith<BudgetCategoryItem> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BudgetCategoryItemCopyWith<$Res> {
  factory $BudgetCategoryItemCopyWith(
          BudgetCategoryItem value, $Res Function(BudgetCategoryItem) then) =
      _$BudgetCategoryItemCopyWithImpl<$Res>;
  $Res call(
      {num? accountCategorySeq,
      String? accountCategoryName,
      num? userSeq,
      double? budgetAmt,
      double? totPayAmt,
      double? budgetBalanceAmt,
      String? supAccountCategorySeq,
      String? color,
      String? typeCd,
      String? imgURL});
}

/// @nodoc
class _$BudgetCategoryItemCopyWithImpl<$Res>
    implements $BudgetCategoryItemCopyWith<$Res> {
  _$BudgetCategoryItemCopyWithImpl(this._value, this._then);

  final BudgetCategoryItem _value;
  // ignore: unused_field
  final $Res Function(BudgetCategoryItem) _then;

  @override
  $Res call({
    Object? accountCategorySeq = freezed,
    Object? accountCategoryName = freezed,
    Object? userSeq = freezed,
    Object? budgetAmt = freezed,
    Object? totPayAmt = freezed,
    Object? budgetBalanceAmt = freezed,
    Object? supAccountCategorySeq = freezed,
    Object? color = freezed,
    Object? typeCd = freezed,
    Object? imgURL = freezed,
  }) {
    return _then(_value.copyWith(
      accountCategorySeq: accountCategorySeq == freezed
          ? _value.accountCategorySeq
          : accountCategorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountCategoryName: accountCategoryName == freezed
          ? _value.accountCategoryName
          : accountCategoryName // ignore: cast_nullable_to_non_nullable
              as String?,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      budgetAmt: budgetAmt == freezed
          ? _value.budgetAmt
          : budgetAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      totPayAmt: totPayAmt == freezed
          ? _value.totPayAmt
          : totPayAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      budgetBalanceAmt: budgetBalanceAmt == freezed
          ? _value.budgetBalanceAmt
          : budgetBalanceAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      supAccountCategorySeq: supAccountCategorySeq == freezed
          ? _value.supAccountCategorySeq
          : supAccountCategorySeq // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      typeCd: typeCd == freezed
          ? _value.typeCd
          : typeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$BudgetCategoryItemCopyWith<$Res>
    implements $BudgetCategoryItemCopyWith<$Res> {
  factory _$BudgetCategoryItemCopyWith(
          _BudgetCategoryItem value, $Res Function(_BudgetCategoryItem) then) =
      __$BudgetCategoryItemCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? accountCategorySeq,
      String? accountCategoryName,
      num? userSeq,
      double? budgetAmt,
      double? totPayAmt,
      double? budgetBalanceAmt,
      String? supAccountCategorySeq,
      String? color,
      String? typeCd,
      String? imgURL});
}

/// @nodoc
class __$BudgetCategoryItemCopyWithImpl<$Res>
    extends _$BudgetCategoryItemCopyWithImpl<$Res>
    implements _$BudgetCategoryItemCopyWith<$Res> {
  __$BudgetCategoryItemCopyWithImpl(
      _BudgetCategoryItem _value, $Res Function(_BudgetCategoryItem) _then)
      : super(_value, (v) => _then(v as _BudgetCategoryItem));

  @override
  _BudgetCategoryItem get _value => super._value as _BudgetCategoryItem;

  @override
  $Res call({
    Object? accountCategorySeq = freezed,
    Object? accountCategoryName = freezed,
    Object? userSeq = freezed,
    Object? budgetAmt = freezed,
    Object? totPayAmt = freezed,
    Object? budgetBalanceAmt = freezed,
    Object? supAccountCategorySeq = freezed,
    Object? color = freezed,
    Object? typeCd = freezed,
    Object? imgURL = freezed,
  }) {
    return _then(_BudgetCategoryItem(
      accountCategorySeq: accountCategorySeq == freezed
          ? _value.accountCategorySeq
          : accountCategorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountCategoryName: accountCategoryName == freezed
          ? _value.accountCategoryName
          : accountCategoryName // ignore: cast_nullable_to_non_nullable
              as String?,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      budgetAmt: budgetAmt == freezed
          ? _value.budgetAmt
          : budgetAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      totPayAmt: totPayAmt == freezed
          ? _value.totPayAmt
          : totPayAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      budgetBalanceAmt: budgetBalanceAmt == freezed
          ? _value.budgetBalanceAmt
          : budgetBalanceAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      supAccountCategorySeq: supAccountCategorySeq == freezed
          ? _value.supAccountCategorySeq
          : supAccountCategorySeq // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      typeCd: typeCd == freezed
          ? _value.typeCd
          : typeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BudgetCategoryItem implements _BudgetCategoryItem {
  _$_BudgetCategoryItem(
      {this.accountCategorySeq,
      this.accountCategoryName,
      this.userSeq,
      this.budgetAmt,
      this.totPayAmt,
      this.budgetBalanceAmt,
      this.supAccountCategorySeq,
      this.color,
      this.typeCd,
      this.imgURL});

  factory _$_BudgetCategoryItem.fromJson(Map<String, dynamic> json) =>
      _$$_BudgetCategoryItemFromJson(json);

  @override
  final num? accountCategorySeq;
  @override
  final String? accountCategoryName;
  @override
  final num? userSeq;
  @override
  final double? budgetAmt;
  @override
  final double? totPayAmt;
  @override
  final double? budgetBalanceAmt;
  @override
  final String? supAccountCategorySeq;
  @override
  final String? color;
  @override
  final String? typeCd;
  @override
  final String? imgURL;

  @override
  String toString() {
    return 'BudgetCategoryItem(accountCategorySeq: $accountCategorySeq, accountCategoryName: $accountCategoryName, userSeq: $userSeq, budgetAmt: $budgetAmt, totPayAmt: $totPayAmt, budgetBalanceAmt: $budgetBalanceAmt, supAccountCategorySeq: $supAccountCategorySeq, color: $color, typeCd: $typeCd, imgURL: $imgURL)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BudgetCategoryItem &&
            (identical(other.accountCategorySeq, accountCategorySeq) ||
                other.accountCategorySeq == accountCategorySeq) &&
            (identical(other.accountCategoryName, accountCategoryName) ||
                other.accountCategoryName == accountCategoryName) &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.budgetAmt, budgetAmt) ||
                other.budgetAmt == budgetAmt) &&
            (identical(other.totPayAmt, totPayAmt) ||
                other.totPayAmt == totPayAmt) &&
            (identical(other.budgetBalanceAmt, budgetBalanceAmt) ||
                other.budgetBalanceAmt == budgetBalanceAmt) &&
            (identical(other.supAccountCategorySeq, supAccountCategorySeq) ||
                other.supAccountCategorySeq == supAccountCategorySeq) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.typeCd, typeCd) || other.typeCd == typeCd) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      accountCategorySeq,
      accountCategoryName,
      userSeq,
      budgetAmt,
      totPayAmt,
      budgetBalanceAmt,
      supAccountCategorySeq,
      color,
      typeCd,
      imgURL);

  @JsonKey(ignore: true)
  @override
  _$BudgetCategoryItemCopyWith<_BudgetCategoryItem> get copyWith =>
      __$BudgetCategoryItemCopyWithImpl<_BudgetCategoryItem>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BudgetCategoryItemToJson(this);
  }
}

abstract class _BudgetCategoryItem implements BudgetCategoryItem {
  factory _BudgetCategoryItem(
      {num? accountCategorySeq,
      String? accountCategoryName,
      num? userSeq,
      double? budgetAmt,
      double? totPayAmt,
      double? budgetBalanceAmt,
      String? supAccountCategorySeq,
      String? color,
      String? typeCd,
      String? imgURL}) = _$_BudgetCategoryItem;

  factory _BudgetCategoryItem.fromJson(Map<String, dynamic> json) =
      _$_BudgetCategoryItem.fromJson;

  @override
  num? get accountCategorySeq;
  @override
  String? get accountCategoryName;
  @override
  num? get userSeq;
  @override
  double? get budgetAmt;
  @override
  double? get totPayAmt;
  @override
  double? get budgetBalanceAmt;
  @override
  String? get supAccountCategorySeq;
  @override
  String? get color;
  @override
  String? get typeCd;
  @override
  String? get imgURL;
  @override
  @JsonKey(ignore: true)
  _$BudgetCategoryItemCopyWith<_BudgetCategoryItem> get copyWith =>
      throw _privateConstructorUsedError;
}
