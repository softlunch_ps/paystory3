// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_category_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TransactionCategoryDto _$$_TransactionCategoryDtoFromJson(
        Map<String, dynamic> json) =>
    _$_TransactionCategoryDto(
      categorySeq: json['accountCategorySeq'] as int,
      categoryName: json['accountCategoryName'] as String? ?? '',
      categoryId: json['typeCd'] as String,
      imgURL: json['imgURL'] as String?,
      backgroundColor: json['color'] as String? ?? '000000',
      gpsInfo: json['gpsInfo'] == null
          ? null
          : GpsInfo.fromJson(json['gpsInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_TransactionCategoryDtoToJson(
        _$_TransactionCategoryDto instance) =>
    <String, dynamic>{
      'accountCategorySeq': instance.categorySeq,
      'accountCategoryName': instance.categoryName,
      'typeCd': instance.categoryId,
      'imgURL': instance.imgURL,
      'color': instance.backgroundColor,
      'gpsInfo': instance.gpsInfo,
    };
