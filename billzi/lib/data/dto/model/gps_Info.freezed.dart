// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'gps_Info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

GpsInfo _$GpsInfoFromJson(Map<String, dynamic> json) {
  return _GpsInfo.fromJson(json);
}

/// @nodoc
class _$GpsInfoTearOff {
  const _$GpsInfoTearOff();

  _GpsInfo call({required double latitude, required double longitude}) {
    return _GpsInfo(
      latitude: latitude,
      longitude: longitude,
    );
  }

  GpsInfo fromJson(Map<String, Object?> json) {
    return GpsInfo.fromJson(json);
  }
}

/// @nodoc
const $GpsInfo = _$GpsInfoTearOff();

/// @nodoc
mixin _$GpsInfo {
  double get latitude => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GpsInfoCopyWith<GpsInfo> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GpsInfoCopyWith<$Res> {
  factory $GpsInfoCopyWith(GpsInfo value, $Res Function(GpsInfo) then) =
      _$GpsInfoCopyWithImpl<$Res>;
  $Res call({double latitude, double longitude});
}

/// @nodoc
class _$GpsInfoCopyWithImpl<$Res> implements $GpsInfoCopyWith<$Res> {
  _$GpsInfoCopyWithImpl(this._value, this._then);

  final GpsInfo _value;
  // ignore: unused_field
  final $Res Function(GpsInfo) _then;

  @override
  $Res call({
    Object? latitude = freezed,
    Object? longitude = freezed,
  }) {
    return _then(_value.copyWith(
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
abstract class _$GpsInfoCopyWith<$Res> implements $GpsInfoCopyWith<$Res> {
  factory _$GpsInfoCopyWith(_GpsInfo value, $Res Function(_GpsInfo) then) =
      __$GpsInfoCopyWithImpl<$Res>;
  @override
  $Res call({double latitude, double longitude});
}

/// @nodoc
class __$GpsInfoCopyWithImpl<$Res> extends _$GpsInfoCopyWithImpl<$Res>
    implements _$GpsInfoCopyWith<$Res> {
  __$GpsInfoCopyWithImpl(_GpsInfo _value, $Res Function(_GpsInfo) _then)
      : super(_value, (v) => _then(v as _GpsInfo));

  @override
  _GpsInfo get _value => super._value as _GpsInfo;

  @override
  $Res call({
    Object? latitude = freezed,
    Object? longitude = freezed,
  }) {
    return _then(_GpsInfo(
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_GpsInfo implements _GpsInfo {
  _$_GpsInfo({required this.latitude, required this.longitude});

  factory _$_GpsInfo.fromJson(Map<String, dynamic> json) =>
      _$$_GpsInfoFromJson(json);

  @override
  final double latitude;
  @override
  final double longitude;

  @override
  String toString() {
    return 'GpsInfo(latitude: $latitude, longitude: $longitude)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _GpsInfo &&
            (identical(other.latitude, latitude) ||
                other.latitude == latitude) &&
            (identical(other.longitude, longitude) ||
                other.longitude == longitude));
  }

  @override
  int get hashCode => Object.hash(runtimeType, latitude, longitude);

  @JsonKey(ignore: true)
  @override
  _$GpsInfoCopyWith<_GpsInfo> get copyWith =>
      __$GpsInfoCopyWithImpl<_GpsInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_GpsInfoToJson(this);
  }
}

abstract class _GpsInfo implements GpsInfo {
  factory _GpsInfo({required double latitude, required double longitude}) =
      _$_GpsInfo;

  factory _GpsInfo.fromJson(Map<String, dynamic> json) = _$_GpsInfo.fromJson;

  @override
  double get latitude;
  @override
  double get longitude;
  @override
  @JsonKey(ignore: true)
  _$GpsInfoCopyWith<_GpsInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
