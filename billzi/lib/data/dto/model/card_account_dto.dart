import 'package:freezed_annotation/freezed_annotation.dart';

part 'card_account_dto.freezed.dart';
part 'card_account_dto.g.dart';

@freezed
class CardAccountDto with _$CardAccountDto {
  factory CardAccountDto({
    num? userSeq,
    @Default('') String? title,
    @Default('') String? crdName,
    @Default('0000') String? crdNum,
    @Default('00') String? crdIssueCmpnyCd,
    @Default('') String? accountTypeCd,
    @Default('') String? memo,
    @Default('') String? useYN,
    @Default('') String? inputCd,
    String? updtDt,
  }) = _CardAccountDto;

  factory CardAccountDto.fromJson(Map<String, dynamic> json) =>
      _$CardAccountDtoFromJson(json);
}