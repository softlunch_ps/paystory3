import 'package:billzi/data/dto/model/gps_Info.dart';
import 'package:billzi/data/dto/model/store_visit_info.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'store_info.freezed.dart';
part 'store_info.g.dart';

@freezed
class StoreInfo with _$StoreInfo {
  factory StoreInfo({
    num? storeSeq,
    String? storeName,
    String? storeNameRcpt,
    String? bizCategory,
    num? categorySeq,
    String? addr1,
    String? addr2,
    String? email,
    String? tel1,
    String? info,
    String? inputCd,
    String? storeNameInputCd,
    GpsInfo? gpsInfo,
    StoreVisitInfo? storeVisitInfo,
  }) = _StoreInfo;

  factory StoreInfo.fromJson(Map<String, dynamic> json) =>
      _$StoreInfoFromJson(json);
}