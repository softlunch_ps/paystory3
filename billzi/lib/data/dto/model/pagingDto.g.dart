// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagingDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PagingDto _$$_PagingDtoFromJson(Map<String, dynamic> json) => _$_PagingDto(
      offset: json['offset'] as int? ?? 0,
      limit: json['limit'] as int? ?? 0,
      nextOffset: json['nextOffset'] as int? ?? 0,
      listSize: json['listSize'] as int? ?? 0,
      totalCount: json['totalCount'] as int? ?? 0,
    );

Map<String, dynamic> _$$_PagingDtoToJson(_$_PagingDto instance) =>
    <String, dynamic>{
      'offset': instance.offset,
      'limit': instance.limit,
      'nextOffset': instance.nextOffset,
      'listSize': instance.listSize,
      'totalCount': instance.totalCount,
    };
