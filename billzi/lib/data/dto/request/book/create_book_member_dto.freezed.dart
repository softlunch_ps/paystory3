// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'create_book_member_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CreateBookMemberDto _$CreateBookMemberDtoFromJson(Map<String, dynamic> json) {
  return _CreateBookMemberDto.fromJson(json);
}

/// @nodoc
class _$CreateBookMemberDtoTearOff {
  const _$CreateBookMemberDtoTearOff();

  _CreateBookMemberDto call(
      {num? targetUserSeq,
      String? userNickname,
      String? userProfileImgUrl,
      MemberGrade? role}) {
    return _CreateBookMemberDto(
      targetUserSeq: targetUserSeq,
      userNickname: userNickname,
      userProfileImgUrl: userProfileImgUrl,
      role: role,
    );
  }

  CreateBookMemberDto fromJson(Map<String, Object?> json) {
    return CreateBookMemberDto.fromJson(json);
  }
}

/// @nodoc
const $CreateBookMemberDto = _$CreateBookMemberDtoTearOff();

/// @nodoc
mixin _$CreateBookMemberDto {
  num? get targetUserSeq => throw _privateConstructorUsedError;
  String? get userNickname => throw _privateConstructorUsedError;
  String? get userProfileImgUrl => throw _privateConstructorUsedError;
  MemberGrade? get role => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CreateBookMemberDtoCopyWith<CreateBookMemberDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateBookMemberDtoCopyWith<$Res> {
  factory $CreateBookMemberDtoCopyWith(
          CreateBookMemberDto value, $Res Function(CreateBookMemberDto) then) =
      _$CreateBookMemberDtoCopyWithImpl<$Res>;
  $Res call(
      {num? targetUserSeq,
      String? userNickname,
      String? userProfileImgUrl,
      MemberGrade? role});
}

/// @nodoc
class _$CreateBookMemberDtoCopyWithImpl<$Res>
    implements $CreateBookMemberDtoCopyWith<$Res> {
  _$CreateBookMemberDtoCopyWithImpl(this._value, this._then);

  final CreateBookMemberDto _value;
  // ignore: unused_field
  final $Res Function(CreateBookMemberDto) _then;

  @override
  $Res call({
    Object? targetUserSeq = freezed,
    Object? userNickname = freezed,
    Object? userProfileImgUrl = freezed,
    Object? role = freezed,
  }) {
    return _then(_value.copyWith(
      targetUserSeq: targetUserSeq == freezed
          ? _value.targetUserSeq
          : targetUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as String?,
      userProfileImgUrl: userProfileImgUrl == freezed
          ? _value.userProfileImgUrl
          : userProfileImgUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      role: role == freezed
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as MemberGrade?,
    ));
  }
}

/// @nodoc
abstract class _$CreateBookMemberDtoCopyWith<$Res>
    implements $CreateBookMemberDtoCopyWith<$Res> {
  factory _$CreateBookMemberDtoCopyWith(_CreateBookMemberDto value,
          $Res Function(_CreateBookMemberDto) then) =
      __$CreateBookMemberDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? targetUserSeq,
      String? userNickname,
      String? userProfileImgUrl,
      MemberGrade? role});
}

/// @nodoc
class __$CreateBookMemberDtoCopyWithImpl<$Res>
    extends _$CreateBookMemberDtoCopyWithImpl<$Res>
    implements _$CreateBookMemberDtoCopyWith<$Res> {
  __$CreateBookMemberDtoCopyWithImpl(
      _CreateBookMemberDto _value, $Res Function(_CreateBookMemberDto) _then)
      : super(_value, (v) => _then(v as _CreateBookMemberDto));

  @override
  _CreateBookMemberDto get _value => super._value as _CreateBookMemberDto;

  @override
  $Res call({
    Object? targetUserSeq = freezed,
    Object? userNickname = freezed,
    Object? userProfileImgUrl = freezed,
    Object? role = freezed,
  }) {
    return _then(_CreateBookMemberDto(
      targetUserSeq: targetUserSeq == freezed
          ? _value.targetUserSeq
          : targetUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as String?,
      userProfileImgUrl: userProfileImgUrl == freezed
          ? _value.userProfileImgUrl
          : userProfileImgUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      role: role == freezed
          ? _value.role
          : role // ignore: cast_nullable_to_non_nullable
              as MemberGrade?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CreateBookMemberDto implements _CreateBookMemberDto {
  _$_CreateBookMemberDto(
      {this.targetUserSeq,
      this.userNickname,
      this.userProfileImgUrl,
      this.role});

  factory _$_CreateBookMemberDto.fromJson(Map<String, dynamic> json) =>
      _$$_CreateBookMemberDtoFromJson(json);

  @override
  final num? targetUserSeq;
  @override
  final String? userNickname;
  @override
  final String? userProfileImgUrl;
  @override
  final MemberGrade? role;

  @override
  String toString() {
    return 'CreateBookMemberDto(targetUserSeq: $targetUserSeq, userNickname: $userNickname, userProfileImgUrl: $userProfileImgUrl, role: $role)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CreateBookMemberDto &&
            (identical(other.targetUserSeq, targetUserSeq) ||
                other.targetUserSeq == targetUserSeq) &&
            (identical(other.userNickname, userNickname) ||
                other.userNickname == userNickname) &&
            (identical(other.userProfileImgUrl, userProfileImgUrl) ||
                other.userProfileImgUrl == userProfileImgUrl) &&
            (identical(other.role, role) || other.role == role));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, targetUserSeq, userNickname, userProfileImgUrl, role);

  @JsonKey(ignore: true)
  @override
  _$CreateBookMemberDtoCopyWith<_CreateBookMemberDto> get copyWith =>
      __$CreateBookMemberDtoCopyWithImpl<_CreateBookMemberDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CreateBookMemberDtoToJson(this);
  }
}

abstract class _CreateBookMemberDto implements CreateBookMemberDto {
  factory _CreateBookMemberDto(
      {num? targetUserSeq,
      String? userNickname,
      String? userProfileImgUrl,
      MemberGrade? role}) = _$_CreateBookMemberDto;

  factory _CreateBookMemberDto.fromJson(Map<String, dynamic> json) =
      _$_CreateBookMemberDto.fromJson;

  @override
  num? get targetUserSeq;
  @override
  String? get userNickname;
  @override
  String? get userProfileImgUrl;
  @override
  MemberGrade? get role;
  @override
  @JsonKey(ignore: true)
  _$CreateBookMemberDtoCopyWith<_CreateBookMemberDto> get copyWith =>
      throw _privateConstructorUsedError;
}
