// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_book_member_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CreateBookMemberDto _$$_CreateBookMemberDtoFromJson(
        Map<String, dynamic> json) =>
    _$_CreateBookMemberDto(
      targetUserSeq: json['targetUserSeq'] as num?,
      userNickname: json['userNickname'] as String?,
      userProfileImgUrl: json['userProfileImgUrl'] as String?,
      role: $enumDecodeNullable(_$MemberGradeEnumMap, json['role']),
    );

Map<String, dynamic> _$$_CreateBookMemberDtoToJson(
        _$_CreateBookMemberDto instance) =>
    <String, dynamic>{
      'targetUserSeq': instance.targetUserSeq,
      'userNickname': instance.userNickname,
      'userProfileImgUrl': instance.userProfileImgUrl,
      'role': _$MemberGradeEnumMap[instance.role],
    };

const _$MemberGradeEnumMap = {
  MemberGrade.NONMEMBER: 'NONMEMBER',
  MemberGrade.MEMBER: 'MEMBER',
  MemberGrade.ADMIN: 'ADMIN',
  MemberGrade.OWNER: 'OWNER',
};
