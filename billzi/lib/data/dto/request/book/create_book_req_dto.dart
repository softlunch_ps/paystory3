import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';

part 'create_book_req_dto.freezed.dart';
part 'create_book_req_dto.g.dart';

@freezed
class CreateBookReqDto with _$CreateBookReqDto {
  factory CreateBookReqDto({
    required String accountBookName,
    String? accountBookDesc,
    String? imgURL,
    String? color,
    DateTime? startDate,
    DateTime? endDate,
    CreateBookMemberDto? userInfo,
  }) = _CreateBookReqDto;

  factory CreateBookReqDto.fromJson(Map<String, dynamic> json) =>
      _$CreateBookReqDtoFromJson(json);
}
