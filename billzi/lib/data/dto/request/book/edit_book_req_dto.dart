import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';

part 'edit_book_req_dto.freezed.dart';
part 'edit_book_req_dto.g.dart';

@freezed
class EditBookReqDto with _$EditBookReqDto {
  factory EditBookReqDto({
    required num accountBookSeq,
    String? accountBookName,
    String? accountBookDesc,
    DateTime? startDate,
    DateTime? endDate,
    String? imgURL,
    String? color,
    String? useYn,
    CreateBookMemberDto? userInfo,
  }) = _EditBookReqDto;

  factory EditBookReqDto.fromJson(Map<String, dynamic> json) =>
      _$EditBookReqDtoFromJson(json);
}
