import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/book/book_member_res_dto.dart';

part 'create_book_member_dto.freezed.dart';
part 'create_book_member_dto.g.dart';

@freezed
class CreateBookMemberDto with _$CreateBookMemberDto {
  factory CreateBookMemberDto({
    num? targetUserSeq,
    String? userNickname,
    String? userProfileImgUrl,
    MemberGrade? role,
  }) = _CreateBookMemberDto;

  factory CreateBookMemberDto.fromJson(Map<String, dynamic> json) =>
      _$CreateBookMemberDtoFromJson(json);
}
