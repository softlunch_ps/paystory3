// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'edit_book_req_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

EditBookReqDto _$EditBookReqDtoFromJson(Map<String, dynamic> json) {
  return _EditBookReqDto.fromJson(json);
}

/// @nodoc
class _$EditBookReqDtoTearOff {
  const _$EditBookReqDtoTearOff();

  _EditBookReqDto call(
      {required num accountBookSeq,
      String? accountBookName,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn,
      CreateBookMemberDto? userInfo}) {
    return _EditBookReqDto(
      accountBookSeq: accountBookSeq,
      accountBookName: accountBookName,
      accountBookDesc: accountBookDesc,
      startDate: startDate,
      endDate: endDate,
      imgURL: imgURL,
      color: color,
      useYn: useYn,
      userInfo: userInfo,
    );
  }

  EditBookReqDto fromJson(Map<String, Object?> json) {
    return EditBookReqDto.fromJson(json);
  }
}

/// @nodoc
const $EditBookReqDto = _$EditBookReqDtoTearOff();

/// @nodoc
mixin _$EditBookReqDto {
  num get accountBookSeq => throw _privateConstructorUsedError;
  String? get accountBookName => throw _privateConstructorUsedError;
  String? get accountBookDesc => throw _privateConstructorUsedError;
  DateTime? get startDate => throw _privateConstructorUsedError;
  DateTime? get endDate => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  String? get useYn => throw _privateConstructorUsedError;
  CreateBookMemberDto? get userInfo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EditBookReqDtoCopyWith<EditBookReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EditBookReqDtoCopyWith<$Res> {
  factory $EditBookReqDtoCopyWith(
          EditBookReqDto value, $Res Function(EditBookReqDto) then) =
      _$EditBookReqDtoCopyWithImpl<$Res>;
  $Res call(
      {num accountBookSeq,
      String? accountBookName,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn,
      CreateBookMemberDto? userInfo});

  $CreateBookMemberDtoCopyWith<$Res>? get userInfo;
}

/// @nodoc
class _$EditBookReqDtoCopyWithImpl<$Res>
    implements $EditBookReqDtoCopyWith<$Res> {
  _$EditBookReqDtoCopyWithImpl(this._value, this._then);

  final EditBookReqDto _value;
  // ignore: unused_field
  final $Res Function(EditBookReqDto) _then;

  @override
  $Res call({
    Object? accountBookSeq = freezed,
    Object? accountBookName = freezed,
    Object? accountBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? useYn = freezed,
    Object? userInfo = freezed,
  }) {
    return _then(_value.copyWith(
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String?,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
      userInfo: userInfo == freezed
          ? _value.userInfo
          : userInfo // ignore: cast_nullable_to_non_nullable
              as CreateBookMemberDto?,
    ));
  }

  @override
  $CreateBookMemberDtoCopyWith<$Res>? get userInfo {
    if (_value.userInfo == null) {
      return null;
    }

    return $CreateBookMemberDtoCopyWith<$Res>(_value.userInfo!, (value) {
      return _then(_value.copyWith(userInfo: value));
    });
  }
}

/// @nodoc
abstract class _$EditBookReqDtoCopyWith<$Res>
    implements $EditBookReqDtoCopyWith<$Res> {
  factory _$EditBookReqDtoCopyWith(
          _EditBookReqDto value, $Res Function(_EditBookReqDto) then) =
      __$EditBookReqDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num accountBookSeq,
      String? accountBookName,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn,
      CreateBookMemberDto? userInfo});

  @override
  $CreateBookMemberDtoCopyWith<$Res>? get userInfo;
}

/// @nodoc
class __$EditBookReqDtoCopyWithImpl<$Res>
    extends _$EditBookReqDtoCopyWithImpl<$Res>
    implements _$EditBookReqDtoCopyWith<$Res> {
  __$EditBookReqDtoCopyWithImpl(
      _EditBookReqDto _value, $Res Function(_EditBookReqDto) _then)
      : super(_value, (v) => _then(v as _EditBookReqDto));

  @override
  _EditBookReqDto get _value => super._value as _EditBookReqDto;

  @override
  $Res call({
    Object? accountBookSeq = freezed,
    Object? accountBookName = freezed,
    Object? accountBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? useYn = freezed,
    Object? userInfo = freezed,
  }) {
    return _then(_EditBookReqDto(
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String?,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
      userInfo: userInfo == freezed
          ? _value.userInfo
          : userInfo // ignore: cast_nullable_to_non_nullable
              as CreateBookMemberDto?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_EditBookReqDto implements _EditBookReqDto {
  _$_EditBookReqDto(
      {required this.accountBookSeq,
      this.accountBookName,
      this.accountBookDesc,
      this.startDate,
      this.endDate,
      this.imgURL,
      this.color,
      this.useYn,
      this.userInfo});

  factory _$_EditBookReqDto.fromJson(Map<String, dynamic> json) =>
      _$$_EditBookReqDtoFromJson(json);

  @override
  final num accountBookSeq;
  @override
  final String? accountBookName;
  @override
  final String? accountBookDesc;
  @override
  final DateTime? startDate;
  @override
  final DateTime? endDate;
  @override
  final String? imgURL;
  @override
  final String? color;
  @override
  final String? useYn;
  @override
  final CreateBookMemberDto? userInfo;

  @override
  String toString() {
    return 'EditBookReqDto(accountBookSeq: $accountBookSeq, accountBookName: $accountBookName, accountBookDesc: $accountBookDesc, startDate: $startDate, endDate: $endDate, imgURL: $imgURL, color: $color, useYn: $useYn, userInfo: $userInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _EditBookReqDto &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq) &&
            (identical(other.accountBookName, accountBookName) ||
                other.accountBookName == accountBookName) &&
            (identical(other.accountBookDesc, accountBookDesc) ||
                other.accountBookDesc == accountBookDesc) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.endDate, endDate) || other.endDate == endDate) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.useYn, useYn) || other.useYn == useYn) &&
            (identical(other.userInfo, userInfo) ||
                other.userInfo == userInfo));
  }

  @override
  int get hashCode => Object.hash(runtimeType, accountBookSeq, accountBookName,
      accountBookDesc, startDate, endDate, imgURL, color, useYn, userInfo);

  @JsonKey(ignore: true)
  @override
  _$EditBookReqDtoCopyWith<_EditBookReqDto> get copyWith =>
      __$EditBookReqDtoCopyWithImpl<_EditBookReqDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_EditBookReqDtoToJson(this);
  }
}

abstract class _EditBookReqDto implements EditBookReqDto {
  factory _EditBookReqDto(
      {required num accountBookSeq,
      String? accountBookName,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn,
      CreateBookMemberDto? userInfo}) = _$_EditBookReqDto;

  factory _EditBookReqDto.fromJson(Map<String, dynamic> json) =
      _$_EditBookReqDto.fromJson;

  @override
  num get accountBookSeq;
  @override
  String? get accountBookName;
  @override
  String? get accountBookDesc;
  @override
  DateTime? get startDate;
  @override
  DateTime? get endDate;
  @override
  String? get imgURL;
  @override
  String? get color;
  @override
  String? get useYn;
  @override
  CreateBookMemberDto? get userInfo;
  @override
  @JsonKey(ignore: true)
  _$EditBookReqDtoCopyWith<_EditBookReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}
