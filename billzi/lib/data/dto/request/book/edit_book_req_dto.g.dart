// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_book_req_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_EditBookReqDto _$$_EditBookReqDtoFromJson(Map<String, dynamic> json) =>
    _$_EditBookReqDto(
      accountBookSeq: json['accountBookSeq'] as num,
      accountBookName: json['accountBookName'] as String?,
      accountBookDesc: json['accountBookDesc'] as String?,
      startDate: json['startDate'] == null
          ? null
          : DateTime.parse(json['startDate'] as String),
      endDate: json['endDate'] == null
          ? null
          : DateTime.parse(json['endDate'] as String),
      imgURL: json['imgURL'] as String?,
      color: json['color'] as String?,
      useYn: json['useYn'] as String?,
      userInfo: json['userInfo'] == null
          ? null
          : CreateBookMemberDto.fromJson(
              json['userInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_EditBookReqDtoToJson(_$_EditBookReqDto instance) =>
    <String, dynamic>{
      'accountBookSeq': instance.accountBookSeq,
      'accountBookName': instance.accountBookName,
      'accountBookDesc': instance.accountBookDesc,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
      'imgURL': instance.imgURL,
      'color': instance.color,
      'useYn': instance.useYn,
      'userInfo': instance.userInfo,
    };
