// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'reorder_book_list_req_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ReorderBooksReqDto _$ReorderBooksReqDtoFromJson(Map<String, dynamic> json) {
  return _ReorderBooksReqDto.fromJson(json);
}

/// @nodoc
class _$ReorderBooksReqDtoTearOff {
  const _$ReorderBooksReqDtoTearOff();

  _ReorderBooksReqDto call(
      {required num accountBookSeq, required num orderNo}) {
    return _ReorderBooksReqDto(
      accountBookSeq: accountBookSeq,
      orderNo: orderNo,
    );
  }

  ReorderBooksReqDto fromJson(Map<String, Object?> json) {
    return ReorderBooksReqDto.fromJson(json);
  }
}

/// @nodoc
const $ReorderBooksReqDto = _$ReorderBooksReqDtoTearOff();

/// @nodoc
mixin _$ReorderBooksReqDto {
  num get accountBookSeq => throw _privateConstructorUsedError;
  num get orderNo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ReorderBooksReqDtoCopyWith<ReorderBooksReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReorderBooksReqDtoCopyWith<$Res> {
  factory $ReorderBooksReqDtoCopyWith(
          ReorderBooksReqDto value, $Res Function(ReorderBooksReqDto) then) =
      _$ReorderBooksReqDtoCopyWithImpl<$Res>;
  $Res call({num accountBookSeq, num orderNo});
}

/// @nodoc
class _$ReorderBooksReqDtoCopyWithImpl<$Res>
    implements $ReorderBooksReqDtoCopyWith<$Res> {
  _$ReorderBooksReqDtoCopyWithImpl(this._value, this._then);

  final ReorderBooksReqDto _value;
  // ignore: unused_field
  final $Res Function(ReorderBooksReqDto) _then;

  @override
  $Res call({
    Object? accountBookSeq = freezed,
    Object? orderNo = freezed,
  }) {
    return _then(_value.copyWith(
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      orderNo: orderNo == freezed
          ? _value.orderNo
          : orderNo // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
abstract class _$ReorderBooksReqDtoCopyWith<$Res>
    implements $ReorderBooksReqDtoCopyWith<$Res> {
  factory _$ReorderBooksReqDtoCopyWith(
          _ReorderBooksReqDto value, $Res Function(_ReorderBooksReqDto) then) =
      __$ReorderBooksReqDtoCopyWithImpl<$Res>;
  @override
  $Res call({num accountBookSeq, num orderNo});
}

/// @nodoc
class __$ReorderBooksReqDtoCopyWithImpl<$Res>
    extends _$ReorderBooksReqDtoCopyWithImpl<$Res>
    implements _$ReorderBooksReqDtoCopyWith<$Res> {
  __$ReorderBooksReqDtoCopyWithImpl(
      _ReorderBooksReqDto _value, $Res Function(_ReorderBooksReqDto) _then)
      : super(_value, (v) => _then(v as _ReorderBooksReqDto));

  @override
  _ReorderBooksReqDto get _value => super._value as _ReorderBooksReqDto;

  @override
  $Res call({
    Object? accountBookSeq = freezed,
    Object? orderNo = freezed,
  }) {
    return _then(_ReorderBooksReqDto(
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      orderNo: orderNo == freezed
          ? _value.orderNo
          : orderNo // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ReorderBooksReqDto implements _ReorderBooksReqDto {
  _$_ReorderBooksReqDto({required this.accountBookSeq, required this.orderNo});

  factory _$_ReorderBooksReqDto.fromJson(Map<String, dynamic> json) =>
      _$$_ReorderBooksReqDtoFromJson(json);

  @override
  final num accountBookSeq;
  @override
  final num orderNo;

  @override
  String toString() {
    return 'ReorderBooksReqDto(accountBookSeq: $accountBookSeq, orderNo: $orderNo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ReorderBooksReqDto &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq) &&
            (identical(other.orderNo, orderNo) || other.orderNo == orderNo));
  }

  @override
  int get hashCode => Object.hash(runtimeType, accountBookSeq, orderNo);

  @JsonKey(ignore: true)
  @override
  _$ReorderBooksReqDtoCopyWith<_ReorderBooksReqDto> get copyWith =>
      __$ReorderBooksReqDtoCopyWithImpl<_ReorderBooksReqDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ReorderBooksReqDtoToJson(this);
  }
}

abstract class _ReorderBooksReqDto implements ReorderBooksReqDto {
  factory _ReorderBooksReqDto(
      {required num accountBookSeq,
      required num orderNo}) = _$_ReorderBooksReqDto;

  factory _ReorderBooksReqDto.fromJson(Map<String, dynamic> json) =
      _$_ReorderBooksReqDto.fromJson;

  @override
  num get accountBookSeq;
  @override
  num get orderNo;
  @override
  @JsonKey(ignore: true)
  _$ReorderBooksReqDtoCopyWith<_ReorderBooksReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}
