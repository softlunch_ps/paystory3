// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_book_req_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CreateBookReqDto _$$_CreateBookReqDtoFromJson(Map<String, dynamic> json) =>
    _$_CreateBookReqDto(
      accountBookName: json['accountBookName'] as String,
      accountBookDesc: json['accountBookDesc'] as String?,
      imgURL: json['imgURL'] as String?,
      color: json['color'] as String?,
      startDate: json['startDate'] == null
          ? null
          : DateTime.parse(json['startDate'] as String),
      endDate: json['endDate'] == null
          ? null
          : DateTime.parse(json['endDate'] as String),
      userInfo: json['userInfo'] == null
          ? null
          : CreateBookMemberDto.fromJson(
              json['userInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_CreateBookReqDtoToJson(_$_CreateBookReqDto instance) =>
    <String, dynamic>{
      'accountBookName': instance.accountBookName,
      'accountBookDesc': instance.accountBookDesc,
      'imgURL': instance.imgURL,
      'color': instance.color,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
      'userInfo': instance.userInfo,
    };
