// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'create_book_req_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CreateBookReqDto _$CreateBookReqDtoFromJson(Map<String, dynamic> json) {
  return _CreateBookReqDto.fromJson(json);
}

/// @nodoc
class _$CreateBookReqDtoTearOff {
  const _$CreateBookReqDtoTearOff();

  _CreateBookReqDto call(
      {required String accountBookName,
      String? accountBookDesc,
      String? imgURL,
      String? color,
      DateTime? startDate,
      DateTime? endDate,
      CreateBookMemberDto? userInfo}) {
    return _CreateBookReqDto(
      accountBookName: accountBookName,
      accountBookDesc: accountBookDesc,
      imgURL: imgURL,
      color: color,
      startDate: startDate,
      endDate: endDate,
      userInfo: userInfo,
    );
  }

  CreateBookReqDto fromJson(Map<String, Object?> json) {
    return CreateBookReqDto.fromJson(json);
  }
}

/// @nodoc
const $CreateBookReqDto = _$CreateBookReqDtoTearOff();

/// @nodoc
mixin _$CreateBookReqDto {
  String get accountBookName => throw _privateConstructorUsedError;
  String? get accountBookDesc => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  DateTime? get startDate => throw _privateConstructorUsedError;
  DateTime? get endDate => throw _privateConstructorUsedError;
  CreateBookMemberDto? get userInfo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CreateBookReqDtoCopyWith<CreateBookReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateBookReqDtoCopyWith<$Res> {
  factory $CreateBookReqDtoCopyWith(
          CreateBookReqDto value, $Res Function(CreateBookReqDto) then) =
      _$CreateBookReqDtoCopyWithImpl<$Res>;
  $Res call(
      {String accountBookName,
      String? accountBookDesc,
      String? imgURL,
      String? color,
      DateTime? startDate,
      DateTime? endDate,
      CreateBookMemberDto? userInfo});

  $CreateBookMemberDtoCopyWith<$Res>? get userInfo;
}

/// @nodoc
class _$CreateBookReqDtoCopyWithImpl<$Res>
    implements $CreateBookReqDtoCopyWith<$Res> {
  _$CreateBookReqDtoCopyWithImpl(this._value, this._then);

  final CreateBookReqDto _value;
  // ignore: unused_field
  final $Res Function(CreateBookReqDto) _then;

  @override
  $Res call({
    Object? accountBookName = freezed,
    Object? accountBookDesc = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? userInfo = freezed,
  }) {
    return _then(_value.copyWith(
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      userInfo: userInfo == freezed
          ? _value.userInfo
          : userInfo // ignore: cast_nullable_to_non_nullable
              as CreateBookMemberDto?,
    ));
  }

  @override
  $CreateBookMemberDtoCopyWith<$Res>? get userInfo {
    if (_value.userInfo == null) {
      return null;
    }

    return $CreateBookMemberDtoCopyWith<$Res>(_value.userInfo!, (value) {
      return _then(_value.copyWith(userInfo: value));
    });
  }
}

/// @nodoc
abstract class _$CreateBookReqDtoCopyWith<$Res>
    implements $CreateBookReqDtoCopyWith<$Res> {
  factory _$CreateBookReqDtoCopyWith(
          _CreateBookReqDto value, $Res Function(_CreateBookReqDto) then) =
      __$CreateBookReqDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String accountBookName,
      String? accountBookDesc,
      String? imgURL,
      String? color,
      DateTime? startDate,
      DateTime? endDate,
      CreateBookMemberDto? userInfo});

  @override
  $CreateBookMemberDtoCopyWith<$Res>? get userInfo;
}

/// @nodoc
class __$CreateBookReqDtoCopyWithImpl<$Res>
    extends _$CreateBookReqDtoCopyWithImpl<$Res>
    implements _$CreateBookReqDtoCopyWith<$Res> {
  __$CreateBookReqDtoCopyWithImpl(
      _CreateBookReqDto _value, $Res Function(_CreateBookReqDto) _then)
      : super(_value, (v) => _then(v as _CreateBookReqDto));

  @override
  _CreateBookReqDto get _value => super._value as _CreateBookReqDto;

  @override
  $Res call({
    Object? accountBookName = freezed,
    Object? accountBookDesc = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? userInfo = freezed,
  }) {
    return _then(_CreateBookReqDto(
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      userInfo: userInfo == freezed
          ? _value.userInfo
          : userInfo // ignore: cast_nullable_to_non_nullable
              as CreateBookMemberDto?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CreateBookReqDto implements _CreateBookReqDto {
  _$_CreateBookReqDto(
      {required this.accountBookName,
      this.accountBookDesc,
      this.imgURL,
      this.color,
      this.startDate,
      this.endDate,
      this.userInfo});

  factory _$_CreateBookReqDto.fromJson(Map<String, dynamic> json) =>
      _$$_CreateBookReqDtoFromJson(json);

  @override
  final String accountBookName;
  @override
  final String? accountBookDesc;
  @override
  final String? imgURL;
  @override
  final String? color;
  @override
  final DateTime? startDate;
  @override
  final DateTime? endDate;
  @override
  final CreateBookMemberDto? userInfo;

  @override
  String toString() {
    return 'CreateBookReqDto(accountBookName: $accountBookName, accountBookDesc: $accountBookDesc, imgURL: $imgURL, color: $color, startDate: $startDate, endDate: $endDate, userInfo: $userInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CreateBookReqDto &&
            (identical(other.accountBookName, accountBookName) ||
                other.accountBookName == accountBookName) &&
            (identical(other.accountBookDesc, accountBookDesc) ||
                other.accountBookDesc == accountBookDesc) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.endDate, endDate) || other.endDate == endDate) &&
            (identical(other.userInfo, userInfo) ||
                other.userInfo == userInfo));
  }

  @override
  int get hashCode => Object.hash(runtimeType, accountBookName, accountBookDesc,
      imgURL, color, startDate, endDate, userInfo);

  @JsonKey(ignore: true)
  @override
  _$CreateBookReqDtoCopyWith<_CreateBookReqDto> get copyWith =>
      __$CreateBookReqDtoCopyWithImpl<_CreateBookReqDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CreateBookReqDtoToJson(this);
  }
}

abstract class _CreateBookReqDto implements CreateBookReqDto {
  factory _CreateBookReqDto(
      {required String accountBookName,
      String? accountBookDesc,
      String? imgURL,
      String? color,
      DateTime? startDate,
      DateTime? endDate,
      CreateBookMemberDto? userInfo}) = _$_CreateBookReqDto;

  factory _CreateBookReqDto.fromJson(Map<String, dynamic> json) =
      _$_CreateBookReqDto.fromJson;

  @override
  String get accountBookName;
  @override
  String? get accountBookDesc;
  @override
  String? get imgURL;
  @override
  String? get color;
  @override
  DateTime? get startDate;
  @override
  DateTime? get endDate;
  @override
  CreateBookMemberDto? get userInfo;
  @override
  @JsonKey(ignore: true)
  _$CreateBookReqDtoCopyWith<_CreateBookReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}
