// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detach_book_req_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DetachBookReqDto _$$_DetachBookReqDtoFromJson(Map<String, dynamic> json) =>
    _$_DetachBookReqDto(
      targetUserSeq: json['targetUserSeq'] as num,
      accountBookSeq: json['accountBookSeq'] as num,
    );

Map<String, dynamic> _$$_DetachBookReqDtoToJson(_$_DetachBookReqDto instance) =>
    <String, dynamic>{
      'targetUserSeq': instance.targetUserSeq,
      'accountBookSeq': instance.accountBookSeq,
    };
