import 'package:freezed_annotation/freezed_annotation.dart';

part 'reorder_book_list_req_dto.freezed.dart';
part 'reorder_book_list_req_dto.g.dart';

@freezed
class ReorderBooksReqDto with _$ReorderBooksReqDto {
  factory ReorderBooksReqDto({
    required num accountBookSeq,
    required num orderNo,
  }) = _ReorderBooksReqDto;

  factory ReorderBooksReqDto.fromJson(Map<String, dynamic> json) =>
      _$ReorderBooksReqDtoFromJson(json);
}
