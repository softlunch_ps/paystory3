import 'package:freezed_annotation/freezed_annotation.dart';

part 'detach_book_req_dto.freezed.dart';
part 'detach_book_req_dto.g.dart';

@freezed
class DetachBookReqDto with _$DetachBookReqDto {
  factory DetachBookReqDto({
    required num targetUserSeq,
    required num accountBookSeq,
  }) = _DetachBookReqDto;

  factory DetachBookReqDto.fromJson(Map<String, dynamic> json) =>
      _$DetachBookReqDtoFromJson(json);
}
