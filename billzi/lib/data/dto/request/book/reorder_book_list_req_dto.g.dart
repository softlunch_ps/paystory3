// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reorder_book_list_req_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ReorderBooksReqDto _$$_ReorderBooksReqDtoFromJson(
        Map<String, dynamic> json) =>
    _$_ReorderBooksReqDto(
      accountBookSeq: json['accountBookSeq'] as num,
      orderNo: json['orderNo'] as num,
    );

Map<String, dynamic> _$$_ReorderBooksReqDtoToJson(
        _$_ReorderBooksReqDto instance) =>
    <String, dynamic>{
      'accountBookSeq': instance.accountBookSeq,
      'orderNo': instance.orderNo,
    };
