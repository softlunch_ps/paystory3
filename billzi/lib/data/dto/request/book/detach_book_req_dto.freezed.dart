// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'detach_book_req_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DetachBookReqDto _$DetachBookReqDtoFromJson(Map<String, dynamic> json) {
  return _DetachBookReqDto.fromJson(json);
}

/// @nodoc
class _$DetachBookReqDtoTearOff {
  const _$DetachBookReqDtoTearOff();

  _DetachBookReqDto call(
      {required num targetUserSeq, required num accountBookSeq}) {
    return _DetachBookReqDto(
      targetUserSeq: targetUserSeq,
      accountBookSeq: accountBookSeq,
    );
  }

  DetachBookReqDto fromJson(Map<String, Object?> json) {
    return DetachBookReqDto.fromJson(json);
  }
}

/// @nodoc
const $DetachBookReqDto = _$DetachBookReqDtoTearOff();

/// @nodoc
mixin _$DetachBookReqDto {
  num get targetUserSeq => throw _privateConstructorUsedError;
  num get accountBookSeq => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DetachBookReqDtoCopyWith<DetachBookReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DetachBookReqDtoCopyWith<$Res> {
  factory $DetachBookReqDtoCopyWith(
          DetachBookReqDto value, $Res Function(DetachBookReqDto) then) =
      _$DetachBookReqDtoCopyWithImpl<$Res>;
  $Res call({num targetUserSeq, num accountBookSeq});
}

/// @nodoc
class _$DetachBookReqDtoCopyWithImpl<$Res>
    implements $DetachBookReqDtoCopyWith<$Res> {
  _$DetachBookReqDtoCopyWithImpl(this._value, this._then);

  final DetachBookReqDto _value;
  // ignore: unused_field
  final $Res Function(DetachBookReqDto) _then;

  @override
  $Res call({
    Object? targetUserSeq = freezed,
    Object? accountBookSeq = freezed,
  }) {
    return _then(_value.copyWith(
      targetUserSeq: targetUserSeq == freezed
          ? _value.targetUserSeq
          : targetUserSeq // ignore: cast_nullable_to_non_nullable
              as num,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
abstract class _$DetachBookReqDtoCopyWith<$Res>
    implements $DetachBookReqDtoCopyWith<$Res> {
  factory _$DetachBookReqDtoCopyWith(
          _DetachBookReqDto value, $Res Function(_DetachBookReqDto) then) =
      __$DetachBookReqDtoCopyWithImpl<$Res>;
  @override
  $Res call({num targetUserSeq, num accountBookSeq});
}

/// @nodoc
class __$DetachBookReqDtoCopyWithImpl<$Res>
    extends _$DetachBookReqDtoCopyWithImpl<$Res>
    implements _$DetachBookReqDtoCopyWith<$Res> {
  __$DetachBookReqDtoCopyWithImpl(
      _DetachBookReqDto _value, $Res Function(_DetachBookReqDto) _then)
      : super(_value, (v) => _then(v as _DetachBookReqDto));

  @override
  _DetachBookReqDto get _value => super._value as _DetachBookReqDto;

  @override
  $Res call({
    Object? targetUserSeq = freezed,
    Object? accountBookSeq = freezed,
  }) {
    return _then(_DetachBookReqDto(
      targetUserSeq: targetUserSeq == freezed
          ? _value.targetUserSeq
          : targetUserSeq // ignore: cast_nullable_to_non_nullable
              as num,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DetachBookReqDto implements _DetachBookReqDto {
  _$_DetachBookReqDto(
      {required this.targetUserSeq, required this.accountBookSeq});

  factory _$_DetachBookReqDto.fromJson(Map<String, dynamic> json) =>
      _$$_DetachBookReqDtoFromJson(json);

  @override
  final num targetUserSeq;
  @override
  final num accountBookSeq;

  @override
  String toString() {
    return 'DetachBookReqDto(targetUserSeq: $targetUserSeq, accountBookSeq: $accountBookSeq)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _DetachBookReqDto &&
            (identical(other.targetUserSeq, targetUserSeq) ||
                other.targetUserSeq == targetUserSeq) &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq));
  }

  @override
  int get hashCode => Object.hash(runtimeType, targetUserSeq, accountBookSeq);

  @JsonKey(ignore: true)
  @override
  _$DetachBookReqDtoCopyWith<_DetachBookReqDto> get copyWith =>
      __$DetachBookReqDtoCopyWithImpl<_DetachBookReqDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DetachBookReqDtoToJson(this);
  }
}

abstract class _DetachBookReqDto implements DetachBookReqDto {
  factory _DetachBookReqDto(
      {required num targetUserSeq,
      required num accountBookSeq}) = _$_DetachBookReqDto;

  factory _DetachBookReqDto.fromJson(Map<String, dynamic> json) =
      _$_DetachBookReqDto.fromJson;

  @override
  num get targetUserSeq;
  @override
  num get accountBookSeq;
  @override
  @JsonKey(ignore: true)
  _$DetachBookReqDtoCopyWith<_DetachBookReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}
