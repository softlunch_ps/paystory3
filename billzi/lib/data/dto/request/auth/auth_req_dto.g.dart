// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_req_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AuthReqDto _$$_AuthReqDtoFromJson(Map<String, dynamic> json) =>
    _$_AuthReqDto(
      prtyUserId: json['prtyUserId'] as String,
    );

Map<String, dynamic> _$$_AuthReqDtoToJson(_$_AuthReqDto instance) =>
    <String, dynamic>{
      'prtyUserId': instance.prtyUserId,
    };
