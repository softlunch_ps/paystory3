// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'logout_req_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LogoutReqDto _$LogoutReqDtoFromJson(Map<String, dynamic> json) {
  return _LogoutReqDto.fromJson(json);
}

/// @nodoc
class _$LogoutReqDtoTearOff {
  const _$LogoutReqDtoTearOff();

  _LogoutReqDto call({required num userSeq}) {
    return _LogoutReqDto(
      userSeq: userSeq,
    );
  }

  LogoutReqDto fromJson(Map<String, Object?> json) {
    return LogoutReqDto.fromJson(json);
  }
}

/// @nodoc
const $LogoutReqDto = _$LogoutReqDtoTearOff();

/// @nodoc
mixin _$LogoutReqDto {
  num get userSeq => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LogoutReqDtoCopyWith<LogoutReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LogoutReqDtoCopyWith<$Res> {
  factory $LogoutReqDtoCopyWith(
          LogoutReqDto value, $Res Function(LogoutReqDto) then) =
      _$LogoutReqDtoCopyWithImpl<$Res>;
  $Res call({num userSeq});
}

/// @nodoc
class _$LogoutReqDtoCopyWithImpl<$Res> implements $LogoutReqDtoCopyWith<$Res> {
  _$LogoutReqDtoCopyWithImpl(this._value, this._then);

  final LogoutReqDto _value;
  // ignore: unused_field
  final $Res Function(LogoutReqDto) _then;

  @override
  $Res call({
    Object? userSeq = freezed,
  }) {
    return _then(_value.copyWith(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
abstract class _$LogoutReqDtoCopyWith<$Res>
    implements $LogoutReqDtoCopyWith<$Res> {
  factory _$LogoutReqDtoCopyWith(
          _LogoutReqDto value, $Res Function(_LogoutReqDto) then) =
      __$LogoutReqDtoCopyWithImpl<$Res>;
  @override
  $Res call({num userSeq});
}

/// @nodoc
class __$LogoutReqDtoCopyWithImpl<$Res> extends _$LogoutReqDtoCopyWithImpl<$Res>
    implements _$LogoutReqDtoCopyWith<$Res> {
  __$LogoutReqDtoCopyWithImpl(
      _LogoutReqDto _value, $Res Function(_LogoutReqDto) _then)
      : super(_value, (v) => _then(v as _LogoutReqDto));

  @override
  _LogoutReqDto get _value => super._value as _LogoutReqDto;

  @override
  $Res call({
    Object? userSeq = freezed,
  }) {
    return _then(_LogoutReqDto(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LogoutReqDto implements _LogoutReqDto {
  _$_LogoutReqDto({required this.userSeq});

  factory _$_LogoutReqDto.fromJson(Map<String, dynamic> json) =>
      _$$_LogoutReqDtoFromJson(json);

  @override
  final num userSeq;

  @override
  String toString() {
    return 'LogoutReqDto(userSeq: $userSeq)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _LogoutReqDto &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userSeq);

  @JsonKey(ignore: true)
  @override
  _$LogoutReqDtoCopyWith<_LogoutReqDto> get copyWith =>
      __$LogoutReqDtoCopyWithImpl<_LogoutReqDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LogoutReqDtoToJson(this);
  }
}

abstract class _LogoutReqDto implements LogoutReqDto {
  factory _LogoutReqDto({required num userSeq}) = _$_LogoutReqDto;

  factory _LogoutReqDto.fromJson(Map<String, dynamic> json) =
      _$_LogoutReqDto.fromJson;

  @override
  num get userSeq;
  @override
  @JsonKey(ignore: true)
  _$LogoutReqDtoCopyWith<_LogoutReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}
