// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'auth_req_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AuthReqDto _$AuthReqDtoFromJson(Map<String, dynamic> json) {
  return _AuthReqDto.fromJson(json);
}

/// @nodoc
class _$AuthReqDtoTearOff {
  const _$AuthReqDtoTearOff();

  _AuthReqDto call({required String prtyUserId}) {
    return _AuthReqDto(
      prtyUserId: prtyUserId,
    );
  }

  AuthReqDto fromJson(Map<String, Object?> json) {
    return AuthReqDto.fromJson(json);
  }
}

/// @nodoc
const $AuthReqDto = _$AuthReqDtoTearOff();

/// @nodoc
mixin _$AuthReqDto {
  String get prtyUserId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthReqDtoCopyWith<AuthReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthReqDtoCopyWith<$Res> {
  factory $AuthReqDtoCopyWith(
          AuthReqDto value, $Res Function(AuthReqDto) then) =
      _$AuthReqDtoCopyWithImpl<$Res>;
  $Res call({String prtyUserId});
}

/// @nodoc
class _$AuthReqDtoCopyWithImpl<$Res> implements $AuthReqDtoCopyWith<$Res> {
  _$AuthReqDtoCopyWithImpl(this._value, this._then);

  final AuthReqDto _value;
  // ignore: unused_field
  final $Res Function(AuthReqDto) _then;

  @override
  $Res call({
    Object? prtyUserId = freezed,
  }) {
    return _then(_value.copyWith(
      prtyUserId: prtyUserId == freezed
          ? _value.prtyUserId
          : prtyUserId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$AuthReqDtoCopyWith<$Res> implements $AuthReqDtoCopyWith<$Res> {
  factory _$AuthReqDtoCopyWith(
          _AuthReqDto value, $Res Function(_AuthReqDto) then) =
      __$AuthReqDtoCopyWithImpl<$Res>;
  @override
  $Res call({String prtyUserId});
}

/// @nodoc
class __$AuthReqDtoCopyWithImpl<$Res> extends _$AuthReqDtoCopyWithImpl<$Res>
    implements _$AuthReqDtoCopyWith<$Res> {
  __$AuthReqDtoCopyWithImpl(
      _AuthReqDto _value, $Res Function(_AuthReqDto) _then)
      : super(_value, (v) => _then(v as _AuthReqDto));

  @override
  _AuthReqDto get _value => super._value as _AuthReqDto;

  @override
  $Res call({
    Object? prtyUserId = freezed,
  }) {
    return _then(_AuthReqDto(
      prtyUserId: prtyUserId == freezed
          ? _value.prtyUserId
          : prtyUserId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AuthReqDto implements _AuthReqDto {
  _$_AuthReqDto({required this.prtyUserId});

  factory _$_AuthReqDto.fromJson(Map<String, dynamic> json) =>
      _$$_AuthReqDtoFromJson(json);

  @override
  final String prtyUserId;

  @override
  String toString() {
    return 'AuthReqDto(prtyUserId: $prtyUserId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthReqDto &&
            (identical(other.prtyUserId, prtyUserId) ||
                other.prtyUserId == prtyUserId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, prtyUserId);

  @JsonKey(ignore: true)
  @override
  _$AuthReqDtoCopyWith<_AuthReqDto> get copyWith =>
      __$AuthReqDtoCopyWithImpl<_AuthReqDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AuthReqDtoToJson(this);
  }
}

abstract class _AuthReqDto implements AuthReqDto {
  factory _AuthReqDto({required String prtyUserId}) = _$_AuthReqDto;

  factory _AuthReqDto.fromJson(Map<String, dynamic> json) =
      _$_AuthReqDto.fromJson;

  @override
  String get prtyUserId;
  @override
  @JsonKey(ignore: true)
  _$AuthReqDtoCopyWith<_AuthReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}
