import 'package:freezed_annotation/freezed_annotation.dart';

part 'logout_req_dto.freezed.dart';
part 'logout_req_dto.g.dart';

@freezed
class LogoutReqDto with _$LogoutReqDto {
  factory LogoutReqDto({
    required num userSeq,
  }) = _LogoutReqDto;

  factory LogoutReqDto.fromJson(Map<String, dynamic> json) =>
      _$LogoutReqDtoFromJson(json);
}
