import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_req_dto.freezed.dart';
part 'auth_req_dto.g.dart';

@freezed
class AuthReqDto with _$AuthReqDto {
  factory AuthReqDto({
    required String prtyUserId,
  }) = _AuthReqDto;

  factory AuthReqDto.fromJson(Map<String, dynamic> json) =>
      _$AuthReqDtoFromJson(json);
}
