// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logout_req_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LogoutReqDto _$$_LogoutReqDtoFromJson(Map<String, dynamic> json) =>
    _$_LogoutReqDto(
      userSeq: json['userSeq'] as num,
    );

Map<String, dynamic> _$$_LogoutReqDtoToJson(_$_LogoutReqDto instance) =>
    <String, dynamic>{
      'userSeq': instance.userSeq,
    };
