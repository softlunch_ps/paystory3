// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_req_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ConfigReqDto _$$_ConfigReqDtoFromJson(Map<String, dynamic> json) =>
    _$_ConfigReqDto(
      accountBook: json['accountBook'] == null
          ? null
          : AccountBook.fromJson(json['accountBook'] as Map<String, dynamic>),
      appPush: json['appPush'] == null
          ? null
          : AppPush.fromJson(json['appPush'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ConfigReqDtoToJson(_$_ConfigReqDto instance) =>
    <String, dynamic>{
      'accountBook': instance.accountBook,
      'appPush': instance.appPush,
    };
