// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'config_req_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ConfigReqDto _$ConfigReqDtoFromJson(Map<String, dynamic> json) {
  return _ConfigReqDto.fromJson(json);
}

/// @nodoc
class _$ConfigReqDtoTearOff {
  const _$ConfigReqDtoTearOff();

  _ConfigReqDto call({AccountBook? accountBook, AppPush? appPush}) {
    return _ConfigReqDto(
      accountBook: accountBook,
      appPush: appPush,
    );
  }

  ConfigReqDto fromJson(Map<String, Object?> json) {
    return ConfigReqDto.fromJson(json);
  }
}

/// @nodoc
const $ConfigReqDto = _$ConfigReqDtoTearOff();

/// @nodoc
mixin _$ConfigReqDto {
  AccountBook? get accountBook => throw _privateConstructorUsedError;
  AppPush? get appPush => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ConfigReqDtoCopyWith<ConfigReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ConfigReqDtoCopyWith<$Res> {
  factory $ConfigReqDtoCopyWith(
          ConfigReqDto value, $Res Function(ConfigReqDto) then) =
      _$ConfigReqDtoCopyWithImpl<$Res>;
  $Res call({AccountBook? accountBook, AppPush? appPush});

  $AccountBookCopyWith<$Res>? get accountBook;
  $AppPushCopyWith<$Res>? get appPush;
}

/// @nodoc
class _$ConfigReqDtoCopyWithImpl<$Res> implements $ConfigReqDtoCopyWith<$Res> {
  _$ConfigReqDtoCopyWithImpl(this._value, this._then);

  final ConfigReqDto _value;
  // ignore: unused_field
  final $Res Function(ConfigReqDto) _then;

  @override
  $Res call({
    Object? accountBook = freezed,
    Object? appPush = freezed,
  }) {
    return _then(_value.copyWith(
      accountBook: accountBook == freezed
          ? _value.accountBook
          : accountBook // ignore: cast_nullable_to_non_nullable
              as AccountBook?,
      appPush: appPush == freezed
          ? _value.appPush
          : appPush // ignore: cast_nullable_to_non_nullable
              as AppPush?,
    ));
  }

  @override
  $AccountBookCopyWith<$Res>? get accountBook {
    if (_value.accountBook == null) {
      return null;
    }

    return $AccountBookCopyWith<$Res>(_value.accountBook!, (value) {
      return _then(_value.copyWith(accountBook: value));
    });
  }

  @override
  $AppPushCopyWith<$Res>? get appPush {
    if (_value.appPush == null) {
      return null;
    }

    return $AppPushCopyWith<$Res>(_value.appPush!, (value) {
      return _then(_value.copyWith(appPush: value));
    });
  }
}

/// @nodoc
abstract class _$ConfigReqDtoCopyWith<$Res>
    implements $ConfigReqDtoCopyWith<$Res> {
  factory _$ConfigReqDtoCopyWith(
          _ConfigReqDto value, $Res Function(_ConfigReqDto) then) =
      __$ConfigReqDtoCopyWithImpl<$Res>;
  @override
  $Res call({AccountBook? accountBook, AppPush? appPush});

  @override
  $AccountBookCopyWith<$Res>? get accountBook;
  @override
  $AppPushCopyWith<$Res>? get appPush;
}

/// @nodoc
class __$ConfigReqDtoCopyWithImpl<$Res> extends _$ConfigReqDtoCopyWithImpl<$Res>
    implements _$ConfigReqDtoCopyWith<$Res> {
  __$ConfigReqDtoCopyWithImpl(
      _ConfigReqDto _value, $Res Function(_ConfigReqDto) _then)
      : super(_value, (v) => _then(v as _ConfigReqDto));

  @override
  _ConfigReqDto get _value => super._value as _ConfigReqDto;

  @override
  $Res call({
    Object? accountBook = freezed,
    Object? appPush = freezed,
  }) {
    return _then(_ConfigReqDto(
      accountBook: accountBook == freezed
          ? _value.accountBook
          : accountBook // ignore: cast_nullable_to_non_nullable
              as AccountBook?,
      appPush: appPush == freezed
          ? _value.appPush
          : appPush // ignore: cast_nullable_to_non_nullable
              as AppPush?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ConfigReqDto implements _ConfigReqDto {
  _$_ConfigReqDto({this.accountBook, this.appPush});

  factory _$_ConfigReqDto.fromJson(Map<String, dynamic> json) =>
      _$$_ConfigReqDtoFromJson(json);

  @override
  final AccountBook? accountBook;
  @override
  final AppPush? appPush;

  @override
  String toString() {
    return 'ConfigReqDto(accountBook: $accountBook, appPush: $appPush)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ConfigReqDto &&
            (identical(other.accountBook, accountBook) ||
                other.accountBook == accountBook) &&
            (identical(other.appPush, appPush) || other.appPush == appPush));
  }

  @override
  int get hashCode => Object.hash(runtimeType, accountBook, appPush);

  @JsonKey(ignore: true)
  @override
  _$ConfigReqDtoCopyWith<_ConfigReqDto> get copyWith =>
      __$ConfigReqDtoCopyWithImpl<_ConfigReqDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ConfigReqDtoToJson(this);
  }
}

abstract class _ConfigReqDto implements ConfigReqDto {
  factory _ConfigReqDto({AccountBook? accountBook, AppPush? appPush}) =
      _$_ConfigReqDto;

  factory _ConfigReqDto.fromJson(Map<String, dynamic> json) =
      _$_ConfigReqDto.fromJson;

  @override
  AccountBook? get accountBook;
  @override
  AppPush? get appPush;
  @override
  @JsonKey(ignore: true)
  _$ConfigReqDtoCopyWith<_ConfigReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}
