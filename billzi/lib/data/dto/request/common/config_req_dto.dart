import 'package:billzi/data/dto/model/account_book.dart';
import 'package:billzi/data/dto/model/app_push.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'config_req_dto.freezed.dart';
part 'config_req_dto.g.dart';

@freezed
class ConfigReqDto with _$ConfigReqDto {
  factory ConfigReqDto({
    AccountBook? accountBook,
    AppPush? appPush,
  }) = _ConfigReqDto;

  factory ConfigReqDto.fromJson(Map<String, dynamic> json) =>
      _$ConfigReqDtoFromJson(json);
}