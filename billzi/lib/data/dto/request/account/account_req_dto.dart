import 'package:freezed_annotation/freezed_annotation.dart';

part 'account_req_dto.freezed.dart';
part 'account_req_dto.g.dart';

@freezed
class AccountReqDto with _$AccountReqDto {
  factory AccountReqDto({
    num? userSeq,
    String? accountCategoryName,
    String? imgURL,
    String? color,
    String? typeCd,
    num? supAccountCategorySeq,
  }) = _AccountReqDto;

  factory AccountReqDto.fromJson(Map<String, dynamic> json) =>
      _$AccountReqDtoFromJson(json);
}
