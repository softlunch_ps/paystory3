// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_req_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AccountReqDto _$$_AccountReqDtoFromJson(Map<String, dynamic> json) =>
    _$_AccountReqDto(
      userSeq: json['userSeq'] as num?,
      accountCategoryName: json['accountCategoryName'] as String?,
      imgURL: json['imgURL'] as String?,
      color: json['color'] as String?,
      typeCd: json['typeCd'] as String?,
      supAccountCategorySeq: json['supAccountCategorySeq'] as num?,
    );

Map<String, dynamic> _$$_AccountReqDtoToJson(_$_AccountReqDto instance) =>
    <String, dynamic>{
      'userSeq': instance.userSeq,
      'accountCategoryName': instance.accountCategoryName,
      'imgURL': instance.imgURL,
      'color': instance.color,
      'typeCd': instance.typeCd,
      'supAccountCategorySeq': instance.supAccountCategorySeq,
    };
