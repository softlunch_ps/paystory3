// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'account_req_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AccountReqDto _$AccountReqDtoFromJson(Map<String, dynamic> json) {
  return _AccountReqDto.fromJson(json);
}

/// @nodoc
class _$AccountReqDtoTearOff {
  const _$AccountReqDtoTearOff();

  _AccountReqDto call(
      {num? userSeq,
      String? accountCategoryName,
      String? imgURL,
      String? color,
      String? typeCd,
      num? supAccountCategorySeq}) {
    return _AccountReqDto(
      userSeq: userSeq,
      accountCategoryName: accountCategoryName,
      imgURL: imgURL,
      color: color,
      typeCd: typeCd,
      supAccountCategorySeq: supAccountCategorySeq,
    );
  }

  AccountReqDto fromJson(Map<String, Object?> json) {
    return AccountReqDto.fromJson(json);
  }
}

/// @nodoc
const $AccountReqDto = _$AccountReqDtoTearOff();

/// @nodoc
mixin _$AccountReqDto {
  num? get userSeq => throw _privateConstructorUsedError;
  String? get accountCategoryName => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  String? get typeCd => throw _privateConstructorUsedError;
  num? get supAccountCategorySeq => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AccountReqDtoCopyWith<AccountReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AccountReqDtoCopyWith<$Res> {
  factory $AccountReqDtoCopyWith(
          AccountReqDto value, $Res Function(AccountReqDto) then) =
      _$AccountReqDtoCopyWithImpl<$Res>;
  $Res call(
      {num? userSeq,
      String? accountCategoryName,
      String? imgURL,
      String? color,
      String? typeCd,
      num? supAccountCategorySeq});
}

/// @nodoc
class _$AccountReqDtoCopyWithImpl<$Res>
    implements $AccountReqDtoCopyWith<$Res> {
  _$AccountReqDtoCopyWithImpl(this._value, this._then);

  final AccountReqDto _value;
  // ignore: unused_field
  final $Res Function(AccountReqDto) _then;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? accountCategoryName = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? typeCd = freezed,
    Object? supAccountCategorySeq = freezed,
  }) {
    return _then(_value.copyWith(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountCategoryName: accountCategoryName == freezed
          ? _value.accountCategoryName
          : accountCategoryName // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      typeCd: typeCd == freezed
          ? _value.typeCd
          : typeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      supAccountCategorySeq: supAccountCategorySeq == freezed
          ? _value.supAccountCategorySeq
          : supAccountCategorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
    ));
  }
}

/// @nodoc
abstract class _$AccountReqDtoCopyWith<$Res>
    implements $AccountReqDtoCopyWith<$Res> {
  factory _$AccountReqDtoCopyWith(
          _AccountReqDto value, $Res Function(_AccountReqDto) then) =
      __$AccountReqDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? userSeq,
      String? accountCategoryName,
      String? imgURL,
      String? color,
      String? typeCd,
      num? supAccountCategorySeq});
}

/// @nodoc
class __$AccountReqDtoCopyWithImpl<$Res>
    extends _$AccountReqDtoCopyWithImpl<$Res>
    implements _$AccountReqDtoCopyWith<$Res> {
  __$AccountReqDtoCopyWithImpl(
      _AccountReqDto _value, $Res Function(_AccountReqDto) _then)
      : super(_value, (v) => _then(v as _AccountReqDto));

  @override
  _AccountReqDto get _value => super._value as _AccountReqDto;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? accountCategoryName = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? typeCd = freezed,
    Object? supAccountCategorySeq = freezed,
  }) {
    return _then(_AccountReqDto(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountCategoryName: accountCategoryName == freezed
          ? _value.accountCategoryName
          : accountCategoryName // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      typeCd: typeCd == freezed
          ? _value.typeCd
          : typeCd // ignore: cast_nullable_to_non_nullable
              as String?,
      supAccountCategorySeq: supAccountCategorySeq == freezed
          ? _value.supAccountCategorySeq
          : supAccountCategorySeq // ignore: cast_nullable_to_non_nullable
              as num?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AccountReqDto implements _AccountReqDto {
  _$_AccountReqDto(
      {this.userSeq,
      this.accountCategoryName,
      this.imgURL,
      this.color,
      this.typeCd,
      this.supAccountCategorySeq});

  factory _$_AccountReqDto.fromJson(Map<String, dynamic> json) =>
      _$$_AccountReqDtoFromJson(json);

  @override
  final num? userSeq;
  @override
  final String? accountCategoryName;
  @override
  final String? imgURL;
  @override
  final String? color;
  @override
  final String? typeCd;
  @override
  final num? supAccountCategorySeq;

  @override
  String toString() {
    return 'AccountReqDto(userSeq: $userSeq, accountCategoryName: $accountCategoryName, imgURL: $imgURL, color: $color, typeCd: $typeCd, supAccountCategorySeq: $supAccountCategorySeq)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AccountReqDto &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.accountCategoryName, accountCategoryName) ||
                other.accountCategoryName == accountCategoryName) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.typeCd, typeCd) || other.typeCd == typeCd) &&
            (identical(other.supAccountCategorySeq, supAccountCategorySeq) ||
                other.supAccountCategorySeq == supAccountCategorySeq));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userSeq, accountCategoryName,
      imgURL, color, typeCd, supAccountCategorySeq);

  @JsonKey(ignore: true)
  @override
  _$AccountReqDtoCopyWith<_AccountReqDto> get copyWith =>
      __$AccountReqDtoCopyWithImpl<_AccountReqDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AccountReqDtoToJson(this);
  }
}

abstract class _AccountReqDto implements AccountReqDto {
  factory _AccountReqDto(
      {num? userSeq,
      String? accountCategoryName,
      String? imgURL,
      String? color,
      String? typeCd,
      num? supAccountCategorySeq}) = _$_AccountReqDto;

  factory _AccountReqDto.fromJson(Map<String, dynamic> json) =
      _$_AccountReqDto.fromJson;

  @override
  num? get userSeq;
  @override
  String? get accountCategoryName;
  @override
  String? get imgURL;
  @override
  String? get color;
  @override
  String? get typeCd;
  @override
  num? get supAccountCategorySeq;
  @override
  @JsonKey(ignore: true)
  _$AccountReqDtoCopyWith<_AccountReqDto> get copyWith =>
      throw _privateConstructorUsedError;
}
