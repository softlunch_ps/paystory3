// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'common_result_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CommonResultResDto _$$_CommonResultResDtoFromJson(
        Map<String, dynamic> json) =>
    _$_CommonResultResDto(
      resultCode: json['resultCode'] as String,
      resultMessage: json['resultMessage'] as String,
      httpStatusCode: json['httpStatusCode'] as String,
    );

Map<String, dynamic> _$$_CommonResultResDtoToJson(
        _$_CommonResultResDto instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMessage': instance.resultMessage,
      'httpStatusCode': instance.httpStatusCode,
    };
