// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AuthResDto _$$_AuthResDtoFromJson(Map<String, dynamic> json) =>
    _$_AuthResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      token: json['token'] as String,
      userSeq: json['userSeq'] as int,
    );

Map<String, dynamic> _$$_AuthResDtoToJson(_$_AuthResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
      'token': instance.token,
      'userSeq': instance.userSeq,
    };
