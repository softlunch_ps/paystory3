import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'sign_out_res_dto.freezed.dart';
part 'sign_out_res_dto.g.dart';

@freezed
class SignOutResDto with _$SignOutResDto {
  factory SignOutResDto({
    required CommonResultResDto resultVO,
    //required String token,
    //required int userSeq,
  }) = _SignOutResDto;

  factory SignOutResDto.fromJson(Map<String, dynamic> json) =>
      _$SignOutResDtoFromJson(json);
}