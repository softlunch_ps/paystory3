// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sign_out_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SignOutResDto _$SignOutResDtoFromJson(Map<String, dynamic> json) {
  return _SignOutResDto.fromJson(json);
}

/// @nodoc
class _$SignOutResDtoTearOff {
  const _$SignOutResDtoTearOff();

  _SignOutResDto call({required CommonResultResDto resultVO}) {
    return _SignOutResDto(
      resultVO: resultVO,
    );
  }

  SignOutResDto fromJson(Map<String, Object?> json) {
    return SignOutResDto.fromJson(json);
  }
}

/// @nodoc
const $SignOutResDto = _$SignOutResDtoTearOff();

/// @nodoc
mixin _$SignOutResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SignOutResDtoCopyWith<SignOutResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignOutResDtoCopyWith<$Res> {
  factory $SignOutResDtoCopyWith(
          SignOutResDto value, $Res Function(SignOutResDto) then) =
      _$SignOutResDtoCopyWithImpl<$Res>;
  $Res call({CommonResultResDto resultVO});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$SignOutResDtoCopyWithImpl<$Res>
    implements $SignOutResDtoCopyWith<$Res> {
  _$SignOutResDtoCopyWithImpl(this._value, this._then);

  final SignOutResDto _value;
  // ignore: unused_field
  final $Res Function(SignOutResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$SignOutResDtoCopyWith<$Res>
    implements $SignOutResDtoCopyWith<$Res> {
  factory _$SignOutResDtoCopyWith(
          _SignOutResDto value, $Res Function(_SignOutResDto) then) =
      __$SignOutResDtoCopyWithImpl<$Res>;
  @override
  $Res call({CommonResultResDto resultVO});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$SignOutResDtoCopyWithImpl<$Res>
    extends _$SignOutResDtoCopyWithImpl<$Res>
    implements _$SignOutResDtoCopyWith<$Res> {
  __$SignOutResDtoCopyWithImpl(
      _SignOutResDto _value, $Res Function(_SignOutResDto) _then)
      : super(_value, (v) => _then(v as _SignOutResDto));

  @override
  _SignOutResDto get _value => super._value as _SignOutResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
  }) {
    return _then(_SignOutResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SignOutResDto implements _SignOutResDto {
  _$_SignOutResDto({required this.resultVO});

  factory _$_SignOutResDto.fromJson(Map<String, dynamic> json) =>
      _$$_SignOutResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;

  @override
  String toString() {
    return 'SignOutResDto(resultVO: $resultVO)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SignOutResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO));
  }

  @override
  int get hashCode => Object.hash(runtimeType, resultVO);

  @JsonKey(ignore: true)
  @override
  _$SignOutResDtoCopyWith<_SignOutResDto> get copyWith =>
      __$SignOutResDtoCopyWithImpl<_SignOutResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SignOutResDtoToJson(this);
  }
}

abstract class _SignOutResDto implements SignOutResDto {
  factory _SignOutResDto({required CommonResultResDto resultVO}) =
      _$_SignOutResDto;

  factory _SignOutResDto.fromJson(Map<String, dynamic> json) =
      _$_SignOutResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  @JsonKey(ignore: true)
  _$SignOutResDtoCopyWith<_SignOutResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
