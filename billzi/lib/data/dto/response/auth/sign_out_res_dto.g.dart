// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_out_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SignOutResDto _$$_SignOutResDtoFromJson(Map<String, dynamic> json) =>
    _$_SignOutResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_SignOutResDtoToJson(_$_SignOutResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
    };
