import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'auth_res_dto.freezed.dart';
part 'auth_res_dto.g.dart';

@freezed
class AuthResDto with _$AuthResDto {
  factory AuthResDto({
    required CommonResultResDto resultVO,
    required String token,
    required int userSeq,
  }) = _AuthResDto;

  factory AuthResDto.fromJson(Map<String, dynamic> json) =>
      _$AuthResDtoFromJson(json);
}
