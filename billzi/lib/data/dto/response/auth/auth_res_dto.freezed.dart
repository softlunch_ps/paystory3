// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'auth_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AuthResDto _$AuthResDtoFromJson(Map<String, dynamic> json) {
  return _AuthResDto.fromJson(json);
}

/// @nodoc
class _$AuthResDtoTearOff {
  const _$AuthResDtoTearOff();

  _AuthResDto call(
      {required CommonResultResDto resultVO,
      required String token,
      required int userSeq}) {
    return _AuthResDto(
      resultVO: resultVO,
      token: token,
      userSeq: userSeq,
    );
  }

  AuthResDto fromJson(Map<String, Object?> json) {
    return AuthResDto.fromJson(json);
  }
}

/// @nodoc
const $AuthResDto = _$AuthResDtoTearOff();

/// @nodoc
mixin _$AuthResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  String get token => throw _privateConstructorUsedError;
  int get userSeq => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthResDtoCopyWith<AuthResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthResDtoCopyWith<$Res> {
  factory $AuthResDtoCopyWith(
          AuthResDto value, $Res Function(AuthResDto) then) =
      _$AuthResDtoCopyWithImpl<$Res>;
  $Res call({CommonResultResDto resultVO, String token, int userSeq});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$AuthResDtoCopyWithImpl<$Res> implements $AuthResDtoCopyWith<$Res> {
  _$AuthResDtoCopyWithImpl(this._value, this._then);

  final AuthResDto _value;
  // ignore: unused_field
  final $Res Function(AuthResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? token = freezed,
    Object? userSeq = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      token: token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$AuthResDtoCopyWith<$Res> implements $AuthResDtoCopyWith<$Res> {
  factory _$AuthResDtoCopyWith(
          _AuthResDto value, $Res Function(_AuthResDto) then) =
      __$AuthResDtoCopyWithImpl<$Res>;
  @override
  $Res call({CommonResultResDto resultVO, String token, int userSeq});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$AuthResDtoCopyWithImpl<$Res> extends _$AuthResDtoCopyWithImpl<$Res>
    implements _$AuthResDtoCopyWith<$Res> {
  __$AuthResDtoCopyWithImpl(
      _AuthResDto _value, $Res Function(_AuthResDto) _then)
      : super(_value, (v) => _then(v as _AuthResDto));

  @override
  _AuthResDto get _value => super._value as _AuthResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? token = freezed,
    Object? userSeq = freezed,
  }) {
    return _then(_AuthResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      token: token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AuthResDto implements _AuthResDto {
  _$_AuthResDto(
      {required this.resultVO, required this.token, required this.userSeq});

  factory _$_AuthResDto.fromJson(Map<String, dynamic> json) =>
      _$$_AuthResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;
  @override
  final String token;
  @override
  final int userSeq;

  @override
  String toString() {
    return 'AuthResDto(resultVO: $resultVO, token: $token, userSeq: $userSeq)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq));
  }

  @override
  int get hashCode => Object.hash(runtimeType, resultVO, token, userSeq);

  @JsonKey(ignore: true)
  @override
  _$AuthResDtoCopyWith<_AuthResDto> get copyWith =>
      __$AuthResDtoCopyWithImpl<_AuthResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AuthResDtoToJson(this);
  }
}

abstract class _AuthResDto implements AuthResDto {
  factory _AuthResDto(
      {required CommonResultResDto resultVO,
      required String token,
      required int userSeq}) = _$_AuthResDto;

  factory _AuthResDto.fromJson(Map<String, dynamic> json) =
      _$_AuthResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  String get token;
  @override
  int get userSeq;
  @override
  @JsonKey(ignore: true)
  _$AuthResDtoCopyWith<_AuthResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
