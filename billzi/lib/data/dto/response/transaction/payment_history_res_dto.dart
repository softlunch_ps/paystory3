import 'package:billzi/data/dto/model/pagingDto.dart';
import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'payment_history_res_dto.freezed.dart';
part 'payment_history_res_dto.g.dart';

@freezed
class PaymentHistoryResDto with _$PaymentHistoryResDto {
  factory PaymentHistoryResDto({
    num? userSeq,
    String? startDate,
    String? endDate,
    PagingDto? pagingVO,
    CommonResultResDto? resultVO,
    List<PaymentHistoryDto>? paymentHistoryList,
    double? totPayAmt,
    int? totPayCount,
    int? totPayCountCash,
    int? totPayCountCard,
    int? totPayAmtCash,
    int? totPayAmtCard,
    Map<String, PaymentHistoryDto>? totPayAmtByCard,
  }) = _PaymentHistoryResDto;

  factory PaymentHistoryResDto.fromJson(Map<String, dynamic> json) =>
      _$PaymentHistoryResDtoFromJson(json);
}