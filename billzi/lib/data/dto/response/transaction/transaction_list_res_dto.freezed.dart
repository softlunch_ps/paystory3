// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'transaction_list_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TransactionListResDto _$TransactionListResDtoFromJson(
    Map<String, dynamic> json) {
  return _TransactionListResDto.fromJson(json);
}

/// @nodoc
class _$TransactionListResDtoTearOff {
  const _$TransactionListResDtoTearOff();

  _TransactionListResDto call(
      {required CommonResultResDto resultVO,
      required List<TransactionDto> paymentHistoryList,
      @JsonKey(name: 'pagingVO') required PagingDto pagingDto}) {
    return _TransactionListResDto(
      resultVO: resultVO,
      paymentHistoryList: paymentHistoryList,
      pagingDto: pagingDto,
    );
  }

  TransactionListResDto fromJson(Map<String, Object?> json) {
    return TransactionListResDto.fromJson(json);
  }
}

/// @nodoc
const $TransactionListResDto = _$TransactionListResDtoTearOff();

/// @nodoc
mixin _$TransactionListResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  List<TransactionDto> get paymentHistoryList =>
      throw _privateConstructorUsedError;
  @JsonKey(name: 'pagingVO')
  PagingDto get pagingDto => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionListResDtoCopyWith<TransactionListResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionListResDtoCopyWith<$Res> {
  factory $TransactionListResDtoCopyWith(TransactionListResDto value,
          $Res Function(TransactionListResDto) then) =
      _$TransactionListResDtoCopyWithImpl<$Res>;
  $Res call(
      {CommonResultResDto resultVO,
      List<TransactionDto> paymentHistoryList,
      @JsonKey(name: 'pagingVO') PagingDto pagingDto});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
  $PagingDtoCopyWith<$Res> get pagingDto;
}

/// @nodoc
class _$TransactionListResDtoCopyWithImpl<$Res>
    implements $TransactionListResDtoCopyWith<$Res> {
  _$TransactionListResDtoCopyWithImpl(this._value, this._then);

  final TransactionListResDto _value;
  // ignore: unused_field
  final $Res Function(TransactionListResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? paymentHistoryList = freezed,
    Object? pagingDto = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      paymentHistoryList: paymentHistoryList == freezed
          ? _value.paymentHistoryList
          : paymentHistoryList // ignore: cast_nullable_to_non_nullable
              as List<TransactionDto>,
      pagingDto: pagingDto == freezed
          ? _value.pagingDto
          : pagingDto // ignore: cast_nullable_to_non_nullable
              as PagingDto,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }

  @override
  $PagingDtoCopyWith<$Res> get pagingDto {
    return $PagingDtoCopyWith<$Res>(_value.pagingDto, (value) {
      return _then(_value.copyWith(pagingDto: value));
    });
  }
}

/// @nodoc
abstract class _$TransactionListResDtoCopyWith<$Res>
    implements $TransactionListResDtoCopyWith<$Res> {
  factory _$TransactionListResDtoCopyWith(_TransactionListResDto value,
          $Res Function(_TransactionListResDto) then) =
      __$TransactionListResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {CommonResultResDto resultVO,
      List<TransactionDto> paymentHistoryList,
      @JsonKey(name: 'pagingVO') PagingDto pagingDto});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
  @override
  $PagingDtoCopyWith<$Res> get pagingDto;
}

/// @nodoc
class __$TransactionListResDtoCopyWithImpl<$Res>
    extends _$TransactionListResDtoCopyWithImpl<$Res>
    implements _$TransactionListResDtoCopyWith<$Res> {
  __$TransactionListResDtoCopyWithImpl(_TransactionListResDto _value,
      $Res Function(_TransactionListResDto) _then)
      : super(_value, (v) => _then(v as _TransactionListResDto));

  @override
  _TransactionListResDto get _value => super._value as _TransactionListResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? paymentHistoryList = freezed,
    Object? pagingDto = freezed,
  }) {
    return _then(_TransactionListResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      paymentHistoryList: paymentHistoryList == freezed
          ? _value.paymentHistoryList
          : paymentHistoryList // ignore: cast_nullable_to_non_nullable
              as List<TransactionDto>,
      pagingDto: pagingDto == freezed
          ? _value.pagingDto
          : pagingDto // ignore: cast_nullable_to_non_nullable
              as PagingDto,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TransactionListResDto implements _TransactionListResDto {
  _$_TransactionListResDto(
      {required this.resultVO,
      required this.paymentHistoryList,
      @JsonKey(name: 'pagingVO') required this.pagingDto});

  factory _$_TransactionListResDto.fromJson(Map<String, dynamic> json) =>
      _$$_TransactionListResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;
  @override
  final List<TransactionDto> paymentHistoryList;
  @override
  @JsonKey(name: 'pagingVO')
  final PagingDto pagingDto;

  @override
  String toString() {
    return 'TransactionListResDto(resultVO: $resultVO, paymentHistoryList: $paymentHistoryList, pagingDto: $pagingDto)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TransactionListResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            const DeepCollectionEquality()
                .equals(other.paymentHistoryList, paymentHistoryList) &&
            (identical(other.pagingDto, pagingDto) ||
                other.pagingDto == pagingDto));
  }

  @override
  int get hashCode => Object.hash(runtimeType, resultVO,
      const DeepCollectionEquality().hash(paymentHistoryList), pagingDto);

  @JsonKey(ignore: true)
  @override
  _$TransactionListResDtoCopyWith<_TransactionListResDto> get copyWith =>
      __$TransactionListResDtoCopyWithImpl<_TransactionListResDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TransactionListResDtoToJson(this);
  }
}

abstract class _TransactionListResDto implements TransactionListResDto {
  factory _TransactionListResDto(
          {required CommonResultResDto resultVO,
          required List<TransactionDto> paymentHistoryList,
          @JsonKey(name: 'pagingVO') required PagingDto pagingDto}) =
      _$_TransactionListResDto;

  factory _TransactionListResDto.fromJson(Map<String, dynamic> json) =
      _$_TransactionListResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  List<TransactionDto> get paymentHistoryList;
  @override
  @JsonKey(name: 'pagingVO')
  PagingDto get pagingDto;
  @override
  @JsonKey(ignore: true)
  _$TransactionListResDtoCopyWith<_TransactionListResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
