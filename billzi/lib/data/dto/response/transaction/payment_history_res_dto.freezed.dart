// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'payment_history_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PaymentHistoryResDto _$PaymentHistoryResDtoFromJson(Map<String, dynamic> json) {
  return _PaymentHistoryResDto.fromJson(json);
}

/// @nodoc
class _$PaymentHistoryResDtoTearOff {
  const _$PaymentHistoryResDtoTearOff();

  _PaymentHistoryResDto call(
      {num? userSeq,
      String? startDate,
      String? endDate,
      PagingDto? pagingVO,
      CommonResultResDto? resultVO,
      List<PaymentHistoryDto>? paymentHistoryList,
      double? totPayAmt,
      int? totPayCount,
      int? totPayCountCash,
      int? totPayCountCard,
      int? totPayAmtCash,
      int? totPayAmtCard,
      Map<String, PaymentHistoryDto>? totPayAmtByCard}) {
    return _PaymentHistoryResDto(
      userSeq: userSeq,
      startDate: startDate,
      endDate: endDate,
      pagingVO: pagingVO,
      resultVO: resultVO,
      paymentHistoryList: paymentHistoryList,
      totPayAmt: totPayAmt,
      totPayCount: totPayCount,
      totPayCountCash: totPayCountCash,
      totPayCountCard: totPayCountCard,
      totPayAmtCash: totPayAmtCash,
      totPayAmtCard: totPayAmtCard,
      totPayAmtByCard: totPayAmtByCard,
    );
  }

  PaymentHistoryResDto fromJson(Map<String, Object?> json) {
    return PaymentHistoryResDto.fromJson(json);
  }
}

/// @nodoc
const $PaymentHistoryResDto = _$PaymentHistoryResDtoTearOff();

/// @nodoc
mixin _$PaymentHistoryResDto {
  num? get userSeq => throw _privateConstructorUsedError;
  String? get startDate => throw _privateConstructorUsedError;
  String? get endDate => throw _privateConstructorUsedError;
  PagingDto? get pagingVO => throw _privateConstructorUsedError;
  CommonResultResDto? get resultVO => throw _privateConstructorUsedError;
  List<PaymentHistoryDto>? get paymentHistoryList =>
      throw _privateConstructorUsedError;
  double? get totPayAmt => throw _privateConstructorUsedError;
  int? get totPayCount => throw _privateConstructorUsedError;
  int? get totPayCountCash => throw _privateConstructorUsedError;
  int? get totPayCountCard => throw _privateConstructorUsedError;
  int? get totPayAmtCash => throw _privateConstructorUsedError;
  int? get totPayAmtCard => throw _privateConstructorUsedError;
  Map<String, PaymentHistoryDto>? get totPayAmtByCard =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaymentHistoryResDtoCopyWith<PaymentHistoryResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaymentHistoryResDtoCopyWith<$Res> {
  factory $PaymentHistoryResDtoCopyWith(PaymentHistoryResDto value,
          $Res Function(PaymentHistoryResDto) then) =
      _$PaymentHistoryResDtoCopyWithImpl<$Res>;
  $Res call(
      {num? userSeq,
      String? startDate,
      String? endDate,
      PagingDto? pagingVO,
      CommonResultResDto? resultVO,
      List<PaymentHistoryDto>? paymentHistoryList,
      double? totPayAmt,
      int? totPayCount,
      int? totPayCountCash,
      int? totPayCountCard,
      int? totPayAmtCash,
      int? totPayAmtCard,
      Map<String, PaymentHistoryDto>? totPayAmtByCard});

  $PagingDtoCopyWith<$Res>? get pagingVO;
  $CommonResultResDtoCopyWith<$Res>? get resultVO;
}

/// @nodoc
class _$PaymentHistoryResDtoCopyWithImpl<$Res>
    implements $PaymentHistoryResDtoCopyWith<$Res> {
  _$PaymentHistoryResDtoCopyWithImpl(this._value, this._then);

  final PaymentHistoryResDto _value;
  // ignore: unused_field
  final $Res Function(PaymentHistoryResDto) _then;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? pagingVO = freezed,
    Object? resultVO = freezed,
    Object? paymentHistoryList = freezed,
    Object? totPayAmt = freezed,
    Object? totPayCount = freezed,
    Object? totPayCountCash = freezed,
    Object? totPayCountCard = freezed,
    Object? totPayAmtCash = freezed,
    Object? totPayAmtCard = freezed,
    Object? totPayAmtByCard = freezed,
  }) {
    return _then(_value.copyWith(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as String?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as String?,
      pagingVO: pagingVO == freezed
          ? _value.pagingVO
          : pagingVO // ignore: cast_nullable_to_non_nullable
              as PagingDto?,
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto?,
      paymentHistoryList: paymentHistoryList == freezed
          ? _value.paymentHistoryList
          : paymentHistoryList // ignore: cast_nullable_to_non_nullable
              as List<PaymentHistoryDto>?,
      totPayAmt: totPayAmt == freezed
          ? _value.totPayAmt
          : totPayAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      totPayCount: totPayCount == freezed
          ? _value.totPayCount
          : totPayCount // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayCountCash: totPayCountCash == freezed
          ? _value.totPayCountCash
          : totPayCountCash // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayCountCard: totPayCountCard == freezed
          ? _value.totPayCountCard
          : totPayCountCard // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayAmtCash: totPayAmtCash == freezed
          ? _value.totPayAmtCash
          : totPayAmtCash // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayAmtCard: totPayAmtCard == freezed
          ? _value.totPayAmtCard
          : totPayAmtCard // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayAmtByCard: totPayAmtByCard == freezed
          ? _value.totPayAmtByCard
          : totPayAmtByCard // ignore: cast_nullable_to_non_nullable
              as Map<String, PaymentHistoryDto>?,
    ));
  }

  @override
  $PagingDtoCopyWith<$Res>? get pagingVO {
    if (_value.pagingVO == null) {
      return null;
    }

    return $PagingDtoCopyWith<$Res>(_value.pagingVO!, (value) {
      return _then(_value.copyWith(pagingVO: value));
    });
  }

  @override
  $CommonResultResDtoCopyWith<$Res>? get resultVO {
    if (_value.resultVO == null) {
      return null;
    }

    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO!, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$PaymentHistoryResDtoCopyWith<$Res>
    implements $PaymentHistoryResDtoCopyWith<$Res> {
  factory _$PaymentHistoryResDtoCopyWith(_PaymentHistoryResDto value,
          $Res Function(_PaymentHistoryResDto) then) =
      __$PaymentHistoryResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? userSeq,
      String? startDate,
      String? endDate,
      PagingDto? pagingVO,
      CommonResultResDto? resultVO,
      List<PaymentHistoryDto>? paymentHistoryList,
      double? totPayAmt,
      int? totPayCount,
      int? totPayCountCash,
      int? totPayCountCard,
      int? totPayAmtCash,
      int? totPayAmtCard,
      Map<String, PaymentHistoryDto>? totPayAmtByCard});

  @override
  $PagingDtoCopyWith<$Res>? get pagingVO;
  @override
  $CommonResultResDtoCopyWith<$Res>? get resultVO;
}

/// @nodoc
class __$PaymentHistoryResDtoCopyWithImpl<$Res>
    extends _$PaymentHistoryResDtoCopyWithImpl<$Res>
    implements _$PaymentHistoryResDtoCopyWith<$Res> {
  __$PaymentHistoryResDtoCopyWithImpl(
      _PaymentHistoryResDto _value, $Res Function(_PaymentHistoryResDto) _then)
      : super(_value, (v) => _then(v as _PaymentHistoryResDto));

  @override
  _PaymentHistoryResDto get _value => super._value as _PaymentHistoryResDto;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? pagingVO = freezed,
    Object? resultVO = freezed,
    Object? paymentHistoryList = freezed,
    Object? totPayAmt = freezed,
    Object? totPayCount = freezed,
    Object? totPayCountCash = freezed,
    Object? totPayCountCard = freezed,
    Object? totPayAmtCash = freezed,
    Object? totPayAmtCard = freezed,
    Object? totPayAmtByCard = freezed,
  }) {
    return _then(_PaymentHistoryResDto(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as String?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as String?,
      pagingVO: pagingVO == freezed
          ? _value.pagingVO
          : pagingVO // ignore: cast_nullable_to_non_nullable
              as PagingDto?,
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto?,
      paymentHistoryList: paymentHistoryList == freezed
          ? _value.paymentHistoryList
          : paymentHistoryList // ignore: cast_nullable_to_non_nullable
              as List<PaymentHistoryDto>?,
      totPayAmt: totPayAmt == freezed
          ? _value.totPayAmt
          : totPayAmt // ignore: cast_nullable_to_non_nullable
              as double?,
      totPayCount: totPayCount == freezed
          ? _value.totPayCount
          : totPayCount // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayCountCash: totPayCountCash == freezed
          ? _value.totPayCountCash
          : totPayCountCash // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayCountCard: totPayCountCard == freezed
          ? _value.totPayCountCard
          : totPayCountCard // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayAmtCash: totPayAmtCash == freezed
          ? _value.totPayAmtCash
          : totPayAmtCash // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayAmtCard: totPayAmtCard == freezed
          ? _value.totPayAmtCard
          : totPayAmtCard // ignore: cast_nullable_to_non_nullable
              as int?,
      totPayAmtByCard: totPayAmtByCard == freezed
          ? _value.totPayAmtByCard
          : totPayAmtByCard // ignore: cast_nullable_to_non_nullable
              as Map<String, PaymentHistoryDto>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PaymentHistoryResDto implements _PaymentHistoryResDto {
  _$_PaymentHistoryResDto(
      {this.userSeq,
      this.startDate,
      this.endDate,
      this.pagingVO,
      this.resultVO,
      this.paymentHistoryList,
      this.totPayAmt,
      this.totPayCount,
      this.totPayCountCash,
      this.totPayCountCard,
      this.totPayAmtCash,
      this.totPayAmtCard,
      this.totPayAmtByCard});

  factory _$_PaymentHistoryResDto.fromJson(Map<String, dynamic> json) =>
      _$$_PaymentHistoryResDtoFromJson(json);

  @override
  final num? userSeq;
  @override
  final String? startDate;
  @override
  final String? endDate;
  @override
  final PagingDto? pagingVO;
  @override
  final CommonResultResDto? resultVO;
  @override
  final List<PaymentHistoryDto>? paymentHistoryList;
  @override
  final double? totPayAmt;
  @override
  final int? totPayCount;
  @override
  final int? totPayCountCash;
  @override
  final int? totPayCountCard;
  @override
  final int? totPayAmtCash;
  @override
  final int? totPayAmtCard;
  @override
  final Map<String, PaymentHistoryDto>? totPayAmtByCard;

  @override
  String toString() {
    return 'PaymentHistoryResDto(userSeq: $userSeq, startDate: $startDate, endDate: $endDate, pagingVO: $pagingVO, resultVO: $resultVO, paymentHistoryList: $paymentHistoryList, totPayAmt: $totPayAmt, totPayCount: $totPayCount, totPayCountCash: $totPayCountCash, totPayCountCard: $totPayCountCard, totPayAmtCash: $totPayAmtCash, totPayAmtCard: $totPayAmtCard, totPayAmtByCard: $totPayAmtByCard)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _PaymentHistoryResDto &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.endDate, endDate) || other.endDate == endDate) &&
            (identical(other.pagingVO, pagingVO) ||
                other.pagingVO == pagingVO) &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            const DeepCollectionEquality()
                .equals(other.paymentHistoryList, paymentHistoryList) &&
            (identical(other.totPayAmt, totPayAmt) ||
                other.totPayAmt == totPayAmt) &&
            (identical(other.totPayCount, totPayCount) ||
                other.totPayCount == totPayCount) &&
            (identical(other.totPayCountCash, totPayCountCash) ||
                other.totPayCountCash == totPayCountCash) &&
            (identical(other.totPayCountCard, totPayCountCard) ||
                other.totPayCountCard == totPayCountCard) &&
            (identical(other.totPayAmtCash, totPayAmtCash) ||
                other.totPayAmtCash == totPayAmtCash) &&
            (identical(other.totPayAmtCard, totPayAmtCard) ||
                other.totPayAmtCard == totPayAmtCard) &&
            const DeepCollectionEquality()
                .equals(other.totPayAmtByCard, totPayAmtByCard));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      userSeq,
      startDate,
      endDate,
      pagingVO,
      resultVO,
      const DeepCollectionEquality().hash(paymentHistoryList),
      totPayAmt,
      totPayCount,
      totPayCountCash,
      totPayCountCard,
      totPayAmtCash,
      totPayAmtCard,
      const DeepCollectionEquality().hash(totPayAmtByCard));

  @JsonKey(ignore: true)
  @override
  _$PaymentHistoryResDtoCopyWith<_PaymentHistoryResDto> get copyWith =>
      __$PaymentHistoryResDtoCopyWithImpl<_PaymentHistoryResDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PaymentHistoryResDtoToJson(this);
  }
}

abstract class _PaymentHistoryResDto implements PaymentHistoryResDto {
  factory _PaymentHistoryResDto(
          {num? userSeq,
          String? startDate,
          String? endDate,
          PagingDto? pagingVO,
          CommonResultResDto? resultVO,
          List<PaymentHistoryDto>? paymentHistoryList,
          double? totPayAmt,
          int? totPayCount,
          int? totPayCountCash,
          int? totPayCountCard,
          int? totPayAmtCash,
          int? totPayAmtCard,
          Map<String, PaymentHistoryDto>? totPayAmtByCard}) =
      _$_PaymentHistoryResDto;

  factory _PaymentHistoryResDto.fromJson(Map<String, dynamic> json) =
      _$_PaymentHistoryResDto.fromJson;

  @override
  num? get userSeq;
  @override
  String? get startDate;
  @override
  String? get endDate;
  @override
  PagingDto? get pagingVO;
  @override
  CommonResultResDto? get resultVO;
  @override
  List<PaymentHistoryDto>? get paymentHistoryList;
  @override
  double? get totPayAmt;
  @override
  int? get totPayCount;
  @override
  int? get totPayCountCash;
  @override
  int? get totPayCountCard;
  @override
  int? get totPayAmtCash;
  @override
  int? get totPayAmtCard;
  @override
  Map<String, PaymentHistoryDto>? get totPayAmtByCard;
  @override
  @JsonKey(ignore: true)
  _$PaymentHistoryResDtoCopyWith<_PaymentHistoryResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
