import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:billzi/data/dto/model/pagingDto.dart';
import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'card_list_res_dto.freezed.dart';
part 'card_list_res_dto.g.dart';

@freezed
class CardListResDto with _$CardListResDto {
  factory CardListResDto({
    num? userSeq,
    PagingDto? pagingVO,
    CommonResultResDto? resultVO,
    List<CardAccountDto>? cardAccountList,
  }) = _CardListResDto;

  factory CardListResDto.fromJson(Map<String, dynamic> json) =>
      _$CardListResDtoFromJson(json);
}