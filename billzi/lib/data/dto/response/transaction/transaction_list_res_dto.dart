import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/model/pagingDto.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'transaction_list_res_dto.freezed.dart';

part 'transaction_list_res_dto.g.dart';

@freezed
class TransactionListResDto with _$TransactionListResDto {
  factory TransactionListResDto({
    required CommonResultResDto resultVO,
    required List<TransactionDto> paymentHistoryList,
    @JsonKey(name: 'pagingVO') required PagingDto pagingDto,
  }) = _TransactionListResDto;

  factory TransactionListResDto.fromJson(Map<String, dynamic> json) =>
      _$TransactionListResDtoFromJson(json);
}
