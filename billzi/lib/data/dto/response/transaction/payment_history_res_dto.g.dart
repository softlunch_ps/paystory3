// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_history_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PaymentHistoryResDto _$$_PaymentHistoryResDtoFromJson(
        Map<String, dynamic> json) =>
    _$_PaymentHistoryResDto(
      userSeq: json['userSeq'] as num?,
      startDate: json['startDate'] as String?,
      endDate: json['endDate'] as String?,
      pagingVO: json['pagingVO'] == null
          ? null
          : PagingDto.fromJson(json['pagingVO'] as Map<String, dynamic>),
      resultVO: json['resultVO'] == null
          ? null
          : CommonResultResDto.fromJson(
              json['resultVO'] as Map<String, dynamic>),
      paymentHistoryList: (json['paymentHistoryList'] as List<dynamic>?)
          ?.map((e) => PaymentHistoryDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      totPayAmt: (json['totPayAmt'] as num?)?.toDouble(),
      totPayCount: json['totPayCount'] as int?,
      totPayCountCash: json['totPayCountCash'] as int?,
      totPayCountCard: json['totPayCountCard'] as int?,
      totPayAmtCash: json['totPayAmtCash'] as int?,
      totPayAmtCard: json['totPayAmtCard'] as int?,
      totPayAmtByCard: (json['totPayAmtByCard'] as Map<String, dynamic>?)?.map(
        (k, e) =>
            MapEntry(k, PaymentHistoryDto.fromJson(e as Map<String, dynamic>)),
      ),
    );

Map<String, dynamic> _$$_PaymentHistoryResDtoToJson(
        _$_PaymentHistoryResDto instance) =>
    <String, dynamic>{
      'userSeq': instance.userSeq,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'pagingVO': instance.pagingVO,
      'resultVO': instance.resultVO,
      'paymentHistoryList': instance.paymentHistoryList,
      'totPayAmt': instance.totPayAmt,
      'totPayCount': instance.totPayCount,
      'totPayCountCash': instance.totPayCountCash,
      'totPayCountCard': instance.totPayCountCard,
      'totPayAmtCash': instance.totPayAmtCash,
      'totPayAmtCard': instance.totPayAmtCard,
      'totPayAmtByCard': instance.totPayAmtByCard,
    };
