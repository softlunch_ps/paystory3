// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_list_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TransactionListResDto _$$_TransactionListResDtoFromJson(
        Map<String, dynamic> json) =>
    _$_TransactionListResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      paymentHistoryList: (json['paymentHistoryList'] as List<dynamic>)
          .map((e) => TransactionDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      pagingDto: PagingDto.fromJson(json['pagingVO'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_TransactionListResDtoToJson(
        _$_TransactionListResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
      'paymentHistoryList': instance.paymentHistoryList,
      'pagingVO': instance.pagingDto,
    };
