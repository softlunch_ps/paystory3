import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/model/pagingDto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_res_dto.freezed.dart';
part 'category_res_dto.g.dart';

@freezed
class CategoryResDto with _$CategoryResDto {
  factory CategoryResDto({
    PagingDto? pagingVO,
    CommonResultResDto? resultVO,
    List<CategoryItem>? accountCategoryList,
  }) = _CategoryResDto;

  factory CategoryResDto.fromJson(Map<String, dynamic> json) =>
      _$CategoryResDtoFromJson(json);
}