// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_category_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AccountCategoryResDto _$$_AccountCategoryResDtoFromJson(
        Map<String, dynamic> json) =>
    _$_AccountCategoryResDto(
      pagingVO: json['pagingVO'] == null
          ? null
          : PagingDto.fromJson(json['pagingVO'] as Map<String, dynamic>),
      resultVO: json['resultVO'] == null
          ? null
          : CommonResultResDto.fromJson(
              json['resultVO'] as Map<String, dynamic>),
      accountCategoryList: (json['accountCategoryList'] as List<dynamic>?)
          ?.map((e) => CategoryItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_AccountCategoryResDtoToJson(
        _$_AccountCategoryResDto instance) =>
    <String, dynamic>{
      'pagingVO': instance.pagingVO,
      'resultVO': instance.resultVO,
      'accountCategoryList': instance.accountCategoryList,
    };
