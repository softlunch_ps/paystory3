// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'account_category_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AccountCategoryResDto _$AccountCategoryResDtoFromJson(
    Map<String, dynamic> json) {
  return _AccountCategoryResDto.fromJson(json);
}

/// @nodoc
class _$AccountCategoryResDtoTearOff {
  const _$AccountCategoryResDtoTearOff();

  _AccountCategoryResDto call(
      {PagingDto? pagingVO,
      CommonResultResDto? resultVO,
      List<CategoryItem>? accountCategoryList}) {
    return _AccountCategoryResDto(
      pagingVO: pagingVO,
      resultVO: resultVO,
      accountCategoryList: accountCategoryList,
    );
  }

  AccountCategoryResDto fromJson(Map<String, Object?> json) {
    return AccountCategoryResDto.fromJson(json);
  }
}

/// @nodoc
const $AccountCategoryResDto = _$AccountCategoryResDtoTearOff();

/// @nodoc
mixin _$AccountCategoryResDto {
  PagingDto? get pagingVO => throw _privateConstructorUsedError;
  CommonResultResDto? get resultVO => throw _privateConstructorUsedError;
  List<CategoryItem>? get accountCategoryList =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AccountCategoryResDtoCopyWith<AccountCategoryResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AccountCategoryResDtoCopyWith<$Res> {
  factory $AccountCategoryResDtoCopyWith(AccountCategoryResDto value,
          $Res Function(AccountCategoryResDto) then) =
      _$AccountCategoryResDtoCopyWithImpl<$Res>;
  $Res call(
      {PagingDto? pagingVO,
      CommonResultResDto? resultVO,
      List<CategoryItem>? accountCategoryList});

  $PagingDtoCopyWith<$Res>? get pagingVO;
  $CommonResultResDtoCopyWith<$Res>? get resultVO;
}

/// @nodoc
class _$AccountCategoryResDtoCopyWithImpl<$Res>
    implements $AccountCategoryResDtoCopyWith<$Res> {
  _$AccountCategoryResDtoCopyWithImpl(this._value, this._then);

  final AccountCategoryResDto _value;
  // ignore: unused_field
  final $Res Function(AccountCategoryResDto) _then;

  @override
  $Res call({
    Object? pagingVO = freezed,
    Object? resultVO = freezed,
    Object? accountCategoryList = freezed,
  }) {
    return _then(_value.copyWith(
      pagingVO: pagingVO == freezed
          ? _value.pagingVO
          : pagingVO // ignore: cast_nullable_to_non_nullable
              as PagingDto?,
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto?,
      accountCategoryList: accountCategoryList == freezed
          ? _value.accountCategoryList
          : accountCategoryList // ignore: cast_nullable_to_non_nullable
              as List<CategoryItem>?,
    ));
  }

  @override
  $PagingDtoCopyWith<$Res>? get pagingVO {
    if (_value.pagingVO == null) {
      return null;
    }

    return $PagingDtoCopyWith<$Res>(_value.pagingVO!, (value) {
      return _then(_value.copyWith(pagingVO: value));
    });
  }

  @override
  $CommonResultResDtoCopyWith<$Res>? get resultVO {
    if (_value.resultVO == null) {
      return null;
    }

    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO!, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$AccountCategoryResDtoCopyWith<$Res>
    implements $AccountCategoryResDtoCopyWith<$Res> {
  factory _$AccountCategoryResDtoCopyWith(_AccountCategoryResDto value,
          $Res Function(_AccountCategoryResDto) then) =
      __$AccountCategoryResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {PagingDto? pagingVO,
      CommonResultResDto? resultVO,
      List<CategoryItem>? accountCategoryList});

  @override
  $PagingDtoCopyWith<$Res>? get pagingVO;
  @override
  $CommonResultResDtoCopyWith<$Res>? get resultVO;
}

/// @nodoc
class __$AccountCategoryResDtoCopyWithImpl<$Res>
    extends _$AccountCategoryResDtoCopyWithImpl<$Res>
    implements _$AccountCategoryResDtoCopyWith<$Res> {
  __$AccountCategoryResDtoCopyWithImpl(_AccountCategoryResDto _value,
      $Res Function(_AccountCategoryResDto) _then)
      : super(_value, (v) => _then(v as _AccountCategoryResDto));

  @override
  _AccountCategoryResDto get _value => super._value as _AccountCategoryResDto;

  @override
  $Res call({
    Object? pagingVO = freezed,
    Object? resultVO = freezed,
    Object? accountCategoryList = freezed,
  }) {
    return _then(_AccountCategoryResDto(
      pagingVO: pagingVO == freezed
          ? _value.pagingVO
          : pagingVO // ignore: cast_nullable_to_non_nullable
              as PagingDto?,
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto?,
      accountCategoryList: accountCategoryList == freezed
          ? _value.accountCategoryList
          : accountCategoryList // ignore: cast_nullable_to_non_nullable
              as List<CategoryItem>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AccountCategoryResDto implements _AccountCategoryResDto {
  _$_AccountCategoryResDto(
      {this.pagingVO, this.resultVO, this.accountCategoryList});

  factory _$_AccountCategoryResDto.fromJson(Map<String, dynamic> json) =>
      _$$_AccountCategoryResDtoFromJson(json);

  @override
  final PagingDto? pagingVO;
  @override
  final CommonResultResDto? resultVO;
  @override
  final List<CategoryItem>? accountCategoryList;

  @override
  String toString() {
    return 'AccountCategoryResDto(pagingVO: $pagingVO, resultVO: $resultVO, accountCategoryList: $accountCategoryList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AccountCategoryResDto &&
            (identical(other.pagingVO, pagingVO) ||
                other.pagingVO == pagingVO) &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            const DeepCollectionEquality()
                .equals(other.accountCategoryList, accountCategoryList));
  }

  @override
  int get hashCode => Object.hash(runtimeType, pagingVO, resultVO,
      const DeepCollectionEquality().hash(accountCategoryList));

  @JsonKey(ignore: true)
  @override
  _$AccountCategoryResDtoCopyWith<_AccountCategoryResDto> get copyWith =>
      __$AccountCategoryResDtoCopyWithImpl<_AccountCategoryResDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AccountCategoryResDtoToJson(this);
  }
}

abstract class _AccountCategoryResDto implements AccountCategoryResDto {
  factory _AccountCategoryResDto(
      {PagingDto? pagingVO,
      CommonResultResDto? resultVO,
      List<CategoryItem>? accountCategoryList}) = _$_AccountCategoryResDto;

  factory _AccountCategoryResDto.fromJson(Map<String, dynamic> json) =
      _$_AccountCategoryResDto.fromJson;

  @override
  PagingDto? get pagingVO;
  @override
  CommonResultResDto? get resultVO;
  @override
  List<CategoryItem>? get accountCategoryList;
  @override
  @JsonKey(ignore: true)
  _$AccountCategoryResDtoCopyWith<_AccountCategoryResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
