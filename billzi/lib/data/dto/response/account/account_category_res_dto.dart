import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/model/pagingDto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'account_category_res_dto.freezed.dart';
part 'account_category_res_dto.g.dart';

@freezed
class AccountCategoryResDto with _$AccountCategoryResDto {
  factory AccountCategoryResDto({
    PagingDto? pagingVO,
    CommonResultResDto? resultVO,
    List<CategoryItem>? accountCategoryList,
  }) = _AccountCategoryResDto;

  factory AccountCategoryResDto.fromJson(Map<String, dynamic> json) =>
      _$AccountCategoryResDtoFromJson(json);
}