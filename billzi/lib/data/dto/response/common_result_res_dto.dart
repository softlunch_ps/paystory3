import 'package:freezed_annotation/freezed_annotation.dart';

part 'common_result_res_dto.freezed.dart';
part 'common_result_res_dto.g.dart';

@freezed
class CommonResultResDto with _$CommonResultResDto {
  const CommonResultResDto._();
  factory CommonResultResDto({
    required String resultCode,
    required String resultMessage,
    required String httpStatusCode,
  }) = _CommonResultResDto;

  factory CommonResultResDto.fromJson(Map<String, dynamic> json) =>
      _$CommonResultResDtoFromJson(json);

  bool get isSuccess => resultCode == '0';
}
