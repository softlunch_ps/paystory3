// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'common_result_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CommonResultResDto _$CommonResultResDtoFromJson(Map<String, dynamic> json) {
  return _CommonResultResDto.fromJson(json);
}

/// @nodoc
class _$CommonResultResDtoTearOff {
  const _$CommonResultResDtoTearOff();

  _CommonResultResDto call(
      {required String resultCode,
      required String resultMessage,
      required String httpStatusCode}) {
    return _CommonResultResDto(
      resultCode: resultCode,
      resultMessage: resultMessage,
      httpStatusCode: httpStatusCode,
    );
  }

  CommonResultResDto fromJson(Map<String, Object?> json) {
    return CommonResultResDto.fromJson(json);
  }
}

/// @nodoc
const $CommonResultResDto = _$CommonResultResDtoTearOff();

/// @nodoc
mixin _$CommonResultResDto {
  String get resultCode => throw _privateConstructorUsedError;
  String get resultMessage => throw _privateConstructorUsedError;
  String get httpStatusCode => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CommonResultResDtoCopyWith<CommonResultResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CommonResultResDtoCopyWith<$Res> {
  factory $CommonResultResDtoCopyWith(
          CommonResultResDto value, $Res Function(CommonResultResDto) then) =
      _$CommonResultResDtoCopyWithImpl<$Res>;
  $Res call({String resultCode, String resultMessage, String httpStatusCode});
}

/// @nodoc
class _$CommonResultResDtoCopyWithImpl<$Res>
    implements $CommonResultResDtoCopyWith<$Res> {
  _$CommonResultResDtoCopyWithImpl(this._value, this._then);

  final CommonResultResDto _value;
  // ignore: unused_field
  final $Res Function(CommonResultResDto) _then;

  @override
  $Res call({
    Object? resultCode = freezed,
    Object? resultMessage = freezed,
    Object? httpStatusCode = freezed,
  }) {
    return _then(_value.copyWith(
      resultCode: resultCode == freezed
          ? _value.resultCode
          : resultCode // ignore: cast_nullable_to_non_nullable
              as String,
      resultMessage: resultMessage == freezed
          ? _value.resultMessage
          : resultMessage // ignore: cast_nullable_to_non_nullable
              as String,
      httpStatusCode: httpStatusCode == freezed
          ? _value.httpStatusCode
          : httpStatusCode // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$CommonResultResDtoCopyWith<$Res>
    implements $CommonResultResDtoCopyWith<$Res> {
  factory _$CommonResultResDtoCopyWith(
          _CommonResultResDto value, $Res Function(_CommonResultResDto) then) =
      __$CommonResultResDtoCopyWithImpl<$Res>;
  @override
  $Res call({String resultCode, String resultMessage, String httpStatusCode});
}

/// @nodoc
class __$CommonResultResDtoCopyWithImpl<$Res>
    extends _$CommonResultResDtoCopyWithImpl<$Res>
    implements _$CommonResultResDtoCopyWith<$Res> {
  __$CommonResultResDtoCopyWithImpl(
      _CommonResultResDto _value, $Res Function(_CommonResultResDto) _then)
      : super(_value, (v) => _then(v as _CommonResultResDto));

  @override
  _CommonResultResDto get _value => super._value as _CommonResultResDto;

  @override
  $Res call({
    Object? resultCode = freezed,
    Object? resultMessage = freezed,
    Object? httpStatusCode = freezed,
  }) {
    return _then(_CommonResultResDto(
      resultCode: resultCode == freezed
          ? _value.resultCode
          : resultCode // ignore: cast_nullable_to_non_nullable
              as String,
      resultMessage: resultMessage == freezed
          ? _value.resultMessage
          : resultMessage // ignore: cast_nullable_to_non_nullable
              as String,
      httpStatusCode: httpStatusCode == freezed
          ? _value.httpStatusCode
          : httpStatusCode // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CommonResultResDto extends _CommonResultResDto {
  _$_CommonResultResDto(
      {required this.resultCode,
      required this.resultMessage,
      required this.httpStatusCode})
      : super._();

  factory _$_CommonResultResDto.fromJson(Map<String, dynamic> json) =>
      _$$_CommonResultResDtoFromJson(json);

  @override
  final String resultCode;
  @override
  final String resultMessage;
  @override
  final String httpStatusCode;

  @override
  String toString() {
    return 'CommonResultResDto(resultCode: $resultCode, resultMessage: $resultMessage, httpStatusCode: $httpStatusCode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CommonResultResDto &&
            (identical(other.resultCode, resultCode) ||
                other.resultCode == resultCode) &&
            (identical(other.resultMessage, resultMessage) ||
                other.resultMessage == resultMessage) &&
            (identical(other.httpStatusCode, httpStatusCode) ||
                other.httpStatusCode == httpStatusCode));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, resultCode, resultMessage, httpStatusCode);

  @JsonKey(ignore: true)
  @override
  _$CommonResultResDtoCopyWith<_CommonResultResDto> get copyWith =>
      __$CommonResultResDtoCopyWithImpl<_CommonResultResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CommonResultResDtoToJson(this);
  }
}

abstract class _CommonResultResDto extends CommonResultResDto {
  factory _CommonResultResDto(
      {required String resultCode,
      required String resultMessage,
      required String httpStatusCode}) = _$_CommonResultResDto;
  _CommonResultResDto._() : super._();

  factory _CommonResultResDto.fromJson(Map<String, dynamic> json) =
      _$_CommonResultResDto.fromJson;

  @override
  String get resultCode;
  @override
  String get resultMessage;
  @override
  String get httpStatusCode;
  @override
  @JsonKey(ignore: true)
  _$CommonResultResDtoCopyWith<_CommonResultResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
