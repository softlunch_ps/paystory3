// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_subscription_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookSubscriptionResDto _$$_BookSubscriptionResDtoFromJson(
        Map<String, dynamic> json) =>
    _$_BookSubscriptionResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      delYN: json['delYN'] as String?,
      userSeq: json['userSeq'] as num?,
      accountBookSeq: json['accountBookSeq'] as num?,
    );

Map<String, dynamic> _$$_BookSubscriptionResDtoToJson(
        _$_BookSubscriptionResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
      'delYN': instance.delYN,
      'userSeq': instance.userSeq,
      'accountBookSeq': instance.accountBookSeq,
    };
