// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'set_primary_book_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SetPrimaryBookResDto _$SetPrimaryBookResDtoFromJson(Map<String, dynamic> json) {
  return _SetPrimaryBookResDto.fromJson(json);
}

/// @nodoc
class _$SetPrimaryBookResDtoTearOff {
  const _$SetPrimaryBookResDtoTearOff();

  _SetPrimaryBookResDto call(
      {required num userSeq,
      required num accountBookSeq,
      required CommonResultResDto resultVO}) {
    return _SetPrimaryBookResDto(
      userSeq: userSeq,
      accountBookSeq: accountBookSeq,
      resultVO: resultVO,
    );
  }

  SetPrimaryBookResDto fromJson(Map<String, Object?> json) {
    return SetPrimaryBookResDto.fromJson(json);
  }
}

/// @nodoc
const $SetPrimaryBookResDto = _$SetPrimaryBookResDtoTearOff();

/// @nodoc
mixin _$SetPrimaryBookResDto {
  num get userSeq => throw _privateConstructorUsedError;
  num get accountBookSeq => throw _privateConstructorUsedError;
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SetPrimaryBookResDtoCopyWith<SetPrimaryBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SetPrimaryBookResDtoCopyWith<$Res> {
  factory $SetPrimaryBookResDtoCopyWith(SetPrimaryBookResDto value,
          $Res Function(SetPrimaryBookResDto) then) =
      _$SetPrimaryBookResDtoCopyWithImpl<$Res>;
  $Res call({num userSeq, num accountBookSeq, CommonResultResDto resultVO});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$SetPrimaryBookResDtoCopyWithImpl<$Res>
    implements $SetPrimaryBookResDtoCopyWith<$Res> {
  _$SetPrimaryBookResDtoCopyWithImpl(this._value, this._then);

  final SetPrimaryBookResDto _value;
  // ignore: unused_field
  final $Res Function(SetPrimaryBookResDto) _then;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? accountBookSeq = freezed,
    Object? resultVO = freezed,
  }) {
    return _then(_value.copyWith(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$SetPrimaryBookResDtoCopyWith<$Res>
    implements $SetPrimaryBookResDtoCopyWith<$Res> {
  factory _$SetPrimaryBookResDtoCopyWith(_SetPrimaryBookResDto value,
          $Res Function(_SetPrimaryBookResDto) then) =
      __$SetPrimaryBookResDtoCopyWithImpl<$Res>;
  @override
  $Res call({num userSeq, num accountBookSeq, CommonResultResDto resultVO});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$SetPrimaryBookResDtoCopyWithImpl<$Res>
    extends _$SetPrimaryBookResDtoCopyWithImpl<$Res>
    implements _$SetPrimaryBookResDtoCopyWith<$Res> {
  __$SetPrimaryBookResDtoCopyWithImpl(
      _SetPrimaryBookResDto _value, $Res Function(_SetPrimaryBookResDto) _then)
      : super(_value, (v) => _then(v as _SetPrimaryBookResDto));

  @override
  _SetPrimaryBookResDto get _value => super._value as _SetPrimaryBookResDto;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? accountBookSeq = freezed,
    Object? resultVO = freezed,
  }) {
    return _then(_SetPrimaryBookResDto(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SetPrimaryBookResDto implements _SetPrimaryBookResDto {
  _$_SetPrimaryBookResDto(
      {required this.userSeq,
      required this.accountBookSeq,
      required this.resultVO});

  factory _$_SetPrimaryBookResDto.fromJson(Map<String, dynamic> json) =>
      _$$_SetPrimaryBookResDtoFromJson(json);

  @override
  final num userSeq;
  @override
  final num accountBookSeq;
  @override
  final CommonResultResDto resultVO;

  @override
  String toString() {
    return 'SetPrimaryBookResDto(userSeq: $userSeq, accountBookSeq: $accountBookSeq, resultVO: $resultVO)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SetPrimaryBookResDto &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq) &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, userSeq, accountBookSeq, resultVO);

  @JsonKey(ignore: true)
  @override
  _$SetPrimaryBookResDtoCopyWith<_SetPrimaryBookResDto> get copyWith =>
      __$SetPrimaryBookResDtoCopyWithImpl<_SetPrimaryBookResDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SetPrimaryBookResDtoToJson(this);
  }
}

abstract class _SetPrimaryBookResDto implements SetPrimaryBookResDto {
  factory _SetPrimaryBookResDto(
      {required num userSeq,
      required num accountBookSeq,
      required CommonResultResDto resultVO}) = _$_SetPrimaryBookResDto;

  factory _SetPrimaryBookResDto.fromJson(Map<String, dynamic> json) =
      _$_SetPrimaryBookResDto.fromJson;

  @override
  num get userSeq;
  @override
  num get accountBookSeq;
  @override
  CommonResultResDto get resultVO;
  @override
  @JsonKey(ignore: true)
  _$SetPrimaryBookResDtoCopyWith<_SetPrimaryBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
