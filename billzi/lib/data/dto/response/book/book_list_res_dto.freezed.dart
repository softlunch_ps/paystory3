// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'book_list_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookListResDto _$BookListResDtoFromJson(Map<String, dynamic> json) {
  return _BookListResDto.fromJson(json);
}

/// @nodoc
class _$BookListResDtoTearOff {
  const _$BookListResDtoTearOff();

  _BookListResDto call(
      {required CommonResultResDto resultVO,
      List<BookResDto>? accountBookList}) {
    return _BookListResDto(
      resultVO: resultVO,
      accountBookList: accountBookList,
    );
  }

  BookListResDto fromJson(Map<String, Object?> json) {
    return BookListResDto.fromJson(json);
  }
}

/// @nodoc
const $BookListResDto = _$BookListResDtoTearOff();

/// @nodoc
mixin _$BookListResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  List<BookResDto>? get accountBookList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookListResDtoCopyWith<BookListResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookListResDtoCopyWith<$Res> {
  factory $BookListResDtoCopyWith(
          BookListResDto value, $Res Function(BookListResDto) then) =
      _$BookListResDtoCopyWithImpl<$Res>;
  $Res call({CommonResultResDto resultVO, List<BookResDto>? accountBookList});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$BookListResDtoCopyWithImpl<$Res>
    implements $BookListResDtoCopyWith<$Res> {
  _$BookListResDtoCopyWithImpl(this._value, this._then);

  final BookListResDto _value;
  // ignore: unused_field
  final $Res Function(BookListResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? accountBookList = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      accountBookList: accountBookList == freezed
          ? _value.accountBookList
          : accountBookList // ignore: cast_nullable_to_non_nullable
              as List<BookResDto>?,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$BookListResDtoCopyWith<$Res>
    implements $BookListResDtoCopyWith<$Res> {
  factory _$BookListResDtoCopyWith(
          _BookListResDto value, $Res Function(_BookListResDto) then) =
      __$BookListResDtoCopyWithImpl<$Res>;
  @override
  $Res call({CommonResultResDto resultVO, List<BookResDto>? accountBookList});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$BookListResDtoCopyWithImpl<$Res>
    extends _$BookListResDtoCopyWithImpl<$Res>
    implements _$BookListResDtoCopyWith<$Res> {
  __$BookListResDtoCopyWithImpl(
      _BookListResDto _value, $Res Function(_BookListResDto) _then)
      : super(_value, (v) => _then(v as _BookListResDto));

  @override
  _BookListResDto get _value => super._value as _BookListResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? accountBookList = freezed,
  }) {
    return _then(_BookListResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      accountBookList: accountBookList == freezed
          ? _value.accountBookList
          : accountBookList // ignore: cast_nullable_to_non_nullable
              as List<BookResDto>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookListResDto implements _BookListResDto {
  _$_BookListResDto({required this.resultVO, this.accountBookList});

  factory _$_BookListResDto.fromJson(Map<String, dynamic> json) =>
      _$$_BookListResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;
  @override
  final List<BookResDto>? accountBookList;

  @override
  String toString() {
    return 'BookListResDto(resultVO: $resultVO, accountBookList: $accountBookList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BookListResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            const DeepCollectionEquality()
                .equals(other.accountBookList, accountBookList));
  }

  @override
  int get hashCode => Object.hash(runtimeType, resultVO,
      const DeepCollectionEquality().hash(accountBookList));

  @JsonKey(ignore: true)
  @override
  _$BookListResDtoCopyWith<_BookListResDto> get copyWith =>
      __$BookListResDtoCopyWithImpl<_BookListResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookListResDtoToJson(this);
  }
}

abstract class _BookListResDto implements BookListResDto {
  factory _BookListResDto(
      {required CommonResultResDto resultVO,
      List<BookResDto>? accountBookList}) = _$_BookListResDto;

  factory _BookListResDto.fromJson(Map<String, dynamic> json) =
      _$_BookListResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  List<BookResDto>? get accountBookList;
  @override
  @JsonKey(ignore: true)
  _$BookListResDtoCopyWith<_BookListResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
