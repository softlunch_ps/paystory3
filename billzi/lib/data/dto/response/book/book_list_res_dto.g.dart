// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_list_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookListResDto _$$_BookListResDtoFromJson(Map<String, dynamic> json) =>
    _$_BookListResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      accountBookList: (json['accountBookList'] as List<dynamic>?)
          ?.map((e) => BookResDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_BookListResDtoToJson(_$_BookListResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
      'accountBookList': instance.accountBookList,
    };
