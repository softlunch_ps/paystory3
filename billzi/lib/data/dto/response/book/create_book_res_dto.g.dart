// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_book_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CreateBookResDto _$$_CreateBookResDtoFromJson(Map<String, dynamic> json) =>
    _$_CreateBookResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      accountBookName: json['accountBookName'] as String?,
      accountBookDesc: json['accountBookDesc'] as String?,
      startDate: json['startDate'] as String?,
      endDate: json['endDate'] as String?,
      imgURL: json['imgURL'] as String?,
      color: json['color'] as String?,
      accountBookSeq: json['accountBookSeq'] as num?,
      useYn: json['useYn'] as String?,
      creatorSeq: json['createrUserSeq'] as num?,
      userInfo: json['userInfo'] == null
          ? null
          : CreateBookMemberDto.fromJson(
              json['userInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_CreateBookResDtoToJson(_$_CreateBookResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
      'accountBookName': instance.accountBookName,
      'accountBookDesc': instance.accountBookDesc,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'imgURL': instance.imgURL,
      'color': instance.color,
      'accountBookSeq': instance.accountBookSeq,
      'useYn': instance.useYn,
      'createrUserSeq': instance.creatorSeq,
      'userInfo': instance.userInfo,
    };
