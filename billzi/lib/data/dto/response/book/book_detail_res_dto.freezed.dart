// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'book_detail_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookDetailResDto _$BookDetailResDtoFromJson(Map<String, dynamic> json) {
  return _BookDetailResDto.fromJson(json);
}

/// @nodoc
class _$BookDetailResDtoTearOff {
  const _$BookDetailResDtoTearOff();

  _BookDetailResDto call(
      {required CommonResultResDto resultVO,
      num? accountBookSeq,
      String? accountBookName,
      num? masterUserSeq,
      List<BookMemberResDto>? userList,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn,
      @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
      String? baseYn,
      num? orderNo}) {
    return _BookDetailResDto(
      resultVO: resultVO,
      accountBookSeq: accountBookSeq,
      accountBookName: accountBookName,
      masterUserSeq: masterUserSeq,
      userList: userList,
      accountBookDesc: accountBookDesc,
      startDate: startDate,
      endDate: endDate,
      imgURL: imgURL,
      color: color,
      useYn: useYn,
      creatorUserSeq: creatorUserSeq,
      baseYn: baseYn,
      orderNo: orderNo,
    );
  }

  BookDetailResDto fromJson(Map<String, Object?> json) {
    return BookDetailResDto.fromJson(json);
  }
}

/// @nodoc
const $BookDetailResDto = _$BookDetailResDtoTearOff();

/// @nodoc
mixin _$BookDetailResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  num? get accountBookSeq => throw _privateConstructorUsedError;
  String? get accountBookName => throw _privateConstructorUsedError;
  num? get masterUserSeq => throw _privateConstructorUsedError;
  List<BookMemberResDto>? get userList => throw _privateConstructorUsedError;
  String? get accountBookDesc => throw _privateConstructorUsedError;
  DateTime? get startDate => throw _privateConstructorUsedError;
  DateTime? get endDate => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  String? get useYn => throw _privateConstructorUsedError;
  @JsonKey(name: 'createrUserSeq')
  num? get creatorUserSeq => throw _privateConstructorUsedError;
  String? get baseYn => throw _privateConstructorUsedError;
  num? get orderNo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookDetailResDtoCopyWith<BookDetailResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookDetailResDtoCopyWith<$Res> {
  factory $BookDetailResDtoCopyWith(
          BookDetailResDto value, $Res Function(BookDetailResDto) then) =
      _$BookDetailResDtoCopyWithImpl<$Res>;
  $Res call(
      {CommonResultResDto resultVO,
      num? accountBookSeq,
      String? accountBookName,
      num? masterUserSeq,
      List<BookMemberResDto>? userList,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn,
      @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
      String? baseYn,
      num? orderNo});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$BookDetailResDtoCopyWithImpl<$Res>
    implements $BookDetailResDtoCopyWith<$Res> {
  _$BookDetailResDtoCopyWithImpl(this._value, this._then);

  final BookDetailResDto _value;
  // ignore: unused_field
  final $Res Function(BookDetailResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? accountBookSeq = freezed,
    Object? accountBookName = freezed,
    Object? masterUserSeq = freezed,
    Object? userList = freezed,
    Object? accountBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? useYn = freezed,
    Object? creatorUserSeq = freezed,
    Object? baseYn = freezed,
    Object? orderNo = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String?,
      masterUserSeq: masterUserSeq == freezed
          ? _value.masterUserSeq
          : masterUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userList: userList == freezed
          ? _value.userList
          : userList // ignore: cast_nullable_to_non_nullable
              as List<BookMemberResDto>?,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
      creatorUserSeq: creatorUserSeq == freezed
          ? _value.creatorUserSeq
          : creatorUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      baseYn: baseYn == freezed
          ? _value.baseYn
          : baseYn // ignore: cast_nullable_to_non_nullable
              as String?,
      orderNo: orderNo == freezed
          ? _value.orderNo
          : orderNo // ignore: cast_nullable_to_non_nullable
              as num?,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$BookDetailResDtoCopyWith<$Res>
    implements $BookDetailResDtoCopyWith<$Res> {
  factory _$BookDetailResDtoCopyWith(
          _BookDetailResDto value, $Res Function(_BookDetailResDto) then) =
      __$BookDetailResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {CommonResultResDto resultVO,
      num? accountBookSeq,
      String? accountBookName,
      num? masterUserSeq,
      List<BookMemberResDto>? userList,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn,
      @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
      String? baseYn,
      num? orderNo});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$BookDetailResDtoCopyWithImpl<$Res>
    extends _$BookDetailResDtoCopyWithImpl<$Res>
    implements _$BookDetailResDtoCopyWith<$Res> {
  __$BookDetailResDtoCopyWithImpl(
      _BookDetailResDto _value, $Res Function(_BookDetailResDto) _then)
      : super(_value, (v) => _then(v as _BookDetailResDto));

  @override
  _BookDetailResDto get _value => super._value as _BookDetailResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? accountBookSeq = freezed,
    Object? accountBookName = freezed,
    Object? masterUserSeq = freezed,
    Object? userList = freezed,
    Object? accountBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? useYn = freezed,
    Object? creatorUserSeq = freezed,
    Object? baseYn = freezed,
    Object? orderNo = freezed,
  }) {
    return _then(_BookDetailResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String?,
      masterUserSeq: masterUserSeq == freezed
          ? _value.masterUserSeq
          : masterUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userList: userList == freezed
          ? _value.userList
          : userList // ignore: cast_nullable_to_non_nullable
              as List<BookMemberResDto>?,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
      creatorUserSeq: creatorUserSeq == freezed
          ? _value.creatorUserSeq
          : creatorUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      baseYn: baseYn == freezed
          ? _value.baseYn
          : baseYn // ignore: cast_nullable_to_non_nullable
              as String?,
      orderNo: orderNo == freezed
          ? _value.orderNo
          : orderNo // ignore: cast_nullable_to_non_nullable
              as num?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookDetailResDto extends _BookDetailResDto {
  _$_BookDetailResDto(
      {required this.resultVO,
      this.accountBookSeq,
      this.accountBookName,
      this.masterUserSeq,
      this.userList,
      this.accountBookDesc,
      this.startDate,
      this.endDate,
      this.imgURL,
      this.color,
      this.useYn,
      @JsonKey(name: 'createrUserSeq') this.creatorUserSeq,
      this.baseYn,
      this.orderNo})
      : super._();

  factory _$_BookDetailResDto.fromJson(Map<String, dynamic> json) =>
      _$$_BookDetailResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;
  @override
  final num? accountBookSeq;
  @override
  final String? accountBookName;
  @override
  final num? masterUserSeq;
  @override
  final List<BookMemberResDto>? userList;
  @override
  final String? accountBookDesc;
  @override
  final DateTime? startDate;
  @override
  final DateTime? endDate;
  @override
  final String? imgURL;
  @override
  final String? color;
  @override
  final String? useYn;
  @override
  @JsonKey(name: 'createrUserSeq')
  final num? creatorUserSeq;
  @override
  final String? baseYn;
  @override
  final num? orderNo;

  @override
  String toString() {
    return 'BookDetailResDto(resultVO: $resultVO, accountBookSeq: $accountBookSeq, accountBookName: $accountBookName, masterUserSeq: $masterUserSeq, userList: $userList, accountBookDesc: $accountBookDesc, startDate: $startDate, endDate: $endDate, imgURL: $imgURL, color: $color, useYn: $useYn, creatorUserSeq: $creatorUserSeq, baseYn: $baseYn, orderNo: $orderNo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BookDetailResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq) &&
            (identical(other.accountBookName, accountBookName) ||
                other.accountBookName == accountBookName) &&
            (identical(other.masterUserSeq, masterUserSeq) ||
                other.masterUserSeq == masterUserSeq) &&
            const DeepCollectionEquality().equals(other.userList, userList) &&
            (identical(other.accountBookDesc, accountBookDesc) ||
                other.accountBookDesc == accountBookDesc) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.endDate, endDate) || other.endDate == endDate) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.useYn, useYn) || other.useYn == useYn) &&
            (identical(other.creatorUserSeq, creatorUserSeq) ||
                other.creatorUserSeq == creatorUserSeq) &&
            (identical(other.baseYn, baseYn) || other.baseYn == baseYn) &&
            (identical(other.orderNo, orderNo) || other.orderNo == orderNo));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      resultVO,
      accountBookSeq,
      accountBookName,
      masterUserSeq,
      const DeepCollectionEquality().hash(userList),
      accountBookDesc,
      startDate,
      endDate,
      imgURL,
      color,
      useYn,
      creatorUserSeq,
      baseYn,
      orderNo);

  @JsonKey(ignore: true)
  @override
  _$BookDetailResDtoCopyWith<_BookDetailResDto> get copyWith =>
      __$BookDetailResDtoCopyWithImpl<_BookDetailResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookDetailResDtoToJson(this);
  }
}

abstract class _BookDetailResDto extends BookDetailResDto {
  factory _BookDetailResDto(
      {required CommonResultResDto resultVO,
      num? accountBookSeq,
      String? accountBookName,
      num? masterUserSeq,
      List<BookMemberResDto>? userList,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn,
      @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
      String? baseYn,
      num? orderNo}) = _$_BookDetailResDto;
  _BookDetailResDto._() : super._();

  factory _BookDetailResDto.fromJson(Map<String, dynamic> json) =
      _$_BookDetailResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  num? get accountBookSeq;
  @override
  String? get accountBookName;
  @override
  num? get masterUserSeq;
  @override
  List<BookMemberResDto>? get userList;
  @override
  String? get accountBookDesc;
  @override
  DateTime? get startDate;
  @override
  DateTime? get endDate;
  @override
  String? get imgURL;
  @override
  String? get color;
  @override
  String? get useYn;
  @override
  @JsonKey(name: 'createrUserSeq')
  num? get creatorUserSeq;
  @override
  String? get baseYn;
  @override
  num? get orderNo;
  @override
  @JsonKey(ignore: true)
  _$BookDetailResDtoCopyWith<_BookDetailResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
