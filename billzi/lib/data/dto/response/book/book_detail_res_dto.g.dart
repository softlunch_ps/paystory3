// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_detail_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookDetailResDto _$$_BookDetailResDtoFromJson(Map<String, dynamic> json) =>
    _$_BookDetailResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      accountBookSeq: json['accountBookSeq'] as num?,
      accountBookName: json['accountBookName'] as String?,
      masterUserSeq: json['masterUserSeq'] as num?,
      userList: (json['userList'] as List<dynamic>?)
          ?.map((e) => BookMemberResDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      accountBookDesc: json['accountBookDesc'] as String?,
      startDate: json['startDate'] == null
          ? null
          : DateTime.parse(json['startDate'] as String),
      endDate: json['endDate'] == null
          ? null
          : DateTime.parse(json['endDate'] as String),
      imgURL: json['imgURL'] as String?,
      color: json['color'] as String?,
      useYn: json['useYn'] as String?,
      creatorUserSeq: json['createrUserSeq'] as num?,
      baseYn: json['baseYn'] as String?,
      orderNo: json['orderNo'] as num?,
    );

Map<String, dynamic> _$$_BookDetailResDtoToJson(_$_BookDetailResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
      'accountBookSeq': instance.accountBookSeq,
      'accountBookName': instance.accountBookName,
      'masterUserSeq': instance.masterUserSeq,
      'userList': instance.userList,
      'accountBookDesc': instance.accountBookDesc,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
      'imgURL': instance.imgURL,
      'color': instance.color,
      'useYn': instance.useYn,
      'createrUserSeq': instance.creatorUserSeq,
      'baseYn': instance.baseYn,
      'orderNo': instance.orderNo,
    };
