// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_member_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookMemberResDto _$$_BookMemberResDtoFromJson(Map<String, dynamic> json) =>
    _$_BookMemberResDto(
      userSeq: json['userSeq'] as num,
      prtyUserId: json['prtyUserId'] as String,
      userNickname: json['userNickname'] as String?,
      userProfileImgUrl: json['userProfileImgUrl'] as String?,
      email: json['email'] as String?,
      grade: $enumDecodeNullable(_$MemberGradeEnumMap, json['role']),
    );

Map<String, dynamic> _$$_BookMemberResDtoToJson(_$_BookMemberResDto instance) =>
    <String, dynamic>{
      'userSeq': instance.userSeq,
      'prtyUserId': instance.prtyUserId,
      'userNickname': instance.userNickname,
      'userProfileImgUrl': instance.userProfileImgUrl,
      'email': instance.email,
      'role': _$MemberGradeEnumMap[instance.grade],
    };

const _$MemberGradeEnumMap = {
  MemberGrade.NONMEMBER: 'NONMEMBER',
  MemberGrade.MEMBER: 'MEMBER',
  MemberGrade.ADMIN: 'ADMIN',
  MemberGrade.OWNER: 'OWNER',
};
