// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'book_subscription_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookSubscriptionResDto _$BookSubscriptionResDtoFromJson(
    Map<String, dynamic> json) {
  return _BookSubscriptionResDto.fromJson(json);
}

/// @nodoc
class _$BookSubscriptionResDtoTearOff {
  const _$BookSubscriptionResDtoTearOff();

  _BookSubscriptionResDto call(
      {required CommonResultResDto resultVO,
      String? delYN,
      num? userSeq,
      num? accountBookSeq}) {
    return _BookSubscriptionResDto(
      resultVO: resultVO,
      delYN: delYN,
      userSeq: userSeq,
      accountBookSeq: accountBookSeq,
    );
  }

  BookSubscriptionResDto fromJson(Map<String, Object?> json) {
    return BookSubscriptionResDto.fromJson(json);
  }
}

/// @nodoc
const $BookSubscriptionResDto = _$BookSubscriptionResDtoTearOff();

/// @nodoc
mixin _$BookSubscriptionResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  String? get delYN => throw _privateConstructorUsedError;
  num? get userSeq => throw _privateConstructorUsedError;
  num? get accountBookSeq => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookSubscriptionResDtoCopyWith<BookSubscriptionResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookSubscriptionResDtoCopyWith<$Res> {
  factory $BookSubscriptionResDtoCopyWith(BookSubscriptionResDto value,
          $Res Function(BookSubscriptionResDto) then) =
      _$BookSubscriptionResDtoCopyWithImpl<$Res>;
  $Res call(
      {CommonResultResDto resultVO,
      String? delYN,
      num? userSeq,
      num? accountBookSeq});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$BookSubscriptionResDtoCopyWithImpl<$Res>
    implements $BookSubscriptionResDtoCopyWith<$Res> {
  _$BookSubscriptionResDtoCopyWithImpl(this._value, this._then);

  final BookSubscriptionResDto _value;
  // ignore: unused_field
  final $Res Function(BookSubscriptionResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? delYN = freezed,
    Object? userSeq = freezed,
    Object? accountBookSeq = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      delYN: delYN == freezed
          ? _value.delYN
          : delYN // ignore: cast_nullable_to_non_nullable
              as String?,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num?,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$BookSubscriptionResDtoCopyWith<$Res>
    implements $BookSubscriptionResDtoCopyWith<$Res> {
  factory _$BookSubscriptionResDtoCopyWith(_BookSubscriptionResDto value,
          $Res Function(_BookSubscriptionResDto) then) =
      __$BookSubscriptionResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {CommonResultResDto resultVO,
      String? delYN,
      num? userSeq,
      num? accountBookSeq});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$BookSubscriptionResDtoCopyWithImpl<$Res>
    extends _$BookSubscriptionResDtoCopyWithImpl<$Res>
    implements _$BookSubscriptionResDtoCopyWith<$Res> {
  __$BookSubscriptionResDtoCopyWithImpl(_BookSubscriptionResDto _value,
      $Res Function(_BookSubscriptionResDto) _then)
      : super(_value, (v) => _then(v as _BookSubscriptionResDto));

  @override
  _BookSubscriptionResDto get _value => super._value as _BookSubscriptionResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? delYN = freezed,
    Object? userSeq = freezed,
    Object? accountBookSeq = freezed,
  }) {
    return _then(_BookSubscriptionResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      delYN: delYN == freezed
          ? _value.delYN
          : delYN // ignore: cast_nullable_to_non_nullable
              as String?,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookSubscriptionResDto implements _BookSubscriptionResDto {
  _$_BookSubscriptionResDto(
      {required this.resultVO, this.delYN, this.userSeq, this.accountBookSeq});

  factory _$_BookSubscriptionResDto.fromJson(Map<String, dynamic> json) =>
      _$$_BookSubscriptionResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;
  @override
  final String? delYN;
  @override
  final num? userSeq;
  @override
  final num? accountBookSeq;

  @override
  String toString() {
    return 'BookSubscriptionResDto(resultVO: $resultVO, delYN: $delYN, userSeq: $userSeq, accountBookSeq: $accountBookSeq)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BookSubscriptionResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            (identical(other.delYN, delYN) || other.delYN == delYN) &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, resultVO, delYN, userSeq, accountBookSeq);

  @JsonKey(ignore: true)
  @override
  _$BookSubscriptionResDtoCopyWith<_BookSubscriptionResDto> get copyWith =>
      __$BookSubscriptionResDtoCopyWithImpl<_BookSubscriptionResDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookSubscriptionResDtoToJson(this);
  }
}

abstract class _BookSubscriptionResDto implements BookSubscriptionResDto {
  factory _BookSubscriptionResDto(
      {required CommonResultResDto resultVO,
      String? delYN,
      num? userSeq,
      num? accountBookSeq}) = _$_BookSubscriptionResDto;

  factory _BookSubscriptionResDto.fromJson(Map<String, dynamic> json) =
      _$_BookSubscriptionResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  String? get delYN;
  @override
  num? get userSeq;
  @override
  num? get accountBookSeq;
  @override
  @JsonKey(ignore: true)
  _$BookSubscriptionResDtoCopyWith<_BookSubscriptionResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
