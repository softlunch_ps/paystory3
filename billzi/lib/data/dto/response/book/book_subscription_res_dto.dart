import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'book_subscription_res_dto.freezed.dart';
part 'book_subscription_res_dto.g.dart';

@freezed
class BookSubscriptionResDto with _$BookSubscriptionResDto {
  factory BookSubscriptionResDto({
    required CommonResultResDto resultVO,
    String? delYN,
    num? userSeq,
    num? accountBookSeq,
  }) = _BookSubscriptionResDto;

  factory BookSubscriptionResDto.fromJson(Map<String, dynamic> json) =>
      _$BookSubscriptionResDtoFromJson(json);
}
