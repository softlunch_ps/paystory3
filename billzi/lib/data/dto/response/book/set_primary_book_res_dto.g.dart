// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'set_primary_book_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SetPrimaryBookResDto _$$_SetPrimaryBookResDtoFromJson(
        Map<String, dynamic> json) =>
    _$_SetPrimaryBookResDto(
      userSeq: json['userSeq'] as num,
      accountBookSeq: json['accountBookSeq'] as num,
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_SetPrimaryBookResDtoToJson(
        _$_SetPrimaryBookResDto instance) =>
    <String, dynamic>{
      'userSeq': instance.userSeq,
      'accountBookSeq': instance.accountBookSeq,
      'resultVO': instance.resultVO,
    };
