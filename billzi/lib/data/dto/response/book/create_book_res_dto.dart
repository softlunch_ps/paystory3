import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'create_book_res_dto.freezed.dart';
part 'create_book_res_dto.g.dart';

@freezed
class CreateBookResDto with _$CreateBookResDto {
  factory CreateBookResDto({
    required CommonResultResDto resultVO,
    String? accountBookName,
    String? accountBookDesc,
    String? startDate,
    String? endDate,
    String? imgURL,
    String? color,
    num? accountBookSeq,
    String? useYn,
    @JsonKey(name: 'createrUserSeq') num? creatorSeq,
    CreateBookMemberDto? userInfo,
  }) = _CreateBookResDto;

  factory CreateBookResDto.fromJson(Map<String, dynamic> json) =>
      _$CreateBookResDtoFromJson(json);
}
