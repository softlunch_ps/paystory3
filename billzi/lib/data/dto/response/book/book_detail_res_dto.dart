import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/book/book_member_res_dto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'book_detail_res_dto.freezed.dart';
part 'book_detail_res_dto.g.dart';

@freezed
class BookDetailResDto with _$BookDetailResDto {
  const BookDetailResDto._();

  factory BookDetailResDto({
    required CommonResultResDto resultVO,
    num? accountBookSeq,
    String? accountBookName,
    num? masterUserSeq,
    List<BookMemberResDto>? userList,
    String? accountBookDesc,
    DateTime? startDate,
    DateTime? endDate,
    String? imgURL,
    String? color,
    String? useYn,
    @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
    String? baseYn,
    num? orderNo,
  }) = _BookDetailResDto;

  factory BookDetailResDto.fromJson(Map<String, dynamic> json) =>
      _$BookDetailResDtoFromJson(json);
}
