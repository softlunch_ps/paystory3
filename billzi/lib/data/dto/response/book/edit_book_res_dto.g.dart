// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_book_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_EditBookResDto _$$_EditBookResDtoFromJson(Map<String, dynamic> json) =>
    _$_EditBookResDto(
      accountBookSeq: json['accountBookSeq'] as num,
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      accountBookName: json['accountBookName'] as String?,
      acntBookDesc: json['acntBookDesc'] as String?,
      startDate: json['startDate'] == null
          ? null
          : DateTime.parse(json['startDate'] as String),
      endDate: json['endDate'] == null
          ? null
          : DateTime.parse(json['endDate'] as String),
      imgURL: json['imgURL'] as String?,
      color: json['color'] as String?,
      useYn: json['useYn'] as String?,
    );

Map<String, dynamic> _$$_EditBookResDtoToJson(_$_EditBookResDto instance) =>
    <String, dynamic>{
      'accountBookSeq': instance.accountBookSeq,
      'resultVO': instance.resultVO,
      'accountBookName': instance.accountBookName,
      'acntBookDesc': instance.acntBookDesc,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
      'imgURL': instance.imgURL,
      'color': instance.color,
      'useYn': instance.useYn,
    };
