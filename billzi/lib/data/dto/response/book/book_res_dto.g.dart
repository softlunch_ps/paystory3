// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookResDto _$$_BookResDtoFromJson(Map<String, dynamic> json) =>
    _$_BookResDto(
      accountBookSeq: json['accountBookSeq'] as num,
      accountBookName: json['accountBookName'] as String,
      accountBookDesc: json['accountBookDesc'] as String?,
      startDate: json['startDate'] == null
          ? null
          : DateTime.parse(json['startDate'] as String),
      endDate: json['endDate'] == null
          ? null
          : DateTime.parse(json['endDate'] as String),
      color: json['color'] as String?,
      useYn: json['useYn'] as String?,
      imgURL: json['imgURL'] as String?,
      creatorUserSeq: json['createrUserSeq'] as num?,
      masterUserSeq: json['masterUserSeq'] as num?,
      orderNo: json['orderNo'] as num?,
      userList: (json['userList'] as List<dynamic>?)
          ?.map((e) => BookMemberResDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_BookResDtoToJson(_$_BookResDto instance) =>
    <String, dynamic>{
      'accountBookSeq': instance.accountBookSeq,
      'accountBookName': instance.accountBookName,
      'accountBookDesc': instance.accountBookDesc,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
      'color': instance.color,
      'useYn': instance.useYn,
      'imgURL': instance.imgURL,
      'createrUserSeq': instance.creatorUserSeq,
      'masterUserSeq': instance.masterUserSeq,
      'orderNo': instance.orderNo,
      'userList': instance.userList,
    };
