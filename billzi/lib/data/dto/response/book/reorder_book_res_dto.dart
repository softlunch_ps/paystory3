import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'reorder_book_res_dto.freezed.dart';
part 'reorder_book_res_dto.g.dart';

@freezed
class ReorderBookResDto with _$ReorderBookResDto {
  factory ReorderBookResDto({
    required CommonResultResDto resultVO,
  }) = _ReorderBookResDto;

  factory ReorderBookResDto.fromJson(Map<String, dynamic> json) =>
      _$ReorderBookResDtoFromJson(json);
}
