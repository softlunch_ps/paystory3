// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'create_book_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CreateBookResDto _$CreateBookResDtoFromJson(Map<String, dynamic> json) {
  return _CreateBookResDto.fromJson(json);
}

/// @nodoc
class _$CreateBookResDtoTearOff {
  const _$CreateBookResDtoTearOff();

  _CreateBookResDto call(
      {required CommonResultResDto resultVO,
      String? accountBookName,
      String? accountBookDesc,
      String? startDate,
      String? endDate,
      String? imgURL,
      String? color,
      num? accountBookSeq,
      String? useYn,
      @JsonKey(name: 'createrUserSeq') num? creatorSeq,
      CreateBookMemberDto? userInfo}) {
    return _CreateBookResDto(
      resultVO: resultVO,
      accountBookName: accountBookName,
      accountBookDesc: accountBookDesc,
      startDate: startDate,
      endDate: endDate,
      imgURL: imgURL,
      color: color,
      accountBookSeq: accountBookSeq,
      useYn: useYn,
      creatorSeq: creatorSeq,
      userInfo: userInfo,
    );
  }

  CreateBookResDto fromJson(Map<String, Object?> json) {
    return CreateBookResDto.fromJson(json);
  }
}

/// @nodoc
const $CreateBookResDto = _$CreateBookResDtoTearOff();

/// @nodoc
mixin _$CreateBookResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  String? get accountBookName => throw _privateConstructorUsedError;
  String? get accountBookDesc => throw _privateConstructorUsedError;
  String? get startDate => throw _privateConstructorUsedError;
  String? get endDate => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  num? get accountBookSeq => throw _privateConstructorUsedError;
  String? get useYn => throw _privateConstructorUsedError;
  @JsonKey(name: 'createrUserSeq')
  num? get creatorSeq => throw _privateConstructorUsedError;
  CreateBookMemberDto? get userInfo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CreateBookResDtoCopyWith<CreateBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateBookResDtoCopyWith<$Res> {
  factory $CreateBookResDtoCopyWith(
          CreateBookResDto value, $Res Function(CreateBookResDto) then) =
      _$CreateBookResDtoCopyWithImpl<$Res>;
  $Res call(
      {CommonResultResDto resultVO,
      String? accountBookName,
      String? accountBookDesc,
      String? startDate,
      String? endDate,
      String? imgURL,
      String? color,
      num? accountBookSeq,
      String? useYn,
      @JsonKey(name: 'createrUserSeq') num? creatorSeq,
      CreateBookMemberDto? userInfo});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
  $CreateBookMemberDtoCopyWith<$Res>? get userInfo;
}

/// @nodoc
class _$CreateBookResDtoCopyWithImpl<$Res>
    implements $CreateBookResDtoCopyWith<$Res> {
  _$CreateBookResDtoCopyWithImpl(this._value, this._then);

  final CreateBookResDto _value;
  // ignore: unused_field
  final $Res Function(CreateBookResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? accountBookName = freezed,
    Object? accountBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? accountBookSeq = freezed,
    Object? useYn = freezed,
    Object? creatorSeq = freezed,
    Object? userInfo = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String?,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as String?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
      creatorSeq: creatorSeq == freezed
          ? _value.creatorSeq
          : creatorSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userInfo: userInfo == freezed
          ? _value.userInfo
          : userInfo // ignore: cast_nullable_to_non_nullable
              as CreateBookMemberDto?,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }

  @override
  $CreateBookMemberDtoCopyWith<$Res>? get userInfo {
    if (_value.userInfo == null) {
      return null;
    }

    return $CreateBookMemberDtoCopyWith<$Res>(_value.userInfo!, (value) {
      return _then(_value.copyWith(userInfo: value));
    });
  }
}

/// @nodoc
abstract class _$CreateBookResDtoCopyWith<$Res>
    implements $CreateBookResDtoCopyWith<$Res> {
  factory _$CreateBookResDtoCopyWith(
          _CreateBookResDto value, $Res Function(_CreateBookResDto) then) =
      __$CreateBookResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {CommonResultResDto resultVO,
      String? accountBookName,
      String? accountBookDesc,
      String? startDate,
      String? endDate,
      String? imgURL,
      String? color,
      num? accountBookSeq,
      String? useYn,
      @JsonKey(name: 'createrUserSeq') num? creatorSeq,
      CreateBookMemberDto? userInfo});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
  @override
  $CreateBookMemberDtoCopyWith<$Res>? get userInfo;
}

/// @nodoc
class __$CreateBookResDtoCopyWithImpl<$Res>
    extends _$CreateBookResDtoCopyWithImpl<$Res>
    implements _$CreateBookResDtoCopyWith<$Res> {
  __$CreateBookResDtoCopyWithImpl(
      _CreateBookResDto _value, $Res Function(_CreateBookResDto) _then)
      : super(_value, (v) => _then(v as _CreateBookResDto));

  @override
  _CreateBookResDto get _value => super._value as _CreateBookResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? accountBookName = freezed,
    Object? accountBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? accountBookSeq = freezed,
    Object? useYn = freezed,
    Object? creatorSeq = freezed,
    Object? userInfo = freezed,
  }) {
    return _then(_CreateBookResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String?,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as String?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
      creatorSeq: creatorSeq == freezed
          ? _value.creatorSeq
          : creatorSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      userInfo: userInfo == freezed
          ? _value.userInfo
          : userInfo // ignore: cast_nullable_to_non_nullable
              as CreateBookMemberDto?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CreateBookResDto implements _CreateBookResDto {
  _$_CreateBookResDto(
      {required this.resultVO,
      this.accountBookName,
      this.accountBookDesc,
      this.startDate,
      this.endDate,
      this.imgURL,
      this.color,
      this.accountBookSeq,
      this.useYn,
      @JsonKey(name: 'createrUserSeq') this.creatorSeq,
      this.userInfo});

  factory _$_CreateBookResDto.fromJson(Map<String, dynamic> json) =>
      _$$_CreateBookResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;
  @override
  final String? accountBookName;
  @override
  final String? accountBookDesc;
  @override
  final String? startDate;
  @override
  final String? endDate;
  @override
  final String? imgURL;
  @override
  final String? color;
  @override
  final num? accountBookSeq;
  @override
  final String? useYn;
  @override
  @JsonKey(name: 'createrUserSeq')
  final num? creatorSeq;
  @override
  final CreateBookMemberDto? userInfo;

  @override
  String toString() {
    return 'CreateBookResDto(resultVO: $resultVO, accountBookName: $accountBookName, accountBookDesc: $accountBookDesc, startDate: $startDate, endDate: $endDate, imgURL: $imgURL, color: $color, accountBookSeq: $accountBookSeq, useYn: $useYn, creatorSeq: $creatorSeq, userInfo: $userInfo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CreateBookResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            (identical(other.accountBookName, accountBookName) ||
                other.accountBookName == accountBookName) &&
            (identical(other.accountBookDesc, accountBookDesc) ||
                other.accountBookDesc == accountBookDesc) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.endDate, endDate) || other.endDate == endDate) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq) &&
            (identical(other.useYn, useYn) || other.useYn == useYn) &&
            (identical(other.creatorSeq, creatorSeq) ||
                other.creatorSeq == creatorSeq) &&
            (identical(other.userInfo, userInfo) ||
                other.userInfo == userInfo));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      resultVO,
      accountBookName,
      accountBookDesc,
      startDate,
      endDate,
      imgURL,
      color,
      accountBookSeq,
      useYn,
      creatorSeq,
      userInfo);

  @JsonKey(ignore: true)
  @override
  _$CreateBookResDtoCopyWith<_CreateBookResDto> get copyWith =>
      __$CreateBookResDtoCopyWithImpl<_CreateBookResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CreateBookResDtoToJson(this);
  }
}

abstract class _CreateBookResDto implements CreateBookResDto {
  factory _CreateBookResDto(
      {required CommonResultResDto resultVO,
      String? accountBookName,
      String? accountBookDesc,
      String? startDate,
      String? endDate,
      String? imgURL,
      String? color,
      num? accountBookSeq,
      String? useYn,
      @JsonKey(name: 'createrUserSeq') num? creatorSeq,
      CreateBookMemberDto? userInfo}) = _$_CreateBookResDto;

  factory _CreateBookResDto.fromJson(Map<String, dynamic> json) =
      _$_CreateBookResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  String? get accountBookName;
  @override
  String? get accountBookDesc;
  @override
  String? get startDate;
  @override
  String? get endDate;
  @override
  String? get imgURL;
  @override
  String? get color;
  @override
  num? get accountBookSeq;
  @override
  String? get useYn;
  @override
  @JsonKey(name: 'createrUserSeq')
  num? get creatorSeq;
  @override
  CreateBookMemberDto? get userInfo;
  @override
  @JsonKey(ignore: true)
  _$CreateBookResDtoCopyWith<_CreateBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
