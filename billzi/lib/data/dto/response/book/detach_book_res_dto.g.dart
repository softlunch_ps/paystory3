// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detach_book_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DetachBookResDto _$$_DetachBookResDtoFromJson(Map<String, dynamic> json) =>
    _$_DetachBookResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      userSeq: json['userSeq'] as num?,
      accountBookSeq: json['accountBookSeq'] as num?,
      delYN: json['delYN'] as String?,
    );

Map<String, dynamic> _$$_DetachBookResDtoToJson(_$_DetachBookResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
      'userSeq': instance.userSeq,
      'accountBookSeq': instance.accountBookSeq,
      'delYN': instance.delYN,
    };
