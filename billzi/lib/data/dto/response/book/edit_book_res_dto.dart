import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'edit_book_res_dto.freezed.dart';
part 'edit_book_res_dto.g.dart';

@freezed
class EditBookResDto with _$EditBookResDto {
  factory EditBookResDto({
    required num accountBookSeq,
    required CommonResultResDto resultVO,
    String? accountBookName,
    String? acntBookDesc,
    DateTime? startDate,
    DateTime? endDate,
    String? imgURL,
    String? color,
    String? useYn,
  }) = _EditBookResDto;

  factory EditBookResDto.fromJson(Map<String, dynamic> json) =>
      _$EditBookResDtoFromJson(json);
}
