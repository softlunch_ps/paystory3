// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'edit_book_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

EditBookResDto _$EditBookResDtoFromJson(Map<String, dynamic> json) {
  return _EditBookResDto.fromJson(json);
}

/// @nodoc
class _$EditBookResDtoTearOff {
  const _$EditBookResDtoTearOff();

  _EditBookResDto call(
      {required num accountBookSeq,
      required CommonResultResDto resultVO,
      String? accountBookName,
      String? acntBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn}) {
    return _EditBookResDto(
      accountBookSeq: accountBookSeq,
      resultVO: resultVO,
      accountBookName: accountBookName,
      acntBookDesc: acntBookDesc,
      startDate: startDate,
      endDate: endDate,
      imgURL: imgURL,
      color: color,
      useYn: useYn,
    );
  }

  EditBookResDto fromJson(Map<String, Object?> json) {
    return EditBookResDto.fromJson(json);
  }
}

/// @nodoc
const $EditBookResDto = _$EditBookResDtoTearOff();

/// @nodoc
mixin _$EditBookResDto {
  num get accountBookSeq => throw _privateConstructorUsedError;
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  String? get accountBookName => throw _privateConstructorUsedError;
  String? get acntBookDesc => throw _privateConstructorUsedError;
  DateTime? get startDate => throw _privateConstructorUsedError;
  DateTime? get endDate => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  String? get useYn => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EditBookResDtoCopyWith<EditBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EditBookResDtoCopyWith<$Res> {
  factory $EditBookResDtoCopyWith(
          EditBookResDto value, $Res Function(EditBookResDto) then) =
      _$EditBookResDtoCopyWithImpl<$Res>;
  $Res call(
      {num accountBookSeq,
      CommonResultResDto resultVO,
      String? accountBookName,
      String? acntBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$EditBookResDtoCopyWithImpl<$Res>
    implements $EditBookResDtoCopyWith<$Res> {
  _$EditBookResDtoCopyWithImpl(this._value, this._then);

  final EditBookResDto _value;
  // ignore: unused_field
  final $Res Function(EditBookResDto) _then;

  @override
  $Res call({
    Object? accountBookSeq = freezed,
    Object? resultVO = freezed,
    Object? accountBookName = freezed,
    Object? acntBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? useYn = freezed,
  }) {
    return _then(_value.copyWith(
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String?,
      acntBookDesc: acntBookDesc == freezed
          ? _value.acntBookDesc
          : acntBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$EditBookResDtoCopyWith<$Res>
    implements $EditBookResDtoCopyWith<$Res> {
  factory _$EditBookResDtoCopyWith(
          _EditBookResDto value, $Res Function(_EditBookResDto) then) =
      __$EditBookResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num accountBookSeq,
      CommonResultResDto resultVO,
      String? accountBookName,
      String? acntBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$EditBookResDtoCopyWithImpl<$Res>
    extends _$EditBookResDtoCopyWithImpl<$Res>
    implements _$EditBookResDtoCopyWith<$Res> {
  __$EditBookResDtoCopyWithImpl(
      _EditBookResDto _value, $Res Function(_EditBookResDto) _then)
      : super(_value, (v) => _then(v as _EditBookResDto));

  @override
  _EditBookResDto get _value => super._value as _EditBookResDto;

  @override
  $Res call({
    Object? accountBookSeq = freezed,
    Object? resultVO = freezed,
    Object? accountBookName = freezed,
    Object? acntBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? imgURL = freezed,
    Object? color = freezed,
    Object? useYn = freezed,
  }) {
    return _then(_EditBookResDto(
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String?,
      acntBookDesc: acntBookDesc == freezed
          ? _value.acntBookDesc
          : acntBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_EditBookResDto implements _EditBookResDto {
  _$_EditBookResDto(
      {required this.accountBookSeq,
      required this.resultVO,
      this.accountBookName,
      this.acntBookDesc,
      this.startDate,
      this.endDate,
      this.imgURL,
      this.color,
      this.useYn});

  factory _$_EditBookResDto.fromJson(Map<String, dynamic> json) =>
      _$$_EditBookResDtoFromJson(json);

  @override
  final num accountBookSeq;
  @override
  final CommonResultResDto resultVO;
  @override
  final String? accountBookName;
  @override
  final String? acntBookDesc;
  @override
  final DateTime? startDate;
  @override
  final DateTime? endDate;
  @override
  final String? imgURL;
  @override
  final String? color;
  @override
  final String? useYn;

  @override
  String toString() {
    return 'EditBookResDto(accountBookSeq: $accountBookSeq, resultVO: $resultVO, accountBookName: $accountBookName, acntBookDesc: $acntBookDesc, startDate: $startDate, endDate: $endDate, imgURL: $imgURL, color: $color, useYn: $useYn)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _EditBookResDto &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq) &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            (identical(other.accountBookName, accountBookName) ||
                other.accountBookName == accountBookName) &&
            (identical(other.acntBookDesc, acntBookDesc) ||
                other.acntBookDesc == acntBookDesc) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.endDate, endDate) || other.endDate == endDate) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.useYn, useYn) || other.useYn == useYn));
  }

  @override
  int get hashCode => Object.hash(runtimeType, accountBookSeq, resultVO,
      accountBookName, acntBookDesc, startDate, endDate, imgURL, color, useYn);

  @JsonKey(ignore: true)
  @override
  _$EditBookResDtoCopyWith<_EditBookResDto> get copyWith =>
      __$EditBookResDtoCopyWithImpl<_EditBookResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_EditBookResDtoToJson(this);
  }
}

abstract class _EditBookResDto implements EditBookResDto {
  factory _EditBookResDto(
      {required num accountBookSeq,
      required CommonResultResDto resultVO,
      String? accountBookName,
      String? acntBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? imgURL,
      String? color,
      String? useYn}) = _$_EditBookResDto;

  factory _EditBookResDto.fromJson(Map<String, dynamic> json) =
      _$_EditBookResDto.fromJson;

  @override
  num get accountBookSeq;
  @override
  CommonResultResDto get resultVO;
  @override
  String? get accountBookName;
  @override
  String? get acntBookDesc;
  @override
  DateTime? get startDate;
  @override
  DateTime? get endDate;
  @override
  String? get imgURL;
  @override
  String? get color;
  @override
  String? get useYn;
  @override
  @JsonKey(ignore: true)
  _$EditBookResDtoCopyWith<_EditBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
