// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reorder_book_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ReorderBookResDto _$$_ReorderBookResDtoFromJson(Map<String, dynamic> json) =>
    _$_ReorderBookResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ReorderBookResDtoToJson(
        _$_ReorderBookResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
    };
