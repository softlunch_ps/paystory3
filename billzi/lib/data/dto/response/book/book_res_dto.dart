import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/book/book_member_res_dto.dart';

part 'book_res_dto.freezed.dart';
part 'book_res_dto.g.dart';

@freezed
class BookResDto with _$BookResDto {
  const BookResDto._();

  factory BookResDto({
    required num accountBookSeq,
    required String accountBookName,
    String? accountBookDesc,
    DateTime? startDate,
    DateTime? endDate,
    String? color,
    String? useYn,
    String? imgURL,
    @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
    num? masterUserSeq,
    num? orderNo,
    List<BookMemberResDto>? userList,
  }) = _BookResDto;

  factory BookResDto.fromJson(Map<String, dynamic> json) =>
      _$BookResDtoFromJson(json);

  bool get isClosed => useYn == 'N';
}
