// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'detach_book_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DetachBookResDto _$DetachBookResDtoFromJson(Map<String, dynamic> json) {
  return _DetachBookResDto.fromJson(json);
}

/// @nodoc
class _$DetachBookResDtoTearOff {
  const _$DetachBookResDtoTearOff();

  _DetachBookResDto call(
      {required CommonResultResDto resultVO,
      num? userSeq,
      num? accountBookSeq,
      String? delYN}) {
    return _DetachBookResDto(
      resultVO: resultVO,
      userSeq: userSeq,
      accountBookSeq: accountBookSeq,
      delYN: delYN,
    );
  }

  DetachBookResDto fromJson(Map<String, Object?> json) {
    return DetachBookResDto.fromJson(json);
  }
}

/// @nodoc
const $DetachBookResDto = _$DetachBookResDtoTearOff();

/// @nodoc
mixin _$DetachBookResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  num? get userSeq => throw _privateConstructorUsedError;
  num? get accountBookSeq => throw _privateConstructorUsedError;
  String? get delYN => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DetachBookResDtoCopyWith<DetachBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DetachBookResDtoCopyWith<$Res> {
  factory $DetachBookResDtoCopyWith(
          DetachBookResDto value, $Res Function(DetachBookResDto) then) =
      _$DetachBookResDtoCopyWithImpl<$Res>;
  $Res call(
      {CommonResultResDto resultVO,
      num? userSeq,
      num? accountBookSeq,
      String? delYN});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$DetachBookResDtoCopyWithImpl<$Res>
    implements $DetachBookResDtoCopyWith<$Res> {
  _$DetachBookResDtoCopyWithImpl(this._value, this._then);

  final DetachBookResDto _value;
  // ignore: unused_field
  final $Res Function(DetachBookResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? userSeq = freezed,
    Object? accountBookSeq = freezed,
    Object? delYN = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      delYN: delYN == freezed
          ? _value.delYN
          : delYN // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$DetachBookResDtoCopyWith<$Res>
    implements $DetachBookResDtoCopyWith<$Res> {
  factory _$DetachBookResDtoCopyWith(
          _DetachBookResDto value, $Res Function(_DetachBookResDto) then) =
      __$DetachBookResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {CommonResultResDto resultVO,
      num? userSeq,
      num? accountBookSeq,
      String? delYN});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$DetachBookResDtoCopyWithImpl<$Res>
    extends _$DetachBookResDtoCopyWithImpl<$Res>
    implements _$DetachBookResDtoCopyWith<$Res> {
  __$DetachBookResDtoCopyWithImpl(
      _DetachBookResDto _value, $Res Function(_DetachBookResDto) _then)
      : super(_value, (v) => _then(v as _DetachBookResDto));

  @override
  _DetachBookResDto get _value => super._value as _DetachBookResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? userSeq = freezed,
    Object? accountBookSeq = freezed,
    Object? delYN = freezed,
  }) {
    return _then(_DetachBookResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      delYN: delYN == freezed
          ? _value.delYN
          : delYN // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DetachBookResDto implements _DetachBookResDto {
  _$_DetachBookResDto(
      {required this.resultVO, this.userSeq, this.accountBookSeq, this.delYN});

  factory _$_DetachBookResDto.fromJson(Map<String, dynamic> json) =>
      _$$_DetachBookResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;
  @override
  final num? userSeq;
  @override
  final num? accountBookSeq;
  @override
  final String? delYN;

  @override
  String toString() {
    return 'DetachBookResDto(resultVO: $resultVO, userSeq: $userSeq, accountBookSeq: $accountBookSeq, delYN: $delYN)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _DetachBookResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq) &&
            (identical(other.delYN, delYN) || other.delYN == delYN));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, resultVO, userSeq, accountBookSeq, delYN);

  @JsonKey(ignore: true)
  @override
  _$DetachBookResDtoCopyWith<_DetachBookResDto> get copyWith =>
      __$DetachBookResDtoCopyWithImpl<_DetachBookResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DetachBookResDtoToJson(this);
  }
}

abstract class _DetachBookResDto implements DetachBookResDto {
  factory _DetachBookResDto(
      {required CommonResultResDto resultVO,
      num? userSeq,
      num? accountBookSeq,
      String? delYN}) = _$_DetachBookResDto;

  factory _DetachBookResDto.fromJson(Map<String, dynamic> json) =
      _$_DetachBookResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  num? get userSeq;
  @override
  num? get accountBookSeq;
  @override
  String? get delYN;
  @override
  @JsonKey(ignore: true)
  _$DetachBookResDtoCopyWith<_DetachBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
