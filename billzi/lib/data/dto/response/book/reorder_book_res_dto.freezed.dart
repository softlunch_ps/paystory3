// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'reorder_book_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ReorderBookResDto _$ReorderBookResDtoFromJson(Map<String, dynamic> json) {
  return _ReorderBookResDto.fromJson(json);
}

/// @nodoc
class _$ReorderBookResDtoTearOff {
  const _$ReorderBookResDtoTearOff();

  _ReorderBookResDto call({required CommonResultResDto resultVO}) {
    return _ReorderBookResDto(
      resultVO: resultVO,
    );
  }

  ReorderBookResDto fromJson(Map<String, Object?> json) {
    return ReorderBookResDto.fromJson(json);
  }
}

/// @nodoc
const $ReorderBookResDto = _$ReorderBookResDtoTearOff();

/// @nodoc
mixin _$ReorderBookResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ReorderBookResDtoCopyWith<ReorderBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReorderBookResDtoCopyWith<$Res> {
  factory $ReorderBookResDtoCopyWith(
          ReorderBookResDto value, $Res Function(ReorderBookResDto) then) =
      _$ReorderBookResDtoCopyWithImpl<$Res>;
  $Res call({CommonResultResDto resultVO});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$ReorderBookResDtoCopyWithImpl<$Res>
    implements $ReorderBookResDtoCopyWith<$Res> {
  _$ReorderBookResDtoCopyWithImpl(this._value, this._then);

  final ReorderBookResDto _value;
  // ignore: unused_field
  final $Res Function(ReorderBookResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$ReorderBookResDtoCopyWith<$Res>
    implements $ReorderBookResDtoCopyWith<$Res> {
  factory _$ReorderBookResDtoCopyWith(
          _ReorderBookResDto value, $Res Function(_ReorderBookResDto) then) =
      __$ReorderBookResDtoCopyWithImpl<$Res>;
  @override
  $Res call({CommonResultResDto resultVO});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$ReorderBookResDtoCopyWithImpl<$Res>
    extends _$ReorderBookResDtoCopyWithImpl<$Res>
    implements _$ReorderBookResDtoCopyWith<$Res> {
  __$ReorderBookResDtoCopyWithImpl(
      _ReorderBookResDto _value, $Res Function(_ReorderBookResDto) _then)
      : super(_value, (v) => _then(v as _ReorderBookResDto));

  @override
  _ReorderBookResDto get _value => super._value as _ReorderBookResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
  }) {
    return _then(_ReorderBookResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ReorderBookResDto implements _ReorderBookResDto {
  _$_ReorderBookResDto({required this.resultVO});

  factory _$_ReorderBookResDto.fromJson(Map<String, dynamic> json) =>
      _$$_ReorderBookResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;

  @override
  String toString() {
    return 'ReorderBookResDto(resultVO: $resultVO)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ReorderBookResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO));
  }

  @override
  int get hashCode => Object.hash(runtimeType, resultVO);

  @JsonKey(ignore: true)
  @override
  _$ReorderBookResDtoCopyWith<_ReorderBookResDto> get copyWith =>
      __$ReorderBookResDtoCopyWithImpl<_ReorderBookResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ReorderBookResDtoToJson(this);
  }
}

abstract class _ReorderBookResDto implements ReorderBookResDto {
  factory _ReorderBookResDto({required CommonResultResDto resultVO}) =
      _$_ReorderBookResDto;

  factory _ReorderBookResDto.fromJson(Map<String, dynamic> json) =
      _$_ReorderBookResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  @JsonKey(ignore: true)
  _$ReorderBookResDtoCopyWith<_ReorderBookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
