import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'book_list_res_dto.freezed.dart';
part 'book_list_res_dto.g.dart';

@freezed
class BookListResDto with _$BookListResDto {
  factory BookListResDto({
    required CommonResultResDto resultVO,
    List<BookResDto>? accountBookList,
  }) = _BookListResDto;

  factory BookListResDto.fromJson(Map<String, dynamic> json) =>
      _$BookListResDtoFromJson(json);
}
