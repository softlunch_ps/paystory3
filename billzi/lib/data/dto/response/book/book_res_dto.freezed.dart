// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'book_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookResDto _$BookResDtoFromJson(Map<String, dynamic> json) {
  return _BookResDto.fromJson(json);
}

/// @nodoc
class _$BookResDtoTearOff {
  const _$BookResDtoTearOff();

  _BookResDto call(
      {required num accountBookSeq,
      required String accountBookName,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? color,
      String? useYn,
      String? imgURL,
      @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
      num? masterUserSeq,
      num? orderNo,
      List<BookMemberResDto>? userList}) {
    return _BookResDto(
      accountBookSeq: accountBookSeq,
      accountBookName: accountBookName,
      accountBookDesc: accountBookDesc,
      startDate: startDate,
      endDate: endDate,
      color: color,
      useYn: useYn,
      imgURL: imgURL,
      creatorUserSeq: creatorUserSeq,
      masterUserSeq: masterUserSeq,
      orderNo: orderNo,
      userList: userList,
    );
  }

  BookResDto fromJson(Map<String, Object?> json) {
    return BookResDto.fromJson(json);
  }
}

/// @nodoc
const $BookResDto = _$BookResDtoTearOff();

/// @nodoc
mixin _$BookResDto {
  num get accountBookSeq => throw _privateConstructorUsedError;
  String get accountBookName => throw _privateConstructorUsedError;
  String? get accountBookDesc => throw _privateConstructorUsedError;
  DateTime? get startDate => throw _privateConstructorUsedError;
  DateTime? get endDate => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  String? get useYn => throw _privateConstructorUsedError;
  String? get imgURL => throw _privateConstructorUsedError;
  @JsonKey(name: 'createrUserSeq')
  num? get creatorUserSeq => throw _privateConstructorUsedError;
  num? get masterUserSeq => throw _privateConstructorUsedError;
  num? get orderNo => throw _privateConstructorUsedError;
  List<BookMemberResDto>? get userList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookResDtoCopyWith<BookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookResDtoCopyWith<$Res> {
  factory $BookResDtoCopyWith(
          BookResDto value, $Res Function(BookResDto) then) =
      _$BookResDtoCopyWithImpl<$Res>;
  $Res call(
      {num accountBookSeq,
      String accountBookName,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? color,
      String? useYn,
      String? imgURL,
      @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
      num? masterUserSeq,
      num? orderNo,
      List<BookMemberResDto>? userList});
}

/// @nodoc
class _$BookResDtoCopyWithImpl<$Res> implements $BookResDtoCopyWith<$Res> {
  _$BookResDtoCopyWithImpl(this._value, this._then);

  final BookResDto _value;
  // ignore: unused_field
  final $Res Function(BookResDto) _then;

  @override
  $Res call({
    Object? accountBookSeq = freezed,
    Object? accountBookName = freezed,
    Object? accountBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? color = freezed,
    Object? useYn = freezed,
    Object? imgURL = freezed,
    Object? creatorUserSeq = freezed,
    Object? masterUserSeq = freezed,
    Object? orderNo = freezed,
    Object? userList = freezed,
  }) {
    return _then(_value.copyWith(
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      creatorUserSeq: creatorUserSeq == freezed
          ? _value.creatorUserSeq
          : creatorUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      masterUserSeq: masterUserSeq == freezed
          ? _value.masterUserSeq
          : masterUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      orderNo: orderNo == freezed
          ? _value.orderNo
          : orderNo // ignore: cast_nullable_to_non_nullable
              as num?,
      userList: userList == freezed
          ? _value.userList
          : userList // ignore: cast_nullable_to_non_nullable
              as List<BookMemberResDto>?,
    ));
  }
}

/// @nodoc
abstract class _$BookResDtoCopyWith<$Res> implements $BookResDtoCopyWith<$Res> {
  factory _$BookResDtoCopyWith(
          _BookResDto value, $Res Function(_BookResDto) then) =
      __$BookResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num accountBookSeq,
      String accountBookName,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? color,
      String? useYn,
      String? imgURL,
      @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
      num? masterUserSeq,
      num? orderNo,
      List<BookMemberResDto>? userList});
}

/// @nodoc
class __$BookResDtoCopyWithImpl<$Res> extends _$BookResDtoCopyWithImpl<$Res>
    implements _$BookResDtoCopyWith<$Res> {
  __$BookResDtoCopyWithImpl(
      _BookResDto _value, $Res Function(_BookResDto) _then)
      : super(_value, (v) => _then(v as _BookResDto));

  @override
  _BookResDto get _value => super._value as _BookResDto;

  @override
  $Res call({
    Object? accountBookSeq = freezed,
    Object? accountBookName = freezed,
    Object? accountBookDesc = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
    Object? color = freezed,
    Object? useYn = freezed,
    Object? imgURL = freezed,
    Object? creatorUserSeq = freezed,
    Object? masterUserSeq = freezed,
    Object? orderNo = freezed,
    Object? userList = freezed,
  }) {
    return _then(_BookResDto(
      accountBookSeq: accountBookSeq == freezed
          ? _value.accountBookSeq
          : accountBookSeq // ignore: cast_nullable_to_non_nullable
              as num,
      accountBookName: accountBookName == freezed
          ? _value.accountBookName
          : accountBookName // ignore: cast_nullable_to_non_nullable
              as String,
      accountBookDesc: accountBookDesc == freezed
          ? _value.accountBookDesc
          : accountBookDesc // ignore: cast_nullable_to_non_nullable
              as String?,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      useYn: useYn == freezed
          ? _value.useYn
          : useYn // ignore: cast_nullable_to_non_nullable
              as String?,
      imgURL: imgURL == freezed
          ? _value.imgURL
          : imgURL // ignore: cast_nullable_to_non_nullable
              as String?,
      creatorUserSeq: creatorUserSeq == freezed
          ? _value.creatorUserSeq
          : creatorUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      masterUserSeq: masterUserSeq == freezed
          ? _value.masterUserSeq
          : masterUserSeq // ignore: cast_nullable_to_non_nullable
              as num?,
      orderNo: orderNo == freezed
          ? _value.orderNo
          : orderNo // ignore: cast_nullable_to_non_nullable
              as num?,
      userList: userList == freezed
          ? _value.userList
          : userList // ignore: cast_nullable_to_non_nullable
              as List<BookMemberResDto>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookResDto extends _BookResDto {
  _$_BookResDto(
      {required this.accountBookSeq,
      required this.accountBookName,
      this.accountBookDesc,
      this.startDate,
      this.endDate,
      this.color,
      this.useYn,
      this.imgURL,
      @JsonKey(name: 'createrUserSeq') this.creatorUserSeq,
      this.masterUserSeq,
      this.orderNo,
      this.userList})
      : super._();

  factory _$_BookResDto.fromJson(Map<String, dynamic> json) =>
      _$$_BookResDtoFromJson(json);

  @override
  final num accountBookSeq;
  @override
  final String accountBookName;
  @override
  final String? accountBookDesc;
  @override
  final DateTime? startDate;
  @override
  final DateTime? endDate;
  @override
  final String? color;
  @override
  final String? useYn;
  @override
  final String? imgURL;
  @override
  @JsonKey(name: 'createrUserSeq')
  final num? creatorUserSeq;
  @override
  final num? masterUserSeq;
  @override
  final num? orderNo;
  @override
  final List<BookMemberResDto>? userList;

  @override
  String toString() {
    return 'BookResDto(accountBookSeq: $accountBookSeq, accountBookName: $accountBookName, accountBookDesc: $accountBookDesc, startDate: $startDate, endDate: $endDate, color: $color, useYn: $useYn, imgURL: $imgURL, creatorUserSeq: $creatorUserSeq, masterUserSeq: $masterUserSeq, orderNo: $orderNo, userList: $userList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BookResDto &&
            (identical(other.accountBookSeq, accountBookSeq) ||
                other.accountBookSeq == accountBookSeq) &&
            (identical(other.accountBookName, accountBookName) ||
                other.accountBookName == accountBookName) &&
            (identical(other.accountBookDesc, accountBookDesc) ||
                other.accountBookDesc == accountBookDesc) &&
            (identical(other.startDate, startDate) ||
                other.startDate == startDate) &&
            (identical(other.endDate, endDate) || other.endDate == endDate) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.useYn, useYn) || other.useYn == useYn) &&
            (identical(other.imgURL, imgURL) || other.imgURL == imgURL) &&
            (identical(other.creatorUserSeq, creatorUserSeq) ||
                other.creatorUserSeq == creatorUserSeq) &&
            (identical(other.masterUserSeq, masterUserSeq) ||
                other.masterUserSeq == masterUserSeq) &&
            (identical(other.orderNo, orderNo) || other.orderNo == orderNo) &&
            const DeepCollectionEquality().equals(other.userList, userList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      accountBookSeq,
      accountBookName,
      accountBookDesc,
      startDate,
      endDate,
      color,
      useYn,
      imgURL,
      creatorUserSeq,
      masterUserSeq,
      orderNo,
      const DeepCollectionEquality().hash(userList));

  @JsonKey(ignore: true)
  @override
  _$BookResDtoCopyWith<_BookResDto> get copyWith =>
      __$BookResDtoCopyWithImpl<_BookResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookResDtoToJson(this);
  }
}

abstract class _BookResDto extends BookResDto {
  factory _BookResDto(
      {required num accountBookSeq,
      required String accountBookName,
      String? accountBookDesc,
      DateTime? startDate,
      DateTime? endDate,
      String? color,
      String? useYn,
      String? imgURL,
      @JsonKey(name: 'createrUserSeq') num? creatorUserSeq,
      num? masterUserSeq,
      num? orderNo,
      List<BookMemberResDto>? userList}) = _$_BookResDto;
  _BookResDto._() : super._();

  factory _BookResDto.fromJson(Map<String, dynamic> json) =
      _$_BookResDto.fromJson;

  @override
  num get accountBookSeq;
  @override
  String get accountBookName;
  @override
  String? get accountBookDesc;
  @override
  DateTime? get startDate;
  @override
  DateTime? get endDate;
  @override
  String? get color;
  @override
  String? get useYn;
  @override
  String? get imgURL;
  @override
  @JsonKey(name: 'createrUserSeq')
  num? get creatorUserSeq;
  @override
  num? get masterUserSeq;
  @override
  num? get orderNo;
  @override
  List<BookMemberResDto>? get userList;
  @override
  @JsonKey(ignore: true)
  _$BookResDtoCopyWith<_BookResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
