// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'book_member_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookMemberResDto _$BookMemberResDtoFromJson(Map<String, dynamic> json) {
  return _BookMemberResDto.fromJson(json);
}

/// @nodoc
class _$BookMemberResDtoTearOff {
  const _$BookMemberResDtoTearOff();

  _BookMemberResDto call(
      {required num userSeq,
      required String prtyUserId,
      String? userNickname,
      String? userProfileImgUrl,
      String? email,
      @JsonKey(name: 'role') MemberGrade? grade}) {
    return _BookMemberResDto(
      userSeq: userSeq,
      prtyUserId: prtyUserId,
      userNickname: userNickname,
      userProfileImgUrl: userProfileImgUrl,
      email: email,
      grade: grade,
    );
  }

  BookMemberResDto fromJson(Map<String, Object?> json) {
    return BookMemberResDto.fromJson(json);
  }
}

/// @nodoc
const $BookMemberResDto = _$BookMemberResDtoTearOff();

/// @nodoc
mixin _$BookMemberResDto {
  num get userSeq => throw _privateConstructorUsedError;
  String get prtyUserId => throw _privateConstructorUsedError;
  String? get userNickname => throw _privateConstructorUsedError;
  String? get userProfileImgUrl => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  @JsonKey(name: 'role')
  MemberGrade? get grade => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookMemberResDtoCopyWith<BookMemberResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookMemberResDtoCopyWith<$Res> {
  factory $BookMemberResDtoCopyWith(
          BookMemberResDto value, $Res Function(BookMemberResDto) then) =
      _$BookMemberResDtoCopyWithImpl<$Res>;
  $Res call(
      {num userSeq,
      String prtyUserId,
      String? userNickname,
      String? userProfileImgUrl,
      String? email,
      @JsonKey(name: 'role') MemberGrade? grade});
}

/// @nodoc
class _$BookMemberResDtoCopyWithImpl<$Res>
    implements $BookMemberResDtoCopyWith<$Res> {
  _$BookMemberResDtoCopyWithImpl(this._value, this._then);

  final BookMemberResDto _value;
  // ignore: unused_field
  final $Res Function(BookMemberResDto) _then;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? prtyUserId = freezed,
    Object? userNickname = freezed,
    Object? userProfileImgUrl = freezed,
    Object? email = freezed,
    Object? grade = freezed,
  }) {
    return _then(_value.copyWith(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num,
      prtyUserId: prtyUserId == freezed
          ? _value.prtyUserId
          : prtyUserId // ignore: cast_nullable_to_non_nullable
              as String,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as String?,
      userProfileImgUrl: userProfileImgUrl == freezed
          ? _value.userProfileImgUrl
          : userProfileImgUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      grade: grade == freezed
          ? _value.grade
          : grade // ignore: cast_nullable_to_non_nullable
              as MemberGrade?,
    ));
  }
}

/// @nodoc
abstract class _$BookMemberResDtoCopyWith<$Res>
    implements $BookMemberResDtoCopyWith<$Res> {
  factory _$BookMemberResDtoCopyWith(
          _BookMemberResDto value, $Res Function(_BookMemberResDto) then) =
      __$BookMemberResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {num userSeq,
      String prtyUserId,
      String? userNickname,
      String? userProfileImgUrl,
      String? email,
      @JsonKey(name: 'role') MemberGrade? grade});
}

/// @nodoc
class __$BookMemberResDtoCopyWithImpl<$Res>
    extends _$BookMemberResDtoCopyWithImpl<$Res>
    implements _$BookMemberResDtoCopyWith<$Res> {
  __$BookMemberResDtoCopyWithImpl(
      _BookMemberResDto _value, $Res Function(_BookMemberResDto) _then)
      : super(_value, (v) => _then(v as _BookMemberResDto));

  @override
  _BookMemberResDto get _value => super._value as _BookMemberResDto;

  @override
  $Res call({
    Object? userSeq = freezed,
    Object? prtyUserId = freezed,
    Object? userNickname = freezed,
    Object? userProfileImgUrl = freezed,
    Object? email = freezed,
    Object? grade = freezed,
  }) {
    return _then(_BookMemberResDto(
      userSeq: userSeq == freezed
          ? _value.userSeq
          : userSeq // ignore: cast_nullable_to_non_nullable
              as num,
      prtyUserId: prtyUserId == freezed
          ? _value.prtyUserId
          : prtyUserId // ignore: cast_nullable_to_non_nullable
              as String,
      userNickname: userNickname == freezed
          ? _value.userNickname
          : userNickname // ignore: cast_nullable_to_non_nullable
              as String?,
      userProfileImgUrl: userProfileImgUrl == freezed
          ? _value.userProfileImgUrl
          : userProfileImgUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      grade: grade == freezed
          ? _value.grade
          : grade // ignore: cast_nullable_to_non_nullable
              as MemberGrade?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookMemberResDto extends _BookMemberResDto {
  _$_BookMemberResDto(
      {required this.userSeq,
      required this.prtyUserId,
      this.userNickname,
      this.userProfileImgUrl,
      this.email,
      @JsonKey(name: 'role') this.grade})
      : super._();

  factory _$_BookMemberResDto.fromJson(Map<String, dynamic> json) =>
      _$$_BookMemberResDtoFromJson(json);

  @override
  final num userSeq;
  @override
  final String prtyUserId;
  @override
  final String? userNickname;
  @override
  final String? userProfileImgUrl;
  @override
  final String? email;
  @override
  @JsonKey(name: 'role')
  final MemberGrade? grade;

  @override
  String toString() {
    return 'BookMemberResDto(userSeq: $userSeq, prtyUserId: $prtyUserId, userNickname: $userNickname, userProfileImgUrl: $userProfileImgUrl, email: $email, grade: $grade)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _BookMemberResDto &&
            (identical(other.userSeq, userSeq) || other.userSeq == userSeq) &&
            (identical(other.prtyUserId, prtyUserId) ||
                other.prtyUserId == prtyUserId) &&
            (identical(other.userNickname, userNickname) ||
                other.userNickname == userNickname) &&
            (identical(other.userProfileImgUrl, userProfileImgUrl) ||
                other.userProfileImgUrl == userProfileImgUrl) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.grade, grade) || other.grade == grade));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userSeq, prtyUserId,
      userNickname, userProfileImgUrl, email, grade);

  @JsonKey(ignore: true)
  @override
  _$BookMemberResDtoCopyWith<_BookMemberResDto> get copyWith =>
      __$BookMemberResDtoCopyWithImpl<_BookMemberResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookMemberResDtoToJson(this);
  }
}

abstract class _BookMemberResDto extends BookMemberResDto {
  factory _BookMemberResDto(
      {required num userSeq,
      required String prtyUserId,
      String? userNickname,
      String? userProfileImgUrl,
      String? email,
      @JsonKey(name: 'role') MemberGrade? grade}) = _$_BookMemberResDto;
  _BookMemberResDto._() : super._();

  factory _BookMemberResDto.fromJson(Map<String, dynamic> json) =
      _$_BookMemberResDto.fromJson;

  @override
  num get userSeq;
  @override
  String get prtyUserId;
  @override
  String? get userNickname;
  @override
  String? get userProfileImgUrl;
  @override
  String? get email;
  @override
  @JsonKey(name: 'role')
  MemberGrade? get grade;
  @override
  @JsonKey(ignore: true)
  _$BookMemberResDtoCopyWith<_BookMemberResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
