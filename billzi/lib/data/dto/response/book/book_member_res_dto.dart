import 'package:freezed_annotation/freezed_annotation.dart';

part 'book_member_res_dto.freezed.dart';
part 'book_member_res_dto.g.dart';

@freezed
class BookMemberResDto with _$BookMemberResDto {
  const BookMemberResDto._();
  factory BookMemberResDto({
    required num userSeq,
    required String prtyUserId,
    String? userNickname,
    String? userProfileImgUrl,
    String? email,
    @JsonKey(name: 'role') MemberGrade? grade,
  }) = _BookMemberResDto;

  factory BookMemberResDto.fromJson(Map<String, dynamic> json) =>
      _$BookMemberResDtoFromJson(json);

  get memberStatus {
    switch (grade) {
      case MemberGrade.MEMBER:
        return 'member';
      case MemberGrade.ADMIN:
        return 'admin';
      case MemberGrade.OWNER:
        return 'owner';
      default:
        return 'non member';
    }
  }

  List<MemberGrade> get allowedMemberGrades {
    var grades = <MemberGrade>[];
    switch (grade) {
      case MemberGrade.ADMIN:
        grades
          ..add(MemberGrade.ADMIN)
          ..add(MemberGrade.MEMBER);
        break;
      case MemberGrade.OWNER:
      case MemberGrade.MEMBER:
        grades
          ..add(MemberGrade.OWNER)
          ..add(MemberGrade.ADMIN)
          ..add(MemberGrade.MEMBER);
        break;
      default:
        break;
    }
    return grades;
  }

  get isAllowedChangeGrade => (grade?.index ?? 0) >= MemberGrade.ADMIN.index;
}

enum MemberGrade { NONMEMBER, MEMBER, ADMIN, OWNER }
