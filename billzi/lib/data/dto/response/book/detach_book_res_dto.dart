import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'detach_book_res_dto.freezed.dart';
part 'detach_book_res_dto.g.dart';

@freezed
class DetachBookResDto with _$DetachBookResDto {
  factory DetachBookResDto({
    required CommonResultResDto resultVO,
    num? userSeq,
    num? accountBookSeq,
    String? delYN,
  }) = _DetachBookResDto;

  factory DetachBookResDto.fromJson(Map<String, dynamic> json) =>
      _$DetachBookResDtoFromJson(json);
}
