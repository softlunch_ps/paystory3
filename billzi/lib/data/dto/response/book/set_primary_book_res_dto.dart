import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';

part 'set_primary_book_res_dto.freezed.dart';
part 'set_primary_book_res_dto.g.dart';

@freezed
class SetPrimaryBookResDto with _$SetPrimaryBookResDto {
  factory SetPrimaryBookResDto({
    required num userSeq,
    required num accountBookSeq,
    required CommonResultResDto resultVO,
  }) = _SetPrimaryBookResDto;

  factory SetPrimaryBookResDto.fromJson(Map<String, dynamic> json) =>
      _$SetPrimaryBookResDtoFromJson(json);
}
