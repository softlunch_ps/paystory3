// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ConfigResDto _$$_ConfigResDtoFromJson(Map<String, dynamic> json) =>
    _$_ConfigResDto(
      appPush: AppPush.fromJson(json['appPush'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ConfigResDtoToJson(_$_ConfigResDto instance) =>
    <String, dynamic>{
      'appPush': instance.appPush,
    };
