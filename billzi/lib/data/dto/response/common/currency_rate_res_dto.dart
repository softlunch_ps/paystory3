import 'package:billzi/data/dto/response/common_result_res_dto.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'currency_rate_res_dto.freezed.dart';
part 'currency_rate_res_dto.g.dart';

@freezed
class CurrencyRateResDto with _$CurrencyRateResDto {
  factory CurrencyRateResDto({
    required CommonResultResDto resultVO,
    String? type,
    String? result,
    String? cur_unit,
    String? ttb,
    String? tts,
    String? deal_bas_r,
    String? bkpr,
    String? yy_efee_r,
    String? ten_dd_efee_r,
    String? kftc_bkpr,
    String? kftc_deal_bas_r,
    String? cur_nm,
  }) = _CurrencyRateResDto;

  factory CurrencyRateResDto.fromJson(Map<String, dynamic> json) =>
      _$CurrencyRateResDtoFromJson(json);
}