import 'package:billzi/data/dto/model/app_push.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'config_res_dto.freezed.dart';
part 'config_res_dto.g.dart';

@freezed
class ConfigResDto with _$ConfigResDto {
  factory ConfigResDto({
    required AppPush appPush,
  }) = _ConfigResDto;

  factory ConfigResDto.fromJson(Map<String, dynamic> json) =>
      _$ConfigResDtoFromJson(json);
}