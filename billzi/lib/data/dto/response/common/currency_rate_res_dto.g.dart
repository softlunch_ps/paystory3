// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency_rate_res_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CurrencyRateResDto _$$_CurrencyRateResDtoFromJson(
        Map<String, dynamic> json) =>
    _$_CurrencyRateResDto(
      resultVO:
          CommonResultResDto.fromJson(json['resultVO'] as Map<String, dynamic>),
      type: json['type'] as String?,
      result: json['result'] as String?,
      cur_unit: json['cur_unit'] as String?,
      ttb: json['ttb'] as String?,
      tts: json['tts'] as String?,
      deal_bas_r: json['deal_bas_r'] as String?,
      bkpr: json['bkpr'] as String?,
      yy_efee_r: json['yy_efee_r'] as String?,
      ten_dd_efee_r: json['ten_dd_efee_r'] as String?,
      kftc_bkpr: json['kftc_bkpr'] as String?,
      kftc_deal_bas_r: json['kftc_deal_bas_r'] as String?,
      cur_nm: json['cur_nm'] as String?,
    );

Map<String, dynamic> _$$_CurrencyRateResDtoToJson(
        _$_CurrencyRateResDto instance) =>
    <String, dynamic>{
      'resultVO': instance.resultVO,
      'type': instance.type,
      'result': instance.result,
      'cur_unit': instance.cur_unit,
      'ttb': instance.ttb,
      'tts': instance.tts,
      'deal_bas_r': instance.deal_bas_r,
      'bkpr': instance.bkpr,
      'yy_efee_r': instance.yy_efee_r,
      'ten_dd_efee_r': instance.ten_dd_efee_r,
      'kftc_bkpr': instance.kftc_bkpr,
      'kftc_deal_bas_r': instance.kftc_deal_bas_r,
      'cur_nm': instance.cur_nm,
    };
