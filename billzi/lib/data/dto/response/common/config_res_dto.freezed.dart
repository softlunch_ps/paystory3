// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'config_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ConfigResDto _$ConfigResDtoFromJson(Map<String, dynamic> json) {
  return _ConfigResDto.fromJson(json);
}

/// @nodoc
class _$ConfigResDtoTearOff {
  const _$ConfigResDtoTearOff();

  _ConfigResDto call({required AppPush appPush}) {
    return _ConfigResDto(
      appPush: appPush,
    );
  }

  ConfigResDto fromJson(Map<String, Object?> json) {
    return ConfigResDto.fromJson(json);
  }
}

/// @nodoc
const $ConfigResDto = _$ConfigResDtoTearOff();

/// @nodoc
mixin _$ConfigResDto {
  AppPush get appPush => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ConfigResDtoCopyWith<ConfigResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ConfigResDtoCopyWith<$Res> {
  factory $ConfigResDtoCopyWith(
          ConfigResDto value, $Res Function(ConfigResDto) then) =
      _$ConfigResDtoCopyWithImpl<$Res>;
  $Res call({AppPush appPush});

  $AppPushCopyWith<$Res> get appPush;
}

/// @nodoc
class _$ConfigResDtoCopyWithImpl<$Res> implements $ConfigResDtoCopyWith<$Res> {
  _$ConfigResDtoCopyWithImpl(this._value, this._then);

  final ConfigResDto _value;
  // ignore: unused_field
  final $Res Function(ConfigResDto) _then;

  @override
  $Res call({
    Object? appPush = freezed,
  }) {
    return _then(_value.copyWith(
      appPush: appPush == freezed
          ? _value.appPush
          : appPush // ignore: cast_nullable_to_non_nullable
              as AppPush,
    ));
  }

  @override
  $AppPushCopyWith<$Res> get appPush {
    return $AppPushCopyWith<$Res>(_value.appPush, (value) {
      return _then(_value.copyWith(appPush: value));
    });
  }
}

/// @nodoc
abstract class _$ConfigResDtoCopyWith<$Res>
    implements $ConfigResDtoCopyWith<$Res> {
  factory _$ConfigResDtoCopyWith(
          _ConfigResDto value, $Res Function(_ConfigResDto) then) =
      __$ConfigResDtoCopyWithImpl<$Res>;
  @override
  $Res call({AppPush appPush});

  @override
  $AppPushCopyWith<$Res> get appPush;
}

/// @nodoc
class __$ConfigResDtoCopyWithImpl<$Res> extends _$ConfigResDtoCopyWithImpl<$Res>
    implements _$ConfigResDtoCopyWith<$Res> {
  __$ConfigResDtoCopyWithImpl(
      _ConfigResDto _value, $Res Function(_ConfigResDto) _then)
      : super(_value, (v) => _then(v as _ConfigResDto));

  @override
  _ConfigResDto get _value => super._value as _ConfigResDto;

  @override
  $Res call({
    Object? appPush = freezed,
  }) {
    return _then(_ConfigResDto(
      appPush: appPush == freezed
          ? _value.appPush
          : appPush // ignore: cast_nullable_to_non_nullable
              as AppPush,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ConfigResDto implements _ConfigResDto {
  _$_ConfigResDto({required this.appPush});

  factory _$_ConfigResDto.fromJson(Map<String, dynamic> json) =>
      _$$_ConfigResDtoFromJson(json);

  @override
  final AppPush appPush;

  @override
  String toString() {
    return 'ConfigResDto(appPush: $appPush)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ConfigResDto &&
            (identical(other.appPush, appPush) || other.appPush == appPush));
  }

  @override
  int get hashCode => Object.hash(runtimeType, appPush);

  @JsonKey(ignore: true)
  @override
  _$ConfigResDtoCopyWith<_ConfigResDto> get copyWith =>
      __$ConfigResDtoCopyWithImpl<_ConfigResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ConfigResDtoToJson(this);
  }
}

abstract class _ConfigResDto implements ConfigResDto {
  factory _ConfigResDto({required AppPush appPush}) = _$_ConfigResDto;

  factory _ConfigResDto.fromJson(Map<String, dynamic> json) =
      _$_ConfigResDto.fromJson;

  @override
  AppPush get appPush;
  @override
  @JsonKey(ignore: true)
  _$ConfigResDtoCopyWith<_ConfigResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
