// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'currency_rate_res_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CurrencyRateResDto _$CurrencyRateResDtoFromJson(Map<String, dynamic> json) {
  return _CurrencyRateResDto.fromJson(json);
}

/// @nodoc
class _$CurrencyRateResDtoTearOff {
  const _$CurrencyRateResDtoTearOff();

  _CurrencyRateResDto call(
      {required CommonResultResDto resultVO,
      String? type,
      String? result,
      String? cur_unit,
      String? ttb,
      String? tts,
      String? deal_bas_r,
      String? bkpr,
      String? yy_efee_r,
      String? ten_dd_efee_r,
      String? kftc_bkpr,
      String? kftc_deal_bas_r,
      String? cur_nm}) {
    return _CurrencyRateResDto(
      resultVO: resultVO,
      type: type,
      result: result,
      cur_unit: cur_unit,
      ttb: ttb,
      tts: tts,
      deal_bas_r: deal_bas_r,
      bkpr: bkpr,
      yy_efee_r: yy_efee_r,
      ten_dd_efee_r: ten_dd_efee_r,
      kftc_bkpr: kftc_bkpr,
      kftc_deal_bas_r: kftc_deal_bas_r,
      cur_nm: cur_nm,
    );
  }

  CurrencyRateResDto fromJson(Map<String, Object?> json) {
    return CurrencyRateResDto.fromJson(json);
  }
}

/// @nodoc
const $CurrencyRateResDto = _$CurrencyRateResDtoTearOff();

/// @nodoc
mixin _$CurrencyRateResDto {
  CommonResultResDto get resultVO => throw _privateConstructorUsedError;
  String? get type => throw _privateConstructorUsedError;
  String? get result => throw _privateConstructorUsedError;
  String? get cur_unit => throw _privateConstructorUsedError;
  String? get ttb => throw _privateConstructorUsedError;
  String? get tts => throw _privateConstructorUsedError;
  String? get deal_bas_r => throw _privateConstructorUsedError;
  String? get bkpr => throw _privateConstructorUsedError;
  String? get yy_efee_r => throw _privateConstructorUsedError;
  String? get ten_dd_efee_r => throw _privateConstructorUsedError;
  String? get kftc_bkpr => throw _privateConstructorUsedError;
  String? get kftc_deal_bas_r => throw _privateConstructorUsedError;
  String? get cur_nm => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CurrencyRateResDtoCopyWith<CurrencyRateResDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CurrencyRateResDtoCopyWith<$Res> {
  factory $CurrencyRateResDtoCopyWith(
          CurrencyRateResDto value, $Res Function(CurrencyRateResDto) then) =
      _$CurrencyRateResDtoCopyWithImpl<$Res>;
  $Res call(
      {CommonResultResDto resultVO,
      String? type,
      String? result,
      String? cur_unit,
      String? ttb,
      String? tts,
      String? deal_bas_r,
      String? bkpr,
      String? yy_efee_r,
      String? ten_dd_efee_r,
      String? kftc_bkpr,
      String? kftc_deal_bas_r,
      String? cur_nm});

  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class _$CurrencyRateResDtoCopyWithImpl<$Res>
    implements $CurrencyRateResDtoCopyWith<$Res> {
  _$CurrencyRateResDtoCopyWithImpl(this._value, this._then);

  final CurrencyRateResDto _value;
  // ignore: unused_field
  final $Res Function(CurrencyRateResDto) _then;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? type = freezed,
    Object? result = freezed,
    Object? cur_unit = freezed,
    Object? ttb = freezed,
    Object? tts = freezed,
    Object? deal_bas_r = freezed,
    Object? bkpr = freezed,
    Object? yy_efee_r = freezed,
    Object? ten_dd_efee_r = freezed,
    Object? kftc_bkpr = freezed,
    Object? kftc_deal_bas_r = freezed,
    Object? cur_nm = freezed,
  }) {
    return _then(_value.copyWith(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      result: result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as String?,
      cur_unit: cur_unit == freezed
          ? _value.cur_unit
          : cur_unit // ignore: cast_nullable_to_non_nullable
              as String?,
      ttb: ttb == freezed
          ? _value.ttb
          : ttb // ignore: cast_nullable_to_non_nullable
              as String?,
      tts: tts == freezed
          ? _value.tts
          : tts // ignore: cast_nullable_to_non_nullable
              as String?,
      deal_bas_r: deal_bas_r == freezed
          ? _value.deal_bas_r
          : deal_bas_r // ignore: cast_nullable_to_non_nullable
              as String?,
      bkpr: bkpr == freezed
          ? _value.bkpr
          : bkpr // ignore: cast_nullable_to_non_nullable
              as String?,
      yy_efee_r: yy_efee_r == freezed
          ? _value.yy_efee_r
          : yy_efee_r // ignore: cast_nullable_to_non_nullable
              as String?,
      ten_dd_efee_r: ten_dd_efee_r == freezed
          ? _value.ten_dd_efee_r
          : ten_dd_efee_r // ignore: cast_nullable_to_non_nullable
              as String?,
      kftc_bkpr: kftc_bkpr == freezed
          ? _value.kftc_bkpr
          : kftc_bkpr // ignore: cast_nullable_to_non_nullable
              as String?,
      kftc_deal_bas_r: kftc_deal_bas_r == freezed
          ? _value.kftc_deal_bas_r
          : kftc_deal_bas_r // ignore: cast_nullable_to_non_nullable
              as String?,
      cur_nm: cur_nm == freezed
          ? _value.cur_nm
          : cur_nm // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO {
    return $CommonResultResDtoCopyWith<$Res>(_value.resultVO, (value) {
      return _then(_value.copyWith(resultVO: value));
    });
  }
}

/// @nodoc
abstract class _$CurrencyRateResDtoCopyWith<$Res>
    implements $CurrencyRateResDtoCopyWith<$Res> {
  factory _$CurrencyRateResDtoCopyWith(
          _CurrencyRateResDto value, $Res Function(_CurrencyRateResDto) then) =
      __$CurrencyRateResDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {CommonResultResDto resultVO,
      String? type,
      String? result,
      String? cur_unit,
      String? ttb,
      String? tts,
      String? deal_bas_r,
      String? bkpr,
      String? yy_efee_r,
      String? ten_dd_efee_r,
      String? kftc_bkpr,
      String? kftc_deal_bas_r,
      String? cur_nm});

  @override
  $CommonResultResDtoCopyWith<$Res> get resultVO;
}

/// @nodoc
class __$CurrencyRateResDtoCopyWithImpl<$Res>
    extends _$CurrencyRateResDtoCopyWithImpl<$Res>
    implements _$CurrencyRateResDtoCopyWith<$Res> {
  __$CurrencyRateResDtoCopyWithImpl(
      _CurrencyRateResDto _value, $Res Function(_CurrencyRateResDto) _then)
      : super(_value, (v) => _then(v as _CurrencyRateResDto));

  @override
  _CurrencyRateResDto get _value => super._value as _CurrencyRateResDto;

  @override
  $Res call({
    Object? resultVO = freezed,
    Object? type = freezed,
    Object? result = freezed,
    Object? cur_unit = freezed,
    Object? ttb = freezed,
    Object? tts = freezed,
    Object? deal_bas_r = freezed,
    Object? bkpr = freezed,
    Object? yy_efee_r = freezed,
    Object? ten_dd_efee_r = freezed,
    Object? kftc_bkpr = freezed,
    Object? kftc_deal_bas_r = freezed,
    Object? cur_nm = freezed,
  }) {
    return _then(_CurrencyRateResDto(
      resultVO: resultVO == freezed
          ? _value.resultVO
          : resultVO // ignore: cast_nullable_to_non_nullable
              as CommonResultResDto,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      result: result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as String?,
      cur_unit: cur_unit == freezed
          ? _value.cur_unit
          : cur_unit // ignore: cast_nullable_to_non_nullable
              as String?,
      ttb: ttb == freezed
          ? _value.ttb
          : ttb // ignore: cast_nullable_to_non_nullable
              as String?,
      tts: tts == freezed
          ? _value.tts
          : tts // ignore: cast_nullable_to_non_nullable
              as String?,
      deal_bas_r: deal_bas_r == freezed
          ? _value.deal_bas_r
          : deal_bas_r // ignore: cast_nullable_to_non_nullable
              as String?,
      bkpr: bkpr == freezed
          ? _value.bkpr
          : bkpr // ignore: cast_nullable_to_non_nullable
              as String?,
      yy_efee_r: yy_efee_r == freezed
          ? _value.yy_efee_r
          : yy_efee_r // ignore: cast_nullable_to_non_nullable
              as String?,
      ten_dd_efee_r: ten_dd_efee_r == freezed
          ? _value.ten_dd_efee_r
          : ten_dd_efee_r // ignore: cast_nullable_to_non_nullable
              as String?,
      kftc_bkpr: kftc_bkpr == freezed
          ? _value.kftc_bkpr
          : kftc_bkpr // ignore: cast_nullable_to_non_nullable
              as String?,
      kftc_deal_bas_r: kftc_deal_bas_r == freezed
          ? _value.kftc_deal_bas_r
          : kftc_deal_bas_r // ignore: cast_nullable_to_non_nullable
              as String?,
      cur_nm: cur_nm == freezed
          ? _value.cur_nm
          : cur_nm // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CurrencyRateResDto implements _CurrencyRateResDto {
  _$_CurrencyRateResDto(
      {required this.resultVO,
      this.type,
      this.result,
      this.cur_unit,
      this.ttb,
      this.tts,
      this.deal_bas_r,
      this.bkpr,
      this.yy_efee_r,
      this.ten_dd_efee_r,
      this.kftc_bkpr,
      this.kftc_deal_bas_r,
      this.cur_nm});

  factory _$_CurrencyRateResDto.fromJson(Map<String, dynamic> json) =>
      _$$_CurrencyRateResDtoFromJson(json);

  @override
  final CommonResultResDto resultVO;
  @override
  final String? type;
  @override
  final String? result;
  @override
  final String? cur_unit;
  @override
  final String? ttb;
  @override
  final String? tts;
  @override
  final String? deal_bas_r;
  @override
  final String? bkpr;
  @override
  final String? yy_efee_r;
  @override
  final String? ten_dd_efee_r;
  @override
  final String? kftc_bkpr;
  @override
  final String? kftc_deal_bas_r;
  @override
  final String? cur_nm;

  @override
  String toString() {
    return 'CurrencyRateResDto(resultVO: $resultVO, type: $type, result: $result, cur_unit: $cur_unit, ttb: $ttb, tts: $tts, deal_bas_r: $deal_bas_r, bkpr: $bkpr, yy_efee_r: $yy_efee_r, ten_dd_efee_r: $ten_dd_efee_r, kftc_bkpr: $kftc_bkpr, kftc_deal_bas_r: $kftc_deal_bas_r, cur_nm: $cur_nm)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CurrencyRateResDto &&
            (identical(other.resultVO, resultVO) ||
                other.resultVO == resultVO) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.result, result) || other.result == result) &&
            (identical(other.cur_unit, cur_unit) ||
                other.cur_unit == cur_unit) &&
            (identical(other.ttb, ttb) || other.ttb == ttb) &&
            (identical(other.tts, tts) || other.tts == tts) &&
            (identical(other.deal_bas_r, deal_bas_r) ||
                other.deal_bas_r == deal_bas_r) &&
            (identical(other.bkpr, bkpr) || other.bkpr == bkpr) &&
            (identical(other.yy_efee_r, yy_efee_r) ||
                other.yy_efee_r == yy_efee_r) &&
            (identical(other.ten_dd_efee_r, ten_dd_efee_r) ||
                other.ten_dd_efee_r == ten_dd_efee_r) &&
            (identical(other.kftc_bkpr, kftc_bkpr) ||
                other.kftc_bkpr == kftc_bkpr) &&
            (identical(other.kftc_deal_bas_r, kftc_deal_bas_r) ||
                other.kftc_deal_bas_r == kftc_deal_bas_r) &&
            (identical(other.cur_nm, cur_nm) || other.cur_nm == cur_nm));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      resultVO,
      type,
      result,
      cur_unit,
      ttb,
      tts,
      deal_bas_r,
      bkpr,
      yy_efee_r,
      ten_dd_efee_r,
      kftc_bkpr,
      kftc_deal_bas_r,
      cur_nm);

  @JsonKey(ignore: true)
  @override
  _$CurrencyRateResDtoCopyWith<_CurrencyRateResDto> get copyWith =>
      __$CurrencyRateResDtoCopyWithImpl<_CurrencyRateResDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CurrencyRateResDtoToJson(this);
  }
}

abstract class _CurrencyRateResDto implements CurrencyRateResDto {
  factory _CurrencyRateResDto(
      {required CommonResultResDto resultVO,
      String? type,
      String? result,
      String? cur_unit,
      String? ttb,
      String? tts,
      String? deal_bas_r,
      String? bkpr,
      String? yy_efee_r,
      String? ten_dd_efee_r,
      String? kftc_bkpr,
      String? kftc_deal_bas_r,
      String? cur_nm}) = _$_CurrencyRateResDto;

  factory _CurrencyRateResDto.fromJson(Map<String, dynamic> json) =
      _$_CurrencyRateResDto.fromJson;

  @override
  CommonResultResDto get resultVO;
  @override
  String? get type;
  @override
  String? get result;
  @override
  String? get cur_unit;
  @override
  String? get ttb;
  @override
  String? get tts;
  @override
  String? get deal_bas_r;
  @override
  String? get bkpr;
  @override
  String? get yy_efee_r;
  @override
  String? get ten_dd_efee_r;
  @override
  String? get kftc_bkpr;
  @override
  String? get kftc_deal_bas_r;
  @override
  String? get cur_nm;
  @override
  @JsonKey(ignore: true)
  _$CurrencyRateResDtoCopyWith<_CurrencyRateResDto> get copyWith =>
      throw _privateConstructorUsedError;
}
