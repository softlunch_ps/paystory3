// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'four_square_location_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FourSquareLocationDto _$FourSquareLocationDtoFromJson(
    Map<String, dynamic> json) {
  return _FourSquareLocationDto.fromJson(json);
}

/// @nodoc
class _$FourSquareLocationDtoTearOff {
  const _$FourSquareLocationDtoTearOff();

  _FourSquareLocationDto call(
      {String? address,
      String? crossStreet,
      double? lat,
      double? lng,
      List<Map<String, dynamic>>? labeledLatLngs,
      double? distance,
      String? postalCode,
      String? cc,
      String? city,
      String? state,
      String? country,
      List<String>? formattedAddress}) {
    return _FourSquareLocationDto(
      address: address,
      crossStreet: crossStreet,
      lat: lat,
      lng: lng,
      labeledLatLngs: labeledLatLngs,
      distance: distance,
      postalCode: postalCode,
      cc: cc,
      city: city,
      state: state,
      country: country,
      formattedAddress: formattedAddress,
    );
  }

  FourSquareLocationDto fromJson(Map<String, Object?> json) {
    return FourSquareLocationDto.fromJson(json);
  }
}

/// @nodoc
const $FourSquareLocationDto = _$FourSquareLocationDtoTearOff();

/// @nodoc
mixin _$FourSquareLocationDto {
  String? get address => throw _privateConstructorUsedError;
  String? get crossStreet => throw _privateConstructorUsedError;
  double? get lat => throw _privateConstructorUsedError;
  double? get lng => throw _privateConstructorUsedError;
  List<Map<String, dynamic>>? get labeledLatLngs =>
      throw _privateConstructorUsedError;
  double? get distance => throw _privateConstructorUsedError;
  String? get postalCode => throw _privateConstructorUsedError;
  String? get cc => throw _privateConstructorUsedError;
  String? get city => throw _privateConstructorUsedError;
  String? get state => throw _privateConstructorUsedError;
  String? get country => throw _privateConstructorUsedError;
  List<String>? get formattedAddress => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FourSquareLocationDtoCopyWith<FourSquareLocationDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FourSquareLocationDtoCopyWith<$Res> {
  factory $FourSquareLocationDtoCopyWith(FourSquareLocationDto value,
          $Res Function(FourSquareLocationDto) then) =
      _$FourSquareLocationDtoCopyWithImpl<$Res>;
  $Res call(
      {String? address,
      String? crossStreet,
      double? lat,
      double? lng,
      List<Map<String, dynamic>>? labeledLatLngs,
      double? distance,
      String? postalCode,
      String? cc,
      String? city,
      String? state,
      String? country,
      List<String>? formattedAddress});
}

/// @nodoc
class _$FourSquareLocationDtoCopyWithImpl<$Res>
    implements $FourSquareLocationDtoCopyWith<$Res> {
  _$FourSquareLocationDtoCopyWithImpl(this._value, this._then);

  final FourSquareLocationDto _value;
  // ignore: unused_field
  final $Res Function(FourSquareLocationDto) _then;

  @override
  $Res call({
    Object? address = freezed,
    Object? crossStreet = freezed,
    Object? lat = freezed,
    Object? lng = freezed,
    Object? labeledLatLngs = freezed,
    Object? distance = freezed,
    Object? postalCode = freezed,
    Object? cc = freezed,
    Object? city = freezed,
    Object? state = freezed,
    Object? country = freezed,
    Object? formattedAddress = freezed,
  }) {
    return _then(_value.copyWith(
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      crossStreet: crossStreet == freezed
          ? _value.crossStreet
          : crossStreet // ignore: cast_nullable_to_non_nullable
              as String?,
      lat: lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double?,
      lng: lng == freezed
          ? _value.lng
          : lng // ignore: cast_nullable_to_non_nullable
              as double?,
      labeledLatLngs: labeledLatLngs == freezed
          ? _value.labeledLatLngs
          : labeledLatLngs // ignore: cast_nullable_to_non_nullable
              as List<Map<String, dynamic>>?,
      distance: distance == freezed
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as double?,
      postalCode: postalCode == freezed
          ? _value.postalCode
          : postalCode // ignore: cast_nullable_to_non_nullable
              as String?,
      cc: cc == freezed
          ? _value.cc
          : cc // ignore: cast_nullable_to_non_nullable
              as String?,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      state: state == freezed
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as String?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      formattedAddress: formattedAddress == freezed
          ? _value.formattedAddress
          : formattedAddress // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
abstract class _$FourSquareLocationDtoCopyWith<$Res>
    implements $FourSquareLocationDtoCopyWith<$Res> {
  factory _$FourSquareLocationDtoCopyWith(_FourSquareLocationDto value,
          $Res Function(_FourSquareLocationDto) then) =
      __$FourSquareLocationDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? address,
      String? crossStreet,
      double? lat,
      double? lng,
      List<Map<String, dynamic>>? labeledLatLngs,
      double? distance,
      String? postalCode,
      String? cc,
      String? city,
      String? state,
      String? country,
      List<String>? formattedAddress});
}

/// @nodoc
class __$FourSquareLocationDtoCopyWithImpl<$Res>
    extends _$FourSquareLocationDtoCopyWithImpl<$Res>
    implements _$FourSquareLocationDtoCopyWith<$Res> {
  __$FourSquareLocationDtoCopyWithImpl(_FourSquareLocationDto _value,
      $Res Function(_FourSquareLocationDto) _then)
      : super(_value, (v) => _then(v as _FourSquareLocationDto));

  @override
  _FourSquareLocationDto get _value => super._value as _FourSquareLocationDto;

  @override
  $Res call({
    Object? address = freezed,
    Object? crossStreet = freezed,
    Object? lat = freezed,
    Object? lng = freezed,
    Object? labeledLatLngs = freezed,
    Object? distance = freezed,
    Object? postalCode = freezed,
    Object? cc = freezed,
    Object? city = freezed,
    Object? state = freezed,
    Object? country = freezed,
    Object? formattedAddress = freezed,
  }) {
    return _then(_FourSquareLocationDto(
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      crossStreet: crossStreet == freezed
          ? _value.crossStreet
          : crossStreet // ignore: cast_nullable_to_non_nullable
              as String?,
      lat: lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double?,
      lng: lng == freezed
          ? _value.lng
          : lng // ignore: cast_nullable_to_non_nullable
              as double?,
      labeledLatLngs: labeledLatLngs == freezed
          ? _value.labeledLatLngs
          : labeledLatLngs // ignore: cast_nullable_to_non_nullable
              as List<Map<String, dynamic>>?,
      distance: distance == freezed
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as double?,
      postalCode: postalCode == freezed
          ? _value.postalCode
          : postalCode // ignore: cast_nullable_to_non_nullable
              as String?,
      cc: cc == freezed
          ? _value.cc
          : cc // ignore: cast_nullable_to_non_nullable
              as String?,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      state: state == freezed
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as String?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      formattedAddress: formattedAddress == freezed
          ? _value.formattedAddress
          : formattedAddress // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FourSquareLocationDto implements _FourSquareLocationDto {
  _$_FourSquareLocationDto(
      {this.address,
      this.crossStreet,
      this.lat,
      this.lng,
      this.labeledLatLngs,
      this.distance,
      this.postalCode,
      this.cc,
      this.city,
      this.state,
      this.country,
      this.formattedAddress});

  factory _$_FourSquareLocationDto.fromJson(Map<String, dynamic> json) =>
      _$$_FourSquareLocationDtoFromJson(json);

  @override
  final String? address;
  @override
  final String? crossStreet;
  @override
  final double? lat;
  @override
  final double? lng;
  @override
  final List<Map<String, dynamic>>? labeledLatLngs;
  @override
  final double? distance;
  @override
  final String? postalCode;
  @override
  final String? cc;
  @override
  final String? city;
  @override
  final String? state;
  @override
  final String? country;
  @override
  final List<String>? formattedAddress;

  @override
  String toString() {
    return 'FourSquareLocationDto(address: $address, crossStreet: $crossStreet, lat: $lat, lng: $lng, labeledLatLngs: $labeledLatLngs, distance: $distance, postalCode: $postalCode, cc: $cc, city: $city, state: $state, country: $country, formattedAddress: $formattedAddress)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _FourSquareLocationDto &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.crossStreet, crossStreet) ||
                other.crossStreet == crossStreet) &&
            (identical(other.lat, lat) || other.lat == lat) &&
            (identical(other.lng, lng) || other.lng == lng) &&
            const DeepCollectionEquality()
                .equals(other.labeledLatLngs, labeledLatLngs) &&
            (identical(other.distance, distance) ||
                other.distance == distance) &&
            (identical(other.postalCode, postalCode) ||
                other.postalCode == postalCode) &&
            (identical(other.cc, cc) || other.cc == cc) &&
            (identical(other.city, city) || other.city == city) &&
            (identical(other.state, state) || other.state == state) &&
            (identical(other.country, country) || other.country == country) &&
            const DeepCollectionEquality()
                .equals(other.formattedAddress, formattedAddress));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      address,
      crossStreet,
      lat,
      lng,
      const DeepCollectionEquality().hash(labeledLatLngs),
      distance,
      postalCode,
      cc,
      city,
      state,
      country,
      const DeepCollectionEquality().hash(formattedAddress));

  @JsonKey(ignore: true)
  @override
  _$FourSquareLocationDtoCopyWith<_FourSquareLocationDto> get copyWith =>
      __$FourSquareLocationDtoCopyWithImpl<_FourSquareLocationDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FourSquareLocationDtoToJson(this);
  }
}

abstract class _FourSquareLocationDto implements FourSquareLocationDto {
  factory _FourSquareLocationDto(
      {String? address,
      String? crossStreet,
      double? lat,
      double? lng,
      List<Map<String, dynamic>>? labeledLatLngs,
      double? distance,
      String? postalCode,
      String? cc,
      String? city,
      String? state,
      String? country,
      List<String>? formattedAddress}) = _$_FourSquareLocationDto;

  factory _FourSquareLocationDto.fromJson(Map<String, dynamic> json) =
      _$_FourSquareLocationDto.fromJson;

  @override
  String? get address;
  @override
  String? get crossStreet;
  @override
  double? get lat;
  @override
  double? get lng;
  @override
  List<Map<String, dynamic>>? get labeledLatLngs;
  @override
  double? get distance;
  @override
  String? get postalCode;
  @override
  String? get cc;
  @override
  String? get city;
  @override
  String? get state;
  @override
  String? get country;
  @override
  List<String>? get formattedAddress;
  @override
  @JsonKey(ignore: true)
  _$FourSquareLocationDtoCopyWith<_FourSquareLocationDto> get copyWith =>
      throw _privateConstructorUsedError;
}
