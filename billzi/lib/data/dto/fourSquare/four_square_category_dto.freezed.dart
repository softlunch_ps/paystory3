// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'four_square_category_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FourSquareCategoryDto _$FourSquareCategoryDtoFromJson(
    Map<String, dynamic> json) {
  return _FourSquareCategoryDto.fromJson(json);
}

/// @nodoc
class _$FourSquareCategoryDtoTearOff {
  const _$FourSquareCategoryDtoTearOff();

  _FourSquareCategoryDto call(
      {String? id,
      String? name,
      String? pluralName,
      String? shortName,
      Map<String, dynamic>? icon,
      bool? primary}) {
    return _FourSquareCategoryDto(
      id: id,
      name: name,
      pluralName: pluralName,
      shortName: shortName,
      icon: icon,
      primary: primary,
    );
  }

  FourSquareCategoryDto fromJson(Map<String, Object?> json) {
    return FourSquareCategoryDto.fromJson(json);
  }
}

/// @nodoc
const $FourSquareCategoryDto = _$FourSquareCategoryDtoTearOff();

/// @nodoc
mixin _$FourSquareCategoryDto {
  String? get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get pluralName => throw _privateConstructorUsedError;
  String? get shortName => throw _privateConstructorUsedError;
  Map<String, dynamic>? get icon => throw _privateConstructorUsedError;
  bool? get primary => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FourSquareCategoryDtoCopyWith<FourSquareCategoryDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FourSquareCategoryDtoCopyWith<$Res> {
  factory $FourSquareCategoryDtoCopyWith(FourSquareCategoryDto value,
          $Res Function(FourSquareCategoryDto) then) =
      _$FourSquareCategoryDtoCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      String? name,
      String? pluralName,
      String? shortName,
      Map<String, dynamic>? icon,
      bool? primary});
}

/// @nodoc
class _$FourSquareCategoryDtoCopyWithImpl<$Res>
    implements $FourSquareCategoryDtoCopyWith<$Res> {
  _$FourSquareCategoryDtoCopyWithImpl(this._value, this._then);

  final FourSquareCategoryDto _value;
  // ignore: unused_field
  final $Res Function(FourSquareCategoryDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? pluralName = freezed,
    Object? shortName = freezed,
    Object? icon = freezed,
    Object? primary = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      pluralName: pluralName == freezed
          ? _value.pluralName
          : pluralName // ignore: cast_nullable_to_non_nullable
              as String?,
      shortName: shortName == freezed
          ? _value.shortName
          : shortName // ignore: cast_nullable_to_non_nullable
              as String?,
      icon: icon == freezed
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>?,
      primary: primary == freezed
          ? _value.primary
          : primary // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
abstract class _$FourSquareCategoryDtoCopyWith<$Res>
    implements $FourSquareCategoryDtoCopyWith<$Res> {
  factory _$FourSquareCategoryDtoCopyWith(_FourSquareCategoryDto value,
          $Res Function(_FourSquareCategoryDto) then) =
      __$FourSquareCategoryDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? id,
      String? name,
      String? pluralName,
      String? shortName,
      Map<String, dynamic>? icon,
      bool? primary});
}

/// @nodoc
class __$FourSquareCategoryDtoCopyWithImpl<$Res>
    extends _$FourSquareCategoryDtoCopyWithImpl<$Res>
    implements _$FourSquareCategoryDtoCopyWith<$Res> {
  __$FourSquareCategoryDtoCopyWithImpl(_FourSquareCategoryDto _value,
      $Res Function(_FourSquareCategoryDto) _then)
      : super(_value, (v) => _then(v as _FourSquareCategoryDto));

  @override
  _FourSquareCategoryDto get _value => super._value as _FourSquareCategoryDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? pluralName = freezed,
    Object? shortName = freezed,
    Object? icon = freezed,
    Object? primary = freezed,
  }) {
    return _then(_FourSquareCategoryDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      pluralName: pluralName == freezed
          ? _value.pluralName
          : pluralName // ignore: cast_nullable_to_non_nullable
              as String?,
      shortName: shortName == freezed
          ? _value.shortName
          : shortName // ignore: cast_nullable_to_non_nullable
              as String?,
      icon: icon == freezed
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>?,
      primary: primary == freezed
          ? _value.primary
          : primary // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FourSquareCategoryDto implements _FourSquareCategoryDto {
  _$_FourSquareCategoryDto(
      {this.id,
      this.name,
      this.pluralName,
      this.shortName,
      this.icon,
      this.primary});

  factory _$_FourSquareCategoryDto.fromJson(Map<String, dynamic> json) =>
      _$$_FourSquareCategoryDtoFromJson(json);

  @override
  final String? id;
  @override
  final String? name;
  @override
  final String? pluralName;
  @override
  final String? shortName;
  @override
  final Map<String, dynamic>? icon;
  @override
  final bool? primary;

  @override
  String toString() {
    return 'FourSquareCategoryDto(id: $id, name: $name, pluralName: $pluralName, shortName: $shortName, icon: $icon, primary: $primary)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _FourSquareCategoryDto &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.pluralName, pluralName) ||
                other.pluralName == pluralName) &&
            (identical(other.shortName, shortName) ||
                other.shortName == shortName) &&
            const DeepCollectionEquality().equals(other.icon, icon) &&
            (identical(other.primary, primary) || other.primary == primary));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, name, pluralName, shortName,
      const DeepCollectionEquality().hash(icon), primary);

  @JsonKey(ignore: true)
  @override
  _$FourSquareCategoryDtoCopyWith<_FourSquareCategoryDto> get copyWith =>
      __$FourSquareCategoryDtoCopyWithImpl<_FourSquareCategoryDto>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FourSquareCategoryDtoToJson(this);
  }
}

abstract class _FourSquareCategoryDto implements FourSquareCategoryDto {
  factory _FourSquareCategoryDto(
      {String? id,
      String? name,
      String? pluralName,
      String? shortName,
      Map<String, dynamic>? icon,
      bool? primary}) = _$_FourSquareCategoryDto;

  factory _FourSquareCategoryDto.fromJson(Map<String, dynamic> json) =
      _$_FourSquareCategoryDto.fromJson;

  @override
  String? get id;
  @override
  String? get name;
  @override
  String? get pluralName;
  @override
  String? get shortName;
  @override
  Map<String, dynamic>? get icon;
  @override
  bool? get primary;
  @override
  @JsonKey(ignore: true)
  _$FourSquareCategoryDtoCopyWith<_FourSquareCategoryDto> get copyWith =>
      throw _privateConstructorUsedError;
}
