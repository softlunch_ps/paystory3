import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/response/category/category_res_dto.dart';
import 'package:billzi/domain/repositories/category_repo.dart';
import 'package:billzi/domain/usecase/category_usecase.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/network/apis/common/api_result.dart';
import 'package:billzi/network/pay_story_http_client.dart';

class CategoryRepositoryImpl implements CategoryRepository {

  @override
  Future<ApiResult<CategoryResDto>> getCategoryList({required CategoryType categoryType}) async {
    try {
      var response = await PayStoryHttpClient.accountApi.getMyCategoryList(
        UserManager().getUserSeq()!,
        getCategoryTransactionTypeCode(categoryType),
        getCategoryTypeCode(categoryType),
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<CategoryItem>> postMyCategory({required CategoryItem categoryItem}) async {
    try {
      var response = await PayStoryHttpClient.accountApi.postMyCategory(
        UserManager().getUserSeq()!,
        categoryItem,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<CategoryItem>> putMyCategory({required CategoryItem categoryItem, required num accountCategorySeq}) async {
    try {
      var response = await PayStoryHttpClient.accountApi.putMyCategory(
        UserManager().getUserSeq()!,
        accountCategorySeq,
        categoryItem,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  String getCategoryTypeCode(CategoryType categoryType) {
    switch (categoryType) {

      case CategoryType.expense:
        return '0,1';
      case CategoryType.income:
        return '5,6';
    }
  }

  String getCategoryTransactionTypeCode(CategoryType categoryType) {
    switch (categoryType) {

      case CategoryType.expense:
        return 'C';
      case CategoryType.income:
        return 'I';
    }
  }
}