import 'dart:convert';

import 'package:get_storage/get_storage.dart';
import 'package:billzi/app/common/currency/currencies.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/testDto/test_item.dart';
import 'package:billzi/data/dto/testDto/test_payment_type.dart';
import 'package:billzi/data/dto/testDto/test_storage.dart';
import 'package:billzi/data/dto/testDto/test_transaction_type.dart';
import 'package:billzi/domain/repositories/easy_form_repo.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/network/apis/common/api_result.dart';
import 'package:billzi/network/pay_story_http_client.dart';
import 'package:billzi/utils/constants.dart';
import 'package:billzi/utils/test_data.dart';

class EasyFormRepositoryImpl implements EasyFormRepository {
  @override
  Future<ApiResult<TransactionDto>> saveEasyForm(TransactionDto transactionDto) async {
    ///local
    // final box = GetStorage();
    // List<TestItem>? testItemList;
    // String jsonString;
    // if (box.read(StorageKeyHistoryList) != null) {
    //   jsonString = box.read(StorageKeyHistoryList);
    //   TestStorage? testStorage = TestStorage.fromJson(json.decode(jsonString));
    //   testItemList = testStorage.testItemList;
    // } else {
    //   testItemList = [];
    // }
    //
    // testItemList.add(testItem);
    // TestStorage newTestStorage = TestStorage(testItemList: testItemList);
    //
    // box.write(StorageKeyHistoryList, json.encode(newTestStorage.toJson()));

    try {
      var response = await PayStoryHttpClient.transactionApi
          .postTransaction(UserManager().getUserSeq()!, transactionDto);
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  List<TestItem> getTestItemList() {
    final box = GetStorage();
    String jsonString;
    List<TestItem>? testItemList;
    if (box.read(StorageKeyHistoryList) != null) {
      jsonString = box.read(StorageKeyHistoryList);
      TestStorage? testStorage = TestStorage.fromJson(json.decode(jsonString));
      testItemList = testStorage.testItemList;
    } else {
      testItemList = [];
    }

    return testItemList;
  }

  @override
  Currency getCurrency() {
    final box = GetStorage();
    if (box.read(StorageKeyCurrencyString) != null) {
      return Currency.fromJson(
          json.decode(GetStorage().read(StorageKeyCurrencyString)));
    } else {
      //todo : set default currency
      return Currency.fromJson(currencies[0]);
    }
  }

  @override
  saveCurrency(Currency currency) {
    GetStorage().write(StorageKeyCurrencyString, json.encode(currency));
  }

  @override
  TestPaymentType getLastTestPaymentType() {
    final box = GetStorage();
    if (box.read(StorageKeyLastSelectedPaymentType) != null) {
      return TestPaymentType.fromJson(
          json.decode(box.read(StorageKeyLastSelectedPaymentType)));
    } else {
      return TestData.getTestPaymentTypeList()[0];
    }
  }

  @override
  TestTransactionType getLastTestTransactionType() {
    final box = GetStorage();
    if (box.read(StorageKeyLastSelectedTransactionType) != null) {
      return TestTransactionType.fromJson(
          json.decode(box.read(StorageKeyLastSelectedTransactionType)));
    } else {
      return TestData.getTestTransactionTypeList()[0];
    }
  }

  @override
  setLastTestPaymentType(TestPaymentType testPaymentType) {
    GetStorage().write(StorageKeyLastSelectedPaymentType,
        json.encode(testPaymentType.toJson()));
  }

  @override
  setLastTestTransactionType(TestTransactionType testTransactionType) {
    GetStorage().write(StorageKeyLastSelectedTransactionType,
        json.encode(testTransactionType.toJson()));
  }
}
