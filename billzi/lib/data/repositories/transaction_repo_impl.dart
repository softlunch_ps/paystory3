import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:billzi/data/dto/response/transaction/card_list_res_dto.dart';
import 'package:billzi/data/dto/response/transaction/payment_history_res_dto.dart';
import 'package:billzi/data/dto/response/transaction/transaction_list_res_dto.dart';
import 'package:billzi/domain/repositories/transaction_repo.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/network/apis/common/api_result.dart';
import 'package:billzi/network/pay_story_http_client.dart';
import 'package:billzi/utils/constants.dart';

class TransactionRepositoryImpl implements TransactionRepository {
  @override
  Future<ApiResult<TransactionListResDto>> getTransactionList(
      {required num accountBookSeq,
      int limit = timelinePageItemCount,
      required int offset}) async {
    try {
      var response = await PayStoryHttpClient.transactionApi.getTransactionList(
          UserManager().getUserSeq()!,
          accountBookSeq,
          limit,
          offset,
          '2021-12-01T05:25:39.280279Z',
          DateTime.now().toUtc().toIso8601String());

      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<PaymentHistoryResDto>> getNewTransactionList(
      {int limit = timelinePageItemCount, required int offset}) async {
    try {
      var response = await PayStoryHttpClient.accountApi.getPaymentHistories(
        UserManager().getUserSeq()!,
        // 900000000000000,
        '20000101',
        '20230405',
        offset,
        limit,
        null,
        null,
      );

      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<CardListResDto>> getCardList() async {
    try {
      var response = await PayStoryHttpClient.accountApi.getCardAccountList(
        UserManager().getUserSeq()!,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<CardAccountDto>> postAddCard(
      {required CardAccountDto cardAccountDto}) async {
    try {
      var response = await PayStoryHttpClient.accountApi.postCardAccount(
        UserManager().getUserSeq()!,
        cardAccountDto,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<PaymentHistoryDto>> postNewTransaction({required PaymentHistoryDto paymentHistoryDto}) async {
    try {
      var response = await PayStoryHttpClient.newTransactionApi.postPaymentHistory(
        UserManager().getUserSeq()!,
        paymentHistoryDto,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }
}
