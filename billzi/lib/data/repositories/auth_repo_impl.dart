import 'package:billzi/data/dto/model/auth_dto.dart';
import 'package:billzi/data/dto/model/user_dto.dart';
import 'package:billzi/data/dto/response/auth/sign_out_res_dto.dart';
import 'package:billzi/utils/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:billzi/data/dto/request/auth/auth_req_dto.dart';
import 'package:billzi/data/dto/request/auth/logout_req_dto.dart';
import 'package:billzi/data/dto/response/auth/auth_res_dto.dart';
import 'package:billzi/data/dto/response/common_result_res_dto.dart';
import 'package:billzi/domain/repositories/auth_repo.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/network/apis/common/api_result.dart';
import 'package:billzi/network/pay_story_http_client.dart';
import 'package:billzi/utils/crashlytics_helper.dart';
import 'package:billzi/utils/logger.dart';
import 'package:billzi/utils/utils.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class AuthRepositoryImpl implements AuthRepository {
  @override
  Future<User?> appleLogin() async {
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    final appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
      ],
      nonce: nonce,
    );

    final oauthCredential = OAuthProvider("apple.com").credential(
      idToken: appleCredential.identityToken,
      rawNonce: rawNonce,
    );

    try {
      final UserCredential userCredential =
          await FirebaseAuth.instance.signInWithCredential(oauthCredential);

      if (userCredential.user != null) {
        var result = await _postAuthResult(userCredential.user!.uid, authTypeCd: authTypeCodeApple);

        result.when(
          success: (response) {
            return userCredential.user;
          },
          failure: (error) {
            throw Exception('fail to server login');
          },
        );
      }
    } on FirebaseAuthException catch (e) {
      CrashlyticsReportKeySet? crashlyticsReportKeySet;
      if (e.code == 'too-many-requests' || e.code == 'operation-not-allowed') {
        crashlyticsReportKeySet =
            CrashlyticsReportKeySet('key_too_many_request', e.code);
      }

      await reportErrorByCrashlytics(
          exception: e,
          reason: e.code,
          withDebugLog: true,
          crashlyticsReportKeySet: crashlyticsReportKeySet);
    } catch (e) {
      logger.d(e);
      return null;
    }
  }

  @override
  Future<User?> facebookLogin() async {
    final LoginResult result = await FacebookAuth.instance.login(
      permissions: ['email'],
    );
    if (result.status == LoginStatus.success) {
      final OAuthCredential facebookAuthCredential =
          FacebookAuthProvider.credential(result.accessToken!.token);

      try {
        final UserCredential userCredential = await FirebaseAuth.instance
            .signInWithCredential(facebookAuthCredential);

        if (userCredential.user != null) {
          var result = await _postAuthResult(userCredential.user!.uid, authTypeCd: authTypeCodeFacebook);

          result.when(
            success: (response) {
              return userCredential.user;
            },
            failure: (error) {
              throw Exception('fail to server login');
            },
          );
        }

        return userCredential.user;
      } on FirebaseAuthException catch (e) {
        CrashlyticsReportKeySet? crashlyticsReportKeySet;
        if (e.code == 'too-many-requests' ||
            e.code == 'operation-not-allowed') {
          crashlyticsReportKeySet =
              CrashlyticsReportKeySet('key_too_many_request', e.code);
        }
        await reportErrorByCrashlytics(
          exception: e,
          reason: e.code,
          withDebugLog: true,
          crashlyticsReportKeySet: crashlyticsReportKeySet,
        );
      } catch (e) {
        logger.d(e);
        return null;
      }
    } else {
      return null;
    }
  }

  @override
  Future<User?> googleLogin() async {
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: [
        'email',
      ],
    );

    GoogleSignInAccount? googleSignInAccount = await _googleSignIn.signIn();
    final GoogleSignInAuthentication? googleAuth =
        await googleSignInAccount?.authentication;

    if (googleAuth == null) {
      return null;
    }
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    try {
      final UserCredential userCredential =
          await FirebaseAuth.instance.signInWithCredential(credential);

      if (userCredential.user != null) {
        var result = await _postAuthResult(userCredential.user!.uid, authTypeCd: authTypeCodeGoogle);

        result.when(
          success: (response) {
            return userCredential.user;
          },
          failure: (error) {
            throw Exception('fail to server login');
          },
        );
      }

      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      CrashlyticsReportKeySet? crashlyticsReportKeySet;
      if (e.code == 'too-many-requests' || e.code == 'operation-not-allowed') {
        crashlyticsReportKeySet =
            CrashlyticsReportKeySet('key_too_many_request', e.code);
      }
      await reportErrorByCrashlytics(
        exception: e,
        reason: e.code,
        withDebugLog: true,
        crashlyticsReportKeySet: crashlyticsReportKeySet,
      );
    } catch (e) {
      logger.d(e);
      return null;
    }
  }

  @override
  bool isLoggedIn() {
    User? user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<ApiResult<SignOutResDto>> signOut() async {
    try {
      if (UserManager().getUserSeq() != null) {
        var response = await PayStoryHttpClient.authApi
            .postLogout(LogoutReqDto(userSeq: UserManager().getUserSeq()!));
        await FirebaseAuth.instance.signOut();
        return ApiResult.success(data: response);
      } else {
        throw Exception('no use seq');
      }
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  Future<ApiResult<AuthDto>> _postAuthResult(String uid, {String authTypeCd = '3'}) async {
    await PayStoryHttpClient.memberApi.signUp(UserDto(prtyUserId: uid, authTypeCd: authTypeCd, userId: uid));
    try {
      var response = await PayStoryHttpClient.newAuthApi.postAuth(AuthDto(prtyUserId: uid, authTypeCd: authTypeCd));
      //set user seq
      UserManager().setUserSeq(response.userSeq);
      //set Token
      UserManager().setServerToken(response.token!);
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<bool> loginToPayStoryServer({bool shouldCheckSnsLogin = false, String authTypeCd = '3'}) async {
    bool isSuccessLogin = false;
    if (shouldCheckSnsLogin) {
      if (isLoggedIn()) {
          var result = await _postAuthResult(FirebaseAuth.instance.currentUser!.uid, authTypeCd: authTypeCd);
          result.when(
            success: (response) {
              logger.d('server login success');
              isSuccessLogin = true;
            },
            failure: (error) {
              logger.d('server login fail');
              isSuccessLogin = false;
            },
          );
      } else {
        //error case
        isSuccessLogin = false;
      }
    } else {
      var result = await _postAuthResult(FirebaseAuth.instance.currentUser!.uid, authTypeCd: authTypeCd);

      result.when(
        success: (response) {
          logger.d('server login success');
          isSuccessLogin = true;
        },
        failure: (error) {
          logger.d('server login fail');
          isSuccessLogin = false;
        },
      );
    }
    return isSuccessLogin;
  }
}
