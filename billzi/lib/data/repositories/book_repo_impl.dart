import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';
import 'package:billzi/data/dto/request/book/create_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/detach_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/edit_book_req_dto.dart';
import 'package:billzi/data/dto/request/book/reorder_book_list_req_dto.dart';
import 'package:billzi/data/dto/response/book/book_detail_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_list_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_subscription_res_dto.dart';
import 'package:billzi/data/dto/response/book/create_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/detach_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/edit_book_res_dto.dart';
import 'package:billzi/data/dto/response/book/reorder_book_res_dto.dart';
import 'package:billzi/domain/repositories/book_repo.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/network/apis/common/api_result.dart';
import 'package:billzi/network/pay_story_http_client.dart';

class BookRepositoryImpl implements BookRepository {
  @override
  Future<ApiResult<CreateBookResDto>> createBook(CreateBookReqDto book) async {
    try {
      var response = await PayStoryHttpClient.bookApi.createBook(
        UserManager().getUserSeq()!,
        book,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<BookListResDto>> getBookList(
      {bool isClosedOnly = false}) async {
    try {
      var response = await PayStoryHttpClient.bookApi.getBooks(
        UserManager().getUserSeq()!,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  Future<ApiResult<BookDetailResDto>> getBookDetail(num bookSeq) async {
    try {
      var response = await PayStoryHttpClient.bookApi.getBookDetail(
        UserManager().getUserSeq()!,
        bookSeq,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<ReorderBookResDto>> updateReorderedBookList(
      List<ReorderBooksReqDto> list) async {
    try {
      var response = await PayStoryHttpClient.bookApi.reorderBookList(
        UserManager().getUserSeq()!,
        list,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<EditBookResDto>> editBook(EditBookReqDto book) async {
    try {
      var response = await PayStoryHttpClient.bookApi.modifyBook(
        UserManager().getUserSeq()!,
        book,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<BookSubscriptionResDto>> joinBook(
      num accountBook, CreateBookMemberDto memberToJoin) async {
    try {
      var response = await PayStoryHttpClient.bookApi.joinBook(
        UserManager().getUserSeq()!,
        accountBook,
        memberToJoin,
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<ApiResult<DetachBookResDto>> detachBook(
      num targetUserSeq, num bookSeq) async {
    try {
      // if target user is master then it will delete the book
      var response = await PayStoryHttpClient.bookApi.detachBook(
        UserManager().getUserSeq()!,
        DetachBookReqDto(
          targetUserSeq: targetUserSeq,
          accountBookSeq: bookSeq,
        ),
      );
      return ApiResult.success(data: response);
    } catch (e) {
      return ApiResult.failure(error: transformToResponseDto(e));
    }
  }

  @override
  Future<BookResDto?> getSelectedBook() async {

    var result = await getBookList();
    BookResDto? selectedBook;
    result.when(
      success: (response) {
        selectedBook = response.accountBookList!.firstWhere((element) => !element.isClosed);
      },
      failure: (error) {
      },
    );
    return selectedBook;
  }
}
