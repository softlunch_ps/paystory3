import 'dart:convert';

import 'package:get_storage/get_storage.dart';
import 'package:billzi/data/dto/request/book/detach_book_req_dto.dart';
import 'package:billzi/data/dto/testDto/book_member_info_storage.dart';
import 'package:billzi/data/dto/testDto/test_book_member_info.dart';
import 'package:billzi/domain/repositories/book_member_repo.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/network/pay_story_http_client.dart';
import 'package:billzi/utils/constants.dart';

class BookMemberRepositoryImpl extends BookMemberRepository {
  BookMemberRepositoryImpl() {
    // TODO: remove me when server API is available
    var isEmpty = _loadUserInfoList().length == 0;
    if (isEmpty) {
      addMember(
        BookMemberInfo(
          id: 'papa@softlunch.co.kr',
          iconUnicode: '😆',
          nickName: 'papa',
          grade: MemberGrade.MEMBER,
        ),
      );
      addMember(
        BookMemberInfo(
          id: 'mama@softlunch.co.kr',
          iconUnicode: '🥰',
          nickName: 'mama',
          grade: MemberGrade.MEMBER,
        ),
      );
      addMember(
        BookMemberInfo(
          id: 'hq.han@softlunch.co.kr',
          iconUnicode: '🤓',
          nickName: 'hans',
          grade: MemberGrade.ADMIN,
        ),
      );
    }
  }
  @override
  List<BookMemberInfo> getMemberListByBookId({String? bookId}) {
    List<BookMemberInfo> userInfoList = _loadUserInfoList();
    return bookId == null
        ? userInfoList
        : userInfoList
            .where((element) => element.bookIds.contains(bookId))
            .toList();
  }

  @override
  bool updateMemberInfo(BookMemberInfo user) {
    List<BookMemberInfo> userInfoList = _loadUserInfoList();
    var indexToReplace = userInfoList.indexWhere((item) => item.id == user.id);
    var isFound = indexToReplace != -1;
    if (isFound) {
      userInfoList[indexToReplace] = user;
      _saveList(userInfoList);
    }
    return isFound;
  }

  @override
  addMember(BookMemberInfo user) async {
    List<BookMemberInfo> userInfoList = _loadUserInfoList();
    userInfoList.add(user);
    _saveList(userInfoList);
  }

  @override
  Future<bool> removeMember(num memberSeq, num bookSeq) async {
    var response = await PayStoryHttpClient.bookApi.detachBook(
      UserManager().getUserSeq()!,
      DetachBookReqDto(
        targetUserSeq: memberSeq,
        accountBookSeq: bookSeq,
      ),
    );

    return response.resultVO.isSuccess;

    // List<BookMemberInfo> userInfoList = _loadUserInfoList();
    // var previousLength = userInfoList.length;
    // userInfoList.removeWhere((element) => element.id == memberId);
    // var isRemoved = previousLength != userInfoList.length;
    // _saveList(userInfoList);
    // return isRemoved;
  }

  _saveList(List<BookMemberInfo> userList) {
    final localBox = GetStorage();
    BookMemberStorage newTestStorage =
        BookMemberStorage(userInfoList: userList);
    localBox.write(
        StorageKeyUserRightInfoList, json.encode(newTestStorage.toJson()));
  }

  List<BookMemberInfo> _loadUserInfoList() {
    final localBox = GetStorage();
    var userInfoList = <BookMemberInfo>[];
    if (localBox.hasData(StorageKeyUserRightInfoList)) {
      String jsonString =
          localBox.read<String>(StorageKeyUserRightInfoList) ?? '';
      BookMemberStorage? userInfo =
          BookMemberStorage.fromJson(json.decode(jsonString));
      userInfoList = userInfo.userInfoList;
    }
    return userInfoList;
  }
}
