import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/timeline/timeline_controller.dart';
import 'package:billzi/app/timeline/timeline_list.dart';
import 'package:billzi/route/route_name.dart';

class TimelinePage extends StatelessWidget {
  const TimelinePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TimelineController timelineController = Get.find<TimelineController>();

    return Scaffold(
        body: NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            // backwardsCompatibility: false,
            // systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.orange),
            expandedHeight: 200.0,
            floating: false,
            pinned: true,
            title: Text('test'),
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text("Collapsing Toolbar",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                  )),
              background: Image.network(
                "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverPersistentHeader(
            delegate: _TimelineAppbarDelegate(Colors.blue, 'test1'),
            pinned: true,
          ),
        ];
      },
      body: GetBuilder(
        init: timelineController,
        builder: (controller) {
          return TimelineList(
            groupList: timelineController.transactionGroupList,
            onHistoryClickListener: (PaymentHistoryDto paymentHistoryDto) {
              Get.toNamed(Routes.EditPostPage);
            },
            onLoadMoreListener: timelineController.loadTransactionGroupList,
            onRefresh: timelineController.refreshTransactionList,
          );
        },
      ),
    ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Get.toNamed(Routes.AddTypePage);
        },
      ),
    );
  }

//Todo: test
// List<TestItem> _getTimeLineItemFromLocal() {
//   EasyFormRepositoryImpl easyFormRepositoryImpl = EasyFormRepositoryImpl();
//   return easyFormRepositoryImpl.getTestItemList();
// }
}

class _TimelineAppbarDelegate extends SliverPersistentHeaderDelegate {
  final Color backgroundColor;
  final String _title;

  _TimelineAppbarDelegate(this.backgroundColor, this._title);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.TimelineCalendar);
      },
      child: Container(
        color: backgroundColor,
        child: Center(
          child: Text(
            _title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
            ),
          ),
        ),
      ),
    );
  }

  @override
  double get maxExtent => 80;

  @override
  double get minExtent => 50;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
