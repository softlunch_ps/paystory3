import 'package:flutter/material.dart';
import 'package:billzi/utils/app_colors.dart';

class TimelineHeader extends StatelessWidget {

  final String date;
  final String incomeTotal;
  final String expenditureTotal;

  const TimelineHeader({Key? key, required this.date, required this.incomeTotal, required this.expenditureTotal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(width: 1.0, color: AppColors.dividerGray1),
          bottom: BorderSide(width: 1.0, color: AppColors.dividerGray1),
        ),
        color: AppColors.backgroundWhite2,
      ),
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(date,
            style: TextStyle(color: AppColors.textBlack1),
          ),
          Row(
            children: [
              Text('수입 $incomeTotal',
                style: TextStyle(color: AppColors.textBlack1),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 10, right: 10, left: 10),
                width: 1,
                height: 20,
                color: Colors.grey,
              ),
              Text('지출 $expenditureTotal',
                style: TextStyle(color: AppColors.textBlack1),
              ),
            ],
          )
        ],
      ),
    );
  }
}
