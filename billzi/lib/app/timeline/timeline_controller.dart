import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:get/get.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/model/transaction_group.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/repositories/book_repo_impl.dart';
import 'package:billzi/data/repositories/transaction_repo_impl.dart';
import 'package:billzi/domain/usecase/book_usecase.dart';
import 'package:billzi/domain/usecase/transaction_usecase.dart';
import 'package:billzi/utils/extensions.dart';
import 'package:billzi/utils/logger.dart';

class TimelineController extends GetxController {
  RxList<PaymentHistoryDto> transactionList = RxList();
  RxList<TransactionItemGroup> transactionGroupList = RxList();
  int itemOffset = 0;

  @override
  void onInit() {
    super.onInit();
    refreshTransactionList();
  }

  Future<bool> loadTransactionGroupList({int? offset}) async {
    if (offset == null) {
      offset = itemOffset;
    }

    TransactionUseCase transactionUseCase =
        TransactionUseCase(TransactionRepositoryImpl());
    var result = await transactionUseCase.getNewTransactionList(offset: offset);
    bool hasNext = false;
    result.when(
      success: (paymentHistoryDto) {
        if (paymentHistoryDto.paymentHistoryList!.isNotEmpty) {
          TransactionItemGroup? tempGroup;

          for (var item in paymentHistoryDto.paymentHistoryList!) {
            if (tempGroup == null) {

              if (transactionGroupList.isEmpty) {
                transactionGroupList.add(TransactionItemGroup(
                    item.createDateTime!, RxList.of([item])));
              } else {
                if (transactionGroupList.last.dateTime
                    .isSameDate(item.createDateTime!)) {
                  transactionGroupList.last.groupList.add(item);
                } else {
                  tempGroup = TransactionItemGroup(
                      item.createDateTime!, RxList.of([item]));
                }
              }
            } else {
              if (tempGroup.dateTime.isSameDate(item.createDateTime!)) {
                tempGroup.groupList.add(item);
              } else {
                transactionGroupList.add(tempGroup);
                tempGroup = TransactionItemGroup(
                    item.createDateTime!, RxList.of([item]));
              }
            }
          }

          if (tempGroup != null) {
            if (!tempGroup.dateTime
                .isSameDate(transactionGroupList.last.dateTime)) {
              transactionGroupList.add(tempGroup);
            }
          }
        }
        update();
        itemOffset = paymentHistoryDto.pagingVO!.nextOffset;
        if (paymentHistoryDto.pagingVO!.totalCount > 0) {
          if (paymentHistoryDto.pagingVO!.limit >
              paymentHistoryDto.pagingVO!.listSize) {
            hasNext = false;
          } else {
            hasNext = true;
          }
        } else {
          hasNext = false;
        }
      },
      failure: (error) {
        logger.d('fail to load transaction list');
        hasNext = false;
      },
    );
    return hasNext;
  }

  Future<bool> refreshTransactionList() async {
    itemOffset = 0;
    transactionList.clear();
    transactionGroupList.clear();
    bool hasNextOffset = await loadTransactionGroupList(offset: itemOffset);

    return hasNextOffset;
  }
}
