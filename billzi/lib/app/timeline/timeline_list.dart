import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/timeline/timeline_header.dart';
import 'package:billzi/app/timeline/timeline_item.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/model/transaction_group.dart';
import 'package:billzi/utils/currency_helper.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';

typedef void OnHistoryClickListener(PaymentHistoryDto paymentHistoryDto);
typedef OnLoadMoreListener = Future<bool> Function();
typedef OnRefresh = Future<bool> Function();

class TimelineList extends StatefulWidget {
  final List<TransactionItemGroup> groupList;
  final bool shrinkWrap;
  final OnHistoryClickListener? onHistoryClickListener;
  final OnLoadMoreListener onLoadMoreListener;
  final OnRefresh? onRefresh;

  const TimelineList(
      {Key? key,
      required this.groupList,
      this.shrinkWrap = false,
      this.onHistoryClickListener,
      required this.onLoadMoreListener,
      this.onRefresh})
      : super(key: key);

  @override
  State<TimelineList> createState() => _TimelineListState();
}

class _TimelineListState extends State<TimelineList> {
  late RefreshController _refreshController;
  bool _hasMore = true;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SmartRefresher(
        onRefresh: () async {
          if (widget.onRefresh != null) {
            _hasMore = await widget.onRefresh!.call();
          }
          _refreshController.refreshCompleted();
        },
        onLoading: () async {
          _hasMore = await widget.onLoadMoreListener.call();
          _refreshController.loadComplete();
        },
        header: ClassicHeader(),
        footer: ClassicFooter(),
        enablePullDown: true,
        enablePullUp: _hasMore,
        controller: _refreshController,
        child: ListView.builder(
            shrinkWrap: widget.shrinkWrap,
            physics: widget.shrinkWrap
                ? NeverScrollableScrollPhysics()
                : AlwaysScrollableScrollPhysics(),
            itemCount: widget.groupList.length,
            itemBuilder: (context, index) {
              return StickyHeader(
                header: TimelineHeader(
                  date: DateFormat('MM월 d일')
                      .format(widget.groupList[index].dateTime),
                  incomeTotal: getMoneyStringWithCurrency(
                      money: widget.groupList[index].getMoneySum('I')),
                  expenditureTotal: getMoneyStringWithCurrency(
                      money: widget.groupList[index].getMoneySum('E')),
                ),
                content: Container(
                    child: ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: widget.groupList[index].groupList.length,
                        itemBuilder: (context, childIndex) {
                          return TimelineItem(
                            index: childIndex,
                            item: widget.groupList[index].groupList[childIndex],
                            onTimelineItemClickListener: (int childClickIndex) {
                              widget.onHistoryClickListener?.call(widget
                                  .groupList[index].groupList[childClickIndex]);
                            },
                          );
                        })),
              );
            }),
      ),
    );
  }
}
