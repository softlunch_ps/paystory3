import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/common/tag_text.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/utils/currency_helper.dart';
import 'package:billzi/utils/test_data.dart';
import 'package:billzi/utils/utils.dart';

typedef void OnTimelineItemClickListener(int index);

class TimelineItem extends StatelessWidget {

  final int index;
  final PaymentHistoryDto item;
  final OnTimelineItemClickListener onTimelineItemClickListener;

  const TimelineItem(
      {Key? key, required this.item, required this.onTimelineItemClickListener, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10, left: 10),
      child: Card(
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.only(right: 10, left: 10),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Container(color: Colors.blue,),
                        //child: Icon(TestData.getIconByName(item.category.iconName))
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Container(
                                    // child: Text(item.category?.categoryName ?? '')),
                                    child: Text(item.acntCateInfo?.accountCategoryName ?? '카테고리 없음')),
                              ),
                              Text(getMoneyStringWithCurrency(
                                  money: item.totalMoney, currency: findCurrencyByCode(item.currencyCode!))),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            child: TagText(text: item.description ?? ''),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(DateFormat('kk:mm').format(item.createDateTime!.toLocal())),
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(right: 5, left: 5),
                                  child: Text(getTransactionTypeNameById(item.transactionTypeId)),
                                ),
                              ),
                              Text(getPaymentTypeNameById(item.paymentTypeId)),
                            ],
                          )


                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          onTap: () =>
              onTimelineItemClickListener.call(index)
          ,
        ),
      ),
    );
  }
}
