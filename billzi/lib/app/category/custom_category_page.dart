import 'package:billzi/app/category/custom_category_controller.dart';
import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:billzi/utils/pick_helper.dart';
import 'package:get/get.dart';

class CustomCategoryPage extends StatefulWidget {
  const CustomCategoryPage({Key? key}) : super(key: key);

  @override
  State<CustomCategoryPage> createState() => _CustomCategoryPageState();
}

class _CustomCategoryPageState extends State<CustomCategoryPage> {
  final CustomCategoryController _customCategoryController =
      Get.find<CustomCategoryController>();


  @override
  void initState() {
    super.initState();
    _customCategoryController.setArguments();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Obx(() => Text(
              _customCategoryController.isEditMode.value ? '카테고리 수정' : '나만의 카테고리 만들기')),
          centerTitle: true,
        ),
        body: Container(
          margin: const EdgeInsets.only(right: 30, left: 30),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 20),
                child: TextField(
                  controller: _customCategoryController.textEditingController,
                  maxLength: 20,
                  decoration:
                      const InputDecoration(hintText: '카테고리 이름을 입력해주세요'),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                child: Row(
                  children: [
                    const Text('아이콘'),
                    Obx(() => ElevatedButton(
                          onPressed: () {
                            onClickIcon(
                                context: context,
                                selectedIconColor: _customCategoryController
                                    .selectedIconColor.value,
                                iconNameList: _customCategoryController
                                    .getCategoryIconList(),
                                onSelectCustomIcon: (String result) {
                                  _customCategoryController
                                      .setSelectedIconName(result);
                                  onClickColor(
                                      context: context,
                                      selectedIconName:
                                          _customCategoryController
                                              .selectedIconName.value,
                                      iconColorList: _customCategoryController
                                          .getCategoryColorList(),
                                      onSelectCustomColor: (Color color) {
                                        _customCategoryController
                                            .setSelectedIconColor(color);
                                      });
                                });
                          },
                          child: SizedBox(
                            width: 60,
                            height: 60,
                            child: Icon(
                              getCategoryIcon(_customCategoryController
                                  .selectedIconName.value),
                              color: Colors.white,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                              shape: const CircleBorder(),
                              primary: _customCategoryController
                                  .selectedIconColor.value),
                        )),
                    const Text('색상'),
                    Obx(() => ElevatedButton(
                          onPressed: () {
                            onClickColor(
                                context: context,
                                selectedIconName: _customCategoryController
                                    .selectedIconName.value,
                                iconColorList: _customCategoryController
                                    .getCategoryColorList(),
                                onSelectCustomColor: (Color color) {
                                  _customCategoryController
                                      .setSelectedIconColor(color);
                                });

                            if (_customCategoryController
                                    .selectedBaseCategory ==
                                null) {
                              onClickCategory(
                                  context: context,
                                  baseCategoryList: _customCategoryController
                                      .baseCategoryList,
                                  onSelectBaseCategory:
                                      (CategoryItem baseCategory) {
                                    _customCategoryController
                                        .setSelectedBaseCategory(baseCategory);
                                  });
                            }
                          },
                          child: SizedBox(
                            width: 60,
                            height: 60,
                            child: Container(),
                          ),
                          style: ElevatedButton.styleFrom(
                              shape: const CircleBorder(),
                              primary: _customCategoryController
                                  .selectedIconColor.value),
                        )),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  onClickCategory(
                      context: context,
                      baseCategoryList:
                          _customCategoryController.baseCategoryList,
                      onSelectBaseCategory: (CategoryItem baseCategory) {
                        _customCategoryController
                            .setSelectedBaseCategory(baseCategory);
                      });
                },
                child: Container(
                  height: 50,
                  width: double.infinity,
                  child: Row(
                    children: [
                      const Text('연관 카테고리 > '),
                      Obx(() => Text(
                            _customCategoryController
                                .selectedBaseCategoryName.value,
                            style: TextStyle(color: Colors.blue),
                          ))
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                height: 50,
                width: double.infinity,
                child: Obx(() => ElevatedButton(
                    onPressed: () {
                      if (_customCategoryController.isEditMode.value) {
                        _customCategoryController.editCustomCategory();
                      } else {
                        _customCategoryController.addCustomCategory();
                      }

                    },
                    child: Text(_customCategoryController.isEditMode.value ? '카테고리 수정' : '카테고리 생성'),
                    style: ElevatedButton.styleFrom(
                        primary: _customCategoryController.canMakeCategory.value
                            ? Colors.blue
                            : Colors.grey))),
              )
            ],
          ),
        ));
  }
}
