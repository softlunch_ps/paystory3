import 'package:billzi/app/add/addCategory/add_category_controller.dart';
import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/model/custom_category_dto.dart';
import 'package:billzi/data/repositories/category_repo_impl.dart';
import 'package:billzi/domain/usecase/category_usecase.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/utils/logger.dart';
import 'package:billzi/utils/extensions.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomCategoryController extends GetxController {
  RxBool isEditMode = false.obs;
  CategoryItem? originCategoryItem;
  RxString selectedIconName = 'call'.obs;
  Rx<Color> selectedIconColor = Colors.blue.obs;
  CategoryItem? selectedBaseCategory;
  List<CategoryItem> baseCategoryList = [];
  RxString selectedBaseCategoryName = '카테고리를 선택해주세요'.obs;
  TextEditingController textEditingController = TextEditingController();
  RxBool canMakeCategory = false.obs;

  @override
  void onInit() {
    super.onInit();
    textEditingController.addListener(() {
      _checkCanMakeCategory();
    });
  }

  void setArguments() {
    if (Get.arguments != null) {
      CustomCategoryDto categoryDto = Get.arguments as CustomCategoryDto;
      baseCategoryList.addAll(categoryDto.baseCategoryList);

      if (categoryDto.isEditMode) {
        isEditMode.value = categoryDto.isEditMode;
        originCategoryItem = categoryDto.selectedCategory;
        _initEditMode();
      }
    }
  }

  void _initEditMode() {
    textEditingController.text = originCategoryItem!.accountCategoryName!;
    Color inputColor = originCategoryItem!.color!.toColor();
    selectedIconColor = inputColor.obs;
    selectedIconName.value = originCategoryItem!.imgURL!;
    for (var element in baseCategoryList) {
      if (element.accountCategorySeq ==
          originCategoryItem!.supAccountCategorySeq!) {
        selectedBaseCategory = element;
        selectedBaseCategoryName.value = element.accountCategoryName!;
        break;
      }
    }
    canMakeCategory.value = true;
  }

  Future addCustomCategory() async {
    CategoryUseCase categoryUseCase = CategoryUseCase(CategoryRepositoryImpl());
    var result = await categoryUseCase.postMyCategory(
        categoryItem: CategoryItem(
      userSeq: UserManager().getUserSeq()!,
      accountCategoryName: textEditingController.text,
      imgURL: selectedIconName.value,
      color: selectedIconColor.value.toHexRGB(leadingHashSign: false),
      typeCd: selectedBaseCategory!.typeCd == '0' ? '1' : '6',
      supAccountCategorySeq: selectedBaseCategory!.accountCategorySeq,
    ));
    result.when(
      success: (categoryDto) {
        Get.find<AddCategoryController>().addCustomCategory(categoryDto);
        Get.back();
      },
      failure: (error) {
        logger.d('fail to add my category');
      },
    );
  }

  Future editCustomCategory() async {
    CategoryUseCase categoryUseCase = CategoryUseCase(CategoryRepositoryImpl());
    var result = await categoryUseCase.putMyCategory(
      accountCategorySeq: originCategoryItem!.accountCategorySeq!,
      categoryItem: CategoryItem(
        accountCategorySeq: originCategoryItem!.accountCategorySeq!,
        userSeq: UserManager().getUserSeq()!,
        accountCategoryName: textEditingController.text,
        imgURL: selectedIconName.value,
        color: selectedIconColor.value.toHexRGB(leadingHashSign: false),
        typeCd: selectedBaseCategory!.typeCd == '0' ? '1' : '6',
        supAccountCategorySeq: selectedBaseCategory!.accountCategorySeq,
      ),
    );
    result.when(
      success: (categoryDto) {
        CategoryType categoryType = selectedBaseCategory!.typeCd == '0'
            ? CategoryType.expense
            : CategoryType.income;
        Get.find<AddCategoryController>().refreshCategory(categoryType);
        Get.back();
      },
      failure: (error) {
        logger.d('fail to add my category');
      },
    );
  }

  setSelectedIconName(String iconName) {
    selectedIconName.value = iconName;
    update();
  }

  setSelectedIconColor(Color iconColor) {
    selectedIconColor.value = iconColor;
    update();
  }

  setSelectedBaseCategory(CategoryItem baseCategory) {
    selectedBaseCategory = baseCategory;
    selectedBaseCategoryName.value = baseCategory.accountCategoryName!;
    _checkCanMakeCategory();
    update();
  }

  _checkCanMakeCategory() {
    if (textEditingController.text.isNotEmpty && selectedBaseCategory != null) {
      canMakeCategory.value = true;
    } else {
      canMakeCategory.value = false;
    }
  }

  List<String> getCategoryIconList() {
    return [
      'call',
      'info_outline',
      'padding',
      'shop',
      'widgets_sharp',
      'pets_sharp',
      'airplanemode_active_sharp',
      'android',
      'ac_unit',
      'local_hospital_sharp',
      'android_sharp',
      'school_sharp',
      'directions_car_sharp',
      'wifi',
      'restaurant',
      'local_cafe',
      'account_balance_wallet_sharp',
      'account_box_outlined',
      'volunteer_activism',
      'whatshot',
      'alternate_email',
      'vpn_key',
      'watch_later_sharp',
      'wine_bar_rounded',
    ];
  }

  List<Color> getCategoryColorList() {
    return [
      Colors.red,
      Colors.orange,
      Colors.deepOrange,
      Colors.yellow,
      Colors.amber,
      Colors.lightGreen,
      Colors.lime,
      Colors.green,
      Colors.cyan,
      Colors.blue,
      Colors.blueAccent,
      Colors.teal,
      Colors.cyanAccent,
      Colors.indigo,
      Colors.indigoAccent,
      Colors.lightBlue,
      Colors.blueGrey,
      Colors.grey,
      Colors.purple,
      Colors.pink,
      Colors.brown,
      Colors.deepPurple,
      Colors.deepPurpleAccent,
      Colors.yellowAccent,
    ];
  }
}
