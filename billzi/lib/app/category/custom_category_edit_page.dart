import 'package:billzi/app/add/addCategory/add_category_controller.dart';
import 'package:billzi/app/category/custom_category_controller.dart';
import 'package:billzi/app/common/category_grid_view.dart';
import 'package:billzi/data/dto/model/custom_category_dto.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomCategoryEditPage extends StatelessWidget {
  CustomCategoryEditPage({Key? key}) : super(key: key);

  final CustomCategoryController _customCategoryController =
      Get.find<CustomCategoryController>();
  final AddCategoryController _addCategoryController =
      Get.find<AddCategoryController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('카테고리 편집'),
        centerTitle: true,
        // actions: <Widget>[
        //   TextButton(
        //     style: TextButton.styleFrom(
        //       primary: AppColors.textWhite1,
        //     ),
        //     onPressed: () {
        //
        //     },
        //     child: Text('완료'),
        //   )
        // ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              // Container(
              //     margin: const EdgeInsets.only(top: 20, bottom: 20),
              //     child: const Text(
              //       '카테고리를 선택해 주세요.',
              //       style: TextStyle(fontSize: 25, color: Colors.grey),
              //     )),
              Container(
                  margin: const EdgeInsets.only(
                      top: 15, bottom: 10, left: 20, right: 20),
                  alignment: Alignment.centerLeft,
                  child: const Text('내가 만든 카테고리')),
              Obx(
                () => CategoryGridView(
                  onCategoryTab: (int index) {
                    if (index == 0) {
                      //카테고리추가
                      Get.toNamed(Routes.CustomCategoryPage,
                          arguments: CustomCategoryDto(
                              baseCategoryList:
                                  _addCategoryController.categoryList.value));
                    } else {
                      Get.toNamed(Routes.CustomCategoryPage,
                          arguments: CustomCategoryDto(
                              baseCategoryList:
                                  _addCategoryController.categoryList.value,
                              isEditMode: true,
                              selectedCategory: _addCategoryController
                                  .customCategoryList[index]));
                    }
                  },
                  categoryList: _addCategoryController.customCategoryList.value,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
