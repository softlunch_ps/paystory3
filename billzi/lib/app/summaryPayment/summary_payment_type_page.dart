import 'package:billzi/app/common/month_selector.dart';
import 'package:flutter/material.dart';
import 'package:billzi/app/common/guide_tooltip.dart';
import 'package:billzi/app/summaryPayment/line_chart_sample_1.dart';
import 'package:billzi/app/summaryPayment/line_chart_sample_2.dart';
import 'package:billzi/app/timeline/timeline_list.dart';
import 'package:billzi/utils/currency_helper.dart';
import 'package:billzi/utils/test_data.dart';

class SummaryPaymentTypePage extends StatefulWidget {
  const SummaryPaymentTypePage({Key? key}) : super(key: key);

  @override
  _SummaryPaymentTypePageState createState() => _SummaryPaymentTypePageState();
}

class _SummaryPaymentTypePageState extends State<SummaryPaymentTypePage> {
  final MonthSelectorController monthSelectorController =
      MonthSelectorController(
          monthDateRangeText: '(8.1 - 8.31)', monthDateText: '2021년 8월');

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        MonthSelector(monthSelectorController: monthSelectorController),
        Container(
          padding: EdgeInsets.only(right: 20, left: 20, top: 10, bottom: 10),
          color: Colors.grey,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text('결제수단'),
              ),
              Container(
                width: 100,
                child: Text('비율', textAlign: TextAlign.center),
              ),
              Container(
                width: 100,
                child: Text('지출금액', textAlign: TextAlign.center),
              )
            ],
          ),
        ),
        ListView.builder(
            shrinkWrap: true,
            itemCount: TestData.getTestPayments().length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: EdgeInsets.only(right: 20, left: 20, top: 5, bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        child: Text(TestData.getTestPayments()[index].name)),
                    Container(
                      width: 100,
                      child: Text(
                        TestData.getTestPayments()[index].totalMoneyPercent(
                            TestData.getPaymentsTotalMoney()),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: 100,
                      child: Column(
                        children: [
                          Text('총 ${TestData.getTestPayments()[index].count}건'),
                          Text(getKRWCurrencyText(
                              TestData.getTestPayments()[index].totalMoney)),
                        ],
                        crossAxisAlignment: CrossAxisAlignment.center,
                      ),
                    )
                  ],
                ),
              );
            }),
        Container(
          margin: EdgeInsets.only(top: 10, bottom: 5, left: 20, right: 20),
          child: Row(
            children: [
              Text('일별추이'),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10),
                child: GuideTooltip(
                  height: 80,
                    message: '매월 시작일을 바꾸신 경우 바꾼 시작일을 기준으로 월 지출 금액이 집계 됩니다.',
                    child: Icon(Icons.info_outline),

                ),
              )
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 5),
            child: LineChartSample1()),
        Container(
            margin: EdgeInsets.only(top: 10, bottom: 5, left: 20, right: 20),
            child: Text('일별추이')),
        Container(
            margin: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 5),
            child: LineChartSample2()),
        // TimelineList(
        //   shrinkWrap: true,
        //   groupList: TestData.warpListByDate(TestData.getTestCategoryItems()),
        // )
      ],
    ));
  }
}
