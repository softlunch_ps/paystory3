import 'package:billzi/app/common/month_selector.dart';
import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/timeline/timeline_item.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/testDto/test_item.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/currency_helper.dart';
import 'package:billzi/utils/test_data.dart';
import 'package:table_calendar/table_calendar.dart';

class TimelineCalendar extends StatefulWidget {
  const TimelineCalendar({Key? key}) : super(key: key);

  @override
  _TimelineCalendarState createState() => _TimelineCalendarState();
}

class _TimelineCalendarState extends State<TimelineCalendar> {
  MonthSelectorController monthSelectorController = MonthSelectorController(
      monthDateRangeText: 'counter: 0', monthDateText: '');

  late final PageController _pageController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('달력'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    children: [
                      Text('월 수입'),
                      Text('${getKRWCurrencyText(500000)}'),
                    ],
                  ),
                ),
                VerticalDivider(),
                Container(
                  child: Column(
                    children: [
                      Text('월 지출'),
                      Text('${getKRWCurrencyText(400000)}'),
                    ],
                  ),
                ),
                VerticalDivider(),
                Container(
                  child: Column(
                    children: [
                      Text('합계'),
                      Text('+${getKRWCurrencyText(100000)}'),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
              child: MonthSelector(
                  monthSelectorController: monthSelectorController)),
          TableCalendar<Event>(
            firstDay: kFirstDay,
            lastDay: kLastDay,
            focusedDay: _focusedDay,
            selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
            rangeStartDay: _rangeStart,
            rangeEndDay: _rangeEnd,
            calendarFormat: _calendarFormat,
            rangeSelectionMode: _rangeSelectionMode,
            eventLoader: _getEventsForDay,
            startingDayOfWeek: StartingDayOfWeek.sunday,
            calendarBuilders: CalendarBuilders<Event>(
              markerBuilder:
                  (BuildContext context, DateTime day, List<Event> events) {
                return events.length > 0
                    ? Container(
                        margin: EdgeInsets.only(bottom: 5, left: 4, right: 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '50000',
                              style: TextStyle(fontSize: 8),
                            ),
                            Text('100000', style: TextStyle(fontSize: 8))
                          ],
                        ))
                    : Container();
              },
            ),
            calendarStyle: CalendarStyle(
                // Use `CalendarStyle` to customize the UI
                outsideDaysVisible: true,
                cellMargin: EdgeInsets.only(bottom: 0),
                weekendDecoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                      right: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                    shape: BoxShape.rectangle),
                disabledDecoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                      right: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                    shape: BoxShape.rectangle),
                todayDecoration: BoxDecoration(
                    color: Color(0xFF9FA8DA),
                    border: Border(
                      left: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                      right: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                    shape: BoxShape.rectangle),
                defaultDecoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                      right: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                    shape: BoxShape.rectangle),
                holidayDecoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                      right: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                    shape: BoxShape.rectangle),
                outsideDecoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                      right: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                    shape: BoxShape.rectangle),
                selectedDecoration: BoxDecoration(
                  color: AppColors.primary,
                  //shape: BoxShape.circle,
                ),
                rowDecoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.grey,
                      width: 0.5,
                    ),
                    top: BorderSide(
                      color: Colors.grey,
                      width: 0.5,
                    ),
                  ),
                )),
            onDaySelected: _onDaySelected,
            onRangeSelected: _onRangeSelected,
            onCalendarCreated: (controller) => _pageController = controller,
            headerVisible: false,
            onFormatChanged: (format) {
              if (_calendarFormat != format) {
                setState(() {
                  _calendarFormat = format;
                });
              }
            },
            onPageChanged: (focusedDay) {
              _focusedDay = focusedDay;
              setState(() {
                monthSelectorController
                    .setMonthDateText(DateFormat.yMMM().format(_focusedDay));
              });
            },
          ),
          Container(
              margin: const EdgeInsets.only(top: 15, bottom: 5, left: 20, right: 20),
              child: Text('2021년 09월 13일', style: TextStyle(fontSize: 16),)),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text(
                      '총 1건 수입',
                      style: TextStyle(fontSize: 12),
                    ),
                    Text('500,000'),
                  ],
                ),
                const SizedBox(width: 20),
                Column(
                  children: [
                    Text(
                      '총 5건 지출',
                      style: TextStyle(fontSize: 12),
                    ),
                    Text('383,000'),
                  ],
                )
              ],
            ),
          ),
          const SizedBox(height: 8.0),
          Expanded(
            child: ValueListenableBuilder<List<Event>>(
              valueListenable: _selectedEvents,
              builder: (context, value, _) {
                return ListView.builder(
                  itemCount: value.length,
                  itemBuilder: (context, index) {
                    return TimelineItem(
                      item:
                      PaymentHistoryDto(
                          transactionTypeId: 'I',
                          paymentTypeId: '01',
                          //category: TestData.getTestCategories()[2],
                          totalMoney: 10000,
                          trstnDt: '2021-09-13T05:35:05.805Z',
                          currencyCode: 'USD'),
                      index: index,
                      onTimelineItemClickListener: (int index) {},
                    );

                    // return Container(
                    //   margin: const EdgeInsets.symmetric(
                    //     horizontal: 12.0,
                    //     vertical: 4.0,
                    //   ),
                    //   decoration: BoxDecoration(
                    //     border: Border.all(),
                    //     borderRadius: BorderRadius.circular(12.0),
                    //   ),
                    //   child: ListTile(
                    //     onTap: () => print('${value[index]}'),
                    //     title: Text('${value[index]}'),
                    //   ),
                    // );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  late final ValueNotifier<List<Event>> _selectedEvents;
  CalendarFormat _calendarFormat = CalendarFormat.month;
  RangeSelectionMode _rangeSelectionMode = RangeSelectionMode
      .disabled; // Can be toggled on/off by longpressing a date
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  DateTime? _rangeStart;
  DateTime? _rangeEnd;

  @override
  void initState() {
    super.initState();

    _selectedDay = _focusedDay;
    _selectedEvents = ValueNotifier(_getEventsForDay(_selectedDay!));

    monthSelectorController.addNextClickListener(() {
      _pageController.nextPage(
        duration: Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    });
    monthSelectorController.addPreviousClickListener(() {
      _pageController.previousPage(
        duration: Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    });

    monthSelectorController
        .setMonthDateText(DateFormat.yMMM().format(_focusedDay));
  }

  @override
  void dispose() {
    _selectedEvents.dispose();
    super.dispose();
  }

  List<Event> _getEventsForDay(DateTime day) {
    // Implementation example
    return kEvents[day] ?? [];
  }

  List<Event> _getEventsForRange(DateTime start, DateTime end) {
    // Implementation example
    final days = daysInRange(start, end);

    return [
      for (final d in days) ..._getEventsForDay(d),
    ];
  }

  void _onDaySelected(DateTime selectedDay, DateTime focusedDay) {
    if (!isSameDay(_selectedDay, selectedDay)) {
      setState(() {
        _selectedDay = selectedDay;
        _focusedDay = focusedDay;
        _rangeStart = null; // Important to clean those
        _rangeEnd = null;
        _rangeSelectionMode = RangeSelectionMode.toggledOff;
      });

      _selectedEvents.value = _getEventsForDay(selectedDay);
    }
  }

  void _onRangeSelected(DateTime? start, DateTime? end, DateTime focusedDay) {
    setState(() {
      _selectedDay = null;
      _focusedDay = focusedDay;
      _rangeStart = start;
      _rangeEnd = end;
      _rangeSelectionMode = RangeSelectionMode.toggledOn;
    });

    // `start` or `end` could be null
    if (start != null && end != null) {
      _selectedEvents.value = _getEventsForRange(start, end);
    } else if (start != null) {
      _selectedEvents.value = _getEventsForDay(start);
    } else if (end != null) {
      _selectedEvents.value = _getEventsForDay(end);
    }
  }
}
