import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/invite/inviteAccept/invite_accept_controller.dart';
import 'package:billzi/utils/app_colors.dart';

class InviteAcceptPage extends GetView<InviteAcceptAddController> {
  const InviteAcceptPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Invitation'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_rounded),
          onPressed: Get.back,
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          physics: ClampingScrollPhysics(),
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Obx(() => Text(controller.inviterInfo.value))),
              Text('invite you'),
              Container(
                margin:
                    EdgeInsets.only(top: 10, bottom: 10, right: 20, left: 20),
                child: Card(
                  child: Column(
                    children: [
                      Container(
                        width: 300,
                        padding: EdgeInsets.only(
                            top: 20, bottom: 20, left: 20, right: 20),
                        color: Colors.black26,
                        child: Column(
                          children: [
                            Obx(
                              () => Text(
                                controller.bookName.value,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 8),
                              child: Obx(
                                () => Text(
                                  controller.bookDescription.value,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Obx(
                                () => Text(
                                  controller.attendeeList.value,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              child: Obx(
                                () => Text(
                                  '${controller.startDateWithYMD} ~',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 48,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text('set your emoji and name'),
              Container(
                margin: const EdgeInsets.only(
                    top: 20, bottom: 20, left: 20, right: 20),
                child: Row(
                  children: [
                    Text('이모지 선택'),
                    TextButton(
                      onPressed: () => controller.onClickEmojiSelectButton(),
                      child: SizedBox(
                        width: 60,
                        height: 60,
                        child: Obx(() {
                          return Center(
                            child: Text(
                              '${controller.selectedEmoji.value.emoji}',
                              style: TextStyle(fontSize: 36),
                            ),
                          );
                        }),
                        // Icon(
                        //   Icons.add,
                        //   color: Colors.white,
                        // ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  child:
                      TextField(controller: controller.myNickNameController)),
              SizedBox(
                height: 100,
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                height: 56,
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: controller.onClickAccept,
                  child: Text('accept'),
                ),
              ),
              Container(
                  margin:
                      EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
                  height: 56,
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: controller.onClickReject,
                    child: Text('reject'),
                    style: ElevatedButton.styleFrom(
                        primary: AppColors.widgetDisableGray1),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
