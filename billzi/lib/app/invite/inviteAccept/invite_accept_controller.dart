import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/common/emoji_picker.dart';
import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';
import 'package:billzi/data/dto/response/book/book_member_res_dto.dart';
import 'package:billzi/data/repositories/book_repo_impl.dart';
import 'package:billzi/domain/usecase/book_usecase.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/dialog_helper.dart';
import 'package:billzi/utils/logger.dart';

class InviteAcceptAddController extends GetxController {
  late Rx<Emoji> selectedEmoji;
  late BookUseCase bookUseCase;
  late num invitedBookSeq;
  late num hostSeq;

  var inviterInfo = ''.obs;
  var bookName = ''.obs;
  var bookDescription = ''.obs;
  var attendeeList = ''.obs;
  Rx<DateTime?> startDate = DateTime.now().obs;
  String get startDateWithYMD {
    if (startDate.value != null) {
      return DateFormat('yyyy-MM-dd').format(startDate.value!);
    }
    return 'invalid date';
  }

  TextEditingController myNickNameController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    selectedEmoji = Emoji('Grinning Face', '😀').obs;
    bookUseCase = BookUseCase(BookRepositoryImpl());
  }

  void onReady() {
    super.onReady();
    _maybeHandleInputParams();
  }

  _maybeHandleInputParams() async {
    var inputParams = Get.arguments as Map<String, String>;
    if (inputParams.containsKey('bookSeq') &&
        inputParams.containsKey('hostSeq')) {
      invitedBookSeq = num.tryParse(inputParams['bookSeq']!) ?? -1;
      hostSeq = num.tryParse(inputParams['hostSeq']!) ?? -1;
      logger.d(inputParams);

      var progress = LoadingModalView()..show();
      var result = await bookUseCase.getBookDetail(invitedBookSeq);

      result.when(
        success: (response) {
          BookMemberResDto? inviter = response.userList
              ?.firstWhere((element) => element.userSeq == hostSeq);
          if (inviter != null) {
            inviterInfo.value = '${inviter.userNickname} (${inviter.email})';
          }
          bookName.value = response.accountBookName ?? '';
          bookDescription.value = response.accountBookDesc ?? '';
          attendeeList.value = response.userList?.fold<String>(
                '',
                (previousValue, element) {
                  if (previousValue.isNotEmpty) {
                    return '$previousValue, ${element.userNickname}';
                  } else {
                    return '${element.userNickname}';
                  }
                },
              ) ??
              '';
          startDate.value = response.startDate;
        },
        failure: (error) {},
      );
      progress.pop();
    }
  }

  onClickEmojiSelectButton() {
    openCategoryBottomSheet(onEmojiSelected: (Category category, Emoji emoji) {
      selectedEmoji.value = emoji;
    });
  }

  onClickReject() {
    _showRejectDialog(
        bookName: bookName.value,
        emoji: selectedEmoji.value.emoji,
        onClickReject: () async {
          Get.back();
        });
  }

  onClickAccept() async {
    // user must be logged in to see the invitation details
    assert(UserManager().getUserSeq() != null);
    var result = await bookUseCase.joinBook(
      invitedBookSeq,
      CreateBookMemberDto(
          userNickname: myNickNameController.text,
          userProfileImgUrl: selectedEmoji.value.emoji),
    );
    result.when(
        success: (_) {
          Get.offNamed(Routes.BookSelectPage);
        },
        failure: (_) {});
  }

  _showRejectDialog(
      {required String bookName,
      required String emoji,
      required VoidCallback onClickReject}) {
    logger.d(myNickNameController.text);
    showDialog<String>(
      context: Get.context!,
      builder: (BuildContext context) => AlertDialog(
        title: const Text(
            'Do you wan`t to turn down the invitation you received?'),
        content: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              emoji,
              style: TextStyle(fontSize: 36),
            ),
            SizedBox(
              width: 12,
            ),
            Text(bookName),
          ],
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Get.back(),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () {
              onClickReject.call();
              Get.back();
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }
}
