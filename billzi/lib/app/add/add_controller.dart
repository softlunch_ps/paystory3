import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/dto/model/payment_history_dto.dart';
import 'package:billzi/data/dto/model/store_info.dart';
import 'package:billzi/data/repositories/easy_form_repo_impl.dart';
import 'package:billzi/data/repositories/transaction_repo_impl.dart';
import 'package:billzi/domain/usecase/easy_form_usecase.dart';
import 'package:billzi/domain/usecase/transaction_usecase.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/constants.dart';
import 'package:billzi/utils/pick_helper.dart';
import 'package:billzi/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

enum InputType { expense, income }

enum ExpensePaymentType { cash, card }

const maxLength = 12;

class AddController extends GetxController {
  String inputTypeCode = ''; //지출, 소비
  String expensePaymentTypeCode = ''; //지출 유형 : 현금, 카드
  late DateTime inputDateTime; //입력 시간
  late TimeOfDay _timeOfDay;
  late Rx<Currency> currency;
  CardAccountDto? selectedCardAccountDto;
  double money = 0; //지출 금액
  RxString moneyString = '0'.obs; //지출 금액
  TextEditingController textEditingController = TextEditingController();
  CategoryItem? categoryItem;
  String memo = '';
  RxString dateString = ''.obs;
  RxString timeString = ''.obs;

  @override
  void onInit() {
    super.onInit();
    _initDateTime();
    EasyFormUseCase easyFormUseCase = EasyFormUseCase(EasyFormRepositoryImpl());
    currency = easyFormUseCase.getCurrency().obs;
  } //메모

  Future onClickDatePicker(BuildContext context) async {
    DateTime? result = await selectDate(context, inputDateTime);
    if (result != null) {
      inputDateTime = result;
      dateString.value = getDateTimeStringByDateTime(inputDateTime);
      update();
    }
  }

  Future onClickTimePicker(BuildContext context) async {
    TimeOfDay? result = await selectTime(context, _timeOfDay);
    if (result != null) {
      _timeOfDay = result;
      inputDateTime = DateTime(inputDateTime.year, inputDateTime.month,
          inputDateTime.day, _timeOfDay.hour, _timeOfDay.minute);
      timeString.value = getTimeStringByDateTime(inputDateTime);
      update();
    }
  }

  Future uploadTransaction() async {
    String storeNameRcpt = categoryItem!.accountCategoryName!;

    TransactionUseCase transactionUseCase =
        TransactionUseCase(TransactionRepositoryImpl());
    await transactionUseCase.postNewTransaction(
        paymentHistoryDto: PaymentHistoryDto(
      userSeq: UserManager().getUserSeq()!,
      currencyCode: currency.value.code,
      description: memo,
      acntCateInfo: categoryItem,
      transactionTypeId: inputTypeCode,
      crdName: selectedCardAccountDto?.crdName ?? '',
      totalMoney: money,
      totalConvertedMoney: money,
      trstnDt: getOldTimeStringByDateTime(inputDateTime),
      paymentTypeId: expensePaymentTypeCode,
      storeInfo: StoreInfo(storeNameRcpt: storeNameRcpt),
      inputCd: '1',
    ));
    Get.offAllNamed(Routes.HomePage);
  }

  setCash() {
    setExpensePaymentType(ExpensePaymentType.cash);
  }

  setCategory(CategoryItem inputCategory) {
    categoryItem = inputCategory;
  }

  setMoney() {
    money = double.parse(moneyString.value);
  }

  setMemo(String inputMemo) {
    memo = inputMemo;
  }

  setCard(CardAccountDto cardAccountDto) {
    selectedCardAccountDto = cardAccountDto;
    setExpensePaymentType(ExpensePaymentType.card);
  }

  onSelectCurrency(Currency selectedCurrency) {
    currency.value = selectedCurrency;
    _refreshMoneyString();
  }

  setInputType(InputType inputType) {
    switch (inputType) {
      case InputType.expense:
        inputTypeCode = inputTypeCodeExpense;
        break;
      case InputType.income:
        inputTypeCode = inputTypeCodeIncome;
        break;
    }
  }

  setExpensePaymentType(ExpensePaymentType expensePaymentType) {
    switch (expensePaymentType) {
      case ExpensePaymentType.cash:
        expensePaymentTypeCode = expensePaymentTypeCodeCash;
        break;
      case ExpensePaymentType.card:
        expensePaymentTypeCode = expensePaymentTypeCodeCard;
        break;
    }
  }

  String getCategoryTypeCode() {
    String result = '';
    switch (inputTypeCode) {
      case inputTypeCodeExpense:
        result = categoryTypeCodeExpense;
        break;
      case inputTypeCodeIncome:
        result = categoryTypeCodeIncome;
        break;
    }
    return result;
  }

  void _initDateTime() {
    inputDateTime = DateTime.now();
    _timeOfDay = TimeOfDay.now();
    dateString.value = getDateTimeStringByDateTime(inputDateTime);
    timeString.value = getTimeStringByDateTime(inputDateTime);
  }

  onClickKeyPadInput(String number) {
    if (moneyString.value.length < maxLength) {
      moneyString.value = moneyString.value + number;
      _refreshMoneyString();
    }
  }

  onClickKeyPadDelete() {
    if (moneyString.value.length == 1) {
      moneyString.value = '';
      textEditingController.text = moneyString.value;
    } else if (moneyString.value.length > 1) {
      moneyString.value =
          moneyString.value.substring(0, moneyString.value.length - 1);
      _refreshMoneyString();
    }
  }

  _refreshMoneyString() {
    textEditingController.text = NumberFormat.currency(
            symbol: '${currency.value.symbol}', name: currency.value.code)
        .format(int.parse(moneyString.value));
    update();
  }
}
