import 'package:billzi/app/add/add_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/route/route_name.dart';

class AddTypePage extends StatelessWidget {
  const AddTypePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AddController _addController = Get.find<AddController>();
    return Scaffold(body: Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 40),
            child: Text('어떤 가계부를 작성하나요'),
          ),
          Container(
            margin: EdgeInsets.only(right: 20, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: SizedBox(
                      height: 200,
                      child:
                      OutlinedButton(onPressed: () {
                        _addController.setInputType(InputType.expense);
                        Get.toNamed(Routes.AddIncomePage);
                      }, child: Text('지출'))),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: SizedBox(
                    height: 200,
                    child: OutlinedButton(
                        onPressed: () {
                          _addController.setInputType(InputType.income);
                          Get.toNamed(Routes.AddMoneyPage);
                        },
                        child: Text('수입')),
                  ),
                ),
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 30),
              child: ElevatedButton(onPressed: () => Get.back(), child: Text('뒤로가기')))
        ],
      ),
    ));
  }
}
