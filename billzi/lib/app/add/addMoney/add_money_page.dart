import 'package:billzi/app/add/add_controller.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/app/common/numeric_keypad.dart';
import 'package:billzi/utils/constants.dart';
import 'package:billzi/utils/pick_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:billzi/route/route_name.dart';
import 'package:pattern_formatter/pattern_formatter.dart';

class AddMoneyPage extends StatefulWidget {
  const AddMoneyPage({Key? key}) : super(key: key);

  @override
  _AddMoneyPageState createState() => _AddMoneyPageState();
}

class _AddMoneyPageState extends State<AddMoneyPage> {
  final AddController _addController = Get.find<AddController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      margin: const EdgeInsets.only(right: 30, left: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(top: 10),
            height: 40,
            child: Row(
              children: [
                Flexible(
                  flex: 1,
                  child: InkWell(
                    onTap: () {
                      _addController.onClickDatePicker(context);
                    },
                    child: Obx(() => Container(
                        height: 40,
                        alignment: Alignment.centerLeft,
                        child: Center(
                            child: Text(_addController.dateString.value)))),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: InkWell(
                    onTap: () {
                      _addController.onClickTimePicker(context);
                    },
                    child: Obx(() => Container(
                        height: 40,
                        alignment: Alignment.centerLeft,
                        child: Center(
                            child: Text(_addController.timeString.value)))),
                  ),
                ),
              ],
            ),
          ),
          TextField(
            textAlign: TextAlign.center,
            controller: _addController.textEditingController,
            textDirection: TextDirection.rtl,
            maxLines: 1,
            maxLength: 10,
            maxLengthEnforcement: MaxLengthEnforcement.enforced,
            autofocus: false,
            enabled: false,
            keyboardType: const TextInputType.numberWithOptions(
                signed: false, decimal: true),
            inputFormatters: [ThousandsFormatter()],
            style: const TextStyle(
              fontSize: 35,
            ),
            decoration: const InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                hintText: '금액 입력',
                counterText: '',
                hintTextDirection: TextDirection.rtl),
          ),
          const SizedBox(
            height: 10,
          ),
          Obx(() => ElevatedButton(
              onPressed: () async {
                dynamic result = await Get.toNamed(Routes.CurrencySelectPage);
                if (result != null) {
                  _addController.onSelectCurrency(result as Currency);
                }
              },
              child: Text('currency: ${_addController.currency.value.code}'))),
          const SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ElevatedButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: const Text('뒤로가기')),
              ElevatedButton(
                  onPressed: () {
                    _addController.setMoney();
                    Get.toNamed(Routes.AddCategoryPage, arguments: _addController.getCategoryTypeCode());
                  },
                  child: const Text('다음')),
            ],
          ),
          NumericKeypad(
            onKeyboardTap: (String number) {
              _addController.onClickKeyPadInput(number);
            },
            // leftButtonFn: () {
            //   _addController.onClickKeyPadDelete();
            // },
            // leftIcon: const Icon(
            //   Icons.backspace,
            // ),
            //rightWidget: const Text('Next'),
            // rightButtonFn: () {
            //   controller.saveFormData();
            // },
          )
        ],
      ),
    ));
  }
}
