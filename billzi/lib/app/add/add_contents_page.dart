import 'package:billzi/app/add/add_controller.dart';
import 'package:billzi/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:billzi/route/route_name.dart';

class AddContentsPage extends StatefulWidget {
  const AddContentsPage({Key? key}) : super(key: key);

  @override
  _AddContentsPageState createState() => _AddContentsPageState();
}

class _AddContentsPageState extends State<AddContentsPage> {

  final AddController _addController = Get.find<AddController>();
  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          margin: const EdgeInsets.only(right: 30, left: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Container(
                  margin: const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                  child: Column(
                    children: [
                      Container(
                        child: Icon(getCategoryIcon(_addController.categoryItem!.imgURL!)),
                        margin: const EdgeInsets.only(top: 20, bottom: 20),
                      ),
                      Text(_addController.categoryItem!.accountCategoryName!),
                    ],
                  ),
                ),
              ),
              TextField(
                textAlign: TextAlign.left,
                controller: _textEditingController,
                maxLines: 1,
                maxLength: 10,
                maxLengthEnforcement: MaxLengthEnforcement.enforced,
                style: const TextStyle(
                  fontSize: 35,
                ),
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: '메모 입력 (선택)',
                    counterText: '',
                ),

              ),
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: const Text('뒤로가기')),
                  ElevatedButton(
                      onPressed: () {
                        _addController.setMemo(_textEditingController.text);
                        _addController.uploadTransaction();
                      },
                      child: const Text('다음')),
                ],
              )
            ],
          ),
        ));
  }
}
