import 'package:billzi/app/add/addCategory/add_category_controller.dart';
import 'package:billzi/app/add/add_controller.dart';
import 'package:billzi/data/dto/model/custom_category_dto.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/common/category_grid_view.dart';
import 'package:billzi/route/route_name.dart';

class AddCategoryPage extends StatefulWidget {
  const AddCategoryPage({Key? key}) : super(key: key);

  @override
  State<AddCategoryPage> createState() => _AddCategoryPageState();
}

class _AddCategoryPageState extends State<AddCategoryPage> {
  final AddCategoryController _addCategoryController =
      Get.find<AddCategoryController>();
  final AddController _addController =
      Get.find<AddController>();

  @override
  void initState() {
    super.initState();
    String typeCode = Get.arguments ?? '';
    _addCategoryController.initCategory(typeCode);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('카테고리 선택'),
        centerTitle: true,
        actions: <Widget>[
          TextButton(
            style: TextButton.styleFrom(
              primary: AppColors.textWhite1,
            ),
            onPressed: () {
              Get.toNamed(Routes.CustomCategoryEditPage);
            },
            child: Text('카테고리 수정'),
          )
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              // Container(
              //     margin: const EdgeInsets.only(top: 20, bottom: 20),
              //     child: const Text(
              //       '카테고리를 선택해 주세요.',
              //       style: TextStyle(fontSize: 25, color: Colors.grey),
              //     )),
              Container(
                  margin: const EdgeInsets.only(
                      top: 15, bottom: 10, left: 20, right: 20),
                  alignment: Alignment.centerLeft,
                  child: const Text('내가 만든 카테고리')),
              Obx(
                () => CategoryGridView(
                  onCategoryTab: (int index) {
                    if (index == 0) {
                      //카테고리추가
                      Get.toNamed(Routes.CustomCategoryPage, arguments: CustomCategoryDto(baseCategoryList: _addCategoryController.categoryList.value));
                    } else {
                      _addController.setCategory(_addCategoryController.customCategoryList[index]);
                      Get.toNamed(Routes.AddContentsPage);
                    }

                  },
                  categoryList: _addCategoryController.customCategoryList.value,
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(
                      top: 5, bottom: 10, left: 20, right: 20),
                  alignment: Alignment.centerLeft,
                  child: const Text('기본 카테고리')),
              Obx(
                    () => CategoryGridView(
                  onCategoryTab: (int index) {
                    _addController.setCategory(_addCategoryController.categoryList[index]);
                    Get.toNamed(Routes.AddContentsPage);
                  },
                  categoryList: _addCategoryController.categoryList.value,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
