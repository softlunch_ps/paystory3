import 'package:billzi/app/add/add_controller.dart';
import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/data/repositories/category_repo_impl.dart';
import 'package:billzi/domain/usecase/category_usecase.dart';
import 'package:billzi/utils/logger.dart';
import 'package:get/get.dart';

class AddCategoryController extends GetxController {

  late InputType inputType;
  RxList<CategoryItem> categoryList = RxList();
  RxList<CategoryItem> customCategoryList = RxList();

  void initCategory(String typeCode) {
    if (typeCode == 'expense') {
      _initExpenseCategory();
    } else {
      _initIncomeCategory();
    }
  }

  void refreshCategory(CategoryType categoryType) {
    _getCategoryList(categoryType);
    categoryList.refresh();
    customCategoryList.refresh();
  }

  void addCustomCategory(CategoryItem categoryItem) {
    customCategoryList.add(categoryItem);
    customCategoryList.refresh();
  }

  void _initExpenseCategory() {
    inputType = InputType.expense;
    _getCategoryList(CategoryType.expense);
  }

  void _initIncomeCategory() {
    inputType = InputType.income;
    _getCategoryList(CategoryType.income);
  }

  Future<RxList<CategoryItem>> _getCategoryList(
      CategoryType categoryType) async {
    CategoryUseCase categoryUseCase = CategoryUseCase(CategoryRepositoryImpl());
    var result =
        await categoryUseCase.getCategoryList(categoryType: categoryType);
    result.when(
      success: (categoryResDto) {
        categoryList.clear();
        customCategoryList.clear();
        customCategoryList.add(CategoryItem(
          accountCategoryName: '카테고리 추가',
          imgURL: 'add',
        ));
        for (var categoryItem in categoryResDto.accountCategoryList!) {
          if (categoryType == CategoryType.expense) {
            if (categoryItem.typeCd == '0') {
              categoryList.add(categoryItem);
            } else {
              customCategoryList.add(categoryItem);
            }
          } else {
            if (categoryItem.typeCd == '5') {
              categoryList.add(categoryItem);
            } else {
              customCategoryList.add(categoryItem);
            }
          }
        }
      },
      failure: (error) {
        logger.d('fail to load card list');
      },
    );
    return categoryList;
  }


}
