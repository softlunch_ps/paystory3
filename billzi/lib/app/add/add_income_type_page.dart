import 'package:billzi/app/add/add_controller.dart';
import 'package:billzi/app/addCard/card_controller.dart';
import 'package:billzi/app/common/card_select_dialog.dart';
import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/route/route_name.dart';

class AddIncomePage extends StatelessWidget {
  AddIncomePage({Key? key}) : super(key: key);

  final CardController _cardController = Get.find<CardController>();
  final AddController _addController = Get.find<AddController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: 20, bottom: 40),
            child: Text('지출 유형을 골라주세요'),
          ),
          Container(
            margin: EdgeInsets.only(right: 20, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: SizedBox(
                      height: 200,
                      child:
                          OutlinedButton(onPressed: () {
                            _addController.setCash();
                            Get.toNamed(Routes.AddMoneyPage);
                          }, child: Text('현금'))),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: SizedBox(
                    height: 200,
                    child: OutlinedButton(
                        onPressed: () {
                          openCardSelectBottomSheet(
                              onCardItemSelect: (CardAccountDto item) {
                                Get.back();
                                _addController.setCard(item);
                                Get.toNamed(Routes.AddMoneyPage);
                              },
                              cardController: _cardController);
                        },
                        child: Text('카드')),
                  ),
                ),
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 30),
              child: ElevatedButton(
                  onPressed: () => Get.back(), child: Text('뒤로가기')))
        ],
      ),
    ));
  }

}
