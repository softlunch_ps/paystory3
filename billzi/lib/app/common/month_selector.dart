import 'package:flutter/material.dart';

typedef void OnArrowButtonClickListener();

class MonthSelector extends StatefulWidget {

  final MonthSelectorController monthSelectorController;

  const MonthSelector({Key? key, required this.monthSelectorController})
      : super(key: key);

  @override
  _MonthSelectorState createState() => _MonthSelectorState();
}

class _MonthSelectorState extends State<MonthSelector> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          widget.monthSelectorController.isPreviousButtonVisible
              ? widget.monthSelectorController.previousListener != null
                  ? Container(
                      width: 40,
                      height: 40,
                      child: IconButton(
                          onPressed: () => widget
                              .monthSelectorController.previousListener
                              ?.call(),
                          icon: Icon(Icons.arrow_left_outlined)),
                    )
                  : Container(
                      width: 40,
                      height: 40,
                    )
              : Container(
                  width: 40,
                  height: 40,
                ),
          Row(
            children: [
              Text(
                widget.monthSelectorController.monthDateText,
                textScaleFactor: 1.2,
              ),
              Text(widget.monthSelectorController.monthDateRangeText),
            ],
          ),
          widget.monthSelectorController.isNextButtonVisible
              ? widget.monthSelectorController.nextListener != null
                  ? Container(
                      width: 40,
                      height: 40,
                      child: IconButton(
                          onPressed: () => widget
                              .monthSelectorController.nextListener
                              ?.call(),
                          icon: Icon(Icons.arrow_right_outlined)),
                    )
                  : Container(
                      width: 40,
                      height: 40,
                    )
              : Container(
                  width: 40,
                  height: 40,
                ),
        ],
      ),
    );
  }
}

class MonthSelectorController extends ChangeNotifier {
  OnArrowButtonClickListener? previousListener;
  OnArrowButtonClickListener? nextListener;
  bool isPreviousButtonVisible = true;
  bool isNextButtonVisible = true;
  String monthDateText;
  String monthDateRangeText;

  MonthSelectorController(
      {this.monthDateText = '', this.monthDateRangeText = ''});

  void addPreviousClickListener(
      OnArrowButtonClickListener onArrowButtonClickListener) {
    previousListener = onArrowButtonClickListener;
  }

  void addNextClickListener(
      OnArrowButtonClickListener onArrowButtonClickListener) {
    nextListener = onArrowButtonClickListener;
  }

  setPreviousButtonVisible(bool isVisible) {
    isPreviousButtonVisible = isVisible;
    notifyListeners();
  }

  setNextButtonVisible(bool isVisible) {
    isNextButtonVisible = isVisible;
    notifyListeners();
  }

  setMonthDateText(String text) {
    monthDateText = text;
    notifyListeners();
  }

  setMonthDateRangeText(String text) {
    monthDateRangeText = text;
    notifyListeners();
  }
}
