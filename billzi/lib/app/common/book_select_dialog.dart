import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/data/dto/model/selectable_model.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/utils/extensions.dart';


typedef OnBookItemSelect = void Function(int index, SelectableModel<BookResDto> item);

openBookBottomSheet({required OnBookItemSelect onBookItemSelect, required RxList<SelectableModel<BookResDto>> list}) {

  Get.bottomSheet(
    Container(
        height: MediaQuery.of(Get.context!).size.height * 0.5,
        color: Colors.white,
        child: Material(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('북 선택'),
                      TextButton(onPressed: () {
                        Get.back();
                      }, child: Text('확인')),
                    ],
                  ),
                ),
                Expanded(
                  child: Obx(() => ListView.builder(
                      itemCount: list.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        final currentItem = list[index];
                        return Card(
                          margin: EdgeInsets.only(
                              left: 10, right: 10, top: 5, bottom: 5),
                          child: InkWell(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            onTap: () {
                              onBookItemSelect.call(index, currentItem);
                            },
                            child: Container(
                                margin: EdgeInsets.only(left: 20, right: 20),
                                height: 56,
                                child: Row(
                                  children: [
                                    Icon(currentItem.isSelected ? Icons.check_box : Icons.check_box_outline_blank),
                                    Container(
                                        margin:
                                        EdgeInsets.only(left: 20, right: 20),
                                        child: Icon(Icons.attach_money)),
                                    Text('${currentItem.data.accountBookName}'),
                                  ],
                                )),
                          ),
                          color: currentItem.isSelected ? currentItem.data.color!.toColor() : Colors.white,
                        );
                      })),
                ),
              ],
            ),
          ),
        )),
    // shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(35),
    //     side: BorderSide(
    //         width: 5,
    //         color: Colors.black
    //     )
    // ),
    enableDrag: false,
  );
}
