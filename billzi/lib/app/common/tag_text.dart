import 'package:flutter/material.dart';
import 'package:flutter_social_textfield/model/detected_type_enum.dart';
import 'package:flutter_social_textfield/util/regular_expressions.dart';
import 'package:flutter_social_textfield/util/social_text_span_builder.dart';

class TagText extends StatelessWidget {

  final String text;

  const TagText({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: SocialTextSpanBuilder(
        regularExpressions: {
          DetectedType.hashtag: hashTagRegExp
        },
        defaultTextStyle: TextStyle(color: Colors.black),
        detectionTextStyles: {
          DetectedType.hashtag: TextStyle(color: Colors.blue),
        },
      ).build(text),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
    );
  }
}
