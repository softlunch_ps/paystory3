import 'dart:io';

import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/utils/app_colors.dart';

Future openCategoryBottomSheet(
    {required OnEmojiSelected onEmojiSelected}) async {
  await Get.bottomSheet(
    SafeArea(
      child: Container(
          height: 500,
          color: Colors.white,
          child: EmojiPicker(
            onEmojiSelected: (Category category, Emoji emoji) {
              onEmojiSelected.call(category, emoji);
              Get.back();
            },
            onBackspacePressed: () {
              Get.back();
            },
            config: Config(
                columns: 7,
                emojiSizeMax: 32 *
                    (Platform.isIOS
                        ? 1.30
                        : 1.0), // Issue: https://github.com/flutter/flutter/issues/28894
                verticalSpacing: 0,
                horizontalSpacing: 0,
                initCategory: Category.SMILEYS,
                bgColor: Color(0xFFF2F2F2),
                indicatorColor: AppColors.primary,
                iconColor: Colors.grey,
                iconColorSelected: AppColors.primary,
                progressIndicatorColor: AppColors.primary,
                showRecentsTab: false,
                tabIndicatorAnimDuration: kTabScrollDuration,
                categoryIcons: const CategoryIcons(),
                buttonMode: ButtonMode.MATERIAL),
          )),
    ),
    enableDrag: false,
  );
}
