import 'package:billzi/app/addCard/card_controller.dart';
import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:billzi/route/route_name.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

typedef OnCardItemSelect = void Function(CardAccountDto item);

openCardSelectBottomSheet(
    {required CardController cardController,
    required OnCardItemSelect onCardItemSelect}) async {
  await cardController.getCardList();

  Get.bottomSheet(
    Container(
        height: 300,
        color: Colors.white,
        child: Material(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            child: Column(
              children: [
                TextButton(
                    onPressed: () {
                      Get.toNamed(Routes.AddCardPage);
                    },
                    child: Text('카드 추가')),
                Expanded(
                  child: Obx(() => ListView.builder(
                      itemCount: cardController.cardList.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          child: Container(
                              height: 50,
                              child: Center(
                                  child: Text(
                                      cardController.cardList[index].crdName!,
                                      textScaleFactor: 2))),
                          onTap: () {
                            onCardItemSelect.call(cardController.cardList[index]);
                          },
                        );
                      })),
                ),
              ],
            ),
          ),
        )),
    enableDrag: false,
  );
}
