import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/data/dto/model/selectable_model.dart';
import 'package:billzi/data/dto/testDto/test_category.dart';
import 'package:billzi/utils/test_data.dart';
import 'package:billzi/utils/utils.dart';

typedef OnBudgetTypeSelect = void Function(bool shouldUseCategory);
typedef OnPressSelectAll = void Function(bool shouldUseCategory);
typedef OnCategoryItemsSelect = void Function(
    int selectedIndex, SelectableModel<TestCategory> selectedItem);
typedef OnTagItemsSelect = void Function(
    int selectedIndex, SelectableModel<TestTag> selectedItem);

openCategoryTagBottomSheet({
  required RxBool shouldUseCategory,
  required RxList<SelectableModel<TestCategory>> categoryList,
  required RxList<SelectableModel<TestTag>> tagList,
  required OnPressSelectAll onPressSelectAll,
  required OnBudgetTypeSelect onBudgetTypeSelect,
  required OnCategoryItemsSelect onCategoryItemsSelect,
  required OnTagItemsSelect onTagItemsSelect,
}) {
  Get.bottomSheet(
    Container(
        height: MediaQuery.of(Get.context!).size.height * 0.8,
        color: Colors.white,
        child: Material(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Budget for'),
                      TextButton(
                          onPressed: () {
                            Get.back();
                          },
                          child: Text('확인')),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Obx(() => ElevatedButton(
                          onPressed: () => onBudgetTypeSelect.call(true),
                          child: Text('category'),
                          style: ElevatedButton.styleFrom(
                              primary: shouldUseCategory.value
                                  ? Colors.blue
                                  : Colors.grey))),
                      Obx(() => ElevatedButton(
                          onPressed: () => onBudgetTypeSelect.call(false),
                          child: Text('tag'),
                          style: ElevatedButton.styleFrom(
                              primary: shouldUseCategory.value
                                  ? Colors.grey
                                  : Colors.blue))),
                    ],
                  ),
                ),
                Obx(
                  () => TextButton.icon(
                    icon: Icon(isAllSelected(shouldUseCategory.value ? categoryList : tagList) ? Icons.check_box : Icons.check_box_outline_blank),
                    onPressed: () {
                      onPressSelectAll.call(shouldUseCategory.value);
                    },
                    label: Text('Select all'),
                    style: TextButton.styleFrom(),
                  ),
                ),
                Obx(() => Expanded(
                      child: shouldUseCategory.value
                          ? ListView.builder(
                              itemCount: categoryList.length,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                final currentItem = categoryList[index];
                                return Card(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 10, top: 5, bottom: 5),
                                  child: InkWell(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    onTap: () {
                                      onCategoryItemsSelect.call(
                                          index, currentItem);
                                    },
                                    child: Container(
                                        margin: EdgeInsets.only(
                                            left: 20, right: 20),
                                        height: 56,
                                        child: Row(
                                          children: [
                                            Icon(currentItem.isSelected
                                                ? Icons.check_box
                                                : Icons
                                                    .check_box_outline_blank),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: 20, right: 20),
                                              child: Text(
                                                  '${currentItem.data.name}'),
                                            ),
                                          ],
                                        )),
                                  ),
                                  // color: currentItem.isSelected
                                  //     ? currentItem.bookResDto.color!.toColor()
                                  //     : Colors.white,
                                );
                              })
                          : ListView.builder(
                              itemCount: tagList.length,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                final currentItem = tagList[index];
                                return Card(
                                  margin: EdgeInsets.only(
                                      left: 10, right: 10, top: 5, bottom: 5),
                                  child: InkWell(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    onTap: () {
                                      onTagItemsSelect.call(index, currentItem);
                                    },
                                    child: Container(
                                        margin: EdgeInsets.only(
                                            left: 20, right: 20),
                                        height: 56,
                                        child: Row(
                                          children: [
                                            Icon(currentItem.isSelected
                                                ? Icons.check_box
                                                : Icons
                                                    .check_box_outline_blank),
                                            Container(
                                                margin: EdgeInsets.only(
                                                    left: 20, right: 20),
                                                child: Text(
                                                    '${currentItem.data.name}')),
                                          ],
                                        )),
                                  ),
                                  // color: currentItem.isSelected
                                  //     ? currentItem.bookResDto.color!.toColor()
                                  //     : Colors.white,
                                );
                              }),
                    )),
              ],
            ),
          ),
        )),
    // shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(35),
    //     side: BorderSide(
    //         width: 5,
    //         color: Colors.black
    //     )
    // ),
    enableDrag: false,
    isScrollControlled: true,
  );
}
