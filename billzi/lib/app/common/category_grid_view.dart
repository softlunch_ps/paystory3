import 'package:billzi/data/dto/model/category_dto.dart';
import 'package:billzi/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:billzi/data/dto/testDto/test_category.dart';
import 'package:billzi/utils/test_data.dart';

typedef OnCategoryTab = void Function(int index);

class CategoryGridView extends StatelessWidget {
  final List<CategoryItem> categoryList;
  final OnCategoryTab onCategoryTab;
  final bool hasScrollPhysics;

  const CategoryGridView(
      {Key? key, required this.categoryList, required this.onCategoryTab, this.hasScrollPhysics = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: hasScrollPhysics ? ScrollPhysics() : NeverScrollableScrollPhysics(),
      itemCount: categoryList.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 10,
          mainAxisSpacing: 2,
          childAspectRatio: 1.2),
      itemBuilder: (BuildContext context, int index) {
        return Container(
            child: InkWell(
              child: Column(
                children: [
                  Container(
                    child: Icon(getCategoryIcon(categoryList[index].imgURL ?? '')),
                    margin: EdgeInsets.only(top: 20, bottom: 20),
                  ),
                  Text(categoryList[index].accountCategoryName ?? '')
                ],
              ),
              onTap: () => onCategoryTab.call(index),
            ));
      },
    );
  }
}