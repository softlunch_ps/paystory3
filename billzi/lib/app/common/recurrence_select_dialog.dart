import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/data/dto/model/recurrence_model.dart';

typedef OnRecurrenceItemSelect = void Function(int index);

openRecurrenceBottomSheet(
    {required OnRecurrenceItemSelect onBookItemSelect,
    required RxList<RecurrenceModel> list,
    required int selectedIndex}) {
  Get.bottomSheet(
    Container(
        height: MediaQuery.of(Get.context!).size.height * 0.5,
        color: Colors.white,
        child: Material(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                  child: Text('Recurrence'),
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: list.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return Obx(() => Card(
                              margin: EdgeInsets.only(
                                  left: 10, right: 10, top: 5, bottom: 5),
                              child: InkWell(
                                borderRadius: BorderRadius.all(Radius.circular(4)),
                                onTap: () {
                                  onBookItemSelect.call(index);
                                  Get.back();
                                },
                                child: Container(
                                    margin:
                                        EdgeInsets.only(left: 20, right: 20),
                                    height: 56,
                                    child: Row(
                                      children: [
                                        Icon(index == selectedIndex
                                            ? Icons.check
                                            : null),
                                        Container(
                                            margin: EdgeInsets.only(left: 20, right: 20),
                                            child: Text('${list[index].name}')),
                                      ],
                                    )),
                              ),
                              // color: list[index].isSelected ? list[index].bookResDto.color!.toColor() : Colors.white,
                            ));
                      }),
                ),
              ],
            ),
          ),
        )),
    // shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(35),
    //     side: BorderSide(
    //         width: 5,
    //         color: Colors.black
    //     )
    // ),
    enableDrag: false,
  );
}
