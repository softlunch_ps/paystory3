import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:billzi/network/apis/common/api_result.dart';

typedef FallBackErrorHandler = Function(String errorCode);

mixin ErrorHandlerMixIn {
  // TODO : Show error popup or transition to full error screen based on the PayStoryErrorCode
  handleErrors(String errorCode, {FallBackErrorHandler? fallBackErrorHandler}) {
    switch (errorCode.toPayStoryErrorCode()) {
      // handle common errors first
      case PayStoryErrorCode.DB0100:
        break;
      default:
        // if error is not handled then forward it to fallback error handler
        fallBackErrorHandler?.call(errorCode) ??
            Fluttertoast.showToast(
                msg: "Unknown error occurs! Please try it again !",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.white,
                textColor: Colors.black,
                fontSize: 16.0);
    }
  }
}
