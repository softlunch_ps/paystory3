import 'package:flutter/material.dart';

class GuideTooltip extends StatelessWidget {
  final Widget child;
  final String message;
  final double? height;

  GuideTooltip({required this.message, required this.child, this.height});

  @override
  Widget build(BuildContext context) {
    final key = GlobalKey<State<Tooltip>>();
    return Tooltip(
      key: key,
      message: message,
      height: height,
      margin: EdgeInsets.all(10),
      preferBelow: false,
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => _onTap(key),
        child: child,
      ),
    );
  }

  void _onTap(GlobalKey key) {
    final dynamic tooltip = key.currentState;
    tooltip?.ensureTooltipVisible();
  }
}
