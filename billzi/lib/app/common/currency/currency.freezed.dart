// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'currency.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Currency _$CurrencyFromJson(Map<String, dynamic> json) {
  return _Currency.fromJson(json);
}

/// @nodoc
class _$CurrencyTearOff {
  const _$CurrencyTearOff();

  _Currency call(
      {required String code,
      required String name,
      required String symbol,
      required String flag,
      required int number,
      int? decimalDigits,
      String? namePlural,
      bool? symbolOnLeft,
      String? decimalSeparator,
      String? thousandsSeparator,
      bool? spaceBetweenAmountAndSymbol}) {
    return _Currency(
      code: code,
      name: name,
      symbol: symbol,
      flag: flag,
      number: number,
      decimalDigits: decimalDigits,
      namePlural: namePlural,
      symbolOnLeft: symbolOnLeft,
      decimalSeparator: decimalSeparator,
      thousandsSeparator: thousandsSeparator,
      spaceBetweenAmountAndSymbol: spaceBetweenAmountAndSymbol,
    );
  }

  Currency fromJson(Map<String, Object?> json) {
    return Currency.fromJson(json);
  }
}

/// @nodoc
const $Currency = _$CurrencyTearOff();

/// @nodoc
mixin _$Currency {
  String get code => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get symbol => throw _privateConstructorUsedError;
  String get flag => throw _privateConstructorUsedError;
  int get number => throw _privateConstructorUsedError;
  int? get decimalDigits => throw _privateConstructorUsedError;
  String? get namePlural => throw _privateConstructorUsedError;
  bool? get symbolOnLeft => throw _privateConstructorUsedError;
  String? get decimalSeparator => throw _privateConstructorUsedError;
  String? get thousandsSeparator => throw _privateConstructorUsedError;
  bool? get spaceBetweenAmountAndSymbol => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CurrencyCopyWith<Currency> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CurrencyCopyWith<$Res> {
  factory $CurrencyCopyWith(Currency value, $Res Function(Currency) then) =
      _$CurrencyCopyWithImpl<$Res>;
  $Res call(
      {String code,
      String name,
      String symbol,
      String flag,
      int number,
      int? decimalDigits,
      String? namePlural,
      bool? symbolOnLeft,
      String? decimalSeparator,
      String? thousandsSeparator,
      bool? spaceBetweenAmountAndSymbol});
}

/// @nodoc
class _$CurrencyCopyWithImpl<$Res> implements $CurrencyCopyWith<$Res> {
  _$CurrencyCopyWithImpl(this._value, this._then);

  final Currency _value;
  // ignore: unused_field
  final $Res Function(Currency) _then;

  @override
  $Res call({
    Object? code = freezed,
    Object? name = freezed,
    Object? symbol = freezed,
    Object? flag = freezed,
    Object? number = freezed,
    Object? decimalDigits = freezed,
    Object? namePlural = freezed,
    Object? symbolOnLeft = freezed,
    Object? decimalSeparator = freezed,
    Object? thousandsSeparator = freezed,
    Object? spaceBetweenAmountAndSymbol = freezed,
  }) {
    return _then(_value.copyWith(
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      symbol: symbol == freezed
          ? _value.symbol
          : symbol // ignore: cast_nullable_to_non_nullable
              as String,
      flag: flag == freezed
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as String,
      number: number == freezed
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int,
      decimalDigits: decimalDigits == freezed
          ? _value.decimalDigits
          : decimalDigits // ignore: cast_nullable_to_non_nullable
              as int?,
      namePlural: namePlural == freezed
          ? _value.namePlural
          : namePlural // ignore: cast_nullable_to_non_nullable
              as String?,
      symbolOnLeft: symbolOnLeft == freezed
          ? _value.symbolOnLeft
          : symbolOnLeft // ignore: cast_nullable_to_non_nullable
              as bool?,
      decimalSeparator: decimalSeparator == freezed
          ? _value.decimalSeparator
          : decimalSeparator // ignore: cast_nullable_to_non_nullable
              as String?,
      thousandsSeparator: thousandsSeparator == freezed
          ? _value.thousandsSeparator
          : thousandsSeparator // ignore: cast_nullable_to_non_nullable
              as String?,
      spaceBetweenAmountAndSymbol: spaceBetweenAmountAndSymbol == freezed
          ? _value.spaceBetweenAmountAndSymbol
          : spaceBetweenAmountAndSymbol // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
abstract class _$CurrencyCopyWith<$Res> implements $CurrencyCopyWith<$Res> {
  factory _$CurrencyCopyWith(_Currency value, $Res Function(_Currency) then) =
      __$CurrencyCopyWithImpl<$Res>;
  @override
  $Res call(
      {String code,
      String name,
      String symbol,
      String flag,
      int number,
      int? decimalDigits,
      String? namePlural,
      bool? symbolOnLeft,
      String? decimalSeparator,
      String? thousandsSeparator,
      bool? spaceBetweenAmountAndSymbol});
}

/// @nodoc
class __$CurrencyCopyWithImpl<$Res> extends _$CurrencyCopyWithImpl<$Res>
    implements _$CurrencyCopyWith<$Res> {
  __$CurrencyCopyWithImpl(_Currency _value, $Res Function(_Currency) _then)
      : super(_value, (v) => _then(v as _Currency));

  @override
  _Currency get _value => super._value as _Currency;

  @override
  $Res call({
    Object? code = freezed,
    Object? name = freezed,
    Object? symbol = freezed,
    Object? flag = freezed,
    Object? number = freezed,
    Object? decimalDigits = freezed,
    Object? namePlural = freezed,
    Object? symbolOnLeft = freezed,
    Object? decimalSeparator = freezed,
    Object? thousandsSeparator = freezed,
    Object? spaceBetweenAmountAndSymbol = freezed,
  }) {
    return _then(_Currency(
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      symbol: symbol == freezed
          ? _value.symbol
          : symbol // ignore: cast_nullable_to_non_nullable
              as String,
      flag: flag == freezed
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as String,
      number: number == freezed
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int,
      decimalDigits: decimalDigits == freezed
          ? _value.decimalDigits
          : decimalDigits // ignore: cast_nullable_to_non_nullable
              as int?,
      namePlural: namePlural == freezed
          ? _value.namePlural
          : namePlural // ignore: cast_nullable_to_non_nullable
              as String?,
      symbolOnLeft: symbolOnLeft == freezed
          ? _value.symbolOnLeft
          : symbolOnLeft // ignore: cast_nullable_to_non_nullable
              as bool?,
      decimalSeparator: decimalSeparator == freezed
          ? _value.decimalSeparator
          : decimalSeparator // ignore: cast_nullable_to_non_nullable
              as String?,
      thousandsSeparator: thousandsSeparator == freezed
          ? _value.thousandsSeparator
          : thousandsSeparator // ignore: cast_nullable_to_non_nullable
              as String?,
      spaceBetweenAmountAndSymbol: spaceBetweenAmountAndSymbol == freezed
          ? _value.spaceBetweenAmountAndSymbol
          : spaceBetweenAmountAndSymbol // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Currency implements _Currency {
  _$_Currency(
      {required this.code,
      required this.name,
      required this.symbol,
      required this.flag,
      required this.number,
      this.decimalDigits,
      this.namePlural,
      this.symbolOnLeft,
      this.decimalSeparator,
      this.thousandsSeparator,
      this.spaceBetweenAmountAndSymbol});

  factory _$_Currency.fromJson(Map<String, dynamic> json) =>
      _$$_CurrencyFromJson(json);

  @override
  final String code;
  @override
  final String name;
  @override
  final String symbol;
  @override
  final String flag;
  @override
  final int number;
  @override
  final int? decimalDigits;
  @override
  final String? namePlural;
  @override
  final bool? symbolOnLeft;
  @override
  final String? decimalSeparator;
  @override
  final String? thousandsSeparator;
  @override
  final bool? spaceBetweenAmountAndSymbol;

  @override
  String toString() {
    return 'Currency(code: $code, name: $name, symbol: $symbol, flag: $flag, number: $number, decimalDigits: $decimalDigits, namePlural: $namePlural, symbolOnLeft: $symbolOnLeft, decimalSeparator: $decimalSeparator, thousandsSeparator: $thousandsSeparator, spaceBetweenAmountAndSymbol: $spaceBetweenAmountAndSymbol)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Currency &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.symbol, symbol) || other.symbol == symbol) &&
            (identical(other.flag, flag) || other.flag == flag) &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.decimalDigits, decimalDigits) ||
                other.decimalDigits == decimalDigits) &&
            (identical(other.namePlural, namePlural) ||
                other.namePlural == namePlural) &&
            (identical(other.symbolOnLeft, symbolOnLeft) ||
                other.symbolOnLeft == symbolOnLeft) &&
            (identical(other.decimalSeparator, decimalSeparator) ||
                other.decimalSeparator == decimalSeparator) &&
            (identical(other.thousandsSeparator, thousandsSeparator) ||
                other.thousandsSeparator == thousandsSeparator) &&
            (identical(other.spaceBetweenAmountAndSymbol,
                    spaceBetweenAmountAndSymbol) ||
                other.spaceBetweenAmountAndSymbol ==
                    spaceBetweenAmountAndSymbol));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      code,
      name,
      symbol,
      flag,
      number,
      decimalDigits,
      namePlural,
      symbolOnLeft,
      decimalSeparator,
      thousandsSeparator,
      spaceBetweenAmountAndSymbol);

  @JsonKey(ignore: true)
  @override
  _$CurrencyCopyWith<_Currency> get copyWith =>
      __$CurrencyCopyWithImpl<_Currency>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CurrencyToJson(this);
  }
}

abstract class _Currency implements Currency {
  factory _Currency(
      {required String code,
      required String name,
      required String symbol,
      required String flag,
      required int number,
      int? decimalDigits,
      String? namePlural,
      bool? symbolOnLeft,
      String? decimalSeparator,
      String? thousandsSeparator,
      bool? spaceBetweenAmountAndSymbol}) = _$_Currency;

  factory _Currency.fromJson(Map<String, dynamic> json) = _$_Currency.fromJson;

  @override
  String get code;
  @override
  String get name;
  @override
  String get symbol;
  @override
  String get flag;
  @override
  int get number;
  @override
  int? get decimalDigits;
  @override
  String? get namePlural;
  @override
  bool? get symbolOnLeft;
  @override
  String? get decimalSeparator;
  @override
  String? get thousandsSeparator;
  @override
  bool? get spaceBetweenAmountAndSymbol;
  @override
  @JsonKey(ignore: true)
  _$CurrencyCopyWith<_Currency> get copyWith =>
      throw _privateConstructorUsedError;
}
