// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Currency _$$_CurrencyFromJson(Map<String, dynamic> json) => _$_Currency(
      code: json['code'] as String,
      name: json['name'] as String,
      symbol: json['symbol'] as String,
      flag: json['flag'] as String,
      number: json['number'] as int,
      decimalDigits: json['decimalDigits'] as int?,
      namePlural: json['namePlural'] as String?,
      symbolOnLeft: json['symbolOnLeft'] as bool?,
      decimalSeparator: json['decimalSeparator'] as String?,
      thousandsSeparator: json['thousandsSeparator'] as String?,
      spaceBetweenAmountAndSymbol: json['spaceBetweenAmountAndSymbol'] as bool?,
    );

Map<String, dynamic> _$$_CurrencyToJson(_$_Currency instance) =>
    <String, dynamic>{
      'code': instance.code,
      'name': instance.name,
      'symbol': instance.symbol,
      'flag': instance.flag,
      'number': instance.number,
      'decimalDigits': instance.decimalDigits,
      'namePlural': instance.namePlural,
      'symbolOnLeft': instance.symbolOnLeft,
      'decimalSeparator': instance.decimalSeparator,
      'thousandsSeparator': instance.thousandsSeparator,
      'spaceBetweenAmountAndSymbol': instance.spaceBetweenAmountAndSymbol,
    };
