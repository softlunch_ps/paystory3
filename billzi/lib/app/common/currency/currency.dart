import 'package:freezed_annotation/freezed_annotation.dart';

part 'currency.freezed.dart';
part 'currency.g.dart';

@freezed
class Currency with _$Currency {
  factory Currency({
    required String code,
    required String name,
    required String symbol,
    required String flag,
    required int number,
    int? decimalDigits,
    String? namePlural,
    bool? symbolOnLeft,
    String? decimalSeparator,
    String? thousandsSeparator,
    bool? spaceBetweenAmountAndSymbol,
  }) = _Currency;

  factory Currency.fromJson(Map<String, dynamic> json) =>
      _$CurrencyFromJson(json);
}
