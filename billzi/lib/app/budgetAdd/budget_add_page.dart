import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/budgetAdd/budget_add_controller.dart';
import 'package:billzi/app/common/book_select_dialog.dart';
import 'package:billzi/app/common/category_tag_select_dialog.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/app/common/recurrence_select_dialog.dart';
import 'package:billzi/data/dto/model/selectable_model.dart';
import 'package:billzi/data/dto/testDto/test_category.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/test_data.dart';

class BudgetAddPage extends StatefulWidget {
  const BudgetAddPage({Key? key}) : super(key: key);

  @override
  State<BudgetAddPage> createState() => _BudgetAddPageState();
}

class _BudgetAddPageState extends State<BudgetAddPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Add new budget'),
      ),
      body: GetBuilder(
        builder: (BudgetAddController controller) {return Column(
            children: [
              Container(
                margin:
                    EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 10),
                child: TextFormField(
                  decoration: const InputDecoration(
                    icon: Icon(Icons.account_balance_wallet_rounded),
                    hintText: 'What do people call you?',
                    labelText: 'Budget name',
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                child: Row(
                  children: [
                    Flexible(
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          icon: Icon(Icons.attach_money),
                          hintText: '100',
                          labelText: 'Amount',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      width: 100,
                      child: TextFormField(
                        initialValue:
                            '${controller.selectedCurrency.value.code}',
                        readOnly: true,
                        onTap: () async {
                          dynamic result =
                              await Get.toNamed(Routes.CurrencySelectPage);
                          if (result != null) {
                            controller.onSelectCurrency(result as Currency);
                          }
                        },
                        decoration: const InputDecoration(
                          suffixIcon: Icon(Icons.keyboard_arrow_right),
                          labelText: 'Currency',
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              _makeButton(
                  icon: Icons.book,
                  buttonName: 'Books',
                  selectedText: controller.selectedBookText.value,
                  onTap: () async {
                    openBookBottomSheet(
                        list: await controller.getAllBookListFromApi(),
                        onBookItemSelect: (index, item) {
                          controller.onBookItemSelected(index, item);
                        });
                  }),
              _makeButton(
                  icon: Icons.category,
                  buttonName: 'Budget for',
                  selectedText: controller.selectedCategoryTagText.value,
                  onTap: () {
                    openCategoryTagBottomSheet(
                      shouldUseCategory: controller.shouldUseCategory,
                      categoryList: controller.selectCategoryModelList,
                      tagList: controller.selectTagModelList,
                      onBudgetTypeSelect: (bool usingCategory) =>
                          controller.onBudgetTypeSelected(usingCategory),
                      onCategoryItemsSelect:
                          (int index, SelectableModel<TestCategory> item) =>
                              controller.onCategoryItemSelected(index, item),
                      onTagItemsSelect: (int index, SelectableModel<TestTag> item) =>
                          controller.onTagItemSelected(index, item),
                      onPressSelectAll: (bool usingCategory) => controller.onSelectAllPress(usingCategory),
                    );
                  }),
              _makeButton(
                  icon: Icons.access_alarms,
                  buttonName: 'Recurrence',
                  selectedText: controller.selectedRecurrenceText.value,
                  onTap: () {
                    openRecurrenceBottomSheet(
                        selectedIndex: controller.selectedRecurrenceIndex.value,
                        list: controller.recurrenceModelList,
                        onBookItemSelect: (index) {
                          controller.onSelectRecurrence(index);
                        });
                  }),
              _makeButton(
                  icon: Icons.date_range,
                  buttonName: 'Start date',
                  selectedText: controller.selectedDateTimeText.value,
                  onTap: () {
                    controller.selectDate(
                        context: context,
                        initialDate: controller.selectedDateTime);
                  }),
              Expanded(child: Container()),
              Container(
                  margin:
                      EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
                  child: ElevatedButton(
                      onPressed: () {
                        Get.back();
                      }, child: Text('Create new budget')))
            ],
          );
        },
      ),
    );
  }

  Widget _makeButton(
      {required IconData icon,
      required String buttonName,
      required String selectedText,
      required GestureTapCallback onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 56,
        margin: EdgeInsets.only(left: 20, right: 20),
        child: Row(
          children: [
            Icon(icon),
            Expanded(
              child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16),
                  child: Text(
                    buttonName,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  )),
            ),
            Text(selectedText),
            Icon(Icons.keyboard_arrow_right)
          ],
        ),
      ),
    );
  }
}
