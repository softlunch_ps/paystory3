import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/data/dto/model/recurrence_model.dart';
import 'package:billzi/data/dto/model/selectable_model.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/dto/testDto/test_category.dart';
import 'package:billzi/data/repositories/book_repo_impl.dart';
import 'package:billzi/data/repositories/easy_form_repo_impl.dart';
import 'package:billzi/domain/usecase/book_usecase.dart';
import 'package:billzi/domain/usecase/easy_form_usecase.dart';
import 'package:billzi/utils/logger.dart';
import 'package:billzi/utils/test_data.dart';
import 'package:billzi/utils/utils.dart';

class BudgetAddController extends GetxController {
  RxList<RecurrenceModel> recurrenceModelList = RxList();
  RxList<SelectableModel<BookResDto>>? _selectBookModelList;
  RxList<SelectableModel<TestCategory>> selectCategoryModelList = RxList();
  RxList<SelectableModel<TestTag>> selectTagModelList = RxList();
  RxBool shouldUseCategory = true.obs;

  late DateTime selectedDateTime;
  RxString selectedDateTimeText = ''.obs;
  RxString selectedBookText = 'All Books'.obs;
  RxString selectedRecurrenceText = 'Monthly'.obs;
  RxString selectedCategoryTagText = 'All Categories'.obs;
  late Rx<Currency> selectedCurrency;
  RxInt selectedRecurrenceIndex = 3.obs; //default monthly

  @override
  void onInit() {
    super.onInit();
    selectedDateTime = DateTime.now();
    selectedDateTimeText =
        DateFormat('yyyy/MM/dd')
            .format(selectedDateTime)
            .obs;
    _initCurrency();
    _initRecurrence();
    _initCategoryList();
    _initTagList();
  }

  void _initRecurrence() {
    if (recurrenceModelList.isEmpty) {
      recurrenceModelList.addAll(TestData.getTestRecurrenceList());
    }
  }

  void _initCurrency() {
    EasyFormUseCase easyFormUseCase = EasyFormUseCase(EasyFormRepositoryImpl());
    selectedCurrency = easyFormUseCase
        .getCurrency()
        .obs;
  }

  @override
  void dispose() {
    super.dispose();
  }

  //todo add api
  _initCategoryList() {
    TestData.getTestCategories().forEach((element) {
      selectCategoryModelList.add(SelectableModel(element));
    });
  }

  //todo add api
  _initTagList() {
    TestData.getTestTagList().forEach((element) {
      selectTagModelList.add(SelectableModel(element));
    });
  }

  Future<RxList<SelectableModel<BookResDto>>> getAllBookListFromApi() async {
    if (_selectBookModelList == null) {
      BookUseCase bookUseCase = BookUseCase(BookRepositoryImpl());
      var result = await bookUseCase.getBookList();
      RxList<SelectableModel<BookResDto>> returnList = RxList();
      result.when(success: (data) {
        data.accountBookList?.forEach((element) {
          returnList.add(SelectableModel<BookResDto>(element));
        });
      }, failure: (error) {
        logger.d('book load fail: $error');
      });
      _selectBookModelList = returnList;
      return returnList;
    } else {
      return _selectBookModelList!;
    }
  }

  Future<Null> selectDate(
      {required BuildContext context, required DateTime initialDate}) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: initialDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101));
    if (picked != null) {
      selectedDateTime = picked;
      selectedDateTimeText.value =
          DateFormat('yyyy/MM/dd').format(selectedDateTime);
    }
  }

  onBookItemSelected(int index, SelectableModel<BookResDto> item) {
    _selectBookModelList![index] =
    item..isSelected = !_selectBookModelList![index].isSelected;
    selectedBookText.value = _getSelectedNumberText(list: _selectBookModelList!, unitInSingular: 'Book', );
    update();
  }

  onCategoryItemSelected(int index, SelectableModel<TestCategory> item) {
    item.isSelected = !selectCategoryModelList[index].isSelected;
    selectCategoryModelList[index] = item;

    _refreshCategoryTagText(
        shouldUseCategory.value, selectCategoryModelList, selectTagModelList);
    update();
  }

  onTagItemSelected(int index, SelectableModel<TestTag> item) {
    item.isSelected = !selectTagModelList[index].isSelected;
    selectTagModelList[index] = item;

    _refreshCategoryTagText(
        shouldUseCategory.value, selectCategoryModelList, selectTagModelList);
    update();
  }

  onBudgetTypeSelected(bool shouldUseCategoryType) {
    shouldUseCategory.value = shouldUseCategoryType;
    _refreshCategoryTagText(
        shouldUseCategory.value, selectCategoryModelList, selectTagModelList);
    update();
  }

  onSelectAllPress(bool shouldUseCategoryType) {
    if (shouldUseCategoryType) {
      if (isAllSelected(selectCategoryModelList)) {
        //do all clear
        selectCategoryModelList.forEach((element) {
          element.isSelected = false;
        });
      } else {
        //do all selected
        selectCategoryModelList.forEach((element) {
          element.isSelected = true;
        });
      }
    } else {
      if (isAllSelected(selectTagModelList)) {
        //do all clear
        selectTagModelList.forEach((element) {
          element.isSelected = false;
        });
      } else {
        //do all selected
        selectTagModelList.forEach((element) {
          element.isSelected = true;
        });
      }
    }

    selectCategoryModelList.refresh();
    _refreshCategoryTagText(
        shouldUseCategory.value, selectCategoryModelList, selectTagModelList);
    update();
  }

  onSelectCurrency(Currency currency) {
    selectedCurrency.value = currency;
  }

  onSelectRecurrence(int index) {
    selectedRecurrenceIndex.value = index;
    selectedRecurrenceText.value = recurrenceModelList[index].name;
    update();
  }

  _refreshCategoryTagText(bool shouldUseCategoryType,
      RxList<SelectableModel<TestCategory>> categoryList,
      RxList<SelectableModel<TestTag>> tagList) {
    if (shouldUseCategoryType) {
      selectedCategoryTagText.value = _getSelectedNumberText(
          list: selectCategoryModelList,
        unitInSingular: 'category',
        unitInPlural: 'categories'
      );
    } else {
      selectedCategoryTagText.value = _getSelectedNumberText(list: selectTagModelList, unitInSingular: 'Tag');
    }
  }

  String _getSelectedNumberText({
    required RxList<SelectableModel<Object>> list,
    required String unitInSingular,
    String? unitInPlural,
  }) {
    String units = unitInPlural ?? '${unitInSingular}s';
    int selectedCount = list
        .takeWhile((element) => element.isSelected)
        .length;

    if (selectedCount == list.length) {
      return 'All $unitInPlural';
    } else if (selectedCount == 1) {
      return '$selectedCount $unitInSingular Selected';
    } else {
      return '$selectedCount $units Selected';
    }
  }
}
