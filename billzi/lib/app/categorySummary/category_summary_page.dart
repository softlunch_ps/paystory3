import 'package:billzi/app/common/month_selector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:billzi/app/timeline/timeline_list.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/test_data.dart';

class CategorySummaryPage extends StatefulWidget {
  const CategorySummaryPage({Key? key}) : super(key: key);

  @override
  _CategorySummaryPageState createState() => _CategorySummaryPageState();
}

class _CategorySummaryPageState extends State<CategorySummaryPage> {
  MonthSelectorController monthSelectorController =
      MonthSelectorController(monthDateRangeText: 'counter: 0', monthDateText: '');
  int count = 0;

  @override
  void initState() {
    super.initState();
    monthSelectorController.addNextClickListener(() {
      setState(() {

        if (count < 10) {
          count++;
          monthSelectorController.setMonthDateRangeText('counter: $count');
        }


        if (count > 9) {
          monthSelectorController.isNextButtonVisible = false;
        } else {
          monthSelectorController.isNextButtonVisible = true;
        }
        if (count < 1) {
          monthSelectorController.isPreviousButtonVisible = false;
        } else {
          monthSelectorController.isPreviousButtonVisible = true;
        }

      });

    });
    monthSelectorController.addPreviousClickListener(() {
      setState(() {
        if (count > 0) {
          count--;
          monthSelectorController.setMonthDateRangeText('counter: $count');
        }


        if (count > 9) {
          monthSelectorController.isNextButtonVisible = false;
        } else {
          monthSelectorController.isNextButtonVisible = true;
        }
        if (count < 1) {
          monthSelectorController.isPreviousButtonVisible = false;
        } else {
          monthSelectorController.isPreviousButtonVisible = true;
        }

      });
    });
  }

  @override
  void dispose() {
    monthSelectorController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('카테고리 상세')),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              MonthSelector(monthSelectorController: monthSelectorController),
              Container(
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                  child: Text('8월 주요 지출처')),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: TestData.getTestCategoryRanking().length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(right: 10),
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(
                                    color: AppColors.primary,
                                    border: Border.all(
                                      color: AppColors.primary,
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                                child: Center(
                                    child: Text('${index + 1}',
                                        style:
                                            TextStyle(color: Colors.white)))),
                            Expanded(
                              child: Text(
                                '${TestData.getTestCategoryRanking()[index].name}',
                              ),
                            ),
                            Text(
                                '${TestData.getTestCategoryRanking()[index].money.toStringAsFixed(0)}원')
                          ],
                        ),
                      );
                    }),
              ),
              // TimelineList(
              //   shrinkWrap: true,
              //   groupList:
              //       TestData.warpListByDate(TestData.getTestCategoryItems()),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
