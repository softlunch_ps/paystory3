import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/locationSearch/location_search_controller.dart';

class LocationSearchPage extends StatefulWidget {
  const LocationSearchPage({Key? key}) : super(key: key);

  @override
  _LocationSearchPageState createState() => _LocationSearchPageState();
}

class _LocationSearchPageState extends State<LocationSearchPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('위치검색(테스트)'),),
      body: GetBuilder<LocationSearchController>(
        init: Get.find<LocationSearchController>(),
        builder: (LocationSearchController controller) {
          return controller.isLoading.value ? Center(child: CircularProgressIndicator(),) : Column(
            children: [
              // ElevatedButton(onPressed: () {
              //   controller.onClickLocationSearch();
              // }, child: Text('검색 시작')),
              Expanded(
                child: ListView.builder(
                    itemCount: controller.venueDtoList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        child: InkWell(
                          onTap: () {
                            controller.onClickLocationItem(controller.venueDtoList[index]);
                          },
                          radius: 10,
                          child: Container(
                            margin: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                Text(controller.venueDtoList.value[index].name!),

                                Text(controller.venueDtoList.value[index].categories!.first.name!),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              )
            ],
          );
        },
      ),
    );
  }
}
