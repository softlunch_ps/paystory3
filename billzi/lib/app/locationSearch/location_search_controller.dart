import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:billzi/data/dto/fourSquare/four_square_venue_dto.dart';
import 'package:billzi/network/fourSquare/four_square_client.dart';
import 'package:billzi/utils/four_square_utils.dart';
import 'package:billzi/utils/location_utils.dart';

class LocationSearchController extends GetxController {
  RxBool isGetCurrentPosition = false.obs;
  Position? currentPosition;
  RxList<FourSquareVenueDto> venueDtoList = RxList();
  RxBool isLoading = false.obs;
  @override
  void onInit() async {
    super.onInit();
    Position? position = Get.arguments;
    if (position == null) {
      fetchPosition().then((Position position) {
        isGetCurrentPosition = true.obs;
        currentPosition = position;
        update();
      }).catchError((error) {
        print(error);
      });
    } else {
      currentPosition = position;
    }
    await onClickLocationSearch();
  }

  //todo test
  onClickLocationSearch() async {
    if (currentPosition != null) {
      isLoading.value = true;
      update();
      List items = (await FourSquareHttpClient.fourSquareAuthApi
          .getSearch(
              latitudeLongitudeToString(
                  longitude: currentPosition!.longitude,
                  latitude: currentPosition!.latitude))).response!['venues'];

      venueDtoList.value = items.map((item) => FourSquareVenueDto.fromJson(item)).toList();
      isLoading.value = false;
      update();
    }
  }

  onClickLocationItem(FourSquareVenueDto fourSquareVenueDto) {
    Get.back(result: fourSquareVenueDto);
  }
}
