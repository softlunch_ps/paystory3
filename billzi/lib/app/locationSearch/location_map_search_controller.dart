import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:billzi/data/dto/fourSquare/four_square_venue_dto.dart';
import 'package:billzi/network/fourSquare/four_square_client.dart';
import 'package:billzi/utils/four_square_utils.dart';
import 'package:billzi/utils/location_utils.dart';

class LocationMapSearchController extends GetxController {

  RxBool isGetCurrentPosition = false.obs;
  Position? currentPosition;

  RxList<FourSquareVenueDto> venueDtoList = RxList();
  RxBool isLoading = false.obs;
  RxDouble selectedLatitude = RxDouble(0.0);
  RxDouble selectedLongitude = RxDouble(0.0);
  final Set<Marker> markers = {};

  @override
  void onInit() async {
    super.onInit();
    if (currentPosition == null) {
      currentPosition = await fetchPosition();
      await _onClickLocationSearch(
          lat: currentPosition!.latitude, lng: currentPosition!.longitude);
      isGetCurrentPosition = true.obs;
    }
  }

  //todo test
  _onClickLocationSearch({required double lng, required double lat}) async {

    if (!isLoading.value) {
      isLoading.value = true;
      update();

      selectedLongitude.value = lng;
      selectedLatitude.value = lat;

      List items = (await FourSquareHttpClient.fourSquareAuthApi
          .getSearch(
          latitudeLongitudeToString(
              longitude: lng,
              latitude: lat))).response!['venues'];

      venueDtoList.value = items.map((item) => FourSquareVenueDto.fromJson(item)).toList();

      //move marker
      if (markers.length >= 1) {
        markers.clear();
      }
      //reset position

      markers.add(Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId('1'),
        position: LatLng(selectedLatitude.value, selectedLongitude.value),
        infoWindow: InfoWindow(
          title: 'address',
          //  snippet: '5 Star Rating',
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));

      isLoading.value = false;
      update();
    }

  }

  Future onClickLocationItem(FourSquareVenueDto fourSquareVenueDto) async {
    if (fourSquareVenueDto.location != null) {
      await _onClickLocationSearch(lat: fourSquareVenueDto.location!.lat!, lng: fourSquareVenueDto.location!.lng!);
    }
  }



  Future onAddMarkerButtonPressed(LatLng latlang) async {
    await _onClickLocationSearch(lat: latlang.latitude, lng: latlang.longitude);
  }
}
