import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:billzi/app/locationSearch/location_map_search_controller.dart';

class LocationMapSearchPage extends StatefulWidget {
  const LocationMapSearchPage({Key? key}) : super(key: key);

  @override
  _LocationMapSearchPageState createState() => _LocationMapSearchPageState();
}

class _LocationMapSearchPageState extends State<LocationMapSearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('위치검색(테스트)'),
      ),
      body: GetBuilder<LocationMapSearchController>(
        init: Get.find<LocationMapSearchController>(),
        builder: (LocationMapSearchController controller) {
          return Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.5,
                child: controller.isGetCurrentPosition.value
                    ? GoogleMap(
                        mapType: MapType.normal,
                        compassEnabled: false,
                        mapToolbarEnabled: false,
                        rotateGesturesEnabled: false,
                        tiltGesturesEnabled: false,
                        markers: Set.from(controller.markers),
                        initialCameraPosition: CameraPosition(
                          target: LatLng(controller.currentPosition!.longitude,
                              controller.currentPosition!.latitude),
                          zoom: 15,
                        ),
                        onMapCreated:
                            (GoogleMapController googleMapController) {
                              googleMapController.animateCamera(CameraUpdate.newLatLngZoom(LatLng(controller.selectedLatitude.value, controller.selectedLongitude.value), 14));

                          //_controller.complete(controller);
                        },
                        onTap: (LatLng latLang) {
                          controller.onAddMarkerButtonPressed(latLang);

                        },
                      )
                    : Container(),
              ),
              controller.isGetCurrentPosition.value
                  ? Container(
                      child: Column(
                        children: [
                          Text('lat : ${controller.selectedLatitude.value}'),
                          Text(
                              'lng : ${controller.selectedLongitude.value}'),
                        ],
                      ),
                    )
                  : Container(),
              Expanded(
                      child: controller.isLoading.value
                          ? Center(
                        child: CircularProgressIndicator(),
                      )
                          : ListView.builder(
                          itemCount: controller.venueDtoList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              child: InkWell(
                                onTap: () {
                                  controller.onClickLocationItem(
                                      controller.venueDtoList[index]);
                                },
                                radius: 10,
                                child: Container(
                                  margin: EdgeInsets.all(10),
                                  child: Column(
                                    children: [
                                      Text(controller
                                          .venueDtoList.value[index].name ?? 'no name'),
                                      controller.venueDtoList.value[index]
                                          .categories!.length > 0 ? Text(controller.venueDtoList.value[index]
                                          .categories!.first.name ?? '') : Container(),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                    )
            ],
          );
        },
      ),
    );
  }
}
