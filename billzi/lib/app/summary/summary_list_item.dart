import 'package:flutter/material.dart';
import 'package:billzi/utils/test_data.dart';

typedef void OnSummaryItemClickListener(int index);

class SummaryListItem extends StatelessWidget {
  final TestSummaryItem testSummaryItem;
  final int index;
  final double totalMoney;
  final OnSummaryItemClickListener onSummaryItemClickListener;

  const SummaryListItem(
      {Key? key, required this.testSummaryItem, required this.totalMoney, required this.onSummaryItemClickListener, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onSummaryItemClickListener.call(index);
      },
      child: Container(
          height: 56,
          child: Row(
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  child: Icon(testSummaryItem.icon)),
              Expanded(child: Text(testSummaryItem.name)),
              Container(
                  width: 70,
                  child:
                      Text(testSummaryItem.getMoneyPercentWithTotal(totalMoney))),
              Container(
                margin: EdgeInsets.only(right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text('총 ${testSummaryItem.count}건'),
                    Text('${testSummaryItem.money.toStringAsFixed(0)}원'),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
