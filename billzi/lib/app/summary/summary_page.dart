import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/home/home_controller.dart';
import 'package:billzi/app/summary/summary_category_page.dart';
import 'package:billzi/app/summaryPayment/summary_payment_type_page.dart';
import 'package:billzi/route/route_name.dart';

class SummaryPage extends StatelessWidget {
  const SummaryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: GestureDetector(
          onTap: () {
            Get.toNamed(Routes.BookSelectPage);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Obx(
                () => Text(
                  '${Get.find<HomeController>().currentBookName.value}',
                ),
              ),
              Icon(
                Icons.chevron_right,
              ),
            ],
          ),
        ),
        actions: <Widget>[
          Obx(() {
            IconData indicator =
                Get.find<HomeController>().isNewInvitationAvailable.value
                    ? Icons.notifications_active_outlined
                    : Icons.notifications_none_outlined;
            return IconButton(
              icon: Icon(indicator),
              tooltip: 'New Invitations',
              onPressed: () {
                //Todo: alarm list page
                //Get.find<HomeController>().isNewInvitationAvailable.toggle();
                //Todo: test
                Get.toNamed(Routes.InviteAcceptPage);
              },
            );
          }),
        ],
      ),
      body: PageView(
        children: [
          SummaryCategoryPage(),
          SummaryPaymentTypePage(),
        ],
      ),
    );
  }
}
