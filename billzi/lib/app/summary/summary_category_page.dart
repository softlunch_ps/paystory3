import 'package:billzi/app/common/month_selector.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/summary/summary_list_item.dart';
import 'package:billzi/app/summary/summary_pie_chart.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/test_data.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class SummaryCategoryPage extends StatelessWidget {
  SummaryCategoryPage({Key? key}) : super(key: key);

  final MonthSelectorController monthSelectorController = MonthSelectorController(
      monthDateRangeText: '(8.1 - 8.31)', monthDateText: '2021년 8월');

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          MonthSelector(monthSelectorController: monthSelectorController),
          Container(
            margin:
            EdgeInsets.only(top: 20, bottom: 20, left: 10, right: 10),
            child: Card(
              child: InkWell(
                radius: 5,
                onTap: () {
                  Get.toNamed(Routes.BudgetPage);
                },
                child: Container(
                  margin: EdgeInsets.only(
                      top: 10, bottom: 10, left: 10, right: 10),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('한달예산이'),
                              Text('286,500원 넘었어요'),
                            ],
                          ),
                          TextButton(
                              onPressed: () {
                                Get.toNamed(Routes.BudgetPage);
                              },
                              child: Text('상세보기'))
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.all(15.0),
                        child: LinearPercentIndicator(
                          animation: true,
                          lineHeight: 20.0,
                          animationDuration: 2000,
                          percent: 0.9,
                          center: Text("90.0%"),
                          linearStrokeCap: LinearStrokeCap.roundAll,
                          progressColor: AppColors.primary,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('예산'),
                          Text('지출'),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('100,000원'),
                          Text('386,500원'),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
              child: SummaryPieChart()),
          Container(
            margin: EdgeInsets.only(bottom: 80),
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: TestData.getTestSummaryItems().length,
                itemBuilder: (BuildContext context, int index) {
                  return SummaryListItem(
                    index: index,
                    totalMoney: 1000000,
                    testSummaryItem: TestData.getTestSummaryItems()[index],
                    onSummaryItemClickListener: (int index) {
                      Get.toNamed(Routes.CategorySummaryPage);
                    },
                  );
                }),
          )
        ],
      ),
    );
  }
}
