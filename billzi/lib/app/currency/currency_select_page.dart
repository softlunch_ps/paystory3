import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/app/common/currency/currency_list_view.dart';

class CurrencySelectPage extends StatelessWidget {
  const CurrencySelectPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('통화선택'),
        centerTitle: true,
      ),
      body: Container(
        child: CurrencyListView(
          showFlag: true,
          showCurrencyName: true,
          showCurrencyCode: true,
          onSelect: (Currency currency) {
            Get.back(result: currency);
            // print('Select currency: ${currency.name}');
          },
          // favorite: ['SEK'],
        ),
      ),
    );
  }
}
