import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:billzi/app/multiBook/controller/book_info_mixins.dart';
import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';
import 'package:billzi/data/dto/request/book/edit_book_req_dto.dart';
import 'package:billzi/data/dto/response/book/book_member_res_dto.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/repositories/book_repo_impl.dart';
import 'package:billzi/domain/usecase/book_usecase.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/utils/dialog_helper.dart';
import 'package:billzi/utils/extensions.dart';
import 'package:billzi/utils/logger.dart';

class BookSettingPageController extends GetxController with BookInfoMixIn {
  late BookResDto book;
  late BookUseCase bookUseCase;

  var isEditMode = false.obs;

  get isReadOnly => !isEditMode.value || !hasAdminRight || book.isClosed;
  get optionText => isEditMode.isTrue ? 'save' : 'edit';
  get terminateConfirmText => book.isClosed
      ? 'do you want to delete this book?'
      : 'do you want to close this book?';
  get shouldShowTerminateBookBtn =>
      book.isClosed || (isEditMode.value && hasAdminRight);

  bool get hasAdminRight => memberList
      .firstWhere(
        (element) => element.userSeq == UserManager().getUserSeq(),
      )
      .isAllowedChangeGrade;

  MemberGrade get myCurrentGrade =>
      memberList
          .firstWhere(
            (element) => element.userSeq == UserManager().getUserSeq(),
          )
          .grade ??
      MemberGrade.NONMEMBER;

  RxList<BookMemberResDto> memberList = <BookMemberResDto>[].obs;
  TextEditingController myNickNameController = TextEditingController();
  TextEditingController myNickNameControllerForEdit = TextEditingController();

  @override
  void onInit() async {
    super.onInit();
    bookUseCase = BookUseCase(BookRepositoryImpl());

    book = Get.arguments;
    currentSelectedColor.value = book.color!.toColor();
    memberList.value = book.userList ?? <BookMemberResDto>[];
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  onUpdateCurrentSelectedColor(Color color) {
    super.onUpdateCurrentSelectedColor(color);
    update();
  }

  toggleEditMode() {
    if (isEditMode.isTrue) {
      updateBook();
      logger.d('current book is updated with $book');
    }
    isEditMode.toggle();
    update();
  }

  bool shouldAllowGradeChange(BookMemberResDto member) {
    return isEditMode.isTrue &&
        hasAdminRight &&
        (member.grade?.index ?? MemberGrade.NONMEMBER.index) <=
            myCurrentGrade.index;
  }

  Future updateBook({
    bool shouldCloseBook = false,
  }) async {
    if (!hasAdminRight) return;

    // update a data in controller by fetching a form data
    formKey.currentState?.save();

    // update a local book first
    book = book.copyWith(
      accountBookName: currentName,
      accountBookDesc: currentDescription,
      color: currentSelectedColor.value.toHex(),
      useYn: shouldCloseBook ? 'N' : 'Y',
      endDate: shouldCloseBook ? DateTime.now() : book.endDate,
    );

    BookRepositoryImpl bookRepositoryImpl = BookRepositoryImpl();
    await bookRepositoryImpl.editBook(
      EditBookReqDto(
        accountBookSeq: book.accountBookSeq,
        accountBookName: book.accountBookName,
        accountBookDesc: book.accountBookDesc,
        startDate: book.startDate,
        endDate: book.endDate,
        imgURL: book.imgURL,
        color: book.color,
        useYn: book.useYn,
      ),
    );
  }

  Future deleteBook() async {
    if (!hasAdminRight) return;

    BookRepositoryImpl bookRepositoryImpl = BookRepositoryImpl();
    await bookRepositoryImpl.detachBook(
        UserManager().getUserSeq()!, book.accountBookSeq);
  }

  showTerminateBookConfirmPopup() {
    Get.focusScope?.unfocus();
    // TODO: consider making a generic template for popup if a relevant design is available
    var shouldDelete = book.isClosed;
    var bodyWidget = Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Do you want to ${shouldDelete ? 'delete' : 'close'}')
            .marginOnly(bottom: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 10,
              backgroundImage: NetworkImage(
                  'https://www.clipartmax.com/png/full/186-1862439_blob-emoji-template.png'),
            ).marginOnly(right: 10),
            Text(book.accountBookName),
            Text('?'),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              shouldDelete
                  ? '* It will be deleted only in your book list.'
                  : '* It will be closed all the member\'s book list.',
              style: Get.textTheme.button?.copyWith(fontSize: 11),
            ).marginOnly(top: 10),
            Text(
              shouldDelete
                  ? '* It can not be restored!'
                  : '* It can not be reopened!',
              style: Get.textTheme.button?.copyWith(fontSize: 11),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'cancel',
                  style: Get.textTheme.button?.copyWith(color: Colors.white),
                ),
              ).marginOnly(right: 10),
            ),
            GestureDetector(
              onTap: () async {
                if (shouldDelete) {
                  await deleteBook();
                } else {
                  await updateBook(shouldCloseBook: true);
                }
                // dismiss all popup and exit the current screen
                Get.until((route) => !(route is PopupRoute));
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  shouldDelete ? 'delete' : 'close',
                  style: Get.textTheme.button?.copyWith(color: Colors.white),
                ),
              ).marginOnly(left: 10),
            ),
          ],
        ).marginOnly(top: 10),
      ],
    );
    showPopup(bodyWidget);
  }

  // TODO : refactor confirm dialog widget code to make it reusable
  showExitConfirmDialog(BookMemberResDto member) {
    Get.focusScope?.unfocus();
    var bodyWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Do you want to exit').marginOnly(bottom: 10),
            Text('from').marginOnly(bottom: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 10,
                  backgroundImage: NetworkImage(
                      'https://www.clipartmax.com/png/full/186-1862439_blob-emoji-template.png'),
                ).marginOnly(right: 10),
                Text(book.accountBookName),
                Text(' ?'),
              ],
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'cancel',
                  style: Get.textTheme.button?.copyWith(color: Colors.white),
                ),
              ).marginOnly(right: 10),
            ),
            GestureDetector(
              onTap: () {
                _removeMemberFromCurrentBook(member, errorMsg: 'Exit fails');
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'exit',
                  style: Get.textTheme.button?.copyWith(color: Colors.white),
                ),
              ).marginOnly(left: 10),
            ),
          ],
        ).marginOnly(top: 10),
      ],
    );
    showPopup(bodyWidget);
  }

  showExpelConfirmDialog(BookMemberResDto member) {
    Get.focusScope?.unfocus();
    var bodyWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Do you want to expel').marginOnly(bottom: 10),
            Text(member.userNickname ?? ''),
            Text(member.email ?? '').marginOnly(bottom: 10),
            Text('from').marginOnly(bottom: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 10,
                  backgroundImage: NetworkImage(
                      'https://www.clipartmax.com/png/full/186-1862439_blob-emoji-template.png'),
                ).marginOnly(right: 10),
                Text(book.accountBookName),
                Text(' ?'),
              ],
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'cancel',
                  style: Get.textTheme.button?.copyWith(color: Colors.white),
                ),
              ).marginOnly(right: 10),
            ),
            GestureDetector(
              onTap: () {
                _removeMemberFromCurrentBook(member, errorMsg: 'Expel fails');
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'expel',
                  style: Get.textTheme.button?.copyWith(color: Colors.white),
                ),
              ).marginOnly(left: 10),
            ),
          ],
        ).marginOnly(top: 10),
      ],
    );
    showPopup(bodyWidget);
  }

  showAdminRightInfoDialog() {
    Get.focusScope?.unfocus();
    var bodyWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Admin Authorization',
            ).marginOnly(bottom: 10),
            Row(
              children: [
                Icon(Icons.check).marginOnly(right: 10),
                Text('invite new members'),
              ],
            ),
            Row(
              children: [
                Icon(Icons.check).marginOnly(right: 10),
                Text('accept/reject members'),
              ],
            ),
            Row(
              children: [
                Icon(Icons.check).marginOnly(right: 10),
                Text('edit member\'s authorization'),
              ],
            ),
            Row(
              children: [
                Icon(Icons.check).marginOnly(right: 10),
                Text('close the book'),
              ],
            ),
            Text('At least one member should be an admin').marginOnly(top: 10),
          ],
        ),
      ],
    );
    showPopup(bodyWidget);
  }

  bool isMyself(BookMemberResDto member) {
    return member.userSeq == UserManager().getUserSeq();
  }

  bool isAllowedChangeProfile(BookMemberResDto member) {
    return isMyself(member) && isEditMode.isTrue;
  }

  updateExistingMember(BookMemberResDto newMember) async {
    // update local first and revert when server update fails -> optimistic update
    var foundIndex = memberList
        .indexWhere((element) => element.userSeq == newMember.userSeq);
    BookMemberResDto? prevMember;
    if (foundIndex != -1) {
      prevMember = memberList[foundIndex];
      memberList[foundIndex] = newMember;
    }

    var result = await bookUseCase.editBook(
      EditBookReqDto(
        accountBookSeq: book.accountBookSeq,
        userInfo: CreateBookMemberDto(
          targetUserSeq: newMember.userSeq,
          userNickname: newMember.userNickname,
          userProfileImgUrl: newMember.userProfileImgUrl,
          role: newMember.grade,
        ),
      ),
    );

    result.when(
        success: (data) {},
        failure: (error) {
          // revert the local change when server update fails
          if (prevMember != null) {
            memberList[foundIndex] = prevMember;
            Fluttertoast.showToast(
                msg: error.resultMessage,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.black,
                textColor: Colors.white,
                fontSize: 16.0);
          }
        });
  }

  _removeMemberFromCurrentBook(BookMemberResDto member,
      {required String errorMsg}) async {
    // optimistic update
    int prevIndex =
        memberList.indexWhere((element) => element.userSeq == member.userSeq);
    memberList.removeAt(prevIndex);

    var result =
        await bookUseCase.detachBook(member.userSeq, book.accountBookSeq);

    result.when(
        success: (data) {},
        failure: (error) {
          memberList.insert(prevIndex, member);
          Fluttertoast.showToast(
              msg: error.resultMessage,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.black,
              textColor: Colors.white,
              fontSize: 16.0);
        });
  }
}
