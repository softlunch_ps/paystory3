import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/home/home_controller.dart';
import 'package:billzi/data/dto/request/book/reorder_book_list_req_dto.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/repositories/book_repo_impl.dart';
import 'package:billzi/domain/usecase/book_usecase.dart';
import 'package:billzi/utils/dialog_helper.dart';
import 'package:billzi/utils/logger.dart';

class BookPageController extends GetxController {
  RxList<BookResDto> onGoingBookItemList = RxList.empty();
  RxList<BookResDto> closedBookItemList = RxList.empty();
  late BookUseCase bookUseCase;
  bool hasChangedOrder = false;
  bool get shouldShowConfirmPopup => hasChangedOrder;

  @override
  void onInit() async {
    super.onInit();
    bookUseCase = BookUseCase(BookRepositoryImpl());
    _fetchBookList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _fetchBookList() async {
    var result = await bookUseCase.getBookList();
    result.when(
        success: (data) {
          var activeBooks = List<BookResDto>.empty(growable: true);
          var closedBooks = List<BookResDto>.empty(growable: true);
          for (BookResDto item in data.accountBookList!) {
            if (item.isClosed) {
              closedBooks.add(item);
            } else {
              activeBooks.add(item);
            }
          }
          onGoingBookItemList.value = activeBooks;
          closedBookItemList.value = closedBooks;
        },
        failure: (error) {});
  }

  reorderTestBookItem({
    required bool isClosedOnly,
    required List<BookResDto> reorderedList,
  }) {
    hasChangedOrder = true;
    if (isClosedOnly) {
      reorderItem(closedBookItemList, reorderedList);
    } else {
      reorderItem(onGoingBookItemList, reorderedList);
    }
    update();
  }

  reorderItem<T>(List<T> oldList, List<T> reorderedList) {
    oldList
      ..clear()
      ..addAll(reorderedList);
  }

  Future refreshBook() async {
    var progress = LoadingModalView()..show();
    await _fetchBookList();
    // make sure to update the home header area with the current primary book name
    if (onGoingBookItemList.isNotEmpty && Get.isRegistered<HomeController>()) {
      Get.find<HomeController>()
          .updateCurrentPrimaryBookName(onGoingBookItemList[0].accountBookName);
    }
    progress.pop();
  }

  Future updateBookListOrder() async {
    num order = 0;
    var onGoing = List.from(onGoingBookItemList)
        .map<ReorderBooksReqDto>(
          (element) => ReorderBooksReqDto(
            accountBookSeq: element.accountBookSeq,
            orderNo: order++,
          ),
        )
        .toList();

    order = 0;
    var closed = List.from(closedBookItemList)
        .map<ReorderBooksReqDto>(
          (element) => ReorderBooksReqDto(
            accountBookSeq: element.accountBookSeq,
            orderNo: order++,
          ),
        )
        .toList();

    var isSuccess =
        await bookUseCase.updateReorderedBooksList(onGoing + closed);
    logger.d('reorder isSuccess : $isSuccess');
  }

  showSaveOrderConfirmDialog() {
    var bodyWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Do you want to save the order?').marginOnly(bottom: 10),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'cancel',
                  style: Get.textTheme.button?.copyWith(color: Colors.white),
                ),
              ).marginOnly(right: 10),
            ),
            GestureDetector(
              onTap: () async {
                await updateBookListOrder();
                await refreshBook();
                Get.close(2);
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'save',
                  style: Get.textTheme.button?.copyWith(color: Colors.white),
                ),
              ).marginOnly(left: 10),
            ),
          ],
        ).marginOnly(top: 10),
      ],
    );
    showPopup(bodyWidget);
  }
}
