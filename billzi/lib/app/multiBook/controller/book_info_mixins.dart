import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/logger.dart';
import 'package:billzi/utils/pick_helper.dart';

mixin BookInfoMixIn {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  var currentSelectedColor = defaultBookColors[0].obs;
  get foregroundColor => useWhiteForeground(currentSelectedColor.value)
      ? Colors.white
      : Colors.black;
  get defaultBookName =>
      "${FirebaseAuth.instance.currentUser?.displayName ?? "Anonymous"}\'s book";

  String currentName = '';
  String currentDescription = '';

  final emptyValueValidator = (String? value, String messageOnInvalidCase) {
    if (value?.isEmpty ?? true) {
      return messageOnInvalidCase;
    } else {
      return null;
    }
  };

  onUpdateCurrentSelectedColor(Color color) {
    currentSelectedColor.value = color;
    logger.d(color);
  }

  showBookColorPicker() {
    Get.focusScope?.unfocus();
    showColorPicker(
      onUpdateCurrentSelectedColor,
      currentColor: currentSelectedColor.value,
      title: 'Please choose a color for presenting an account book',
    );
  }
}
