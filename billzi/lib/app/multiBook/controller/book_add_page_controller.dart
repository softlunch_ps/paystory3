import 'dart:math';
import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/common/error_handler.dart';
import 'package:billzi/app/multiBook/controller/book_info_mixins.dart';
import 'package:billzi/data/dto/request/book/create_book_member_dto.dart';
import 'package:billzi/data/dto/request/book/create_book_req_dto.dart';
import 'package:billzi/data/repositories/book_repo_impl.dart';
import 'package:billzi/domain/usecase/book_usecase.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/extensions.dart';
import 'package:billzi/utils/logger.dart';

class BookAddPageController extends GetxController
    with BookInfoMixIn, ErrorHandlerMixIn {
  late BookUseCase bookUseCase;

  // TODO: make sure to get a nick name in book create UI
  String get currentUserName =>
      FirebaseAuth.instance.currentUser?.displayName ??
      FirebaseAuth.instance.currentUser?.email ??
      'nick name';

  @override
  void onInit() {
    super.onInit();
    currentSelectedColor.value =
        defaultBookColors[Random().nextInt(defaultBookColors.length)];
    bookUseCase = BookUseCase(BookRepositoryImpl());
  }

  @override
  void onClose() {
    super.onClose();
  }

  onUpdateCurrentSelectedColor(Color color) {
    super.onUpdateCurrentSelectedColor(color);
    update();
    logger.d(color);
  }

  onSaveBook() async {
    assert(UserManager().getUserSeq() != null);
    var result = await bookUseCase.createBook(
      CreateBookReqDto(
        accountBookName: currentName,
        accountBookDesc: currentDescription,
        color: currentSelectedColor.value.toHex(),
        startDate: DateTime.now().toUtc(),
        endDate: DateTime.now().toUtc(),
        userInfo: CreateBookMemberDto(
          userNickname: currentUserName,
          userProfileImgUrl: '😃',
        ),
      ),
    );

    result.when(
      success: (response) {
        logger.d('create book success!');
      },
      failure: (error) {
        handleErrors(error.resultCode, fallBackErrorHandler: (errorCode) {
          // handle book add scenario specific error cases
          // it can ba grouped in to mixin so fallback error handles can be shared b/w those book controllers
        });
      },
    );
  }
}
