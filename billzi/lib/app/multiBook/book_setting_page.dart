import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:get/get.dart';
import 'package:billzi/app/common/emoji_picker.dart';
import 'package:billzi/app/multiBook/controller/book_setting_page_controller.dart';
import 'package:billzi/data/dto/response/book/book_member_res_dto.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/extensions.dart';
import 'package:billzi/utils/logger.dart';

class BookSettingPage extends GetView<BookSettingPageController> {
  Widget _buildHeaderWidget() {
    return Container(
      decoration: BoxDecoration(
        color: controller.book.color!.toColor(),
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            radius: 15,
            backgroundImage: NetworkImage(
                'https://www.clipartmax.com/png/full/186-1862439_blob-emoji-template.png'),
          ).marginOnly(
            right: 10,
          ),
          Text(
            controller.book.accountBookName,
            style: Get.textTheme.button?.copyWith(
              color: controller.foregroundColor,
            ),
          ),
        ],
      ).marginSymmetric(
        vertical: 20,
      ),
    ).marginSymmetric(
      horizontal: 10,
      vertical: 10,
    );
  }

  Widget _buildUserGradeWidget(BookMemberResDto member) {
    return controller.shouldAllowGradeChange(member)
        ? DropdownButton<MemberGrade>(
            value: member.grade,
            onChanged: (MemberGrade? newGrade) {
              if (newGrade != null) {
                controller.updateExistingMember(
                  member.copyWith(grade: newGrade),
                );
              }
            },
            style: Get.theme.textTheme.button?.copyWith(fontSize: 10),
            items:
                member.allowedMemberGrades.map<DropdownMenuItem<MemberGrade>>(
              (MemberGrade value) {
                return DropdownMenuItem<MemberGrade>(
                  value: value,
                  child: Text(
                    value.toStringValue(),
                  ),
                );
              },
            ).toList(),
          )
        : OutlinedButton(
            style: OutlinedButton.styleFrom(
              padding: EdgeInsets.zero,
              minimumSize: Size(44, 36),
            ),
            onPressed: null,
            child: Text(
              member.memberStatus,
              style: Get.theme.textTheme.button?.copyWith(fontSize: 10),
            ),
          );
  }

  Widget _buildActionButton(BookMemberResDto member) {
    if (controller.isEditMode.isFalse) {
      return SizedBox.shrink();
      // leaving it as a commented in case the requirement comes back at a later time
      // if (member.grade == MemberGrade.pre_member) {
      //   return ElevatedButton(
      //     onPressed: () {
      //       controller.showJoinBookAllowDialog(member);
      //     },
      //     child: Text('action'),
      //     style: ButtonStyle(
      //       padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.zero),
      //       shape: MaterialStateProperty.all<OutlinedBorder>(
      //         RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      //       ),
      //     ),
      //   );
      // }
    } else {
      // myself => show exit button
      if (controller.isMyself(member)) {
        return OutlinedButton(
          style: OutlinedButton.styleFrom(
            padding: EdgeInsets.zero,
            minimumSize: Size(44, 36),
          ),
          onPressed: () {
            controller.showExitConfirmDialog(member);
          },
          child: Text(
            'exit',
            style: Get.theme.textTheme.button?.copyWith(fontSize: 10),
          ),
        );
      }

      if (controller.shouldAllowGradeChange(member)) {
        return OutlinedButton(
          style: OutlinedButton.styleFrom(
            padding: EdgeInsets.zero,
            minimumSize: Size(44, 36),
          ),
          onPressed: () {
            controller.showExpelConfirmDialog(member);
          },
          child: Text(
            'expel',
            style: Get.theme.textTheme.button?.copyWith(fontSize: 10),
          ),
        );
      }
    }
    return Container();
  }

  Widget _buildMemberRightManage() {
    return DefaultTextStyle(
      style: TextStyle(fontSize: 13, color: Get.textTheme.button!.color),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Member', style: Get.textTheme.headline5)
                  .marginOnly(bottom: 5),
              IconButton(
                onPressed: controller.showAdminRightInfoDialog,
                icon: Icon(Icons.error_outline),
              )
            ],
          ),
          Container(
            width: double.infinity,
            child: CustomPaint(
              painter: _DashedLinePainter(color: Get.theme.dividerColor),
            ),
          ),
          Obx(
            () => SizedBox(
              height: 150,
              child: ListView.builder(
                itemCount: controller.memberList.length,
                itemBuilder: (BuildContext context, int index) {
                  var item = controller.memberList[index];
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      _buildUserGradeWidget(item),
                      GestureDetector(
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: controller.isAllowedChangeProfile(item)
                                  ? () async {
                                      await openCategoryBottomSheet(
                                        onEmojiSelected: (category, emoji) {
                                          controller.updateExistingMember(
                                            item.copyWith(
                                                userProfileImgUrl: emoji.emoji),
                                          );
                                        },
                                      );
                                    }
                                  : null,
                              child: Text(
                                item.userProfileImgUrl ?? '',
                                style: TextStyle(fontSize: 20),
                              ).marginOnly(
                                right: 2,
                              ),
                            ),
                            controller.isAllowedChangeProfile(item)
                                ? IntrinsicWidth(
                                    child: TextField(
                                      enableInteractiveSelection: false,
                                      showCursor: false,
                                      enabled: controller
                                          .isAllowedChangeProfile(item),
                                      controller:
                                          controller.myNickNameController
                                            ..text = item.userNickname ?? '',
                                      decoration: null,
                                      onSubmitted: (text) {
                                        logger.d('onSubmitted');
                                        controller.updateExistingMember(
                                            item.copyWith(userNickname: text));
                                      },
                                    ).marginOnly(right: 10),
                                  )
                                : Text(item.userNickname ?? '')
                                    .marginOnly(right: 10),
                          ],
                        ),
                      ),
                      _buildActionButton(item),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ).paddingSymmetric(horizontal: 10).marginOnly(top: 10),
    );
  }

  Widget _buildBasicInformation() {
    return Form(
      key: controller.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Basic Information', style: Get.textTheme.headline5)
              .marginOnly(bottom: 5),
          Container(
            width: double.infinity,
            child: CustomPaint(
              painter: _DashedLinePainter(color: Get.theme.dividerColor),
            ),
          ),
          Container(
            child: TextFormField(
              enabled: !controller.isReadOnly,
              initialValue: controller.book.accountBookName,
              maxLength: 40,
              decoration: const InputDecoration(
                hintText: 'Please fill out the book name',
                labelText: 'Name',
              ),
              onSaved: (String? value) {
                if (value?.isEmpty ?? true) {
                  value = controller.defaultBookName;
                }
                controller.currentName = value!;
              },
              // validator: (value) {
              //   return controller.emptyValueValidator(
              //       value, 'enter a book name!');
              // },
            ),
          ),
          Container(
            child: TextFormField(
              enabled: !controller.isReadOnly,
              initialValue: controller.book.accountBookDesc,
              maxLength: 100,
              decoration: const InputDecoration(
                hintText: 'Please fill out the book description.',
                labelText: 'Description',
              ),
              onSaved: (String? value) {
                controller.currentDescription = value ?? '';
              },
              // validator: (value) {
              //   return controller.emptyValueValidator(
              //       value, 'enter a book description!');
              // },
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(
              vertical: 20,
            ),
            child: Row(
              children: [
                ElevatedButton(
                  onPressed: controller.isReadOnly
                      ? null
                      : controller.showBookColorPicker,
                  child: SizedBox(
                    width: 60,
                    height: 60,
                    child: Container(),
                  ),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<OutlinedBorder>(
                      CircleBorder(),
                    ),
                    backgroundColor: MaterialStateProperty.all<Color>(
                        controller.currentSelectedColor.value),
                  ),
                ),
                Text('Theme'),
              ],
            ),
          ),
        ],
      ),
    ).paddingSymmetric(horizontal: 10).marginOnly(top: 20);
  }

  _maybeBuildTerminateButton() {
    if (controller.shouldShowTerminateBookBtn) {
      return ElevatedButton(
        onPressed: controller.showTerminateBookConfirmPopup,
        child: Text(controller.terminateConfirmText),
      ).marginSymmetric(
        vertical: 20,
        horizontal: 50,
      );
    } else {
      return SizedBox.shrink();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: Get.find<BookSettingPageController>(),
      builder: (BookSettingPageController controller) {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text('Book setting'),
            actions: [
              if (!controller.book.isClosed)
                TextButton(
                  style: TextButton.styleFrom(
                    primary: AppColors.textWhite1,
                  ),
                  onPressed: () {
                    controller.toggleEditMode();
                  },
                  child: Text(controller.optionText),
                )
            ],
          ),
          // bottomSheet:
          //     KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
          //   if (isKeyboardVisible) {
          //     controller.myNickNameControllerForEdit.text =
          //         controller.myNickNameController.text;
          //   }
          //   return isKeyboardVisible
          //       ? Container(
          //           decoration: BoxDecoration(
          //             borderRadius: BorderRadius.only(
          //               topLeft: Radius.circular(20),
          //               topRight: Radius.circular(20),
          //             ),
          //             border: Border.all(color: Colors.black),
          //           ),
          //           child: Column(
          //             mainAxisSize: MainAxisSize.min,
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             children: [
          //               Align(
          //                 alignment: Alignment.centerRight,
          //                 child: GestureDetector(
          //                   onTap: () {
          //                     Get.focusScope?.unfocus();
          //                   },
          //                   child: Text('X'),
          //                 ),
          //               ).marginOnly(right: 10),
          //               Text('Change the nick name').marginOnly(bottom: 10),
          //               TextField(
          //                 controller: controller.myNickNameControllerForEdit,
          //                 decoration: InputDecoration(
          //                   border: OutlineInputBorder(),
          //                   hintText: 'Enter your input here',
          //                 ),
          //               ).marginOnly(bottom: 10),
          //               Align(
          //                 alignment: Alignment.center,
          //                 child: ElevatedButton(
          //                   onPressed: () {
          //                     controller.myNickNameController.text =
          //                         controller.myNickNameControllerForEdit.text;
          //                   },
          //                   child: Text('Save'),
          //                 ),
          //               ),
          //             ],
          //           ).paddingAll(10),
          //         )
          //       : SizedBox.shrink();
          // }),
          body: KeyboardDismissOnTap(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _buildHeaderWidget(),
                  _buildMemberRightManage(),
                  _buildBasicInformation(),
                  _maybeBuildTerminateButton(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class _DashedLinePainter extends CustomPainter {
  final Color? color;

  _DashedLinePainter({this.color});

  @override
  void paint(Canvas canvas, Size size) {
    double dashWidth = 5, dashSpace = 5, startX = 0;
    final paint = Paint()
      ..color = (this.color ?? Colors.grey)
      ..strokeWidth = 1;
    while (startX < size.width) {
      canvas.drawLine(Offset(startX, 0), Offset(startX + dashWidth, 0), paint);
      startX += dashWidth + dashSpace;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
