import 'package:flutter/material.dart';
import 'package:implicitly_animated_reorderable_list/implicitly_animated_reorderable_list.dart';
import 'package:implicitly_animated_reorderable_list/transitions.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';

typedef ReorderCallback<TestBook> = void Function(List<TestBook> newItems);

class BookReorderList extends StatelessWidget {
  final List<BookResDto> bookList;
  final ReorderCallback<BookResDto> reorderCallback;

  const BookReorderList(
      {Key? key, required this.bookList, required this.reorderCallback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ImplicitlyAnimatedReorderableList<BookResDto>(
      items: bookList,
      areItemsTheSame: (oldItem, newItem) =>
          oldItem.accountBookSeq == newItem.accountBookSeq,
      onReorderFinished: (item, from, to, newItems) {
        reorderCallback.call(newItems);
      },
      itemBuilder: (context, itemAnimation, item, index) {
        return Reorderable(
          key: ValueKey(item),
          builder: (context, dragAnimation, inDrag) {
            return SizeFadeTransition(
              sizeFraction: 0.7,
              curve: Curves.easeInOut,
              animation: itemAnimation,
              child: Card(
                child: ListTile(
                  title: Text(item.accountBookName),
                  trailing: Handle(
                    delay: const Duration(milliseconds: 100),
                    child: Icon(
                      Icons.list,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
      shrinkWrap: true,
    );
  }
}
