import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/multiBook/controller/book_page_controller.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/manager/dynamic_link_manager.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/extensions.dart';

class BookListPage extends StatelessWidget {
  final RxList<BookResDto> list;
  BookListPage(this.list, {Key? key}) : super(key: key);

  _maybeBuildBookPeriod(BookResDto item) {
    if (item.isClosed) {
      var dateString =
          '${DateFormat('yyyy-MM-dd').format(item.startDate!)} ~ ${DateFormat('yyyy-MM-dd').format(item.endDate!)}';
      return Text(
        dateString,
        style: TextStyle(color: Colors.white, fontSize: 14),
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
      ).marginAll(
        5,
      );
    } else {
      return SizedBox.shrink();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ListView.builder(
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            var item = list[index];
            var memberList = item.userList?.fold<String>(
                  '',
                  (previousValue, element) {
                    if (previousValue.isNotEmpty) {
                      return '$previousValue, ${element.userProfileImgUrl} ${element.userNickname}';
                    } else {
                      return '${element.userProfileImgUrl} ${element.userNickname}';
                    }
                  },
                ) ??
                '';

            return Card(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: item.color!.toColor(), //todo use extension
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //todo: for test
                            index % 2 == 0
                                ? Container(
                                    margin: EdgeInsets.only(left: 20),
                                    padding: EdgeInsets.only(
                                        top: 10,
                                        left: 10,
                                        right: 10,
                                        bottom: 10),
                                    color: Colors.black26,
                                    child: Center(
                                      child: Text(
                                        'New 3 🧾',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  )
                                : Container(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                IconButton(
                                    onPressed: () {
                                      DynamicLinkManager
                                          .shareBookJoinInvitation(item);
                                    },
                                    icon: Icon(Icons.share)),
                                IconButton(
                                    onPressed: () async {
                                      await Get.toNamed(Routes.BookSettingPage,
                                          arguments: item);

                                      Get.find<BookPageController>()
                                          .refreshBook();
                                    },
                                    icon: Icon(Icons.settings)),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 300,
                        padding: EdgeInsets.only(
                            top: 20, bottom: 20, left: 20, right: 20),
                        color: Colors.black26,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              item.accountBookName,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            // Container(
                            //     margin: EdgeInsets.only(top: 8),
                            //     child: Text(
                            //       'description',
                            //       style: TextStyle(
                            //           color: Colors.white, fontSize: 14),
                            //       overflow: TextOverflow.ellipsis,
                            //       maxLines: 1,
                            //     )),
                            // Container(
                            //   margin: EdgeInsets.only(top: 5),
                            //   child: Text(
                            //     item.accountBookDesc ?? '',
                            //     style: TextStyle(
                            //         color: Colors.white, fontSize: 14),
                            //     overflow: TextOverflow.ellipsis,
                            //     maxLines: 1,
                            //   ),
                            // ),
                            Text(
                              memberList,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ).marginSymmetric(
                              vertical: 5,
                            ),
                            _maybeBuildBookPeriod(item),
                            // Container(
                            //     margin: EdgeInsets.only(top: 5),
                            //     child: Text(
                            //       '2021. 11. 02. ~',
                            //       style: TextStyle(
                            //           color: Colors.white, fontSize: 14),
                            //       overflow: TextOverflow.ellipsis,
                            //       maxLines: 1,
                            //     )),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 48,
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
