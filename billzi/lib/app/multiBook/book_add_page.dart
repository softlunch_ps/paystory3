import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/multiBook/controller/book_add_page_controller.dart';

class BookAddPage extends GetView<BookAddPageController> {
  const BookAddPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: controller,
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Create new account book'),
            backgroundColor: controller.currentSelectedColor.value,
            foregroundColor: controller.foregroundColor,
          ),
          body: Form(
            key: controller.formKey,
            child: LayoutBuilder(builder:
                (BuildContext context, BoxConstraints viewportConstraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: viewportConstraints.maxHeight,
                  ),
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: TextFormField(
                            maxLength: 40,
                            decoration: const InputDecoration(
                              hintText: 'Please fill out the book name',
                              labelText: 'Name',
                            ),
                            onSaved: (String? value) {
                              if (value?.isEmpty ?? true) {
                                value = controller.defaultBookName;
                              }
                              controller.currentName = value!;
                            },
                            // validator: (value) {
                            //   return controller.emptyValueValidator(
                            //       value, 'enter a book name!');
                            // },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: TextFormField(
                            maxLength: 100,
                            decoration: const InputDecoration(
                              hintText: 'Please fill out the book description.',
                              labelText: 'Description',
                            ),
                            onSaved: (String? value) {
                              controller.currentDescription = value ?? '';
                            },
                            // validator: (value) {
                            //   return controller.emptyValueValidator(
                            //       value, 'enter a book description!');
                            // },
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(
                            vertical: 20,
                          ),
                          child: Row(
                            children: [
                              ElevatedButton(
                                onPressed: controller.showBookColorPicker,
                                child: SizedBox(
                                  width: 60,
                                  height: 60,
                                  child: Container(),
                                ),
                                style: ElevatedButton.styleFrom(
                                    shape: CircleBorder(),
                                    primary:
                                        controller.currentSelectedColor.value),
                              ),
                              Text('Theme'),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: controller.currentSelectedColor.value,
                            padding: EdgeInsets.symmetric(horizontal: 50),
                            textStyle: Get.theme.textTheme.button?.copyWith(
                                foreground: Paint()
                                  ..color = controller.foregroundColor),
                          ),
                          onPressed: () async {
                            controller.formKey.currentState?.save();
                            await controller.onSaveBook();
                            Get.back();
                          },
                          child: Text('Create'),
                        ).marginOnly(
                          bottom: 20,
                        )
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        );
      },
    );
  }
}
