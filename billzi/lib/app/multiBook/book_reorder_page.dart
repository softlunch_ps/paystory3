import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/multiBook/book_reorderable_list.dart';
import 'package:billzi/app/multiBook/controller/book_page_controller.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/utils/app_colors.dart';

class BookReorderPage extends GetView<BookPageController> {
  const BookReorderPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: WillPopScope(
        onWillPop: () async {
          if (controller.shouldShowConfirmPopup) {
            controller.showSaveOrderConfirmDialog();
            return false;
          }
          return true;
        },
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(text: 'Ongoing'),
                Tab(text: 'Closed'),
              ],
            ),
            title: Text('가계부 변경'),
            centerTitle: true,
            actions: <Widget>[
              TextButton(
                style: TextButton.styleFrom(
                  primary: AppColors.textWhite1,
                ),
                onPressed: () async {
                  await controller.updateBookListOrder();
                  await controller.refreshBook();
                  Get.back();
                },
                child: Text('Complete'),
              )
            ],
          ),
          body: TabBarView(
            children: [
              BookReorderList(
                bookList: controller.onGoingBookItemList,
                reorderCallback: (List<BookResDto> newItems) =>
                    controller.reorderTestBookItem(
                        isClosedOnly: false, reorderedList: newItems),
              ),
              BookReorderList(
                bookList: controller.closedBookItemList,
                reorderCallback: (List<BookResDto> newItems) =>
                    controller.reorderTestBookItem(
                        isClosedOnly: true, reorderedList: newItems),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
