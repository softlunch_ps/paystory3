import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/multiBook/book_list_page.dart';
import 'package:billzi/app/multiBook/controller/book_page_controller.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/app_colors.dart';

class BookSelectPage extends GetView<BookPageController> {
  const BookSelectPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(text: 'Ongoing'),
              Tab(text: 'Closed'),
            ],
          ),
          title: Text('Select a book'),
          centerTitle: true,
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                primary: AppColors.textWhite1,
              ),
              onPressed: () {
                Get.toNamed(Routes.BookReorderPage);
              },
              child: Text('Edit'),
            )
          ],
        ),
        body: TabBarView(
          children: [
            BookListPage(controller.onGoingBookItemList),
            BookListPage(controller.closedBookItemList),
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () async {
            await Get.toNamed(Routes.BookAddPage);
            Get.find<BookPageController>().refreshBook();
          },
          icon: Icon(Icons.add),
          label: Text('create new book'),
        ),
      ),
    );
  }
}
