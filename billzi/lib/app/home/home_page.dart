import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/budget/budget_home_page.dart';
import 'package:billzi/app/home/home_controller.dart';
import 'package:billzi/app/settings/settings_page.dart';
import 'package:billzi/app/summary/summary_page.dart';
import 'package:billzi/app/timeline/timeline_page.dart';
import 'package:billzi/route/route_name.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final HomeController landingPageController = Get.find<HomeController>();

    return Scaffold(
      //appBar: AppBar(title: Text('Pay Story'), centerTitle: true,),
      body: Obx(() => IndexedStack(
            index: landingPageController.tabIndex.value,
            children: [
              SummaryPage(),
              TimelinePage(),
              BudgetHomePage(),
              SettingsPage(),
            ],
          )),
      bottomNavigationBar:
          buildBottomNavigationMenu(context, landingPageController),
    );
  }

  buildBottomNavigationMenu(context, landingPageController) {
    return Obx(() => BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: true,
          showSelectedLabels: true,
          onTap: landingPageController.changeTabIndex,
          currentIndex: landingPageController.tabIndex.value,
          items: [
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: Icon(
                  Icons.home,
                  size: 20.0,
                ),
              ),
              label: 'Home',
              backgroundColor: Color.fromRGBO(36, 54, 101, 1.0),
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: Icon(
                  Icons.search,
                  size: 20.0,
                ),
              ),
              label: 'Transactions',
              backgroundColor: Color.fromRGBO(36, 54, 101, 1.0),
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: Icon(
                  Icons.account_balance_wallet,
                  size: 20.0,
                ),
              ),
              label: 'Budget',
              backgroundColor: Color.fromRGBO(36, 54, 101, 1.0),
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: Icon(
                  Icons.settings,
                  size: 20.0,
                ),
              ),
              label: 'Settings',
              backgroundColor: Color.fromRGBO(36, 54, 101, 1.0),
            ),
          ],
        ));
  }
}
