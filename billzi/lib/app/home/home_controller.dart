import 'package:get/get.dart';
import 'package:billzi/data/repositories/book_repo_impl.dart';
import 'package:billzi/domain/usecase/book_usecase.dart';

class HomeController extends GetxController {
  var tabIndex = 0.obs;

  // TODO: make sure to set availability when new invitation data is available
  var isNewInvitationAvailable = true.obs;
  var currentBookName = "Select a Book".obs;

  late BookUseCase bookUseCase;

  void changeTabIndex(int index) {
    tabIndex.value = index;
  }

  @override
  void onInit() {
    super.onInit();
    bookUseCase = BookUseCase(BookRepositoryImpl());
    _fetchPrimaryBookName();
  }

  @override
  void dispose() {
    super.dispose();
  }

  updateCurrentPrimaryBookName(String bookName) {
    currentBookName.value = bookName;
  }

  _fetchPrimaryBookName() async {
    var result = await bookUseCase.getBookList();

    result.when(
      success: (response) {
        updateCurrentPrimaryBookName(
          response.accountBookList!
              .firstWhere((element) => !element.isClosed)
              .accountBookName,
        );
      },
      failure: (error) {},
    );
  }
}
