import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

// TODO: This is temporary code to test input filed above keyboard
class InputScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(title: const Text('Close')),
        bottomSheet:
            KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
          return isKeyboardVisible
              ? const TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Enter your input here',
                  ),
                )
              : SizedBox.shrink();
        }),
        body: Column(
          children: [
            const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter your input here',
              ),
            ),
            ElevatedButton(
              onPressed: () {},
              child: const Text('Submit'),
            ),
          ],
        ),
      ),
    );
  }
}
