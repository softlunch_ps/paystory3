import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/budget/budget_listview.dart';
import 'package:billzi/app/budget/controller/budget_home_page_controller.dart';
import 'package:billzi/route/route_name.dart';

class BudgetHomePage extends GetView<BudgetHomeController> {
  BudgetHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Budget'),
      ),
      // body: Center(
      //   child: Container(
      //     child: ElevatedButton(
      //       onPressed: () {
      //         Get.toNamed(Routes.BudgetAddPage);
      //       },
      //       child: Text('Create your first budget'),
      //     ),
      //   ),
      // ),
      body: BudgetListView(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Get.toNamed(Routes.BudgetAddPage);
        },
      ),
    );
  }
}
