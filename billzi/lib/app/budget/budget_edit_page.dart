import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BudgetEditPage extends StatefulWidget {
  const BudgetEditPage({Key? key}) : super(key: key);

  @override
  _BudgetEditPageState createState() => _BudgetEditPageState();
}

class _BudgetEditPageState extends State<BudgetEditPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          margin: EdgeInsets.only(right: 30, left: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('예산 설정 하기'),
              Icon(Icons.category),
              Text('건강/의료'),
              TextField(),
              SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('뒤로가기')),
                  ElevatedButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('예산설정')),
                ],
              )
            ],
          ),
        ));
  }
}


