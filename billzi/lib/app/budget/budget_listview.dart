import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/budget/budget_item.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/test_data.dart';

class BudgetListView extends StatelessWidget {
  const BudgetListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: TestData.getTestBudgetList().length,
        itemBuilder: (BuildContext context, int index) {
      return BudgetItem(index: index, onClickBudgetItem: (int index) {
        Get.toNamed(Routes.BudgetDetailPage);
      },);
    });
  }
}
