import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/common/tag_text.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/utils.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class BudgetDetailPage extends StatefulWidget {
  const BudgetDetailPage({Key? key}) : super(key: key);

  @override
  State<BudgetDetailPage> createState() => _BudgetDetailPageState();
}

class _BudgetDetailPageState extends State<BudgetDetailPage>
    with TickerProviderStateMixin {

  List<String> testList = [
    '2021년 1월',
    '2021년 2월',
    '2021년 3월',
    '2021년 4월',
    '2021년 5월',
    '2021년 6월',
    '2021년 7월',
    '2021년 8월',
    '2021년 9월',
    '2021년 10월',
    '2021년 11월',
    '2021년 12월',
    '2022년 1월',
  ];


  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: testList.length, vsync: this);
    _tabController.animateTo(testList.length - 1);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  List<Tab> _getTabList(List<String> list) {
    List<Tab> tabList = [];
    list.forEach((element) {
      tabList.add(Tab(
        text: element
      ));
    });
    return tabList;
  }

  List<Widget> _getDetailList(List<String> list) {
    List<Widget> detailList = [];
    list.forEach((element) {
      detailList.add(_getTransactionList());
    });
    return detailList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('한달 예산'),
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20),
            alignment: Alignment.center,
            child: Text(
              '\$539,575,756',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 26,
                  color: Colors.green),
            ),
          ),
          Center(child: Text(' left out of \$600,000,000')),
          Container(
            margin: EdgeInsets.only(top: 20),
            width: double.infinity,
            decoration: BoxDecoration(color: Colors.transparent),
            child: TabBar(
              labelColor: Colors.black,
              indicatorColor: Colors.black,
              unselectedLabelColor: Colors.black.withOpacity(0.3),
              isScrollable: true,
              labelPadding: EdgeInsets.only(left: 30, right: 30),
              controller: _tabController,
              tabs: _getTabList(testList),
            ),
          ),
          Expanded(
            child: Container(
              height: 80.0,
              child: TabBarView(
                controller: _tabController,
                children: _getDetailList(testList),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getTransactionList() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(right: 20, left: 20),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 15, bottom: 15, right: 0, left: 0),
                child: LinearPercentIndicator(
                  animation: true,
                  lineHeight: 20.0,
                  animationDuration: 2000,
                  percent: 0.9,
                  center: Text("90.0%"),
                  linearStrokeCap: LinearStrokeCap.roundAll,
                  progressColor: AppColors.primary,
                ),
              ),
            ),
            Container(
              margin:
              EdgeInsets.only(bottom: 10, left: 20, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('2022년 1월 1일'),
                  Text('2022년 1월 31일'),
                ],
              ),
            ),
            Container(
                alignment: Alignment.centerLeft,
                margin:
                EdgeInsets.only(right: 20, left: 20, top: 20),
                child: Text(
                  'Transaction',
                  style: TextStyle(
                      fontSize: 24, fontWeight: FontWeight.bold),
                )),
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
              child: ListView.builder(
                  itemCount: 10,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      child: InkWell(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        child: Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.only(right: 10, left: 10),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    child: Container(color: Colors.blue,),
                                    //child: Icon(TestData.getIconByName(item.category.iconName))
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Expanded(
                                              child: Container(
                                                // child: Text(item.category?.categoryName ?? '')),
                                                  child: Text('카테고리')),
                                            ),
                                            Text('\$3,000'),
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 10, bottom: 10),
                                          child: TagText(text: ''),
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(DateFormat('kk:mm').format(DateTime.now())),
                                            Expanded(
                                              child: Container(
                                                margin: EdgeInsets.only(right: 5, left: 5),
                                                child: Text(getTransactionTypeNameById('E')),
                                              ),
                                            ),
                                            Text(getPaymentTypeNameById('01')),
                                          ],
                                        )


                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        onTap: () {}
                        ,
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
