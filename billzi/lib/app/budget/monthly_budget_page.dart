import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/budget/budget_item.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class MonthlyBudgetPage extends StatelessWidget {
  const MonthlyBudgetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('예산'),
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: [
            Card(
              margin: EdgeInsets.only(top: 0, left: 0, right: 0),
              elevation: 3,
              child: Container(
                margin: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('한달예산이'),
                    Text('286,500원 넘었어요'),
                    Padding(
                      padding: EdgeInsets.all(15.0),
                      child: LinearPercentIndicator(
                        animation: true,
                        lineHeight: 20.0,
                        animationDuration: 2000,
                        percent: 0.9,
                        center: Text("90.0%"),
                        linearStrokeCap: LinearStrokeCap.roundAll,
                        progressColor: AppColors.primary,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('예산'),
                        Text('지출'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('100,000원'),
                        Text('386,500원'),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                  itemCount: 15,
                  itemBuilder: (BuildContext context, int index) {
                return BudgetItem(index: index, onClickBudgetItem: (int index) {
                  Get.toNamed(Routes.BudgetEditPage);
                },);
              }),
            ),
          ],
        ),
      ),
    );
  }
}
