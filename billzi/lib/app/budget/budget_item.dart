import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

typedef void OnClickBudgetItem(int index);

class BudgetItem extends StatelessWidget {

  final int index;
  final OnClickBudgetItem onClickBudgetItem;
  const BudgetItem({Key? key, required this.index, required this.onClickBudgetItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onClickBudgetItem.call(index);
      },
      child: Container(
        margin:
        EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
        child: Column(
          children: [
            Row(
              children: [
                // Icon(Icons.shop),
                Container(
                    child: Text('카페/간식', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.black))),
              ],
            ),
            Row(
              children: [
                Text('\$539,575,756', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.green),),
                Text(' left out of \$600,000,000'),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 15, bottom: 15, right: 0, left: 0),
              child: LinearPercentIndicator(
                animation: true,
                lineHeight: 20.0,
                animationDuration: 2000,
                percent: 0.9,
                center: Text("90.0%"),
                linearStrokeCap: LinearStrokeCap.roundAll,
                progressColor: AppColors.primary,
              ),
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     Text('예산'),
            //     Text('지출'),
            //   ],
            // ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('2022년 1월 1일'),
                  Text('2022년 1월 31일'),
                ],
              ),
            ),
            Divider(height: 1,)
          ],
        ),
      ),
    );
  }
}
