import 'dart:async';

import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get_storage/get_storage.dart';
import 'package:billzi/data/repositories/auth_repo_impl.dart';
import 'package:billzi/domain/usecase/auth_usecase.dart';
import 'package:billzi/manager/dynamic_link_manager.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/constants.dart';
import 'package:billzi/utils/logger.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    Timer(Duration(milliseconds: 1500), () async {
      AuthUseCase authUseCase = AuthUseCase(AuthRepositoryImpl());

      try {
        if (authUseCase.isLoggedIn()) {

          //server login
          bool loginSuccess = await authUseCase.loginToPayStoryServer();

          if (loginSuccess) {
            GetStorage box = GetStorage();
            bool? isGoingSimplePage = box.read<bool>(StorageKeySimpleInput);
            if (isGoingSimplePage == null) {
              isGoingSimplePage = false;
            }
            if (isGoingSimplePage) {
              Get.offAllNamed(Routes.EasyFormInputPage);
            } else {
              Get.offAllNamed(Routes.HomePage);
            }
            // handle dynamic link right after showing either a easy form input page or home page if User is logged in
            //DynamicLinkManager().maybeHandleInitialLink();
          } else {
            //어떤 이유로 서버 로그인 실패 한 경우 일단 로그인 페이지로 이동
            Get.offAllNamed(Routes.LoginPage);
          }

        } else {
          Get.offAllNamed(Routes.LoginPage);
        }
      } catch (e) {
        logger.d('login fail: $e');
        Get.offAllNamed(Routes.LoginPage);
      }

    });
  }
}
