import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/splash/splash_controller.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder(
        init: Get.find<SplashController>(),
        builder: (controller) {
          return Center(
            child: Text('Pay Story'),
          );
        },
      )
    );
  }
}
