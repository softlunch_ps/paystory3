import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:billzi/data/repositories/auth_repo_impl.dart';
import 'package:billzi/domain/usecase/auth_usecase.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/constants.dart';
import 'package:billzi/utils/logger.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  //todo test
  final box = GetStorage();
  bool? testSimpleFlag = false;

  @override
  void initState() {
    super.initState();
    testSimpleFlag = box.read<bool>(StorageKeySimpleInput);
    if (testSimpleFlag == null) {
      testSimpleFlag = false;
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('설정'),
        centerTitle: true,
      ),
        body: Container(
          child: SettingsList(
            contentPadding: const EdgeInsets.only(top: 20),
            sections: [
              SettingsSection(
                title: Text('Section'),
                tiles: [
                  SettingsTile.switchTile(
                    title: Text('간편 입력 사용'),
                    leading: Icon(Icons.fingerprint),
                    onToggle: (bool value) {
                      box.write(StorageKeySimpleInput, value);
                      setState(() {
                        testSimpleFlag = value;
                      });
                    }, initialValue: testSimpleFlag,
                  ),
                  SettingsTile(
                    title: Text('Logout'),
                    leading: Icon(Icons.logout),
                    onPressed: (BuildContext context) {
                      logout();
                    },
                  ),
                ],
              ),
            ],
          ),
        )
    );
  }

  //todo go to controller, add error case
  Future logout() async {
    AuthUseCase authUseCase = AuthUseCase(AuthRepositoryImpl());
    var result = await authUseCase.signOut();
    result.when(
      success: (response) {
        logger.d('logout success');
        Get.offAndToNamed(Routes.SplashPage);
      },
      failure: (error) {
        logger.d('logout fail');
      },
    );
  }
}