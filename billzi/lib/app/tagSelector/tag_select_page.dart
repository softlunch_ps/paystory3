import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_social_textfield/flutter_social_textfield.dart';
import 'package:get/get.dart';
import 'package:billzi/utils/app_colors.dart';
//import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

class TagSelectPage extends StatefulWidget {
  const TagSelectPage({Key? key}) : super(key: key);

  @override
  State<TagSelectPage> createState() => _TagSelectPageState();
}

class _TagSelectPageState extends State<TagSelectPage> {
  late SocialTextEditingController _textEditingController;

  bool isSelectorOpen = false;
  FocusNode _focusNode = FocusNode();
  ScrollController _scrollController = ScrollController();
  SocialContentDetection? lastDetection;
  late StreamSubscription<SocialContentDetection> _streamSubscription;

  @override
  void initState() {
    super.initState();
    String inputString = '';
    if (Get.arguments != null) {
      inputString = Get.arguments as String;
    }

    _textEditingController = SocialTextEditingController()
      ..text = inputString
      ..setTextStyle(DetectedType.hashtag,
          TextStyle(color: Colors.blue, fontWeight: FontWeight.w600));
    _streamSubscription =
        _textEditingController.subscribeToDetection(onDetectContent);
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _streamSubscription.cancel();
    _textEditingController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void onDetectContent(SocialContentDetection detection) {
    lastDetection = detection;
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height * 0.3;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('태그 및 메모'),
        actions: [
          TextButton(
            onPressed: () {
              Get.back(result: _textEditingController.value.text);
            },
            child: Text('입력'),
            style: TextButton.styleFrom(
              primary: AppColors.textWhite1,
            ),
          )
        ],
      ),
      body: DefaultSocialTextFieldController(
        focusNode: _focusNode,
        scrollController: _scrollController,
        textEditingController: _textEditingController,
        child: Container(
          padding: EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: TextField(
                  scrollController: _scrollController,
                  focusNode: _focusNode,
                  controller: _textEditingController,
                  expands: true,
                  maxLines: null,
                  minLines: null,
                  decoration: InputDecoration(
                    hintText: "#태그 및 메모를 입력해주세요.",
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                  ),
                ),
              ),
            ],
          ),
        ),
        detectionBuilders: {
          // DetectedType.mention:(context)=>mentionContent(height),
          DetectedType.hashtag: (context) => hashtagContent(height),
          // DetectedType.url:(context)=>urlContent(height)
        },
      ),

      /// 키보드 상단에 버튼 영영 필요한 경우 사용
      // body: KeyboardVisibilityBuilder(
      //   builder: (BuildContext context, bool isKeyboardVisible) {
      //     return Stack(
      //       children: [
      //         DefaultSocialTextFieldController(
      //           focusNode: _focusNode,
      //           scrollController: _scrollController,
      //           textEditingController: _textEditingController,
      //           child: Container(
      //             padding: EdgeInsets.all(8),
      //             child: Column(
      //               mainAxisAlignment: MainAxisAlignment.start,
      //               children: <Widget>[
      //                 Expanded(
      //                   child: TextField(
      //                     scrollController: _scrollController,
      //                     focusNode: _focusNode,
      //                     controller: _textEditingController,
      //                     expands: true,
      //                     maxLines: null,
      //                     minLines: null,
      //                     decoration: InputDecoration(
      //                       hintText: "#태그 및 메모를 입력해주세요.",
      //                       border: InputBorder.none,
      //                       focusedBorder: InputBorder.none,
      //                       enabledBorder: InputBorder.none,
      //                       errorBorder: InputBorder.none,
      //                       disabledBorder: InputBorder.none,
      //                     ),
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //           detectionBuilders: {
      //             // DetectedType.mention:(context)=>mentionContent(height),
      //             DetectedType.hashtag: (context) => hashtagContent(height),
      //             // DetectedType.url:(context)=>urlContent(height)
      //           },
      //         ),
      //         isKeyboardVisible
      //             ? Positioned(
      //           bottom: 0,
      //           left: 0,
      //           right: 0,
      //           child: Container(
      //             height: 50,
      //             child: Text("Aboveeeeee"),
      //             decoration: BoxDecoration(color: Colors.pink),
      //           ),
      //         )
      //             : Container(),
      //       ],
      //     );
      //   },
      // ),
    );
  }

  PreferredSize hashtagContent(double height) {
    return PreferredSize(
      child: Container(
        child: ListView.builder(
            itemBuilder: (context, index) => ListTile(
                  title: Text("#hashtag_$index"),
                  onTap: () {
                    //todo test
                    if (lastDetection != null) {
                      _textEditingController.replaceRange(
                          "#hashtag_$index", lastDetection!.range);
                    }
                  },
                )),
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              color: AppColors.dividerGray2,
              width: 0.5,
            ),
          ),
        ),
      ),
      preferredSize: Size.fromHeight(height),
    );
  }
}
