import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:group_button/group_button.dart';
import 'package:intl/intl.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:pattern_formatter/numeric_formatter.dart';

class EditPostPage extends StatefulWidget {
  const EditPostPage({Key? key}) : super(key: key);

  @override
  _EditPostPageState createState() => _EditPostPageState();
}

class _EditPostPageState extends State<EditPostPage> {
  late String dateTime;

  DateTime selectedDateTime = DateTime.now();

  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);
  String dateString = '2021년 09월 13일';
  String timeString = ' 오후 09시 36분';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('내역 수정'),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.only(top: 20, left: 20, right: 20),
        child: Column(
          children: [
            Container(
              height: 40,
              child: Row(
                children: [
                  Container(
                    child: Center(child: Text('결제수단')),
                    width: 50,
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 20, left: 20),
                    child: GroupButton(
                      isRadio: true,
                      spacing: 20,
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      onSelected: (index, isSelected) =>
                          print('$index button is selected'),
                      buttons: ['카드', '현금'],
                      selectedColor: AppColors.primary,
                      buttonWidth: 70,
                      buttonHeight: 30,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 40,
              child: Row(
                children: [
                  Container(
                    child: Center(child: Text('지출내용')),
                    width: 50,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Flexible(
                      child: TextField(
                    maxLines: 1,
                    decoration: InputDecoration(
                      hintText: '지출 내용을 입력 해 주세요.',
                    ),
                  ))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                children: [
                  Container(
                    child: Container(child: Text('지출금액')),
                    width: 50,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Flexible(
                      child: TextField(
                    maxLines: 1,
                    maxLength: 13,
                    keyboardType: TextInputType.numberWithOptions(
                        signed: false, decimal: true),
                    inputFormatters: [ThousandsFormatter()],
                    decoration: InputDecoration(
                        suffix: Text('원'),
                        hintText: '지출 금액을 입력 해 주세요.',
                        counterText: ''),
                  ))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 40,
              child: Row(
                children: [
                  Container(
                    child: Text('카테고리'),
                    width: 50,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        Get.toNamed(Routes.EditPostCategoryPage);
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 10, left: 10),
                        height: 40,
                        child: Row(
                          children: [
                            Icon(Icons.shop),
                            SizedBox(
                              width: 20,
                            ),
                            Text('건강/의료'),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 40,
              child: Row(
                children: [
                  Container(
                    child: Text('위치'),
                    width: 50,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        Get.toNamed(Routes.LocationMapSearchPage);
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 10, left: 10),
                        height: 40,
                        child: Row(
                          children: [
                            Icon(Icons.location_on),
                            SizedBox(
                              width: 20,
                            ),
                            Text('test'),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 40,
              child: Row(
                children: [
                  Container(
                    child: Text('일시'),
                    width: 50,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Flexible(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        _selectDate(context);
                      },
                      child: Container(
                          height: 40,
                          alignment: Alignment.centerLeft,
                          child: Center(child: Text(dateString))),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        _selectTime(context);
                      },
                      child: Container(
                          height: 40,
                          alignment: Alignment.centerLeft,
                          child: Center(child: Text(timeString))),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 40,
              child: Row(
                children: [
                  Container(
                    child: Text('메모'),
                    width: 50,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Flexible(child: TextField())
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDateTime,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        selectedDateTime = picked;

        dateString =
            formatDate(selectedDateTime, [yyyy, '년 ', mm, '월 ', dd, '일'])
                .toString();
      });
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
        timeString = formatDate(
            DateTime(selectedDateTime.year, selectedDateTime.month,
                selectedDateTime.day, selectedTime.hour, selectedTime.minute),
            [am, ' ', hh, '시', ' ', nn, '분']).toString();
      });
  }
}
