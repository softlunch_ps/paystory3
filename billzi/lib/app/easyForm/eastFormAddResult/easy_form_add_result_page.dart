import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/common/tag_text.dart';
import 'package:billzi/app/easyForm/eastFormAddResult/easy_form_add_result_controller.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/app_colors.dart';
import 'package:billzi/utils/currency_helper.dart';
import 'package:billzi/utils/utils.dart';

class EasyFormResultPage extends StatefulWidget {
  const EasyFormResultPage({Key? key}) : super(key: key);

  @override
  _EasyFormResultPageState createState() => _EasyFormResultPageState();
}

class _EasyFormResultPageState extends State<EasyFormResultPage> {
  Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: Get.find<EasyFormAddResultController>(),
        builder: (EasyFormAddResultController controller) {
          return WillPopScope(
            onWillPop: () async {
              Get.offAllNamed(Routes.HomePage);
              return false;
            },
            child: Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text('Transaction detail'),
                actions: [
                  TextButton(
                    style: TextButton.styleFrom(
                      primary: AppColors.textWhite1,
                    ),
                    onPressed: () {
                      Get.offAllNamed(Routes.HomePage);
                    },
                    child: Text('Complete'),
                  )
                ],
              ),
              body: SafeArea(
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: 40, bottom: 20),
                        child: Text(
                          getMoneyStringWithCurrency(
                              money: controller.transactionDto.value.totalMoney,
                              currency: findCurrencyByCode(controller
                                  .transactionDto.value.currencyCode)),
                          style: TextStyle(fontSize: 30, color: Colors.black),
                        )),
                    Container(
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        child: Text(
                          DateFormat('MMM dd, yyyy hh:mm aaa').format(
                              controller.transactionDto.value.createDateTime),
                          style: TextStyle(fontSize: 20, color: Colors.black),
                        )),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 20, right: 20),
                      child: controller.hasCurrentPosition.value
                          ? Center(
                              child: SizedBox(
                                //width: MediaQuery.of(context).size.width * 0.8,
                                height: MediaQuery.of(context).size.width * 0.2,
                                child: GoogleMap(
                                  mapType: MapType.normal,
                                  compassEnabled: false,
                                  mapToolbarEnabled: false,
                                  rotateGesturesEnabled: false,
                                  scrollGesturesEnabled: false,
                                  zoomGesturesEnabled: false,
                                  zoomControlsEnabled: false,
                                  tiltGesturesEnabled: false,
                                  myLocationEnabled: false,
                                  myLocationButtonEnabled: false,
                                  markers:
                                      Set.from(controller.myPositionMarkers),
                                  initialCameraPosition: CameraPosition(
                                    target: LatLng(controller.testLat!,
                                        controller.testLng!),
                                    zoom: 15,
                                  ),
                                  onMapCreated:
                                      (GoogleMapController controller) {
                                    _controller.complete(controller);
                                  },
                                ),
                              ),
                            )
                          : Expanded(
                              child: Container(
                                child: Text('위치 정보가 없습니다'),
                              ),
                            ),
                    ),

                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: InkWell(
                        onTap: () {
                          //Get.toNamed(Routes.SelectTransactionPage);
                        },
                        child: Container(
                          height: 48,
                          margin: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    getPaymentTypeNameById(controller
                                        .transactionDto.value.paymentTypeId),
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  Text(getTransactionTypeNameById(controller
                                      .transactionDto.value.transactionTypeId))
                                ],
                              ),
                              Icon(Icons.chevron_right),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: InkWell(
                        onTap: () async {
                          dynamic result = await Get.toNamed(
                              Routes.TagSelectPage,
                              arguments: controller.transactionDto.value.description);
                          if (result != null) {
                            //controller.inputDescription(result as String);
                          }
                        },
                        child: Container(
                          height: 48,
                          margin: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                  child: TagText(
                                      text: controller.transactionDto.value
                                              .description!.isEmpty
                                          ? '태그 및 메모 입력하기'
                                          : controller
                                              .transactionDto.value.description!)),
                              Icon(Icons.chevron_right),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // Container(
                    //   margin: EdgeInsets.only(
                    //     top: 20,
                    //     left: 30,
                    //     right: 30,
                    //     bottom: 10,
                    //   ),
                    //   child: SizedBox(
                    //       height: 56,
                    //       width: double.infinity,
                    //       child: TextButton(
                    //           onPressed: () {
                    //             Get.offAndToNamed(Routes.EasyFormInputPage);
                    //           },
                    //           child: Text('다른 내역 더 입력하기'))),
                    // ),
                    // Container(
                    //   margin: EdgeInsets.only(
                    //       top: 10, left: 30, right: 30, bottom: 10),
                    //   child: SizedBox(
                    //       height: 56,
                    //       width: double.infinity,
                    //       child: ElevatedButton(
                    //           onPressed: () {
                    //             Get.offNamedUntil(
                    //                 Routes.HomePage, (route) => false);
                    //           },
                    //           child: Text('페이스토리로 가기'))),
                    // ),
                    // Container(
                    //   margin: EdgeInsets.only(
                    //       top: 10, left: 30, right: 30, bottom: 20),
                    //   child: SizedBox(
                    //     height: 56,
                    //     width: double.infinity,
                    //     child: ElevatedButton(
                    //         onPressed: () {
                    //           SystemNavigator.pop();
                    //         },
                    //         child: Text('종료하기'),
                    //         style: ElevatedButton.styleFrom(
                    //           primary: Colors.red,
                    //         )),
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
