import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';

class EasyFormAddResultController extends GetxController {

  RxBool hasCurrentPosition = false.obs;
  List<Marker> myPositionMarkers = [];
  late Rx<TransactionDto> transactionDto;

  //todo test data
  double? testLat;
  double? testLng;

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      transactionDto = Rx(Get.arguments as TransactionDto);
      _setMapMarker(transactionDto.value);

    }
  }

  _setMapMarker(TransactionDto transactionDto) {
    if (transactionDto.gpsInfo != null) {
      hasCurrentPosition.value = true;
      testLat = transactionDto.gpsInfo!.latitude;
      testLng = transactionDto.gpsInfo!.longitude;
      myPositionMarkers.add(Marker(
          markerId: MarkerId('1'),
          anchor: Offset(0.5, 1.0),
          draggable: true,
          position: LatLng(transactionDto.gpsInfo!.latitude, transactionDto.gpsInfo!.longitude)));

    } else {
      hasCurrentPosition.value = false;
    }
  }
}
