import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/common/category_grid_view.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/test_data.dart';

class EasyFormCategoryPage extends StatelessWidget {
  const EasyFormCategoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(top: 20, bottom: 20),
                child: Text(
                  '카테고리를 선택해 주세요.',
                  style: TextStyle(fontSize: 25, color: Colors.grey),
                )),
            // CategoryGridView(
            //   categoryList: TestData.getTestCategories(),
            //   onCategoryTab: (int index) {
            //     Get.toNamed(Routes.EasyFormResultPage);
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
