import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:billzi/app/common/category_grid_view.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/data/dto/fourSquare/four_square_venue_dto.dart';
import 'package:billzi/data/dto/model/gps_Info.dart';
import 'package:billzi/data/dto/model/transaction_dto.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/data/dto/testDto/test_category.dart';
import 'package:billzi/data/dto/testDto/test_payment_type.dart';
import 'package:billzi/data/dto/testDto/test_transaction_type.dart';
import 'package:billzi/data/repositories/book_repo_impl.dart';
import 'package:billzi/data/repositories/easy_form_repo_impl.dart';
import 'package:billzi/domain/usecase/book_usecase.dart';
import 'package:billzi/domain/usecase/easy_form_usecase.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/constants.dart';
import 'package:billzi/utils/location_utils.dart';
import 'package:billzi/utils/logger.dart';
import 'package:billzi/utils/test_data.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:intl/intl.dart' show NumberFormat;

const maxLength = 12;

class EasyFormAddController extends GetxController {
  late Rx<TestCategory> selectedTestCategory;
  Rx<bool> isLocationUtilized = false.obs;
  Rx<String> moneyString = ''.obs;
  Rx<String> descriptionString = ''.obs;
  late Rx<Currency> currency;
  final box = GetStorage();
  Rx<bool> isLoading = false.obs;
  TextEditingController textEditingController = TextEditingController();
  late Rx<TestTransactionType> selectedTransactionType;
  late Rx<TestPaymentType> selectedPaymentType;
  Rx<FourSquareVenueDto>? selectedFourSquareVenueDto;


  @override
  void onInit() {
    super.onInit();
    EasyFormUseCase easyFormUseCase = EasyFormUseCase(EasyFormRepositoryImpl());

    selectedTransactionType = easyFormUseCase.getLastTestTransactionType().obs;
    selectedPaymentType = easyFormUseCase.getLastTestPaymentType().obs;
    currency = easyFormUseCase.getCurrency().obs;
    selectedTestCategory =
        TestCategory(name: '간편입력', iconName: 'assignment_turned_in').obs;

    //checkUsingLocation();
  }

  Future<bool> checkLocationPermission() async {
    PermissionStatus permissionStatus = await Permission.location.request();
    return permissionStatus.isGranted;
  }

  inputDescription(String description) {
    descriptionString.value = description;
    update();
  }

  onClickLocationSearch() {
    checkLocationPermission().then((isPermissionGranted) async {
      if (isPermissionGranted) {
        Position? currentPosition = await fetchPosition();
        dynamic result = await Get.toNamed(Routes.LocationSearchPage, arguments: currentPosition);
        if (result != null) {
          selectedFourSquareVenueDto = Rx(result);
          update();
        }
      } else {
        //need permission
        _showPermissionDeniedDialog(Get.context!);
      }
    });
  }

  onSelectTransactionType(TestTransactionType testTransactionType) {
    selectedTransactionType.value = testTransactionType;
    update();
  }

  onSelectPaymentType(TestPaymentType testPaymentType) {
    selectedPaymentType.value = testPaymentType;
    update();
  }

  onSelectCurrency(Currency selectedCurrency) {
    currency.value = selectedCurrency;
    EasyFormUseCase easyFormUseCase = EasyFormUseCase(EasyFormRepositoryImpl());
    easyFormUseCase.saveCurrency(selectedCurrency);
    _refreshMoneyString();
  }

  onClickUsingLocation() async {
    if (!isLoading.value) {
      isLoading.value = true;
      if (!isLocationUtilized.value) {
        bool isPermissionGranted = await checkLocationPermission();
        if (isPermissionGranted) {
          await box.write(StorageKeySimpleInputLocation, true);
          isLocationUtilized.value = true;
        } else {
          _showPermissionDeniedDialog(Get.context!);
        }
      } else {
        await box.write(StorageKeySimpleInputLocation, false);
        isLocationUtilized.value = false;
      }
      update();
      isLoading.value = false;
    }
  }

  _callAppSettings() {
    openAppSettings();
  }

  checkUsingLocation() {
    bool? _usingLocation =
        box.read<bool>(StorageKeySimpleInputLocation) ?? true;
    if (_usingLocation) {
      isLocationUtilized.value = true;
    } else {
      isLocationUtilized.value = false;
    }
    update();
  }

  Future<void> _showPermissionDeniedDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('위치 권환이 필요합니다'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('위치 기록 기능을 사용하려면, 위치 권한이 필요합니다.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('취소'),
              onPressed: () {
                Get.back();
              },
            ),
            TextButton(
              child: const Text('위치 권한 설정'),
              onPressed: () {
                _callAppSettings();
              },
            ),
          ],
        );
      },
    );
  }

  saveFormData() async {
    double money;
    if (moneyString.value.length > 0) {
      money = double.parse(moneyString.value);
    } else {
      money = 0;
    }

    BookResDto? selectedBook = await _getSelectedBook();

    if (isLocationUtilized.value) {
      checkLocationPermission().then((isPermissionGranted) async {
        if (isPermissionGranted) {
          //todo go
          //save
          Position? currentPosition = await fetchPosition();

          TransactionDto transactionDto = TransactionDto(
              transactionTypeId: selectedTransactionType.value.id,
              paymentTypeId: selectedPaymentType.value.id,
              //category: selectedTestCategory.value,
              totalMoney: money,
              book: selectedBook,
              currencyCode: currency.value.code,
              createDateTime: DateTime.now().toUtc(),
              description: descriptionString.value,
              gpsInfo: GpsInfo(longitude: currentPosition.longitude, latitude: currentPosition.latitude));

          _saveTransaction(transactionDto);
          //Get.offAndToNamed(Routes.EasyFormResultPage);
        } else {
          //need permission

        }
      });
    } else {
      //without location
      TransactionDto transactionDto = TransactionDto(
          transactionTypeId: selectedTransactionType.value.id,
          paymentTypeId: selectedPaymentType.value.id,
          //category: selectedTestCategory.value,
          totalMoney: money,
          book: selectedBook,
          currencyCode: currency.value.code,
          createDateTime: DateTime.now().toUtc(),
          description: descriptionString.value);

      _saveTransaction(transactionDto);
      //Get.offAndToNamed(Routes.EasyFormResultPage);
    }
  }

  //todo for test
  _saveTransaction(TransactionDto transactionDto) async {
    EasyFormUseCase easyFormUseCase = EasyFormUseCase(EasyFormRepositoryImpl());

    //todo error handling
    var result = await easyFormUseCase.saveEasyForm(transactionDto);
    result.when(
      success: (response) {
        Get.offAllNamed(Routes.EasyFormResultPage, arguments: transactionDto);
      },
      failure: (error) {
        logger.d('easy form add fail');
      },
    );
  }

  //todo test
  Future<BookResDto?> _getSelectedBook() async {
    BookUseCase bookUseCase = BookUseCase(BookRepositoryImpl());
    return await bookUseCase.getSelectedBook();
  }

  openCategoryBottomSheet(BuildContext context) {
    Get.bottomSheet(
      Container(
          height: 500,
          color: Colors.white,
          child: Material(
            child: Container(
              margin: EdgeInsets.only(top: 20),
              child: SingleChildScrollView(
                // child: CategoryGridView(
                //   categoryList: TestData.getTestCategories(),
                //   onCategoryTab: (int index) {
                //     selectedTestCategory.value =
                //         TestData.getTestCategories()[index];
                //     Navigator.of(context).pop();
                //   },
                // ),
              ),
            ),
          )),
      enableDrag: false,
    );
  }

  onClickKeyPadInput(String number) {
    if (moneyString.value.length < maxLength) {
      moneyString.value = moneyString.value + number;
      _refreshMoneyString();
    }
  }

  onClickKeyPadDelete() {
    if (moneyString.value.length == 1) {
      moneyString.value = '';
      textEditingController.text = moneyString.value;
    } else if (moneyString.value.length > 1) {
      moneyString.value =
          moneyString.value.substring(0, moneyString.value.length - 1);
      _refreshMoneyString();
    }
  }

  _refreshMoneyString() {
    textEditingController.text =
        NumberFormat.currency(symbol: '${currency.value.symbol}', name: currency.value.code)
            .format(int.parse(moneyString.value));
    update();
  }
}
