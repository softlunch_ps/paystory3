import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:billzi/app/common/currency/currency.dart';
import 'package:billzi/app/common/numeric_keypad.dart';
import 'package:billzi/app/common/tag_text.dart';
import 'package:billzi/app/easyForm/easyFormAdd/easy_form_add_controller.dart';
import 'package:billzi/route/route_name.dart';
import 'package:pattern_formatter/pattern_formatter.dart';

class EasyFormAddPage extends StatelessWidget {
  const EasyFormAddPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Get.offAllNamed(Routes.HomePage);
        return false;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          title: Text('간편입력'),
        ),
        body: SafeArea(
          //Todo 디자인 변경 시 getbuilder 를 obx로 변경
          child: GetBuilder(
            init: Get.find<EasyFormAddController>(),
            builder: (EasyFormAddController controller) {
              return Container(
                child: Column(
                  children: [
                    // Container(
                    //   margin: EdgeInsets.only(top: 20),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       Container(
                    //           child: Text(
                    //         '간편 입력하기',
                    //         style: TextStyle(fontSize: 25, color: Colors.grey),
                    //       )),
                    //       Container(
                    //         margin: EdgeInsets.only(left: 30, right: 20),
                    //         child: SizedBox(
                    //             height: 56,
                    //             child: TextButton(
                    //                 onPressed: () {
                    //                   Get.offNamedUntil(
                    //                       Routes.HomePage, (route) => false);
                    //                 },
                    //                 child: Text('페이스토리로 가기'))),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    // Center(
                    //   child: GroupButton(
                    //       isRadio: true,
                    //       spacing: 20,
                    //       borderRadius: BorderRadius.all(Radius.circular(5)),
                    //       onSelected: (index, isSelected) =>
                    //           print('$index button is selected'),
                    //       buttons: ['카드', '현금'],
                    //       selectedColor: AppColors.primary,
                    //       buttonWidth: 120,
                    //       buttonHeight: 40,
                    //       selectedButton: 0),
                    // ),

                    // InkWell(
                    //   onTap: () {
                    //     controller.onClickUsingLocation();
                    //   },
                    //   child: Container(
                    //     margin: EdgeInsets.only(right: 20, left: 20),
                    //     height: 56,
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //       children: [
                    //         Text('위치 기록하기'),
                    //         Row(
                    //           children: [
                    //             Text(
                    //               controller.isLocationUtilized.value
                    //                   ? '위치 저장'
                    //                   : '위치 저장안함',
                    //               style: TextStyle(
                    //                   color: controller.isLocationUtilized.value
                    //                       ? AppColors.primary
                    //                       : AppColors.widgetDisableGray1),
                    //             ),
                    //             SizedBox(
                    //               width: 10,
                    //             ),
                    //             Icon(
                    //               controller.isLocationUtilized.value
                    //                   ? Icons.location_on
                    //                   : Icons.location_off,
                    //               color: controller.isLocationUtilized.value
                    //                   ? AppColors.primary
                    //                   : AppColors.widgetDisableGray1,
                    //             ),
                    //           ],
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                    // ),

                    // InkWell(
                    //   onTap: () {
                    //     controller.openCategoryBottomSheet(context);
                    //   },
                    //   child: Container(
                    //     margin: EdgeInsets.only(right: 10, left: 10),
                    //     height: 56,
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //       children: [
                    //         Text('카테고리'),
                    //         Row(
                    //           children: [
                    //             Text(controller.selectedTestCategory.value.name),
                    //             SizedBox(
                    //               width: 10,
                    //             ),
                    //             Icon(TestData.getIconByName(controller
                    //                 .selectedTestCategory.value.iconName)),
                    //           ],
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(top: 10, bottom: 20, left: 20, right: 20),
                        child: TextField(
                          textAlign: TextAlign.center,
                          controller: controller.textEditingController,
                          textDirection: TextDirection.rtl,
                          maxLines: 1,
                          maxLength: 10,
                          maxLengthEnforcement: MaxLengthEnforcement.enforced,
                          autofocus: false,
                          enabled: false,
                          keyboardType: TextInputType.numberWithOptions(
                              signed: false, decimal: true),
                          inputFormatters: [ThousandsFormatter()],
                          style: TextStyle(
                            fontSize: 35,
                          ),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintText: '금액 입력',
                              counterText: '',
                              hintTextDirection: TextDirection.rtl),
                        ),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () async {
                          dynamic result = await Get.toNamed(Routes.CurrencySelectPage);
                          if (result != null) {
                            controller.onSelectCurrency(result as Currency);
                          }
                        },
                        child: Text('currency: ${controller.currency.value.code}')),
                    Container(
                      child: InkWell(
                        onTap: () {
                          Get.toNamed(Routes.SelectTransactionPage);
                        },
                        child: Container(
                          height: 48,
                          margin: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    controller.selectedTransactionType.value.name,
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  Text(controller.selectedPaymentType.value.name)
                                ],
                              ),
                              Icon(Icons.chevron_right),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // Container(
                    //   child: InkWell(
                    //     onTap: () async {
                    //       dynamic result = await Get.toNamed(Routes.TagSelectPage, arguments: controller.descriptionString.value);
                    //       if (result != null) {
                    //         controller.inputDescription(result as String);
                    //       }
                    //     },
                    //     child: Container(
                    //       height: 48,
                    //       margin: EdgeInsets.only(
                    //           left: 20, right: 20, top: 10, bottom: 10),
                    //       child: Row(
                    //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //         children: [
                    //           Expanded(
                    //             child: TagText(text: controller.descriptionString.value.isEmpty ? '태그 및 메모 입력하기' : controller.descriptionString.value)
                    //           ),
                    //           Icon(Icons.chevron_right),
                    //         ],
                    //       ),
                    //     ),
                    //   ),
                    // ),

                    Container(
                      child: InkWell(
                        onTap: () async {
                          controller.onClickLocationSearch();
                        },
                        child: Container(
                          height: 48,
                          margin: EdgeInsets.only(
                              left: 20, right: 20, top: 10, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                  child: TagText(text: controller.selectedFourSquareVenueDto == null ? '위치 입력하기' : controller.selectedFourSquareVenueDto!.value.name!)
                              ),
                              Icon(Icons.chevron_right),
                            ],
                          ),
                        ),
                      ),
                    ),

                    NumericKeypad(
                      onKeyboardTap: (String number) {
                        controller.onClickKeyPadInput(number);
                      },
                      leftButtonFn: () {
                        controller.onClickKeyPadDelete();
                      },
                      leftIcon: Icon(
                        Icons.backspace,
                      ),
                      rightWidget: Text('Next'),
                      rightButtonFn: () {
                        controller.saveFormData();
                      },
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
