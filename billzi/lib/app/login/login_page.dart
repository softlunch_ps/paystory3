import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/login/login_controller.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder(
        init: Get.find<LoginController>(),
        builder: (LoginController controller) {
          return Container(
            margin: EdgeInsets.only(right: 20, left: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('login page'),
                SizedBox(
                  height: 200,
                ),
                Container(
                  width: double.infinity,
                  height: 56,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      onPressed: () {
                        controller.onClickGoogleSignIn();
                      },
                      child: Text('Start with Google')),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  width: double.infinity,
                  height: 56,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      onPressed: () {
                        controller.onClickFacebookLogin();
                      },
                      child: Text('Start with Facebook')),
                ),
                Platform.isIOS
                    ? Container(
                        width: double.infinity,
                        height: 56,
                        child: SignInWithAppleButton(
                          onPressed: () {
                            controller.onClickAppleLogin();
                          },
                        ),
                      )
                    : Container(),
              ],
            ),
          );
        },
      ),
    );
  }
}
