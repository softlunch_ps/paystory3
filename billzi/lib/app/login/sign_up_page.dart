import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:billzi/utils/utils.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('sign up page'),
            Container(
              margin: EdgeInsets.only(right: 20, left: 20),
              child: TextField(
                controller: _textEditingController,
                keyboardType: TextInputType.emailAddress,
                maxLines: 1,
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  signUp(email: 'dkkim@softlunch.co.kr');
                },
                child: Text('Sign Up')),
            ElevatedButton(
                onPressed: () {
                  //
                },
                child: Text('Back'))
          ],
        ),
      ),
    );
  }

  signUp({required String email}) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email,
          password: generatePassword()
      );

      // var acs = ActionCodeSettings(
      //   // URL you want to redirect back to. The domain (www.example.com) for this
      //   // URL must be whitelisted in the Firebase Console.
      //     url: 'https://newpaystory.page.link/openApp',
      //     // This must be true
      //     handleCodeInApp: true,
      //     iOSBundleId: 'com.softlunch.newpaystory',
      //     androidPackageName: 'com.softlunch.newpaystory',
      //     // installIfNotAvailable
      //     androidInstallApp: false,
      //     // minimumVersion
      //     androidMinimumVersion: '1');
      //
      // FirebaseAuth.instance
      //     .sendSignInLinkToEmail(email: email, actionCodeSettings: acs)
      //     .catchError(
      //         (onError) => print('Error sending email verification $onError'))
      //     .then((value) => print('Successfully sent email verification'));

    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }
}
