import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:billzi/data/repositories/auth_repo_impl.dart';
import 'package:billzi/domain/usecase/auth_usecase.dart';
import 'package:billzi/manager/dynamic_link_manager.dart';
import 'package:billzi/route/route_name.dart';

class LoginController extends GetxController {
  //for use indicator
  Rx<bool> isLoading = false.obs;

  onClickGoogleSignIn() async {
    if (!isLoading.value) {
      isLoading.value = true;
      update();

      AuthUseCase authUseCase = AuthUseCase(AuthRepositoryImpl());
      User? user = await authUseCase.googleLogin();
      _maybeTransitionToHome(user);

      isLoading.value = false;
      update();
    }
  }

  onClickFacebookLogin() async {
    if (!isLoading.value) {
      isLoading.value = true;
      update();

      AuthUseCase authUseCase = AuthUseCase(AuthRepositoryImpl());
      User? user = await authUseCase.facebookLogin();
      _maybeTransitionToHome(user);

      isLoading.value = false;
      update();
    }
  }

  onClickAppleLogin() async {
    if (!isLoading.value) {
      isLoading.value = true;
      update();

      AuthUseCase authUseCase = AuthUseCase(AuthRepositoryImpl());
      User? user = await authUseCase.appleLogin();
      _maybeTransitionToHome(user);

      isLoading.value = false;
      update();
    }
  }

  _maybeTransitionToHome(User? user) {
    if (user != null) {
      Get.offAndToNamed(Routes.HomePage);
      DynamicLinkManager().maybeHandleInitialLink();
    } else {
      Get.snackbar('로그인 실패', '잠시 후 다시 시도해주세요.');
    }
  }
}
