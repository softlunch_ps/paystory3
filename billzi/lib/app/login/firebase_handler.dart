import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:billzi/utils/utils.dart';

class FirebaseHandler {
  Future<dynamic>? _deepLinkBackground;
  FirebaseAuth? _auth;

  String _firebaseURL = 'https://newpaystory.page.link/openApp';

  FirebaseHandler() {
    _auth = FirebaseAuth.instance;
    //initialiseFirebaseOnlink(_deepLinkBackground);
  }

  Future getDynamiClikData() async{
    //Returns the deep linked data
    final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();
    return data?.link;
  }

  Future getDynamiBGData(){
    //Returns the deep linked data
    return _deepLinkBackground ?? Future((){
      return false;
    });
  }

  sendEmail({email}) {

    User? user = _auth!.currentUser;
    if (user == null) {
      _auth?.sendSignInLinkToEmail(
        email: email,
        actionCodeSettings: ActionCodeSettings(
          // URL you want to redirect back to. The domain (www.example.com) for this
          // URL must be whitelisted in the Firebase Console.
            url: _firebaseURL,
            // This must be true
            handleCodeInApp: true,
            iOSBundleId: 'com.softlunch.paystory3',
            androidPackageName: 'com.softlunch.paystory3',
            // installIfNotAvailable
            androidInstallApp: false,
            // minimumVersion
            androidMinimumVersion: '21'),
      );
    } else {
      print('usrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr');
    }


  }

  initialiseFirebaseOnlink(_deepLinkBackground){
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData? dynamicLink) async {
          final Uri? deepLink = dynamicLink?.link;

          if (deepLink != null) {
            _deepLinkBackground = Future((){
              return deepLink;
            });
          }
        },
        onError: (OnLinkErrorException e) async {
          print('onLinkError');
          print(e.message);
        }
    );
  }

}