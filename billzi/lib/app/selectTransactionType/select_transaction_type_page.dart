import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:billzi/app/selectTransactionType/select_transaction_type_controller.dart';
import 'package:billzi/utils/app_colors.dart';

class SelectTransactionPage extends GetView<SelectTransactionTypeController> {
  const SelectTransactionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Transaction type'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 10),
              alignment: Alignment.centerLeft,
              child: Text('Income/Expanse'),
              // decoration: BoxDecoration(
              //   border: Border(
              //     bottom: BorderSide(
              //       color: AppColors.dividerGray2,
              //       width: 1.0,
              //     ),
              //   ),
              // ),
            ),
            ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: controller.testTransactionTypeList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Obx(() => _selectButton(
                    buttonText: controller.testTransactionTypeList[index].name,
                    isSelect: controller.selectedTransactionTypeId.value == controller.testTransactionTypeList[index].id,
                    onTap: () => controller.onSelectedTransactionType(controller.testTransactionTypeList[index]),
                  ));
                }),
            Container(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 10),
              alignment: Alignment.centerLeft,
              child: Text('Payment type'),
              // decoration: BoxDecoration(
              //   border: Border(
              //     bottom: BorderSide(
              //       color: AppColors.dividerGray2,
              //       width: 1.0,
              //     ),
              //   ),
              // ),
            ),
            ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: controller.testPaymentTypeList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Obx(() => _selectButton(
                    buttonText: controller.testPaymentTypeList[index].name,
                    isSelect: controller.selectedPaymentTypeId.value == controller.testPaymentTypeList[index].id,
                    onTap: () => controller.onSelectedPaymentType(controller.testPaymentTypeList[index]),
                  ));
                }),
          ],
        ),
      ),
    );
  }

  Widget _selectButton(
      {required String buttonText,
      required GestureTapCallback onTap,
      required bool isSelect}) {
    return Container(
      // decoration: BoxDecoration(
      //   border: Border(
      //     bottom: BorderSide(
      //       color: AppColors.dividerGray2,
      //       width: 1.0,
      //     ),
      //   ),
      // ),
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 56,
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin:
                      EdgeInsets.only(right: 10, left: 10, top: 5, bottom: 5),
                  child: Text(buttonText)),
              isSelect ? Icon(Icons.check) : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
