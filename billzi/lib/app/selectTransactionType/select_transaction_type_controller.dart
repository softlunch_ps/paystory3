import 'package:get/get.dart';
import 'package:billzi/app/easyForm/easyFormAdd/easy_form_add_controller.dart';
import 'package:billzi/data/dto/testDto/test_payment_type.dart';
import 'package:billzi/data/dto/testDto/test_transaction_type.dart';
import 'package:billzi/data/repositories/easy_form_repo_impl.dart';
import 'package:billzi/domain/usecase/easy_form_usecase.dart';
import 'package:billzi/utils/test_data.dart';

class SelectTransactionTypeController extends GetxController {

  Rx<String> selectedTransactionTypeId = '01'.obs;
  Rx<String> selectedPaymentTypeId = '01'.obs;

  RxList<TestTransactionType> testTransactionTypeList = RxList();
  RxList<TestPaymentType> testPaymentTypeList = RxList();

  @override
  void onInit() {
    super.onInit();
    testTransactionTypeList.addAll(TestData.getTestTransactionTypeList());
    testPaymentTypeList.addAll(TestData.getTestPaymentTypeList());
    EasyFormUseCase easyFormUseCase = EasyFormUseCase(EasyFormRepositoryImpl());
    selectedTransactionTypeId.value = easyFormUseCase.getLastTestTransactionType().id;
    selectedPaymentTypeId.value = easyFormUseCase.getLastTestPaymentType().id;
  }

  onSelectedTransactionType(TestTransactionType testTransactionType) {
    selectedTransactionTypeId.value = testTransactionType.id;
    EasyFormUseCase easyFormUseCase = EasyFormUseCase(EasyFormRepositoryImpl());
    easyFormUseCase.setLastTestTransactionType(testTransactionType);
    //todo test
    Get.find<EasyFormAddController>().onSelectTransactionType(testTransactionType);
  }

  onSelectedPaymentType(TestPaymentType testPaymentType) {
    selectedPaymentTypeId.value = testPaymentType.id;
    EasyFormUseCase easyFormUseCase = EasyFormUseCase(EasyFormRepositoryImpl());
    easyFormUseCase.setLastTestPaymentType(testPaymentType);
    //todo test
    Get.find<EasyFormAddController>().onSelectPaymentType(testPaymentType);
  }
}