import 'package:billzi/data/dto/model/card_account_dto.dart';
import 'package:billzi/data/repositories/transaction_repo_impl.dart';
import 'package:billzi/domain/usecase/transaction_usecase.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/utils/logger.dart';
import 'package:get/get.dart';

class CardController extends GetxController {

  RxList<CardAccountDto> cardList = RxList();

  Future<RxList<CardAccountDto>> getCardList() async {
    TransactionUseCase transactionUseCase =
    TransactionUseCase(TransactionRepositoryImpl());
    var result = await transactionUseCase.getCardList();
    result.when(
      success: (cardListResDto) {
        cardList.clear();
        if (cardListResDto.cardAccountList != null) {
          cardList.addAll(cardListResDto.cardAccountList!);
        }

      },
      failure: (error) {
        logger.d('fail to load card list');
      },
    );
    return cardList;
  }

  Future addCard({required String cardName}) async {

    if (cardName.isEmpty) {
      cardName = 'nameless card';
    }

    TransactionUseCase transactionUseCase =
    TransactionUseCase(TransactionRepositoryImpl());
    var result = await transactionUseCase.postAddCard(cardAccountDto: CardAccountDto(
        userSeq: UserManager().getUserSeq()!,
        crdNum: cardName,
        crdName: cardName));
    result.when(
      success: (cardAccountDto) {
        cardList.add(cardAccountDto);
      },
      failure: (error) {
        logger.d('fail to add card');

      },
    );

  }
}