import 'package:billzi/app/addCard/card_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddCardPage extends StatefulWidget {
  const AddCardPage({Key? key}) : super(key: key);

  @override
  _AddCardPageState createState() => _AddCardPageState();
}

class _AddCardPageState extends State<AddCardPage> {

  final CardController _addCardController = Get.find<CardController>();
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          margin: EdgeInsets.only(right: 30, left: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Add card'),
              TextField(
                controller: _controller,
                decoration: InputDecoration(
                  hintText: 'Card nick name'
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('뒤로가기')),
                  ElevatedButton(
                      onPressed: () async {
                        _addCardController.addCard(cardName: _controller.text);
                        Get.back();
                      },
                      child: Text('Add Card')),
                ],
              )
            ],
          ),
        ));
  }
}
