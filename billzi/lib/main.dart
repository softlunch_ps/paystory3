import 'package:billzi/manager/dynamic_link_manager.dart';
import 'package:billzi/route/app_router.dart';
import 'package:billzi/utils/app_theme.dart';
import 'package:billzi/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  await Firebase.initializeApp();

  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'pay story',
      initialRoute: AppRouter.initial,
      getPages: AppRouter.routes,
      theme: AppTheme.light,
      darkTheme: AppTheme.dark,
      onReady: () {
        logger.d('onReady is called!');
        DynamicLinkManager().init();
      },
    ),
  );
}

