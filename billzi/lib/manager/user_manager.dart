class UserManager {

  static final UserManager _userManager = UserManager._internal();

  static num? _userSeq;
  static String _serverToken = '';

  factory UserManager() {
    return _userManager;
  }

  UserManager._internal();

  num? getUserSeq() {
    return _userSeq;
  }

  setUserSeq(num? userSeq) {
    _userSeq = userSeq;
  }

  String getServerToken() {
    return _serverToken;
  }

  setServerToken(String value) {
    _serverToken = value;
  }
}