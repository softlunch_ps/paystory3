import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:billzi/data/dto/response/book/book_res_dto.dart';
import 'package:billzi/manager/user_manager.dart';
import 'package:billzi/route/route_name.dart';
import 'package:billzi/utils/constants.dart';
import 'package:billzi/utils/dialog_helper.dart';
import 'package:billzi/utils/logger.dart';
import 'package:share_plus/share_plus.dart';

typedef ContentShareUrlDecorator = String Function(String);

class DynamicLinkManager {
  static final DynamicLinkManager _instance = DynamicLinkManager._();

  factory DynamicLinkManager() {
    return _instance;
  }

  DynamicLinkManager._();

  Future<void> init() async {
    FirebaseDynamicLinks.instance.onLink(
      onSuccess: (data) => _onDynamicLinkReceived(Get.context!, data),
      onError: (e) async {
        return e;
      },
    );
  }

  /// when app is launched from dynamic link, this function take care of handling the link
  /// initial link is obtainable only one time.
  maybeHandleInitialLink() {
    FirebaseDynamicLinks.instance
        .getInitialLink()
        .then((data) => _onDynamicLinkReceived(Get.context!, data));
  }

  Future<dynamic> _onDynamicLinkReceived(
      BuildContext context, PendingDynamicLinkData? dynamicLink) {
    final Uri? deepLink = dynamicLink?.link;
    if (deepLink != null) {
      // TODO: handle received message gracefully
      // return MessageManager.instance.handleUriMessage(context, deepLink);
      logger.d(deepLink.path);
      logger.d(deepLink.queryParameters);

      switch (deepLink.path) {
        case DynamicLinkInviteBookPath:
          Get.toNamed(Routes.InviteAcceptPage,
              arguments: deepLink.queryParameters);
          break;
        default:
          logger.d('unknown dynamic link path : ${deepLink.path}');
      }
    }
    return Future.value(null);
  }

  static Future<void> shareBookJoinInvitation(BookResDto item) async {
    LoadingModalView modalView = LoadingModalView()..show();
    try {
      assert(FirebaseAuth.instance.currentUser != null,
          'sending an invitation is impossible with a non-logged-in user');
      var currentUserName = FirebaseAuth.instance.currentUser!.displayName ??
          FirebaseAuth.instance.currentUser!.email;
      var currentUserUid = UserManager().getUserSeq();

      ShortDynamicLink params =
          await DynamicLinkManager._getBookInvitationParams(
        urlDecorator: (url) {
          url += '?bookSeq=${item.accountBookSeq}&hostSeq=$currentUserUid';
          return url;
        },
        socialMetaTagParams: SocialMetaTagParameters(
          title:
              '$currentUserName wants you to join ${item.accountBookName} account book !',
          description: '${item.accountBookDesc}',
        ),
      ).buildShortLink();
      Uri uri = params.shortUrl;
      logger.d(uri.toString());
      Share.share(uri.toString());
    } catch (_) {
    } finally {
      modalView.pop();
    }
  }

  static DynamicLinkParameters _getBookInvitationParams({
    required ContentShareUrlDecorator urlDecorator,
    required SocialMetaTagParameters socialMetaTagParams,
  }) {
    String appTargetUrl = FirebaseDynamicLinkDomain + DynamicLinkInviteBookPath;
    appTargetUrl = urlDecorator(appTargetUrl);
    Uri linkUri = Uri.parse(appTargetUrl);
    logger.d('dynamic link : ' + linkUri.toString());

    return DynamicLinkParameters(
      uriPrefix: FirebaseDynamicLinkDomain,
      link: linkUri,
      androidParameters: AndroidParameters(
        packageName: AndAppId,
      ),
      iosParameters: IosParameters(
        bundleId: IOSAppId,
        // appStoreId:
        //     '1561279070', // TODO : make sure to fill out the app store ID when launched
      ),
      socialMetaTagParameters: socialMetaTagParams,
    );
  }
}
